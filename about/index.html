<!doctype html>
<html>
<head>
<!-- for unicode, e.g. embedding Chinese -->
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
<!-- for mobile display -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- ensure all links open in new tab -->
<base target="_blank"/>
<title>About Me</title>
<link rel="stylesheet" type="text/css" href="../css/site.css"></link>
</head>
<body class="note">

<h1 class="header">About Me</h1>
I am a mature ANU student, having my <a href="https://livestream.com/reedgrad/101219an1">PhD conferred</a> in 2019. 
That is why I have time to prepare supplementary materials for you this semester.

<h2>Interactive Theorem Proving (ITP)</h2>
This is the subject of my <a href="https://openresearch-repository.anu.edu.au/handle/1885/177195">thesis</a>: proving theorems by human-computer feedback interaction.
<p>
To prove a theorem is to achieve a goal. There is a logical path towards the goal, which is long and winding. A computer program can do the mechanical checks of the many logical steps along the path. Who designs the path?
<p>
<!--
A computer, when given enough math knowledge, can figure out the logical path, in theory. This is similar to a game of tic-tac-toe, where the goal is clear, the rules are simple, and a computer can figure out the best strategy. However, replacing tic-tac-toe by chess, a pure computer search is hopeless. A computer chess program will need to use some rules to limit the size of the search tree. The rules are given by experts in chess. (for more info, see AI). Raising the level to the game of Go, the rules are learned by the machine: this is called deep learning.
-->
The design requires intelligence, an understanding of the math involved in the proof. These people come together, share their ideas, pass on their tricks, forming the ITP community.
<p>
I craft the high-level proof steps in a script, which is fed into the theorem-proving program to convert into lots of low-level logical steps. The program also checks that, starting from primitive axioms, the resulting very-very-long-logic-chain really leads to desired goal. That's TP = Theorem Proving.
<p>
There is no cookbook for writing the proof script. I just have to learn by running the theorem-prover in interactive mode. I type in my desired goal. The program stores the goal, then ask why it is true. I type in one step. The program tries that step, finds that it'll work if a subgoal is true, and shows me the subgoal. I give another step for the subgoal, the program responses by the next subgoal, and so on .... That's ITP = Interactive Theorem Proving.
<p>
A child asks a question, "Why does it rain?" You give an answer, "The sky cannot hold that much water". She thinks for a while, then comes back with another "why". You give a deeper answer.
<p>
A theorem-prover is like a very logical child, who never forgets. It knows some basic theorems. Therefore if you go deeper and deeper into the basic, finally it will come back with the message "subgoal proved, subgoal proved, ....., initial goal proved." Hurrah!  Collect the working steps and you'll have a proof script.
<p>
The technical term for proving theorems by a computer is <i>mechanisation</i>, or <i>formalisation</i>.
For more information, refer to【1】.


<h2> Agrawal–Kayal–Saxena Algorithm (AKS)</h2>
This is the topic of my thesis【2】, a polynomial-time algorithm to test for primes given by three Indian computer scientists. You can Google it.

<h2>Visualise Your Thesis (VYT)</h2>
In ANU, there is a unit called Research Training. They provide help and guidance for all research students: masters or doctors.
Every year, they organise a Visualise Your Thesis (VYT) competition 【3】, complete with tutorial classes for participants.
<p>
The purpose is to produce a 1 minute eye-catching video clip to showcase the thesis topic.
The video clip will be shown on outdoor screens around the campus, to draw attention to the significance of one's research.
<p>
They will select the best 12 submissions for a final voting session by panels to decide the prizes: 1st, 2nd, and People's choice.
These are (small) cash prizes, but the winner of the 1st prize will enter the National VYT, held at the University of Melbourne.
<p>
I participated in the 2018 VYT. My aim was not the prizes: telling the story of a math topic in 60s is almost impossible.
Rather my aim was to keep me in focus -- think hard to figure out what are the essential points, and how to show my topic to anyone in the street.
<p>
To think hard, I concentrate on the following:
<ul>
<li>How to introduce AKS, an algorithm to test primes?</li>
<li>How to explain AKS by pictures, the steps of an algorithm?</li>
<li>How to show a finite field, the abstract math behind the algorithm?</li>
<li>How to visualise a theorem-prover, the tool for theorem-proving?</li>
<li>How to present what I have done, frame by frame?</li>
<li>How to illustrate the algorithm by a simple, visual example?</li>
</ul>
The creation process is documented in【4】, with a <a href="https://www.bitpaper.io/go/Sketchbook/r1HgdhRL8">whiteboard summary</a>.
My VYT submission managed to get into the final dozen, without any prize.
<p>
However, I did learn how to make a video clip, which is more exciting than making a slide presentation.
Based on the submission video, reworking the timing and adding a few interesting slides, I have an entertaining VYT-like video under 3 minutes【5】.


<h2>Reading My Thesis</h2>
<!-- Unless you are interested in the AKS algorithm and theorem proving, -->
It takes quite some time to read my <a href="phd.thesis.aks.pdf">thesis</a> (190+ pages). Still, you can appreciate how to write a thesis.
<p>
There are 8 chapters in my thesis, with an organisation chart showing their inter-relationship.
The first chapter is Introduction, and the final chapter is Conclusion.
Each chapter starts with a quote, relating to the chapter content.
My selection for the quotes are【6】:
<ul>
<li>Chapter 1: from Nicolaas Govert de Bruijn, a theorem-proving pioneer.</li>
<li>Chapter 2: from Évariste Galois, the founder of group theory.</li>
<li>Chapter 3: from Lao Tzu (老子), a legendary Chinese philosopher.</li>
<li>Chapter 4: from Sophie Germain, a French mathematician and physicist.</li>
<li>Chapter 5: from Godfrey Harold Hardy, an English pure mathematician.</li>
<li>Chapter 6: from HaskellWiki, of the Haskell Programming Language.</li>
<li>Chapter 7: from Ada Lovelace, daughter of the famous poet Lord Byron.</li>
<li>Chapter 8: from Thomas Stearns Eliot, winner of the 1948 Nobel Prize in Literature.</li>
</ul>
Of the many quotes, this one is related to the idea of an algorithm:
<div align="right">
<blockquote class="note">
<i>
We may say most aptly that</br>
the Analytical Engine weaves algebraic patterns,</br>
just as the Jacquard loom weaves flowers and leaves.</br>
--- Ada Lovelace (1843)
</i>
</blockquote>
</div>
This appears in <i>"Notes on the Analytical Engine"</i>,
her appendices to the English translation of an account of Charles Babbage's 1842 lectures in Turin, Italy.
More about Ada Lovelace can be found
<a href="../lovelace/index.html">here</a> (select your language).</br>


<h2>In a Nutshell</h2>
<h3>The AKS Algorithm</h3>
<pre>
<b><i>Input:</i></b> A number n.</br>
<b><i>Output:</i></b> Is n prime? (primality test)</br>
<b><i>Method:</i></b> Test number n in 3 steps.</br>
<b><i>Step 1:</i></b> Check that n is not a square, not a cube, etc. (n is power free)</br>
<b><i>Step 2:</i></b> Obtain a good parameter k from number n; if no good, n is not prime.</br>
<b><i>Step 3:</i></b> Perform a series of equality checks based on n and k.</br>
If number n can pass all the equality checks, n is prime; otherwise n is not prime. &#8718;
</pre>
The running time, or runtime, of an algorithm is given by various classes. The class P, which means polynomial-time class, is deemed "fast", at least in theory. Since all 3 steps are in class P, the entire AKS algorithm belongs to class P.

<h3>The AKS Main Theorem</h3>
<pre>
<b><i>Theorem:</i></b> Given a number n, (prime n) if and only if (aks n).</br>
<b><i>Proof:</i></b> (prime n) implies (aks n) is easy, but (aks n) implies (prime n) is hard.</br>
To show the hard part, suppose the number n is not prime.</br>
Then (<i>after 3 years of hard work</i>) the Pigeonhole Principle is false.</br>
This is impossible, therefore the number n must be prime. &#8718;
</pre>
The hard work is to guide the theorem-prover through the intermediate steps.
The Pigeonhole Principle (鴿巢原理) says, if there are more pigeons than holes, then at least one hole will have more than one pigeon【7】.

<h2>Proof Pearls</h2>
Some theorems with pictorial proofs in the thesis had been published in journals as proof pearls. One of them is a result concerning the least common multiple (LCM) of the numbers 1, 2, 3, up to (n + 1). You can take a look at the paper【8】, and enjoy my talk【9】which is quite easy to understand, as long as you take for granted some properties about the greatest common divisor (GCD) of numbers.


<h2>Links</h2>
<ul>
<li>【1】A Special Issue on Formal Proof
(Notices of the AMS, December 2008, Volume 55 Issue 11).</br>
<a href="http://www.ams.org/notices/200811/200811FullIssue.pdf">http://www.ams.org/notices/200811/</a>
<!-- PDF: http://www.ams.org/notices/200811/200811FullIssue.pdf -->
</li>
<li>【2】Joseph Chan 2019
<a href="phd.thesis.aks.pdf">PhD thesis</a>:</br>
Primality Testing is Polynomial-time: A Mechanised Verification of the AKS Algorithm.
</li>
<li>【3】Visualise Your Thesis competition</br>
<a href="https://services.anu.edu.au/training/visualise-your-thesis-competition">
https://services.anu.edu.au/training/visualise-your-thesis-competition</a>
</li>
<li>【4】Joseph Chan VYT talk slides.</br>
<a href="phd_visual_2019.pdf">
<img src="screen01.png" width="15%"/></a><br/>
</li>
<li>【5】Joseph Chan VYT video - YouTube【2:40】(no cc)</br>
<a href="https://www.youtube.com/watch?v=G8MoY157VHY">
<img src="http://img.youtube.com/vi/G8MoY157VHY/0.jpg" width="15%"/></a><br/>
<!-- https://youtu.be/G8MoY157VHY -->
</li>
<li>【6】Joseph Chan 2019 PhD thesis slides.</br>
<a href="phd_thesis_2019.pdf">
<img src="screen02.png" width="15%"/></a><br/>
</li>
<li>【7】Pigeonhole Principle - YouTube【12:13】(cc)</br>
<a href="https://www.youtube.com/watch?v=IeTLZPNIPJQ">
<img src="http://img.youtube.com/vi/IeTLZPNIPJQ/0.jpg" width="15%"/></a><br/>
</li>
<li>【8】Proof Pearl: Bounding LCM with Triangle.</br>
<a href="lcm.paper.pdf">
<img src="screen03.png" width="15%"/></a><br/>
</li>
<li>【9】Proof Pearl: LCM with Triangle (talk slides).</br>
<a href="lcm.slides.pdf">
<img src="screen04.png" width="15%"/></a><br/>
</li>
</ul>

<!--

Can add links to published papers,
but not for the students!

<h2>Talks</h2>
<ul>
<li><a href="talks/lcm-slides.pdf">LCM and Triangles</a></li>
</ul>

-->

<hr>
<!-- https://jhlchan.bitbucket.io/about/index.html -->
</body>
</html>