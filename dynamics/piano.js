// single function execution
(function() {
    // load CSS styles for piano keys
    function loadStyles() {
        var s = ''
        s += '<style>'
        s += '.container {background-image: linear-gradient(90deg, #9331d4, rgb(239 5 92 / 70%)); height: 100vh; width: 100%;'
        s += ' display: flex; flex-direction: column; align-items: center; justify-content: center;}'
        s += '.intro-container {color: #fff; text-align: center;}'
        s += '.piano-container {display: flex; align-items: center; justify-content: center;}'
        s += '.piano-keys-list {list-style: none; display: flex; justify-content: center;}'
        s += '.piano-keys {width: 5rem; cursor: pointer; position: relative; height: 20rem; border-radius: 10px; border: 1px solid #000;}'
        s += '.white-key { background-color: #fff;}'
        s += '.black-key {width: 2rem; height: 13rem; border-radius: 5px; border-top-left-radius: 0; border-top-right-radius: 0;'
        s += ' background-color: #000; z-index: 2; margin: 0 -20px 0 -20px;}'
        s += '.playing {background: #ddd;}'
        s += '.running {background: #ccc;}'
        s += '@media screen and (min-width: 821px) and (max-width: 1024px) '
        s += '{.piano-keys {width: 4rem; cursor: pointer; position: relative; height: 20rem; border-radius: 10px; border: 1px solid #000;}'
        s += ' .black-key {width: 2rem; height: 13rem; border-radius: 5px; border-top-left-radius: 0; border-top-right-radius: 0;'
        s += ' background-color: #000; z-index: 2; margin: 0 -20px 0 -20px;}}'
        s += '@media screen and (min-width: 768px) and (max-width: 820px) '
        s += '{.piano-keys {width: 3.5rem; cursor: pointer; position: relative; height: 16rem; border-radius: 10px; border: 1px solid #000;}'
        s += ' .black-key {width: 1.8rem; height: 10rem; border-radius: 5px; border-top-left-radius: 0; border-top-right-radius: 0;'
        s += ' background-color: #000; z-index: 2; margin: 0 -20px 0 -20px;}}'
        s += '@media screen and (max-width: 576px) '
        s += '{.piano-container {width: 90%; display: flex; align-items: center; justify-content: flex-start; overflow: auto;}'
        s += ' .piano-keys {width: 3.5rem; cursor: pointer; position: relative; height: 16rem; border-radius: 10px; border: 1px solid #000;}'
        s += ' .black-key {width: 1.8rem; height: 10rem; border-radius: 5px; border-top-left-radius: 0; border-top-right-radius: 0;'
        s += ' background-color: #000; z-index: 2; margin: 0 -20px 0 -20px;}}'
        // disable touch selects: https://stackoverflow.com/questions/66151807/ JC: seems not working.
        // s += '-webkit-touch-callout: none; /* Safari */'
        // s += '-webkit-user-select: none; /* Chrome */'     
        // s += '-moz-user-select: none; /* Firefox */'
        // s += '-ms-user-select: none; /* Internet Explorer/Edge */'
        // s += 'user-select: none;'
        s += '</style>'
        document.write(s)
    }

    // Create audio (context) container
    var audioContext = new (AudioContext || webkitAudioContext)()
    // JC: for iOS only click is treated as user gesture, see initAudio()

    // Table of notes with correspending keyboard codes. Frequencies are in hertz.
    // The notes start from middle C, in correct piano key ordering.
    // See: https://www.seventhstring.com/resources/notefrequencies.html
    // See also: https://www.toptal.com/developers/keycode/table
    // JC: notes as an array
    var notes = [ // keyCode from keyboard, keyName from music note          keyboard    note
        {keyCode: 90,  keyName: 'F,',  frequency: 174.6, black: false }, //  z           f3
        {keyCode: 88,  keyName: '^F,', frequency: 185.0, black: true },  //  x           f3#
        {keyCode: 67,  keyName: 'G,',  frequency: 196.0, black: false }, //  c           g3
        {keyCode: 86,  keyName: '^G,', frequency: 207.7, black: true },  //  v           g3#
        {keyCode: 66,  keyName: 'A,',  frequency: 220.0, black: false }, //  b           a3
        {keyCode: 78,  keyName: '^A,', frequency: 233.1, black: true },  //  n           b3b
        {keyCode: 77,  keyName: 'B,',  frequency: 246.9, black: false }, //  m           b3
        {keyCode: 65,  keyName: 'C',   frequency: 261.6, black: false }, //  a           c4
        {keyCode: 87,  keyName: '^C',  frequency: 277.2, black: true },  //  w           c4#
        {keyCode: 83,  keyName: 'D',   frequency: 293.7, black: false }, //  s           d4
        {keyCode: 69,  keyName: '^D',  frequency: 311.1, black: true },  //  e           e4b
        {keyCode: 68,  keyName: 'E',   frequency: 329.6, black: false }, //  d           e4
        {keyCode: 70,  keyName: 'F',   frequency: 349.2, black: false }, //  f           f4
        {keyCode: 84,  keyName: '^F',  frequency: 370.0, black: true },  //  t           f4#
        {keyCode: 71,  keyName: 'G',   frequency: 392.0, black: false }, //  g           g4
        {keyCode: 89,  keyName: '^G',  frequency: 415.3, black: true },  //  y           g4#
        {keyCode: 72,  keyName: 'A',   frequency: 440.0, black: false }, //  h           a4
        {keyCode: 85,  keyName: '^A',  frequency: 466.2, black: true },  //  u           b4b
        {keyCode: 74,  keyName: 'B',   frequency: 493.9, black: false }, //  j           b4
        {keyCode: 75,  keyName: 'c',   frequency: 523.3, black: false }, //  k           c5
        {keyCode: 79,  keyName: '^c',  frequency: 554.4, black: true },  //  o           c5#
        {keyCode: 76,  keyName: 'd',   frequency: 587.3, black: false }, //  l           d5
        {keyCode: 80,  keyName: '^d',  frequency: 622.3, black: true },  //  p           e5b
        {keyCode: 186, keyName: 'e',   frequency: 659.3, black: false }  //  ;           e5
    ]
    // JC: generate notesByKeyCode as a reverse map
    var notesByKeyCode = {}, notesByName = {}

/* This is the original Sound class in liza-script.js
   JC: this version requires 'click' to activate AudioContext.

    // Sound object constructor
    function Sound(frequency, type) {
        this.osc = audioContext.createOscillator() // Create oscillator node
        this.pressed = false // flag to indicate if sound is playing

        // Set default configuration for sound
        if (typeof frequency !== 'undefined') {
            // Set frequency. If it's not set, the default is used (440Hz)
            this.osc.frequency.value = frequency
        }

        // Set waveform type. Default is actually 'sine' but triangle sounds better :)
        this.osc.type = type || 'triangle'

        // Start playing the sound. You won't hear it yet as the oscillator node needs to be piped to output (AKA your speakers).
        this.osc.start(0)
    }

    Sound.prototype.play = function() {
        if (!this.pressed) {
            this.pressed = true
            this.osc.connect(audioContext.destination)
        }
    }

    Sound.prototype.stop = function() {
        this.pressed = false;
        this.osc.disconnect()
    }
*/

// The following Sound Class is from xylophone,
// with old syntax rather than new syntax: class Sound { ... } with a lot of keyword 'this'.
// The advantage: no need to 'click' for start of AudioContext.

// The Sound Class
function Sound(context, value) {
   // atrributes
   var context = context,
       value = value,
       type = 'triangle', // default waveform
       oscillator, 
       gainNode

   this.setup = function () {
      oscillator = context.createOscillator()
      gainNode = context.createGain()

      oscillator.connect(gainNode)
      gainNode.connect(context.destination)
      // Set waveform type. Default is actually 'sine' but 'triangle' sounds better :)
      // oscillator.type = 'sine'
      oscillator.type = type // default 'triangle' from constructor
   }

   this.play = function () {
      this.setup() // each play needs a new setup

      oscillator.frequency.value = value
      gainNode.gain.setValueAtTime(0, context.currentTime)
      gainNode.gain.linearRampToValueAtTime(1, context.currentTime + 0.01)

      oscillator.start(context.currentTime)
      // this.stop(this.context.currentTime) // JC: no need to stop here.
   }

   this.stop = function () {
      if (typeof gainNode == 'undefined') return // JC: if no gain node, already stopped 
      gainNode.gain.exponentialRampToValueAtTime(0.001, context.currentTime + 1)
      oscillator.stop(context.currentTime + 1)
   }

   this.setWaveform = function(form) {
      type = form // so that play() calls setup() with new type
   }
}

    // Key object constructor
    function Key(keyCode, frequency, black) {
        // JC: rather than create a DIV here, create something like the following:
        // <li class="piano-keys white-key" data-key="01"></li>
        // <li class="piano-keys black-key" data-key="02"></li>
        var keyElement = document.createElement('li')
        keyElement.className = "piano-keys"
        keyElement.classList.add(black ? "black-key" : "white-key")
        keyElement.setAttribute('data-key', keyCode)

        //var keySound = new Sound(frequency, 'triangle') // JC: for old Sound class
        var keySound = new Sound(audioContext, frequency) // JC: for new Sound class
        return {
            element: keyElement,
            sound: keySound
        }
    }

    // Create the Piano Keyboard with notes and append to element with containerId
    function createKeyboard(notes, containerId) {
        var list = document.createElement('ul')
        list.className = "piano-keys-list"
        // Add note keys to DOM
        notes.forEach(note => {
            // Generate playable key
            note.key = new Key(note.keyCode, note.frequency, note.black)
            list.appendChild(note.key.element)
            notesByKeyCode[note.keyCode] = note
            notesByName[note.keyName] = note
            // include also flat notes
            var ch = note.keyName.charAt(0)
            if (ch == '^') { // e.g. ^F = _G, ^G = _A, ^A = _B, ^C = _D, ^c = _d, ^d = _e
                ch = String.fromCharCode(note.keyName.charCodeAt(1) + 1)
                if (ch == 'H') ch = 'A'
                notesByName['_' + ch] = note
            }
        })
        // console.log('JC: notesByKeyCode', notesByKeyCode)
        // console.log('JC: notesByName', notesByName)
        document.getElementById(containerId).appendChild(list)
    }

    // fix KeyCode for Firefox
    function fixKeyCode(keyCode) {
        const isFirefox = navigator.userAgent.toLowerCase().includes('firefox')
        // console.log('JC: isFirefox', isFirefox)
        if (!isFirefox) return keyCode
        // JC: in FireFox, ";" has e.keyCode = 59, other browsers has e.keyCode = 186
        // JC: only 3 keyCode differences, see https://github.com/ccampbell/mousetrap/pull/215
        if (keyCode == 59) return 186   // for ";:" key
        if (keyCode == 61) return 187   // for "=+" key
        if (keyCode == 173) return 189  // for "-_" key
        return keyCode            
    }

    // start a note
    function start(note) {
        // console.log('JC: start ', note.keyName)
        // Pipe sound to output (AKA speakers)
        note.key.sound.play()
        // Highlight key playing
        note.key.element.classList.add('playing')            
    }
    // end a note
    function end(note) {
        // console.log('JC: end ', note.keyName)
        // Kill connection to output
        note.key.sound.stop()
        // Remove key highlight
        note.key.element.classList.remove('playing')
    }
    // play a note with duration
    function play(note, duration) {
        start(note)
        setTimeout(function() {
            end(note) 
        }, duration)
    }
    // handle playNote event
    var playNote = function(event) {
        event.preventDefault()
        // console.log('JC: playNote, event', event.type)
        var keyCode = event.keyCode || event.target.getAttribute('data-key')
        var note = notesByKeyCode[fixKeyCode(keyCode)]
        if (typeof note !== 'undefined') start(note)
    }
    // handle endNote event
    var endNote = function(event) {
        var keyCode = event.keyCode || event.target.getAttribute('data-key')
        var note = notesByKeyCode[fixKeyCode(keyCode)]
        if (typeof note !== 'undefined') end(note)
    }
    // handle set Waveform event
    var setWaveform = function(event) {
        // notes.forEach(note => note.key.sound.osc.type = this.value) // JC: for old Sound class
        notes.forEach(note => note.key.sound.setWaveform(this.value)) // JC: for new Sound class
        // notes.forEach(note => note.key.sound.setup(this.value)) // JC: for new Sound class
        // Unfocus selector so value is not accidentally updated again while playing keys
        this.blur()
    }

    // Add event listeners 
    function addListeners() {
        // add listener for radio buttons
        document.querySelectorAll("input[name=waveform]").forEach(input => input.onclick = setWaveform)
        // add listener for user actions
        ! ['keydown', 'mousedown', 'touchstart'].forEach(type => window.addEventListener(type, playNote))
        // JC: using below will destroy the symmetry of make/release events for duration, causing sound problems.
        // JC: because play() is given by 'keydown' or 'click', but stop() is given by 'keyup' and other release actions.
        // ! ['keydown', 'click'].forEach(type => window.addEventListener(type, playNote))
        ! ['keyup', 'mouseup', 'mouseout', 'touchend', 'touchcancel'].forEach(type => window.addEventListener(type, endNote))
        // JC: this will call endNote more times than playNote, i.e. more stop() than play()
    }


    // Sample tunes
// T: You Are My Sunshine
// z D G A | B2 B2 | z B A B | G2 G2 |
// z G A B | c2 e2 | z e d c | B4    |
// z G A B | c2 e2 | z e d c | B2 G2 |
// z2  G A | B3 c  | A A2 B  | G4    |
// Transpose down: (twice, still needs note d) (twice again) no G,
// z G, C D | E2 E2 | z E D E | C2 C2 |
// z C D E | F2 A2 | z A G F | E4    |
// z C D E | F2 A2 | z A G F | E2 C2 |
// z2  C D | E3 F  | D D2 E  | C4    |
//  ABC:      z G, C D E E   z E D E C C   z C D E F A   z A G F E   z C D E F A   z A G F E C   z C D E F D D E C
var tune1  = 'z G, C D E E z E D E C C z C D E F A z A G F E z C D E F A z A G F E C z C D E F D D E C'.split(' ')
var tempo1 = [1,1,1,1,2,2,  1,1,1,1,2,2,  1,1,1,1,2,2,  1,1,1,1,4,  1,1,1,1,2,2,  1,1,1,1,2,2,  2,1,1,3,1,1,2,1,4]

//
// T: Jingle Bells
// M: 2/4
// L: 1/8
// K: C
// G,E DC | G,3 G,/G,/ | G,E DC | A,4 |  (need G, A,)
// A,F ED | B,4 | GG FD | E4 |           (need A,)
// G,E DC | G,3 G,/G,/ | G,E DC | A,4 |  (need G,A,)
// A,F ED | GG GG | AG FD | C2 G2 ||     (need A,)
// |: EE E2 | EE E2 | EG C3/D/ | E4 |    (only the chorus)
// FF F3/F/ | FE E E/E/ |1 ED DE | D2 G2 ||2 GG FD | C4 ||
//  ABC:      E E E   E E E   E G C D E   F F F   F E E   E D D E D G   E E E   E E E   E G C D E   F F F   F E E   G G F D C
var tune2  = 'E E E E E E E G C D E F F F F E E E D D E D G E E E E E E E G C D E F F F F E E G G F D C'.split(' ')
var tempo2 = [1,1,2,  1,1,2,  1,1,1,1,4,  1,1,2,  1,1,2,  1,1,1,1,2,2,  1,1,2,  1,1,2,  1,1,1,1,4,  1,1,2,  1,1,2,  1,1,1,1,4]
// only chorus
//
// T: Happy Birthday
// G3/G/ | A2 G2 c2 | B4 G3/G/ | A2 G2 d2 | c4 G3/G/ |
// w:Hap-py birth-day to you, hap-py birth-day to you. Hap-py
// g2 e2 c2 | (B2 A2) f3/f/ | e2 c2 d2 | c6 |]
// w:birth-day to some-one, hap-py birth-day to you
// Transpose down: https://michaeleskin.com/abctools/abctools.html
// T: Happy Birthday to You
// M: 3/4
// L: 1/8
// K: F
//   C3/C/ | D2 C2 F2 | E4    C3/C/ | D2 C2 G2 | F4    C3/C/ |
// w:Hap-py birth-day to you, hap-py birth-day to you. Hap-py
// c2 A2 F2 | (E2 D2)       B3/B/ | A2 F2 G2 | F6 |]
// w:birth-day to some-one, hap-py birth-day to you
//  ABC:      C C D C F E   C C D C G F   C C c A F E D   _B _B A F G F
var tune3  = 'C C D C F E C C D C G F C C c A F E D _B _B A F G F'.split(' ')
var tempo3 = [3,1,2,2,2,4,  3,1,2,2,2,4,  3,1,2,2,2,2,4,  3,1,2,2,2,4]
//
// Twinkle Twinkle
// CCGG|AAG2|FFEE|DDC2|
// w: Twin-kle, twin-kle, lit-tle star, How I won-der what you are!
// w: 一 閃 一 閃 亮 晶 晶， 滿 天 都 是 小 星 星。
// |GGFF|EE D2|GGFF|EE D2|
// w: Up a-bove the world so high, Like a dia-mond in the sky.
// w: 掛 在 天 空 放 光 明， 好 像 許 多 小 眼 睛！ 
// CCGG|AAG2|FFEE|(D/C/D/E/) C2|]
// w: Twin-kle, twin-kle, lit-tle star, How I won-der what_ you_ are!
// w: 一 閃 一 閃 亮 晶 晶， 漫 天 都 是 小_ 星_ 星。
//  ABC:      C C G G A A G   F F E E D D C   G G F F E E D   G G F F E E D   C C G G A A G   F F E E D D C
var tune4  = 'C C G G A A G F F E E D D C G G F F E E D G G F F E E D C C G G A A G F F E E D D C'.split(' ')
var tempo4 = [1,1,1,1,1,1,2,  1,1,1,1,1,1,2,  1,1,1,1,1,1,2,  1,1,1,1,1,1,2,  1,1,1,1,1,1,2,  1,1,1,1,1,1,2]
//
// T: Deck The Halls
// K: C
// |"C"G3 F E2 D2|C2 D2 E2 C2|"G"DEFD "C"E3 D|C2 B,2 C4  (need B,)
// |   G3 F E2 D2|C2 D2 E2 C2|"G"DEFD "C"E3 D|C2 B,2 C4  (need B,)
// |"G"D3 E F2 D2|"C"E3 F G2 D2|EF G2 AB c2|"G"B2 A2 G4
// |"C"G3 F E2 D2|C2 D2 E2 C2|"F" AAAA "C"G3 F|E2 "G"D2 "C"C4||
//  ABC:      G F E D C D E C   D E F D E D C B, C   G F E D C D E C   D E F D E D C B, C   D E F D   E F G D  E F G A B c B A G  G F E D C D E C  A A A A G F E D C
var tune5  = 'G F E D C D E C D E F D E D C B, C G F E D C D E C D E F D E D C B, C D E F D E F G D E F G A B c B A G G F E D C D E C A A A A G F E D C'.split(' ')
var tempo5 = [3,1,2,2,2,2,2,2,  1,1,1,1,3,1,2,2,4,  3,1,2,2,2,2,2,2,  1,1,1,1,3,1,2,2,4,  3,1,2,2,  3,1,2,2, 1,1,2,1,1,2,2,2,4, 3,1,2,2,2,2,2,2, 1,1,1,1,3,1,2,2,4]
//
// T: Old Mac Donald
// M: 4/4
// L: 1/4
// K: C
// "C" c c c G|"F" A A "C" G2|"C" e e "G7" d d|"C"c2 z G|
// "C" c c c G|"F" A A "C" G2|"C" e e "G7" d d|"C"c2 z G/G/|
// "C" c c c G/G/| c c c2 | c/c/ c c/c/ c | c/c/c/c/ c c |
// "C" c c c G|"F" A A "C" G2|"C" e e "G7" d d|"C"c3 z |
// (transpose down)
// T: Old Mac Donald
// M: 4/4
// L: 1/4
// K: G
// "G" G G G D|"C" E E "G" D2|"G" B B "D7" A A|"G"G2 z D|
// "G" G G G D|"C" E E "G" D2|"G" B B "D7" A A|"G"G2 z D/D/|
// "G" G G G D/D/| G G G2 | G/G/ G G/G/ G | G/G/G/G/ G G |
// "G" G G G D|"C" E E "G" D2|"G" B B "D7" A A|"G"G3 z |
//  ABC:      G G G D E E D   B B A A G    D G G G D E E D   B B A A G  D D G G G  D D G G G  G G G  G G G  G G G G G G   G G G D E E D   B B A A G
//var tune6  = 'G G G D E E D B B A A G D G G G D E E D B B A A G D D G G G D D G G G G G G G G G G G G G G G G G G D E E D B B A A G'.split(' ')
//var tempo6 = [2,2,2,2,2,2,4,  2,2,2,2,4,   2,2,2,2,2,2,2,4,  2,2,2,2,4, 1,1,2,2,2, 1,1,2,2,4, 1,1,2, 1,1,2, 1,1,1,1,1,4,  2,2,2,2,2,2,4,  2,2,2,2,4]
//  ABC:      G G G D E E D   B B A A G    z D G G G D E E D   B B A A G  z D D G G G  D D G G G  G G G  G G G  G G G G G G   G G G D E E D   B B A A G z
var tune6  = 'G G G D E E D B B A A G z D G G G D E E D B B A A G z D D G G G D D G G G G G G G G G G G G G G G G G G D E E D B B A A G z'.split(' ')
var tempo6 = [2,2,2,2,2,2,4,  2,2,2,2,4,   2,2,2,2,2,2,2,2,4,  2,2,2,2,4, 2,1,1,2,2,2, 1,1,2,2,4, 1,1,2, 1,1,2, 1,1,1,1,1,4,  2,2,2,2,2,2,4,  2,2,2,2,4,2]
//
// sample tunes
// ABC notation:  C  D  E  F  G  A  B  c
//                0  1  2  3  4  5  6  7

    function sampleTunes() {
        document.getElementById('btn1').onclick = function() { playtune(this, tune1, tempo1, 350) }
        document.getElementById('btn2').onclick = function() { playtune(this, tune2, tempo2, 350) }
        document.getElementById('btn3').onclick = function() { playtune(this, tune3, tempo3, 350) }
        document.getElementById('btn4').onclick = function() { playtune(this, tune4, tempo4, 350) }
        document.getElementById('btn5').onclick = function() { playtune(this, tune5, tempo5, 250) }
        document.getElementById('btn6').onclick = function() { playtune(this, tune6, tempo6, 200) }

        // flag to indicate if a tune is chosen
        var chosen = false
        // play a tune
        function playtune(element, tune, tempo, factor) {

           // no playtune when chosen, and prepare to abort
           if (chosen) return
           chosen = true
           var max = tune.length - 1 
           element.classList.add('running')
           for (var j = 0; j <= max; j++) {
              setTimeout(function(idx) {
                 var note = notesByName[tune[idx]],
                     duration = tempo[idx] * factor * 0.8
                 // console.log('JC: key', note.keyName, 'duration', duration, 'idx', idx, tune[idx], tempo[idx])    
                 if (typeof note !== 'undefined') play(note, duration)
                 // JC: although this is equivalent to j == max,
                 //     this is for setTimout, the loop finishes early.
                 if (idx == max) {
                     element.classList.remove('running')
                     chosen = false
                 }
              }, rhythm(tempo, j) * factor, j)
           }
        }
        // compute rhythm from tempo
        function rhythm(tempo, num) {
          var count = 0
          for (var i = 0; i < num ; i++) {
             count = count + tempo[i]
          }
          return count
        }
    }
/* JC: no need to initAudio with 'click' for new Sound class.

    // Emoji: 🔇 speaker mute, 🔊 speaker loud
    // initialize audio context by user gesture
    function initAudio(event) {
        console.log('JC: initAudio')
        audioContext.resume()
        document.getElementById('info').innerHTML = 'Sound enabled: 🔊'
        // info.innerHTML += event.type
        window.removeEventListener('click', initAudio)
        addListeners()
        sampleTunes()
        // console.log('JC: initAudio: event.target', event.target.className)
        if (event.target.classList.contains('piano-keys')) {
            var note = notesByKeyCode[event.target.getAttribute('data-key')]
            play(note, 100)
        }
        if (event.target.classList.contains('btn')) event.target.click()
    }
*/
    // main action
    loadStyles()
    window.addEventListener('load', function() {
        createKeyboard(notes, 'keyboard')
        // set first waveform as default
        document.querySelector("input[name=waveform]").checked = true
        // document.getElementById('info').innerHTML = 'No Sound: 🔇'
        // window.addEventListener('click', initAudio) // JC: see above
        addListeners()
        sampleTunes()
    })
})()
