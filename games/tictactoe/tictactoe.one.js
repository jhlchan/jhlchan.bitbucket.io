/**
 * Tic Tac Toe script
 * 
 * Ideas taken from:
 * Tic-Tac-Toe by Borna Šepić
 * https://github.com/BornaSepic/Tic-Tac-Toe
 */

// single function application
(function() {
'use strict';

/* Utilities */

// open url in name tab
function openInNewTab(url) {
    Object.assign(document.createElement('a'), {
        target: '_blank',
        href: url,
    }).click();
}

/* Part 1: Tic-Tac-Toe */

// load style sheet for TicTacToe
function loadTicTacToeStyle() {
    var s = '';
    s += '<style>';
    s += '.cell { text-align: center; font-family: "Permanent Marker", cursive; ' +
         'box-shadow: 0 0 0 1px #333333; border: 1px solid #333333; cursor: pointer; }';
    s += '.board { position: relative; display:block; margin: 0 auto; border:1px solid coral; }';
    s += '.frame { width:306px; margin: 50px auto; }';
    s += '.choose { font-size:18px }';
    s += '.status { text-align: center; font-family: "Arial", sans-serif; font-size: 20px; }';
    s += '.button { text-align: center; text-decoration: none; margin: 2px;' +
         'z-index:2; position:absolute; right:30px; bottom:20px;' +
         'font: bold 14px/25px Arial, sans-serif; color: #268; border: 1px solid #88aaff;' +
         'border-radius: 10px; cursor: pointer; outline-style:none;' +
         'background: linear-gradient(to top right, rgba(170,190,255,1) 0%, rgba(255,255,255,1) 100%); }';
    s += '.button:hover { background: linear-gradient(to top, rgba(255,255,0,1) 0%, rgba(255,255,255,1) 100%); }';
    s += '</style>'
    document.write(s);
}

/**
 TicTacToe object:
 * game status
 * game state
 * player
 * winning check
 * chat for multiplayer
 */

// construct a TicTacToe of order n
function TicTacToe (n, frame, status, button) {
    // variables
    var currentPlayer;
    var gameActive = false;
    var gameState = [];
    var select; // the select element
    var info; // the select info

    // the cells of Tic-Tac-Toe
    function makeCells(n, frame, width) {
        while (frame.firstChild) { frame.removeChild(frame.lastChild); }
        frame.setAttribute('style', 'display: grid; grid-template-columns: repeat('+n+', auto);');
        var grid = [];
        for (var j = 0; j < n * n; j ++) {
            var cell = document.createElement('div');
            cell.className = 'cell';
            cell.index = j;
            frame.appendChild(cell);
            grid.push(cell);
        }
        var size = width / n;
        grid.forEach(cell => cell.setAttribute('style', 'width: ' + size + 'px; height: ' + size + 'px; ' +
                         'line-height: ' + size + 'px; font-size: ' + (180/n) + 'px;'));
        return grid;
    }

    // fill frame with grid
    var width = 300;
    var grid = makeCells(n, frame, width);

    // for multiplayer
    var players = ['X', 'O'];
    var playerMax = players.length;
    var playerCount = 1; // this player

    var chat = new Chat(this);
    chat.init();

    // rows, columns and diagonals of the grid.
    //     k = 0 1 2
    // j = 0:  0 1 2   v[j] = j * n + k  gives each row
    // j = 1:  3 4 5   v[k] = j + k * n  gives each column
    // j = 3:  6 7 8   v = j + k + k * n gives diagonals
    // For down diagonals,
    // when j = 0, generate  0 (n + 1) (2*n + 2) (3*n + 3) ... so j + k * n + k
    // when j = 1, generate  1 (1 + n + 1) (1 + 2*n + 2)   ... so j + k * n + k
    //        also generate  n (n + n + 1) (n + 2*n + 2)   ... so j + k * n + k + j * (n-1)
    // For up diagonals,
    // when j = 0, generate  2 4 6 ... so (k + 1) * (n - 1) - j
    // when j = 1, generate  1 3 ...   so (k + 1) * (n - 1) - j
    //        also generate  5 7 ...   so (k + 1) * (n - 1) - j + j * (n + 1)
    // For n = 3,
    //     0 1 2
    //     3 4 5
    //     6 7 8
    // For n = 4
    //     0   1  2  3
    //     4   5  6  7
    //     8   9 10 11
    //     12 13 14 15

    // make the rows
    function makeRows(n) {
        var r = [], s;
        for (var j = 0; j < n; j++) {
            s = [];
            for (var k = 0; k < n; k++) s.push(j * n + k); // row value
            r.push(s);
        }
        return r;
    }
    // make the columns
    function makeCols(n) {
        var r = [], s;
        for (var j = 0; j < n; j++) {
            s = [];
            for (var k = 0; k < n; k++) s.push(j + k * n); // column value
            r.push(s);
        }
        return r;
    }
    // make the down diagonals
    function makeDownDiag(n) {
        var r = [], s, m;
        for (var j = 0; j < n; j++) {
            s = [];
            for (var k = 0; k < n; k++) {
                m = j + k * n + k;
                if (m < n + k * n) s.push(m);
            }
            // filter out those too short
            if (s.length > 2) r.push(s);
            if (j > 0) {
                s = [];
                for (var k = 0; k < n; k++) {
                    m = j + k * n + k + j * (n-1);
                    if (m < n + k * n + j * (n-1)) s.push(m);
                }
                // filter out those too short
                if (s.length > 2) r.push(s);
            }
        }
        // alert('Here: ' + r.join('-'));
        // for n = 3, Here: 0,4,8-1,5-3,7-2-6
        // for n = 4, Here: 0,5,10,15-1,6,11-4,9,14-2,7-8,13-3-12
        return r;
    }
    // make the up diagonals
    function makeUpDiag(n) {
        var r = [], s, m;
        for (var j = 0; j < n; j++) {
            s = [];
            for (var k = 0; k < n; k++) {
                m = (k + 1) * (n - 1) - j;
                if (m >= k * n) s.push(m);
            }
            // filter out those too short
            if (s.length > 2) r.push(s);
            if (j > 0) {
                s = [];
                for (var k = 0; k < n; k++) {
                    m = (k + 1) * (n - 1) - j + j * (n + 1);
                    if (m >= k * n + j * n + j) s.push(m);
                }
                // filter out those too short
                if (s.length > 2) r.push(s);
            }
        }
        // alert('Here: ' + r.join('-'));
        // for n = 3, Here: 2,4,6-1,3-5,7-0-8
        // for n = 4, Here: 3,6,9,12-2,5,8-7,10,13-1,4-11,14-0-15
        return r;
    }

    // the rows, columns, diagonals
    function makeGameLines(n) {
        return makeRows(n)
       .concat(makeCols(n))
       .concat(makeDownDiag(n))
       .concat(makeUpDiag(n));
    }

    // form the game lines
    var gameLines = makeGameLines(n);

    // get the gameState of the vector v as a string
    function gameVector(v) {
        var s = [];
        for (var j = 0; j < v.length; j++) {
            s.push(gameState[v[j]]);
        }
        return s.join('');
    }

    // show fireworks
    function showFireWorks() {
        openInNewTab('../fun/fireworks.html');
        status.innerHTML = 'Click <a href="../fun/fireworks.html" target="_blank">fireworks</a>!';
        // var fireworks = new Fireworks();
        // var body = window.document.body;
        // body.parentNode.replaceChild(fireworks.scene, body);
        // fireworks.start();
    }
    // show game has a win
    function winMessage(who) {
        return "Player " + who + " has won!";
        // HTML5 canvas and javascript fireworks tutorial
        // https://codepen.io/whqet/pen/Auzch
        // https://shenhuang.github.io/demo_projects/fireworkdemo.html
    }
    // show game is a draw
    function drawMessage() {
        return "Game ends in a draw!";
    }
    // prompt game player
    function showPlayer() {
        return currentPlayer + ': your turn.';
    }

    // get the winning count
    function getWinCount(n) {
        // how many in a line to win: 3 for 3x3, 4 for 4x5 and 5x5, otherwise 5.
        return n == 3 ? 3 : n < 6 ? 4 : 5;
    }

    // set winning patterns
    function setWins(n) {
        var s = [];
        players.forEach(player => s.push(player.repeat(n)));
        return s;
    }

    // form winning patterns
    var winCount = getWinCount(n);
    var wins = setWins(winCount);

    // check if this is a win
    function roundWon() {
        // no forEach as the return is a break.
        for (var j = 0; j < gameLines.length; j++) {
            var condition = gameVector(gameLines[j]);
            for (var k = 0; k < wins.length; k++) {
                if (condition.includes(wins[k])) return true;
            }
        }
        return false;
    }

    // check if this is a draw
    function roundDraw() {
        return !gameState.includes(' ');
    }

    // handle cell click
    function cellClick(event) {
        var cell = event.target;
        var index = cell.index;

        if (!gameActive || gameState[index] !== ' ') return;

        cellPlayed(index, cell);
        validateGame();
        if (gameActive) changePlayer();
        // show hidden button upon win or draw
        else {
            // add Fireworks for win or draw
            chat.showText('Fireworks in 3 seconds!');
            setTimeout(showFireWorks, 3000); // in milliseconds
            button.style.display = 'block'; // show Reset button
        }
    }

    // update cell upon play
    function cellPlayed(index, cell) {
        gameState[index] = currentPlayer;
        cell.innerHTML = currentPlayer;
        chat.showText('Move:' + currentPlayer + ':' + index);
    }

    // change player
    function changePlayer() {
        currentPlayer = currentPlayer === 'X' ? 'O' : 'X';
        // gameActive = selfPlay();
        status.innerHTML = showPlayer();
    }

    // validate the game
    function validateGame() {
        if (roundWon()) {
            status.innerHTML = winMessage(currentPlayer);
            chat.showText('Over:win:' + currentPlayer);
            gameActive = false;
        }
        else if (roundDraw()) {
            status.innerHTML = drawMessage();
            chat.showText('Over:draw');
            gameActive = false;
        }
    }

    // start the game
    function startGame() {
        for (var j = 0; j < grid.length; j++) gameState[j] = ' '; // no forEach
        grid.forEach(cell => cell.innerHTML = ' ');
        currentPlayer = 'X'; // start with player X
        // gameActive = selfPlay();
        gameActive = true;
        // hide the button until win or draw
        button.style.display = 'none';
        status.innerHTML = showPlayer();
    }
    // restart the game
    function restartGame() {
        playerCount = playerMax; // all players on same page.
        // now this never happens
        if (playerCount == 1) chat.showText('Need to get computer to play.');
        else if (playerCount == playerMax) {
            startGame();
            // tell others to start
            chat.showText('Start:' + currentPlayer);
        }
    }
    // mark selection
    function markSelection(n) {
        // ensure selection is correct
        select.options[n-3].selected = true; // offset = 3
        info.innerHTML = ' Win by ' + getWinCount(n) + ' in a line.';
    }
    // resize the grid
    function resize(m) {
        grid = makeCells(m, frame, width);
        gameLines = makeGameLines(m);
        winCount = getWinCount(m);
        wins = setWins(winCount);
        // ensure selection is correct
        markSelection(m);
        n = m;
    }    

    // add listeners
    grid.forEach(cell => cell.addEventListener('click', cellClick));
    button.innerHTML = 'Restart';
    button.addEventListener('click', restartGame);

    // initialize by checking, with select element
    this.init = function(selection, selectinfo) {
        select = selection;
        info = selectinfo;
        // ensure selection is correct
        markSelection(n);
        // start game
        button.click();
    }
    // resize the game
    this.resize = function(m) {
        resize(m);
        // tell others to resize
        chat.showText('Resize:' + m);
    }

    // get the chat object
    this.getChat = function() {
        return chat;
    }

    return this; // object

} // end of TicTacToe

// make a game board
function makeGameBoard(n) {
    var width = 300;
    var board = document.createElement('section');
    board.className = 'board';
    board.setAttribute('style', 'width:'+(width + 100)+'px; height:'+(width + 200)+'px;');
    var selectText = document.createElement('span');
    selectText.innerHTML = 'Please select: ';
    var selectInfo = document.createElement('span');
    // the selector
    var select = document.createElement('select');
    select.className = 'choose';
    // for n = 3, 4, 5, 6, 7
    for (var j = 3; j < 8; j++) {
        var option = document.createElement('option');
        option.value = '' + j;
        option.text = j + 'x' + j;
        select.appendChild(option);
        if (j == n) option.selected = true;
    }

    // the frame for the game
    var frame = document.createElement('div');
    frame.className = 'frame'; // width:306px;
    // status and button
    var status = document.createElement('div');
    status.className = 'status';
    var button = document.createElement('button');
    button.className = 'button';

    board.appendChild(selectText);
    board.appendChild(select);
    board.appendChild(selectInfo);
    board.appendChild(frame);
    board.appendChild(status);
    board.appendChild(button);

    // game for the board, let game put cells in frame
    var game = new TicTacToe(n, frame, status, button);
    board.appendChild(game.getChat().getFrame());
    game.init(select, selectInfo);

    // when selection change
    select.onchange = function () {
        var n = 3 + select.selectedIndex; // index counts from 0
        game.resize(n);
    }

    return board;
}

// transform the node
function transform(node) {
    var source = node.innerHTML;
    // remove comments and blank lines
    var n = parseInt(source); // the order of Tic Tac Toe
    return makeGameBoard(n);
}

// process all <tictactoe> nodes
function processTicTacToe() {
   // Collect all nodes that will receive <tictactoe> processing
   var nodesArray = Array.from(document.getElementsByClassName('tictactoe'))
            .concat(Array.from(document.getElementsByTagName('tictactoe')));

   // Process all nodes, replacing them as we progress
   nodesArray.forEach(function (node) {
       node.parentNode.replaceChild(transform(node), node);
   });
}

/* Part 2: Chat */

// load style sheet for Chat
function loadChatStyle() {
    var s = '';
    s += '<style>';
    s += '.chat { display:none; height: 100px; width: 400px; ' +
         'border: 2px solid black; overflow: auto; padding: 5px; margin-bottom: 5px; }';
    s += '</style>'
    document.write(s);
}

/**
 Chat as trace display:
 * messages
 */

// construct a Chat display
function Chat (game) {
// send messages back to game.
 
//==============================================================================
// CHAT Messages
//==============================================================================
var chatFrame, chatPane;

// Displays a single chat message
function displayChatMessage (message) {
  // Make the new chat message element
  var msg = document.createElement('span');
  msg.appendChild(document.createTextNode(message));
  msg.appendChild(document.createElement('br'));

  // Append the new message to the chat
  chatPane.appendChild(msg);
  
  // Trim the chat to 500 messages
  if (chatPane.childNodes.length > 500) {
    chatPane.removeChild(chatPane.firstChild);
  }
  chatPane.scrollTop = chatPane.scrollHeight;
}

    // DOM object initialisation
    this.init = function() {
        // make the chat display frame
        chatFrame = document.createElement('div');
        chatPane = document.createElement('div');
        chatPane.className = 'chat';

        chatFrame.appendChild(chatPane);
    }

    // get the Chat frame
    this.getFrame = function() {
        return chatFrame;
    }
    // display text message
    this.showText = function(text) {
        displayChatMessage('>> ' + text);
    }

    return this;
} // end of Chat

// Start from here.
loadChatStyle();      // load the style CSS for Chat
loadTicTacToeStyle(); // load the style CSS for Tic Tac Toe
processTicTacToe();   // find and process all <tictactoe> nodes

})(); // invoke single function
