<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<base target="_blank"/>
<title>Bernoulli Numbers</title>
<link rel="stylesheet" type="text/css" href="../css/site.css"></link>
<script type="text/javascript" src="../scripts/site.js"></script>
</head>
<body class="note" onload="check()">
<!-- Modal Pop-up: https://www.w3schools.com/howto/howto_css_modal_images.asp -->
<!-- The Modal -->
<div id="modal" class="modal">
  <!-- Modal Content (The Image) -->
  <img id="modal-content" />
  <!-- Modal Caption (Image Text) -->
  <div id="modal-caption"></div>
</div>
<div align="right" class="banner">
<input type="radio" name="lang" onclick="set_lang('en')" id="english" value="English" checked /> English |
<input type="radio" name="lang" onclick="set_lang('ch')" id="chinese" value="Chinese" /> 中文
</div>

<h1>Bernoulli Numbers</h1>

Hopefully, the math will lead to the computation of Bernoulli numbers by Ada (unlikely!).

<h2>Summation</h2>
Using <a href="https://www.wolframalpha.com/">Wolfram Alpha</a>,
<pre>
sum j from 1 to n
sum j^2 from 1 to n
sum j^3 from 1 to n
sum j^4 from 1 to n
sum j^5 from 1 to n
sum j^6 from 1 to n
sum j^7 from 1 to n
</pre>
We can find,
<markdeep>
\[
\begin{array}{l}
\displaystyle\sum_{j=1}^{n} j = \frac{n^{2}}{2} + \frac{n}{2}\\
\displaystyle\sum_{j=1}^{n} j^{2} = \frac{n^{3}}{3} + \frac{n^{2}}{2} + \frac{n}{6}\\
\displaystyle\sum_{j=1}^{n} j^{3} = \frac{n^{4}}{4} + \frac{n^{3}}{2} + \frac{n^{2}}{4}\\
\displaystyle\sum_{j=1}^{n} j^{4} = \frac{n^{5}}{5} + \frac{n^{4}}{2} + \frac{n^{3}}{3} - \frac{n}{30}\\
\displaystyle\sum_{j=1}^{n} j^{5} = \frac{n^{6}}{6} + \frac{n^{5}}{2} + \frac{5n^{4}}{12} - \frac{n^{2}}{12}\\
\displaystyle\sum_{j=1}^{n} j^{6} = \frac{n^{7}}{7} + \frac{n^{6}}{2} + \frac{n^{5}}{2} - \frac{n^{3}}{6} + \frac{n}{42}\\
\displaystyle\sum_{j=1}^{n} j^{7} = \frac{n^{8}}{8} + \frac{n^{7}}{2} + \frac{7n^{6}}{12} - \frac{7n^{4}}{24} + \frac{n^{2}}{12}\\
\end{array}
\]
How to deduce a formula from this?
</markdeep>

<h2>Binomial Coefficients</h2>
<markdeep>
Pascal's triangle was known in China in the early 11th century through the work of the Chinese mathematician Jia Xian (賈憲 1010–1070). In the 13th century, Yang Hui (1238–1298) presented the triangle and hence it is still called Yang Hui's triangle (杨辉三角; 楊輝三角) in China.

The triangle can be constructed from adding parent entries, by
\[
\displaystyle\binom{n}{k} = \binom{n-1}{k-1} + \binom{n-1}{k}
\]
This gives $(x + y)^{n} = \displaystyle\sum_{k=0}^{n}\binom{n}{k} x^{k} y^{n-k}$.
Putting $x = y = 1$,
\[
\displaystyle\sum_{k=0}^{n}\binom{n}{k} = (1 + 1)^{n} = 2^{n}
\]
Hence,
\[
\begin{array}{llllllll}
n = 0 & \binom{0}{0}: & 1\\
n = 1 & \binom{1}{k}: & 1 & 1\\
n = 2 & \binom{2}{k}: & 1 & 2 & 1\\
n = 3 & \binom{3}{k}: & 1 & 3 & 3 & 1\\
n = 4 & \binom{4}{k}: & 1 & 4 & 6 & 4 & 1\\
n = 5 & \binom{5}{k}: & 1 & 5 & 10 & 10 & 5 & 1\\
\end{array}
\]
There is no recursive computation, as there are two parameters.
But the expansion of binomial shows the shift-and-add property:
<diagram>
    1  1           1  2  1          1  3  3  1
       1  1           1  2  1          1  3  3  1
   ---------      ------------     ---------------
    1  2  1        1  3  3  1       1  4  6  4  1
</diagram>
Better use this shift-and-add method to introduce the symbol $\binom{n}{k}$,
as a tour of patterns forming triangles of numbers.
</markdeep>

<h2>Method by Jakob Bernoulli</h2>
<markdeep>
The Swiss mathematician Jakob Bernoulli (1654–1705) was the first to realize the existence of a single sequence of constants $B_{0}, B_{1}, B_{2}, \dots$ which provide a uniform formula for all sums of powers.

Bernoulli's result was published posthumously in Ars Conjectandi in 1713. Seki Takakazu independently discovered the Bernoulli numbers and his result was published a year earlier, also posthumously, in 1712. However, Seki did not present his method as a formula based on a sequence of constants.
\[
\begin{array}{ll}
(n + 1)^{(k+1)} & = \displaystyle\sum_{j=0}^{n} \left[(j+1)^{(k+1)} - j^{k}\right]\\
 & = \displaystyle\sum_{j=0}^{n} \left[\binom{k+1}{1} j^{k} + \binom{k+1}{2} j^{k-1} + \dots + \binom{k+1}{k} j^{1} + 1 \right]\\
 & = \displaystyle\binom{k+1}{1}S_{k}(n) + \binom{k+1}{2}S_{k-1}(n) + \dots + \binom{k+1}{k}S_{1}(n) + (n + 1)\\
\end{array}
\]
Putting $k = 1$,
\[
(n + 1)^{2} - (n + 1) = \binom{2}{1}S_{1}(n), \text{ so } S_{1}(n) = (n^{2} + n)/2 = n(n + 1)/2
\]
Putting $k = 2$,
\[
(n + 1)^{3} - (n + 1) = \binom{3}{1}S_{2}(n) + \binom{3}{2}S_{1}(n), \text{ so } S_{2}(n) = [(n^{3} + 3n^{2} + 2n) - 3S_{1}(n)]/3 = n^{3}/3  + n^{2}/2 + n/6
\]
Another presentation,
\[
\begin{array}{l}
1^{3} = (0 + 1)^{3} = (1)0^{3} + (3)0^{2} + (3)0 + 1\\
2^{3} = (1 + 1)^{3} = (1)1^{3} + (3)1^{2} + (3)1 + 1\\
3^{3} = (2 + 1)^{3} = (1)2^{3} + (3)2^{2} + (3)2 + 1\\
\dots\\
(n+1)^{3} = (n + 1)^{3} = (1)n^{3} + (3)n^{2} + (3)n + 1\\
\end{array}
\]
adding all up, and cancelling the same cubes on both left and right,
\[
(n + 1)^{3} = 0^{3} + 3 S_{2}(n) + 3 S_{1}(n) + (n + 1)
\]
where $S_{k}(n) = \displaystyle\sum_{j=0}^{n} j^{k}$.
Putting $S_{0} = n + 1$, since $0^{0} = 1$, we have in general,
\[
(n + 1)^{k} = \displaystyle\sum_{j=0}^{k-1}\binom{k}{j} S_{j}(n)
\]
in particular, for $k = 1, 2, 3, 4, 5, \dots$,
\[
\begin{array}{ll}
(n+1)^{1} = (1)S_{0}(n) &  S_{0}(n) = n + 1\\
(n+1)^{2} = (1)S_{0}(n) + (2)S_{1}(n) &  S_{1}(n) = (n^{2} + n)/2\\
% ((n+1)^{2} - (n+1))/2 = (n^{2} + n)/2 = n(n+1)/2
(n+1)^{3} = (1)S_{0}(n) + (3)S_{1}(n) + (3)S_{2}(n) & S_{2}(n) = \dots\\
% ((n+1)^{3} - (n+1) - 3n(n+1)/2)/3 = n(2n^{2} + 3n + 1)/6 = n^{3}/3 + n^{2}/2 + n/6
(n+1)^{4} = (1)S_{0}(n) + (4)S_{1}(n) + (6)S_{2}(n) + (4)S_{3}(n) & S_{3}(n) = \dots\\
(n+1)^{5} = (1)S_{0}(n) + (5)S_{1}(n) + (10)S_{2}(n) + (10)S_{3}(n) + (5)S_{4}(n) & S_{4}(n) = \dots\\
\end{array}
\]
giving,
\[
\begin{array}{l}
S_{0}(n) = n + 1\\
S_{1}(n) = \frac{n^{2}}{2} + \frac{n}{2}\\
S_{2}(n) = \frac{n^{3}}{3} + \frac{n^{2}}{2} + \frac{n}{6}\\
S_{3}(n) = \frac{n^{4}}{4} + \frac{n^{3}}{2} + \frac{n^{2}}{4}\\
S_{4}(n) = \frac{n^{5}}{5} + \frac{n^{4}}{2} + \frac{n^{3}}{3} - \frac{n}{30}\\
\end{array}
\]
there seems to be no pattern, but try to dig out the binomial coefficients,
\[
\begin{array}{l}
S_{1}(n) = \frac{1}{2}\left[(1)n^{2} + (2)\frac{1}{2}n\right]\\
S_{2}(n) = \frac{1}{3}\left[(1)n^{3} + (3)\frac{1}{2}n^{2} + (3)\frac{1}{6}n\right]\\
S_{3}(n) = \frac{1}{4}\left[(1)n^{4} + (4)\frac{1}{2}n^{3} + (6)\frac{1}{6}n^{2} + (4)(0)n\right]\\
S_{4}(n) = \frac{1}{5}\left[(1)n^{5} + (5)\frac{1}{2}n^{4} + (10)\frac{1}{6}n^{3} + (10)(0)n^{2} + (5)\frac{-1}{30}n\right]\\
\end{array}
\]
Define the Bernoulli Numbers $B_{n}$:
\[
B_{0} = 1, B_{1} = \frac{1}{2}, B_{2} = \frac{1}{6}, B_{3} = 0, B_{4} = -\frac{1}{30}, \dots
\]
In general,
\[
S_{k}(n) = \displaystyle\frac{1}{k+1}\sum_{j=0}^{k} \binom{k+1}{j} B_{j} n^{k+1-j}
\]
</markdeep>

<!--

What are the Bernoulli Numbers?
C. D. Buenger, July 13, 2013.
https://math.osu.edu/sites/math.osu.edu/files/Bernoulli_numbers.pdf
(8 pages) For the "What is?" seminar today we will be investigating the Bernoulli numbers. 
Through slight manipulations Bernoulli arrived at the following reformulation of these sums:

Bernoulli Numbers and their Applications
James B Silva
https://dspace.mit.edu/bitstream/handle/1721.1/78574/18-100c-spring-2006/contents/projects/silva.pdf
(8 pages)
The Bernoulli numbers are a set of numbers that were discovered by Jacob Bernoulli(1654-1705)

The Bernoulli Numbers:  A Brief Primer
Nathaniel Larson, May 10, 2019.
https://www.whitman.edu/documents/Academics/Mathematics/2019/Larson-Balof.pdf
(47 pages) with Table of Content
1    Uncovering the Bernoulli Numbers:  A History
2    Following in Bernoulli’s Footsteps:  Sums of Powers
Seki Takakazu’s method for finding the Bernoulli numbers is not easily converted to Western notation, solet us derive the sequence by Jakob Bernoulli’s method.

Bernoulli numbers and the unity of mathematics
B. Mazur
https://people.math.harvard.edu/~mazur/papers/slides.Bartlett.pdf
(35 pages) has TOC.
2   Bernoulli numbers as “fundamental numbers”.

The Bernoulli Number Page
https://www.bernoulli.org

-->

<h2>Computation of Bernoulli numbers</h2>
<markdeep>
There is a recurrence formula for $B_{n}$:
\[
\displaystyle\sum_{k=0}^{n}\binom{n+1}{k}B_{k} = n+1
\]
so that,
\[
\begin{array}{lll}
n = 0 & (1) B_{0} = 1 & B_{0} = 1\\
n = 1 & (1) B_{0} + (2) B_{1} = 2 & B_{1} = \frac{1}{2}\\
% (2 - 1)/2 = 1/2
n = 2 & (1) B_{0} + (3) B_{1} + (3) B_{2} = 3 & B_{2} = \frac{1}{6}\\
% (3 - 1 - 3/2)/3 = (1/2)/3 = 1/6
n = 3 & (1) B_{0} + (4) B_{1} + (6) B_{2} + (4) B_{3} = 4 & B_{3} = 0\\
% (4 - 1 - 4/2 - 6/6)/4 = 0/4 = 0
n = 4 & (1) B_{0} + (5) B_{1} + (10) B_{2} + (10) B_{3} + (5) B_{4} = 5 & B_{4} = -\frac{1}{30}\\
% (5 - 1 - 5/2 - 10/6 - 0)/5 = (4 - 5/2 - 5/3)/5 = (24 - 15 - 10)/30 = -1/30
\end{array}
\]
</markdeep>

<!--

What is the simplest way to get Bernoulli numbers?
https://math.stackexchange.com/questions/2844290/
The simplest way to calculate them, using very few fancy tools, is the following recursive definition:

-->

<h2><span class="en">Links</span><span class="ch">連結</span></h2>
<ul>
<li>【A1】<a href="https://hk.appledaily.com/lifestyle/20190510/VSTEJHBDVKEFX2GLNGIJ2FFXQU/">【科研人物】重考TOEFL多次！港產天文學家有份影黑洞。</a>（蘋果日報 2019.05.10）</br>
<span class="en">From ApplyDaily, introduces Chi-Kwan Chan (陳志均)【A4】, a black hole expert from Hong Kong.</span>
<span class="ch">記者：司徒港燊。介紹在香港土生土長的黑洞專家陳志均【A4】。</span>
<p>
</li>
<li>【A2】<a href="https://www.wired.com/story/scientists-reveal-the-first-picture-of-a-black-hole/">【WIRED】Scientists Reveal the First Picture of a Black Hole.</a> (Sophia Chen. 04.10.2019)</br>
<span class="en">A featured video (at the end)【A3】explains "What is a black hole" in 5 levels.</br>
Before that there is another video of black hole simulation, by Chi-Kwan Chan (陳志均).</span>
<span class="ch">最後精選視頻【A3】從五個級別解釋了「黑洞是什麼」。</br>
在此之前，還有一則有關黑洞模擬的視頻，由陳志均製作。</span>
<p>
</li>
<li>【A3】Astronomer Explains One Concept in 5 Levels of Difficulty【17:11】cc
<span class="en">(close caption)</span><span class="ch">（字幕）</span><br>
<a href="https://www.youtube.com/watch?v=rUvFbJYWGDA">
<img src="http://img.youtube.com/vi/rUvFbJYWGDA/0.jpg" width="15%"/></a><br>
<span class="en">WIRED has challenged NASA's Varoujan Gorjian to explain black holes to 5 different people.</span>
<span class="ch">WIRED 挑戰 NASA 的 Varoujan Gorjian 向五位不同人士解釋黑洞。</span>
<p>
</li>
<li>【M1】《少女傳奇吳健雄》水墨動畫片。2020最新電影【1:32:24】cc<br>
<a href="https://www.youtube.com/watch?v=zwpPDhRxyjo">
<img src="http://img.youtube.com/vi/zwpPDhRxyjo/0.jpg" width="15%"/></a><br>
<span class="en">This Chinese animation in Mandarin includes English subtitles. The story is based on Chien-Shiung Wu, who is known as "the world's most outstanding female experimental physicist". It reproduces Wu’s childhood anecdotes in Liuhe with the scene effects of Chinese ink painting and the style of traditional Chinese comic strip characters. With white walls and black tiles, small bridges and flowing water, and willows flicking, the 90-minute movie is like a picture scroll. 
</span>
<span class="ch">國語，簡體字幕，附英文字幕。故事以被譽為「世界最傑出女實驗物理學家」的吳健雄為原型創作，用中國水墨畫場景效果和中國傳統連環圖人物的風格，重現了吳健雄在瀏河的童年軼事。粉牆黛瓦、小橋流水、楊柳輕拂，90分鐘的電影宛如一幅畫卷。</span>
<p>
</li>
<!-- end of links -->
</ul>


<hr>
<!-- Markdeep processing, place near the end of the document. -->
<script>window.markdeepOptions = {mode: 'html'};</script> 
<script src="https://morgan3d.github.io/markdeep/latest/markdeep.min.js" charset="utf-8"></script>
<script>window.alreadyProcessedMarkdeep||(document.body.style.visibility="visible")</script>
<!-- https://jhlchan.bitbucket.io/lovelace/bernoulli.html -->
</body>
</html>
