---
title: LEARNING PYTHON
author: Simple and Useful Programming
zdate: 3 November 2023
xtheme: beige
theme: solarized
comment1: https://revealjs.com/themes/ for themes
comment2: syntax highlightings are pygments (the default), kate, monochrome, breezeDark, espresso, zenburn, haddock, and tango.
zhighlight-style: espresso
header-includes: |
    <style>
      .reveal {
      font-size: 16pt;
      line-height: 1.2em;
    }
     .reveal pre {
      font-size: 14pt;
      line-height: 1.2em;
    }
    .reveal h1,
    .reveal h2,
    .reveal h3,
    .reveal h4,
    .reveal h5,
    .reveal h6 {
      text-transform: none;
    }
    .reveal code {
      font-size: 12pt;
    }
    </style>
    <base target="_blank"/>
include-before: |
    <!-- Any raw HTML code right after body tag, before anything else. -->
include-after: |
    <!-- Markdeep processing, place near the end of the document. -->
    <script>window.markdeepOptions = {mode: 'html'};</script>
    <script src="https://morgan3d.github.io/markdeep/latest/markdeep.min.js" charset="utf-8" defer></script>
    <script>window.alreadyProcessedMarkdeep||(document.body.style.visibility="visible")</script>
...


# Overview

## Ways of Learning

* [Python Tutorial](https://www.w3schools.com/python/)
* [Online Python IDE](https://www.online-python.com/)
* [Trinket](https://trinket.io/features/python3)
* [Jupyter Notebook](https://jupyter.org)
* [Google CoLab](https://colab.google/)
* [Official Python](https://www.python.org/)

<!--

Python Tutorial
https://www.w3schools.com/python/
With our "Try it Yourself" editor, you can edit Python code and view the result

Online Python IDE
https://www.online-python.com/
Build, run, and share Python code online for free with the help of online-integrated python's development environment (IDE).
(no import turtle)

Python 3 Trinkets
The easiest way to use the full power of Python 3.
https://trinket.io/features/python3
Edit and run the code, then click Share. There's no simpler way to write & share Python 3 code.

Jupyter
https://jupyter.org
Free software, open standards, and web services for interactive computing across all programming languages

JupyterLab: A Next-Generation Notebook Interface
https://jupyter.org/try
JupyterLab is the latest web-based interactive development environment for notebooks, code, and data. Its flexible interface allows users to configure and arrange workflows in data science, scientific computing, computational journalism, and machine learning. A modular design invites extensions to expand and enrich functionality.

Jupyter Notebook: The Classic Notebook Interface
https://jupyter.org/try
The Jupyter Notebook is the original web application for creating and sharing computational documents. It offers a simple, streamlined, document-centric experience.

Google Colaboratory
https://colab.google/
Colab is a hosted Jupyter Notebook service that requires no setup to use and provides free access to computing resources, including GPUs and TPUs. Colab is especially well suited to machine learning, data science, and education.

Python
https://www.python.org/
Python is a programming language that lets you work quickly and integrate systems more effectively
Python source code and installers are available for download ... Latest: Python 3.12.0

-->

# Python Tutorial

## W3Schools

![](https://upload.wikimedia.org/wikipedia/commons/thumb/a/a0/W3Schools_logo.svg/200px-W3Schools_logo.svg.png){ width=15% }

- W3Schools is a freemium (basic free, pay for premium) educational website for learning coding online.
- Its [Python Tutorial](https://www.w3schools.com/python/) is ideal for any beginner with no experience in Python but eager to code.
- Python topics are listed on the left sidebar, with explanations and examples in the main area.
- Tutorials have "Try it Yourself", with on click shows a code editor on the left, and output on the right.
- For non-Python programmers, this can serve as a quick introduction to the features of Python.
- For basic Python programmers, this can be a quick reference to know the full features of Python.
- Besides Python tutorial, W3Schools offer other speciifc tutorials on Python:
  * [NumPy](https://www.w3schools.com/python/numpy/default.asp)
  * [Pandas](https://www.w3schools.com/python/pandas/default.asp)
  * [SciPy](https://www.w3schools.com/python/scipy/index.php)
  * [Matplotlib](https://www.w3schools.com/python/matplotlib_intro.asp)


- (Pros) Comprehensive tutorial, learn Python at your own pace.\
Try Python code fragments entirely in browser.
- (Cons) No Python turtle graphics (missing module 'tkinter').\
Online editor allows only a single file.

<!--
The W3Schools Python "Try it Yourself" code editor shows no turtle graphics:
import turtle
ModuleNotFoundError: No module named 'tkinter'

import sys
print(sys.version)
3.8.2 (default, Mar 13 2020, 10:14:16) 
[GCC 9.3.0]

-->

## Online Python

![](https://www.online-python.com/logo.png?ezimgfmt=rs:40x34/rscb4/ng:webp/ngcb4){ width=10% }

- This [Online Python](https://www.online-python.com/) is both a Compiler and an Interpreter.
- It presents an online-integrated python's development environment (IDE).
- Online editor can add separate files.
- Build, run, and share Python code online for free.
- Can save and load from local disk.
- Part of [Online IDE](https://www.online-ide.com/) with choices for other programming languages:
  * [Java](https://www.online-java.com/)
  * [Ruby](https://www.online-ide.com/online_ruby_compiler)
  * [R](https://www.online-ide.com/online_r_compiler)

- (Pros) no need to install Python (taking up disk space).\
Build Python applications entirely in browser.\
Online editor has tabs for different files.
- (Cons) No Python turtle graphics (missing module 'tkinter').

<!--

Online Python - IDE, Editor, Compiler, Interpreter
https://www.online-python.com/

import sys
print(sys.version)
3.8.5 (default, Jul 20 2020, 23:11:29) 
[GCC 9.3.0]

import turtle
ModuleNotFoundError: No module named 'tkinter'

-->

## Python Trinkets

![](https://blog.trinket.io/wp-content/uploads/2014/07/Screenshot-2014-07-02-at-8.49.02-AM.png){width="30%"}

- [Trinket](https://trinket.io/) lets you run and write code in any browser, on any device, with [Python 3](https://trinket.io/features/python3).
- [Python with Turtles](https://hourofpython.trinket.io/a-visual-introduction-to-python) (also available in [Chinese](https://hourofpython.trinket.io/ke3-shi4-hua4-python-jian3-jie4)) is based on Python 2.
- There are many [tutorials](https://hourofpython.com/) on specific topics:
  * [Numbers](https://learnpython.trinket.io/learn-python-part-1-numbers#/numbers/numbers-in-python)
  * [Strings](https://learnpython.trinket.io/learn-python-part-3-words-and-letters#/strings/strings)
  * [Lists](https://learnpython.trinket.io/learn-python-part-7-lists#/lists/things-that-are-alike)
  * [Dictionaries](https://learnpython.trinket.io/learn-python-part-9-dictionaries#/dictionaries/storing-information)
  * [Loops](https://learnpython.trinket.io/learn-python-part-8-loops#/while-loops/about-while-loops)

- More information on [Teach Programming with Wikispaces](https://blog.trinket.io/wikispaces/)
- (Pros) Build and run Python with many files in browser.\
Can share code with a public identifier (no sign up).\
Python2 has turtle graphics.

---

<iframe src="https://trinket.io/embed/python/1dc294f501" width="100%" height="356" frameborder="0" marginwidth="0" marginheight="0" allowfullscreen></iframe>
&nbsp;

- Example of a trinket drawing the Trinket Logo using Turtle Graphics.
- Python code is shown on the left editor.
- Click `▶` to Run the Python code, the result is shown on the right.
- When code runs, click `■` to stop execution, `▶` to resume.
- Click `▼` to show the options to share the code.

<!--

Put Interactive Python Anywhere on the Web
https://trinket.io/python
Python 2 example: Turtle Graphics

Hour of Python
https://hourofpython.com/

Python with Turtles
https://hourofpython.trinket.io/a-visual-introduction-to-python
A visual introduction to code using the Python programming language and Turtles.

視覺化Python簡介
https://hourofpython.trinket.io/ke3-shi4-hua4-python-jian3-jie4
使用Python和烏龜對程式設計的視覺化簡介

Teach Programming with Wikispaces
https://blog.trinket.io/wikispaces/

With Turtle Graphics Trinket:
import sys
print(sys.version)

AttributeError: '<invalid type>' object has no attribute 'version' on line 3 in main.py

Trinket Turtle Graphics is based on Skulpt:
https://skulpt.org/
import sys
print(sys.version)

3.7(ish) [Skulpt]

Or, using Interactive:
Python 2.6(ish) (skulpt, Sun Nov 05 2023 00:37:44 GMT+1000 (GMT+10:00))
[Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:109.0) Gecko/20100101 Firefox/118.0] on MacIntel
Don't type "help", "copyright", "credits" or "license" unless you've assigned something to them
>>> print(sys.version)
3.7(ish) [Skulpt]

-->

## Jupyter Notebook

![](https://jupyter.org/assets/homepage/main-logo.svg){width="10%"}

- Project Jupyter is a non-profit, open-source project, born out of the IPython Project in 2014.
- Now supports interactive data science and scientific computing across all programming languages.
- It offers web-based notebook authoring and editing applications.
- [Jupyter Notebook](https://jupyter.org/try) is the Classic Notebook Interface.
- [JupyterLab](https://jupyter.org/try) is the Next-Generation Notebook Interface.
- Notebooks can be shared, via the [Jupyter Notebook Viewer](https://nbviewer.jupyter.org/).
- [IPython](https://ipython.org/) provides a rich architecture for interactive computing, with showcases:
  * [Collection of Examples](https://nbviewer.org/github/ipython/ipython/blob/6.x/examples/IPython%20Kernel/Index.ipynb)
  * [Notebook Gallery](https://github.com/jupyter/jupyter/wiki)
  * [Talks and Presentations](https://ipython.org/presentation.html)

- (Pros) Can try in browser, developing and sharing Notebooks.\
Tours are provided to help understanding how a Notebook works.\
Suitable for both Python beginner and seasoned programmers, even experts.\
Both [Jupyter](https://docs.jupyter.org/en/latest/) and [IPython](https://ipython.readthedocs.io/en/stable/) provides extensive documentation.\
Online book: [IPython Interactive Computing and Visualization Cookbook](http://ipython-books.github.io/).

---

<iframe
  src="https://jupyterlite.github.io/demo/repl/index.html?kernel=python&toolbar=1"
  width="100%"
  height="500px"
>
</iframe>

- This is the JupyterLite REPL (Read-Evaluate-Print Loop), an interactive console for Python.
- Type the following in a cell, then `SHIFT-ENTER` to evaluate, with result shown in an output cell:
```
import sys
print(sys.version)
```
- You can follow the instructions in a Jupyter Notebook and see how things work in Python.

---

![](https://camo.githubusercontent.com/32fd0a468b9d2684fd14913508088e66f57c393d8874d9cf28ff6765b597bdfe/68747470733a2f2f6d69726f2e6d656469756d2e636f6d2f6d61782f313932302f312a563061356a784f6d624266516c4b687453684b7766412e6a706567){width=80%}


- Example of a Jupyter Notebook: [Python Pandas DataFrame basics.](https://github.com/Tanu-N-Prabhu/Python/blob/master/Pandas/Pandas_DataFrame.ipynb)
- Can view this Notebook by typing the URL (right-click and copy) into [Jupyter Notebook Viewer](https://nbviewer.jupyter.org/).
- A record of the interactive session using the [Pandas](https://pandas.pydata.org/) library for high-performance, easy-to-use data analysis in Python.

<!--

Try Jupyter
https://jupyter.org/try
Use our tools without installing anything

Jupyter Notebook: The Classic Notebook Interface
https://jupyter.org/try
The Jupyter Notebook is the original web application for creating and sharing computational documents. It offers a simple, streamlined, document-centric experience.

JupyterLab: A Next-Generation Notebook Interface
https://jupyter.org/try
JupyterLab is the latest web-based interactive development environment for notebooks, code, and data. Its flexible interface allows users to configure and arrange workflows in data science, scientific computing, computational journalism, and machine learning. A modular design invites extensions to expand and enrich functionality.


Project Jupyter Documentation
https://docs.jupyter.org/en/latest/

IPython
https://docs.jupyter.org/en/latest/reference/ipython.html
An interactive Python kernel and REPL

IPython Interactive Computing
https://ipython.org/

IPython documentation
https://ipython.readthedocs.io/en/stable/

Bringing Modern JavaScript to the Jupyter Notebook
by Kyle Kelley, 24 September 2023.
https://blog.jupyter.org/bringing-modern-javascript-to-the-jupyter-notebook-fc998095081e
The Deno kernel is the freshest runtime for Jupyter

import sys
print(sys.version)

3.11.3 (main, Sep 25 2023, 20:45:01) [Clang 18.0.0 (https://github.com/llvm/llvm-project d1e685df45dc5944b43d2547d013

import turtle
ModuleNotFoundError: The module 'turtle' is removed from the Python standard library in the Pyodide distribution due to browser limitations.
See https://pyodide.org/en/stable/usage/loading-packages.html for more details.


Python Pandas DataFrame basics.
https://github.com/Tanu-N-Prabhu/Python/blob/master/Pandas/Pandas_DataFrame.ipynb
Let us understand the basics of Pandas DataFrame from scratch.

-->

## Google CoLab

![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d0/Google_Colaboratory_SVG_Logo.svg/1600px-Google_Colaboratory_SVG_Logo.svg.png){width=36%}

- [Google Colaboratory](https://colab.google/) is a hosted Jupyter Notebook service from Google.
- Google provides free access to computing resources, including GPUs (Graphical Processing Units) and TPUs (Tensor Processing Units).
- Colab is especially well suited to machine learning, data science, and education.
- Basic features are presented in a Notebook: [Overview of Colaboratory Features](https://colab.research.google.com/notebooks/basic_features_overview.ipynb).
- People use Colab in the following areas:
  * [Data Science](https://colab.research.google.com/#working-with-data), visualizing data and making use of BigQuery.
  * [Machine Learning](https://developers.google.com/machine-learning/crash-course/) executing code on Google hardware, including GPUs and TPUs.

- (Pros) All you need is a browser to tap into Google's cloud servers, with accelerated hardware.\
Graphical Processing is described in a Notebook: [TensorFlow with GPUs](https://colab.research.google.com/notebooks/gpu.ipynb).\
Tensor Processing is described in a Notebook: [TPUs in Colab](https://colab.research.google.com/notebooks/tpu.ipynb)
- (Cons) You'll need a Google account to have access to all features.

---

A non-interactive Google CoLab notebook on Turtle Graphics.

<script src="https://gist.github.com/jhlchan/b417753e47a4b8602ff3348fd08c63aa.js"></script>

<!--

Google Colaboratory
https://colab.google/
Colab is a hosted Jupyter Notebook service that requires no setup to use and provides free access to computing resources, including GPUs and TPUs. Colab is especially well suited to machine learning, data science, and education.

Overview of Colaboratory Features
https://colab.research.google.com/notebooks/basic_features_overview.ipynb

Welcome to Colab!
https://colab.research.google.com/
If you're already familiar with Colab, check out this video to learn about interactive tables, the executed code history view, and the command palette.


Working with Data
https://colab.research.google.com/#working-with-data
Charts: visualizing data       https://colab.research.google.com/notebooks/charts.ipynb
Getting started with BigQuery  https://colab.research.google.com/notebooks/bigquery.ipynb

(New Notebook -- after Google sign-in)
import sys
print(sys.version)
3.10.12 (main, Jun 11 2023, 05:26:28) [GCC 11.4.0]

import turtle
t = turtle.Turtle()

TclError: no display name and no $DISPLAY environment variable

Turtle.ipynb
https://colab.research.google.com/github/karinjd/CSCI5525/blob/master/Turtle.ipynb

!pip3 uninstall ColabTurtle

!pip3 install ColabTurtle
from ColabTurtle.Turtle import *

initializeTurtle()

Using turtle graphics in Google colab
https://stackoverflow.com/questions/56876886/using-turtle-graphics-in-google-colab

or:

!pip3 install ColabTurtle
import ColabTurtle.Turtle as t

t.initializeTurtle(initial_speed=5) 
t.color('blue')
t.forward(100)
t.right(45)
t.color('red')
t.forward(50)


t.initializeTurtle(initial_speed=5) 
pos = (t.getx(), t.gety())
print(pos)  # (400, 250), width = 2 * 400 = 800, height = 2 * 250 = 500

import math

R = 100 # Radius of the cycloid
r = 30  #  Radius of the rolling circle
d = 100 # Distance from the center of the rolling circle to the tracing point
n = r // math.gcd(R, r)   # number of rounds
print(f'R = {R}, r = {r}, d = {d}, n = {n}')

a, b = (R - r), (R - r)/r
t.initializeTurtle(initial_speed=5) # speed = 1 to 13, not 0.

def draw_spiro():
    # put turtle at starting point
    t.penup()
    t.goto(400 + R - r + d, 250 + 0)
    t.pendown()
    t.hideturtle()

    # trace the (x,y) by direct computation
    for angle in range(0, n * 360 + 1):  #  to n * 360 inclusive
        theta = math.radians(angle)
        x = a * math.cos(theta) + d * math.cos(b * theta)
        y = a * math.sin(theta) - d * math.sin(b * theta)
        t.goto(400 + x, 250 + y)

    t.hideturtle()

draw_spiro()

The Notebook is saved in your Google account.

previously:
ValueError: new y position must be non-negative.
due to ColabTurtle has (0,0) not at center, but at upper-left corner.

Share: General access, copy link:
https://colab.research.google.com/drive/1fBTxXGn5ts1zwJiLo5hTU6NpOBMkaP2A?usp=sharing

How to Embed a LIVE Colab Notebook in a website?
https://stackoverflow.com/questions/63208821/how-to-embed-a-live-colab-notebook-in-a-website
(not possible)
Embed without executing:
You can export your already executed notebook as a gist and embed this into your webpage.

Save Colab as a Gist (Colab -> File -> Save a copy as Github Gist)   (to your GitHub account)
Make the Gist public (Gist -> Edit -> Make public)  (in https://gist.github.com/, your GitHub account)
Embed the link into your webpage
<script src="https://gist.github.com/jhlchan/cbfb3f2ded3ef2049d709c22a961f509.js"></script>
<script src="https://gist.github.com/jhlchan/ed3afc80473e2a715e9e7c8ca9773202.js"></script>
<script src="https://gist.github.com/jhlchan/b417753e47a4b8602ff3348fd08c63aa.js"></script>

How to embed Google Colaboratory into Medium in 3 steps
by Liyi Zhou, 22 February 2019.
https://liyi-zhou.medium.com/how-to-embed-google-colaboratory-into-medium-in-3-steps-487b525b103c

-->

## Official Python

![](https://docs.python.org/3/_static/og-image.png){width=20%}

- Latest version can be intstalled from the official site for [Python](https://www.python.org/).
- Detailed information is available from [Python Documentation](https://docs.python.org/3/index.html).
- Source code is in a public repository [Python GitHub](https://github.com/python)
- (Pros) This is the standard version, the real thing.\
For the serious application developers.
- (Cons) This takes up local disk storage.

---

![](https://codingnemo.com/wp-content/uploads/2021/03/IDLE-run-time.jpg){width=80%}

- Python installation includes an Integrated Development and Learning Environment (IDLE).
- Can be used to develop Python applications, with Turtle Graphics.
- Some good examples are presented in Help &gt; Turtle Demo.
- Each [example](https://docs.python.org/3/library/turtle.html#module-turtledemo) demonstrates a feature of Turtle Graphics.
- [Videos](https://www.youtube.com/results?search_query=Python+Turtle+Graphics) and [tutorials](https://www.google.com/search?q=python+turtle+graphics+tutorial) on Turtle Graphics are popular on the Web.

<!--

Official Python
https://www.python.org/

Python
https://github.com/python
Repositories related to the Python Programming language

Using Python on a Mac
Author: Bob Savage
https://docs.python.org/3/using/mac.html
5.2. The IDE
MacPython ships with the standard IDLE development environment. A good introduction to using IDLE can be found at
http://www.hashcollision.org/hkn/python/idle_intro/index.html

How to run IDLE on a Mac
https://www.asmarterwaytolearn.com/python/IDLE-for-mac.html

Getting Started with Python Programming for Windows Users 
https://www.cs.utexas.edu/~mitra/csSpring2013/cs313/start.html

Python 3 Notes: Trying out Python for the First Time: Mac
https://sites.pitt.edu/~naraehan/python3/getting_started_mac_first_try.html

IDLE and tkinter with Tcl/Tk on macOS
https://www.python.org/download/mac/tcltk/
Python's integrated development environment, IDLE, and the tkinter GUI toolkit it uses, depend on the Tk GUI toolkit which is not part of Python itself. For best results, it is important that the proper release of Tcl/Tk is installed on your machine. 

Tk On macOS
There are currently three major variants of Tk in common use on macOS:
Aqua Cocoa Tk (built-in 8.6.11)
Aqua Carbon Tk (Intel and PowerPC)
X11 Tk (traditional Unix Tk implementation)

python -m idlelib.idle
2023-11-06 02:52:45.140 Python[22773:15248424] WARNING: Secure coding is not enabled for restorable state! Enable secure coding by implementing NSApplicationDelegate.applicationSupportsSecureRestorableState: and returning YES.

This launches Python with IDLE Shell 3.11.5.

Python 3.11.5 (v3.11.5:cce6ba91b3, Aug 24 2023, 10:50:31) [Clang 13.0.0 (clang-1300.0.29.30)] on darwin
Type "help", "copyright", "credits" or "license()" for more information.
>>>

This is the same as launching IDLE from Finder, although the app icon is different.
Finder shows: IDLE   File Edit Shell Debug Options Windows Help
Terminal has: Python File Edit Shell Debug Options Windows Help, with an IDLE Shell.

import turtle
t = turtle.Turtle()
(gives a Turtle canvas 700x700, with (0,0) at center)
IDLE is a line editor.

Help has Turtle Demo.
Some examples give errors in terminal?!

-->

<!--
pandoc -t revealjs --standalone --mathjax python.md -o python.html
pandoc -t revealjs -s --mathjax python.md -o python.html
-->
