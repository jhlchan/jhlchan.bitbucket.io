---
title: URBAN ANALYTICS
author: data, insights and visualization
date: October 2023
theme: solarized
header-includes: |
    <style>
      .reveal {
      font-size: 16pt;
      line-height: 1.2em;
    }
     .reveal pre code {
      font-size: 14pt;
      line-height: 1.2em;
    }
    .reveal h1,
    .reveal h2,
    .reveal h3,
    .reveal h4,
    .reveal h5,
    .reveal h6 {
      text-transform: none;
    }
    </style>
    <base target="_blank"/>
...


# URBS1004 URBAN ANALYTICS STUDIO

![](https://foa-media.arch.hku.hk/media/upload/2020/10/Accessibility-of-Harbour-Crossings-in-Hong-Kong_1-424x600.jpg){ width=20% }\
_A project by undergraduate 1st year students_

- [URBS1004](https://www.arch.hku.hk/gallery/upad/urbs1004-urban-analytics-studio/) is a course from the Uninversity of Hong Kong
- offered by the Department of Urban Planning and Design
- established within the Faculty of Architecture in 2008
- The 11-page [project report](https://foa-media.arch.hku.hk/media/upload/2020/10/Accessibility-of-Harbour-Crossings-in-Hong-Kong.pdf) looks really professional!

<!--

URBS1004 Urban Analytics Studio
https://www.arch.hku.hk/gallery/upad/urbs1004-urban-analytics-studio/
HOME / Programmes / Urban Planning and Design / Gallery 
Related Staff : Guibo Sun, Kenneth Tang
This course will introduce student to data analytics of urban data and demonstrates how these analytics can be employed in solving urban problems. Students will learn conceptual frameworks, methodologies, software tools, as well as applied cases for urban data analytics. Students will also be introduced to multimedia, data visualization and interactive mapping.

Accessibility of Harbour Crossings in Hong Kong
https://foa-media.arch.hku.hk/media/upload/2020/10/Accessibility-of-Harbour-Crossings-in-Hong-Kong.pdf
-->

# URBAN SDK

![](https://ml.globenewswire.com/Resource/Download/7c3db398-a596-4c21-ab08-8c34dd56ae69){ width=60% }\
[Urban SDK: Urban Planning and Analytics Software](https://www.urbansdk.com/)

1. I am captivated by the demo video.
1. Website design is quite modern, even equipped with online chat!
1. They have a [YouTube Channel](https://www.youtube.com/@urbansdk4047), but no mention of the price.
1. Teams haven't heard about Urban SDK can request a demo.
1. Google "Urban SDK" gives plenty of links comparing this product and alternatives, user likes and dislikes -- making easy the decision to buy!\
\

* Anyway, I am more attracted to "Come join our team!" on the Company > About page.
* Students may help to develop an open standard SDK (software development kit) for Urban Data Analytics!

<!--

https://ml.globenewswire.com/Resource/Download/7c3db398-a596-4c21-ab08-8c34dd56ae69
https://assets.website-files.com/6178b4caa6d45ba3be67c737/6178b513c59b6f4bcd3a71e3_Logo%20White.svg

Urban SDK: Urban Planning and Analytics Software
https://www.urbansdk.com/
Data analytics for cities
We index data from the physical world and turn it into intelligence for you to use.

Urban SDK
https://www.youtube.com/channel/UCPTK3NG9-nPd1guGJnwGp5g

Urban SDK Pricing, Alternatives & More 2023
https://www.capterra.com/p/242073/Urban-SDK/

Urban SDK Pricing, Reviews and Features (September 2023)
https://www.saasworthy.com/product/urban-sdk

Urban SDK Pros and Cons | User Likes & Dislikes
https://www.g2.com/products/urban-sdk/reviews?qs=pros-and-cons
-->

# Generative AI

+ [Urban AI Conversation #16 - Generative AI and Urban Design](https://youtu.be/G1fhUtbIjuA)【1:00:04】cc\
![](http://img.youtube.com/vi/G1fhUtbIjuA/0.jpg){ width=15% }\
The event took place on 17 May 2023, very recent. No comments yet, be the first!

+ [The Role of Artificial Intelligence in Urban Design, Urban planning, Energy Efficiency, Sustainability](https://youtu.be/UICMG4o4S8E)【4:53】cc\
![](http://img.youtube.com/vi/UICMG4o4S8E/0.jpg){ width=15% }\
Ali Khiabanian is an architect, author and digital artist.

. . .

All from YouTube search "Urban Planning and AI". Like to view more?

***

+ [I challenged an AI to design a city and the results are SURPRISING ...](https://youtu.be/EKHgP2EHDos)【12:57】cc\
![](http://img.youtube.com/vi/EKHgP2EHDos/0.jpg){ width=20% }\
Professor Adam G Yates from the University of Waterloo is an aquatic ecologist. His research blends aquatic and landscape sciences to help the sustainability of freshwater ecosystems.

+ [CogX - AI in Designing Better Cities](https://youtu.be/bUv38kHOHbw)【11:27】cc\
![](http://img.youtube.com/vi/bUv38kHOHbw/0.jpg){ width=20% }\
CognitionX is an AI advice platform. Quantifying aspects of urban design on a massive scale is crucial to help build scientific evidence to design more socially cohesive and economically prosperous cities.


<!--

"Urban Planning and AI"

Urban AI Conversation #16 - Generative AI and Urban Design【1:00:04】cc
https://www.youtube.com/watch?v=G1fhUtbIjuA
The event took place on 17 May 2023, very recent. No comments yet, so maybe you can be the first!

The Role of Artificial Intelligence in Urban Design, Urban planning,Energy Efficiency,Sustainability【4:53】cc
https://www.youtube.com/watch?v=UICMG4o4S8E
Ali Khiabanian is an architect, author and digital artist.

I challenged an AI to design a city and the results are SURPRISING...【12:57】cc
https://www.youtube.com/watch?v=EKHgP2EHDos
Professor Adam G Yates from the University of Waterloo is an aquatic ecologist whose research blends aquatic and landscape sciences to generate the knowledge and tools to help solve critical issues threatening the health and sustainability of freshwater ecosystems.

Future cities: Urban planners get creative | DW Documentary【25:56】cc
https://www.youtube.com/watch?v=HBMlQZeXMiA
From DW, a German public broadcast service.
Will the cities of the future be climate neutral? Might they also be able to actively filter carbon dioxide out of the air? Futurologist Vincente Guallarte thinks so. In fact, he says, our cities will soon be able to absorb CO2, just like trees do.

Cities of the future are bio-cities, cities that follow the rules of nature. They are self-sufficient, circular systems, with homes like trees, cities like forests, forming the basis of a new urban model.

I asked AI to Design a New Urbanist Seaside Town【9:34】cc
https://www.youtube.com/watch?v=dmdE8FrkPOU
This is an experiment to see if we can get MidJourney AI to design a version of the New Urbanist town of Seaside, Florida.

CogX - AI in Designing Better Cities | CogX【11:27】cc
https://www.youtube.com/watch?v=bUv38kHOHbw
CognitionX: The AI Advice Platform
CogX Global Leadership Summit and Festival of AI and Breakthroughs Technology - June 8th to 10th 2020.
Chanuki Seresinhe, University of Warwick,
Dr Stephen Law, University College London.
Quantifying aspects of urban design on a massive scale is crucial to help build scientific evidence to design more socially cohesive and economically prosperous cities.

-->

# TED Talks

- TED = Technology, Entertainment, Design, with a mission motto. ![](https://logos-world.net/wp-content/uploads/2022/07/TED-Logo.png){ width=25% }
- TED Talks are videos (under __18 min__) from expert speakers on almost any topics, in 100+ languages. 
- TED speakers are selected, even trained, to present an engaging *talk*, not a lecture or lesson.
- TEDx events are TED-like talks run independently by community, not by TED. 
- YouTube search "urban planning ted" for such amazing talks -- be inspired!
- [Amanda Burden: How public spaces make cities work](https://youtu.be/j7fRIGphgtk)【18:29】cc\
![](http://img.youtube.com/vi/j7fRIGphgtk/0.jpg){ width=20% }\
Amanda Burden shares the challenges of planning parks that people love.
She helped plan some of the city's newest public spaces, drawing on her experience as an animal behaviorist.

<!--

https://logos-world.net/wp-content/uploads/2022/07/TED-Logo.png (with motto)
https://1000logos.net/wp-content/uploads/2020/09/TED-Logo-1024x640.png

"urban planning ted"

Amanda Burden: How public spaces make cities work【18:29】cc
https://www.youtube.com/watch?v=j7fRIGphgtk
Amanda Burden helped plan some of the city's newest public spaces, drawing on her experience as, surprisingly, an animal behaviorist. She shares the unexpected challenges of planning parks people love -- and why it's important.

Always keep yourself up-to-date.

-->

# Deep Learning

![](https://media.springernature.com/lw685/springer-static/image/chp%3A10.1007%2F978-981-15-8983-6_43/MediaObjects/471858_1_En_43_Fig1_HTML.png){ width=45% }

::: incremental
+ In Big Data era, urban studies gather enormous volumes of raw input data.
+ Various AI techniques are applied to filter and clean up the data for use.
+ Machine Learning (ML) enables pattern extraction from data, to show the features.
+ Deep Learning (DL) reveals the connection between features, to achieve some _understanding_.
:::

<!-- comment break for lazy list, no good, just split -->
***

+ [deep learning | Urban Analytics Lab | Singapore](https://ual.sg/tag/deep-learning/)\
  Many papers from National University of Singapore (NUS).
+ [AI and Deep Learning for Urban Computing](https://link.springer.com/chapter/10.1007/978-981-15-8983-6_43)\
  Open Access article from a book, published 7 April 2021.\
  Download the whole book or just this Chapter 43.
+ YouTube search "urban analytics and Deep Learning" gives, e.g.
  - [Urban analytics - the right place: Hillit Meidar-Alfi](https://youtu.be/yXc9EyhUH0E)【13:17】cc\
    ![](http://img.youtube.com/vi/yXc9EyhUH0E/0.jpg){ width=20% }\
    This is a TEDx talk, with focus on the impacts of infrastructure on daily life.
  - [Data-Driven Urban Design](https://youtu.be/M18EZIQe8ec)【1:10:28】cc\
    ![](http://img.youtube.com/vi/M18EZIQe8ec/0.jpg){ width=20% }\
    Hong Kong's high-density urbanism offers valuable lessons in sustainable planning, yet its Compact City model places pressures on public spaces and urban processes.

<!--

"urban analytics and Deep Learning"

deep learning | Urban Analytics Lab | Singapore
https://ual.sg/tag/deep-learning/
Many papers from National University of Singapore (NUS).

     
AI and Deep Learning for Urban Computing
https://link.springer.com/chapter/10.1007/978-981-15-8983-6_43
by Senzhang Wang & Jiannong Cao 
Open Access, First Online: 07 April 2021
A whole book for download (or download just Chapter 43).
Part of the The Urban Book Series book series (UBS)

Urban Informatics (928 pages)
file:///Users/josephchan/Downloads/978-981-15-8983-6.pdf
Chapter 43: AI and Deep Learning for Urban Computing (page 815)
by Senzhang Wang and Jiannong Cao
https://media.springernature.com/lw685/springer-static/image/chp%3A10.1007%2F978-981-15-8983-6_43/MediaObjects/471858_1_En_43_Fig1_HTML.png

Urban analytics - the right place: Hillit Meidar-Alfi at TEDxCoconutGrovet【13:17】cc
https://www.youtube.com/watch?v=yXc9EyhUH0E
Her dissertation research focused on the impacts of infrastructure on daily life, specifically the effects of infrastructure investments on quality of life measures.

Data-Driven Urban Design【1:10:28】cc
https://www.youtube.com/watch?v=M18EZIQe8ec
University of Technology Sydney 26 Oct 2022
Hong Kong's high-density urbanism offers valuable lessons in sustainable planning, yet its Compact City model places pressures on public spaces and urban processes. This lecture presents ongoing research into the data-driven analysis of urban spaces and activities, aimed at informing human-centric and evidence-based methods for the design of liveable and vibrant urban areas.

-->

# Online Courses

![](https://oge.mit.edu/wp-content/uploads/2021/07/dusp.jpg){ width=30% }

+ Google "urban planning courses" shows many institutions offer such courses.
+ A few put their courses online, e.g. Massachusetts Institute of Technology (MIT).
+ The Office of Graduate Education (OGE) of MIT offers [Urban Studies and Planning](https://oge.mit.edu/programs/urban-studies-and-planning/).
+ The course website: [MIT Department of Urban Studies and Planning (DUSP)](https://dusp.mit.edu/)

----

![](https://dusp.mit.edu/sites/default/files/styles/large/public/project-images/MIT_Charles_River_aerial.jpg){ width=50% }

::: incremental
+ [City Design and Development / Urbanism / LCAU Fall 2022 Lecture Series](https://dusp.mit.edu/projects/city-design-and-development-urbanism-lcau-fall-2022-lecture-series), with 14 lectures.
+ [MIT CDD/LCAU/Architecture Fall 2022 Speaker Series](https://www.youtube.com/playlist?list=PLw7-XsDGUXTja9v4IvEA2zfB0cP73DToZ) (playlist)\
  ![](https://i.ytimg.com/vi/Q5DIivSnGb0/hqdefault.jpg){ width=20% }\
  Playlist of the 14 lectures: *What Does Urban Science Have to Say About Urban Design?*
+ [YouTube Channel of DUSP of MIT](https://www.youtube.com/@duspmit2995), heaps of videos.
:::

<!--

"urban planning courses"

MIT Department of Urban Studies and Planning (DUSP)
MIT DUSP - Massachusetts Institute of Technology
https://dusp.mit.edu/

https://www.youtube.com/@duspmit2995
What will this look like in Whatsapp?

City Design and Development / Urbanism / LCAU Fall 2022 Lecture Series 
https://dusp.mit.edu/projects/city-design-and-development-urbanism-lcau-fall-2022-lecture-series
https://youtu.be/Q5DIivSnGb0
Welcome to the MIT Department of Urban Studies and Planning. We are home to the largest urban planning faculty in the United States.

What Does Urban Science Have to Say About Urban Design? MIT CDD/LCAU/Architecture Fall 2022 Speaker Series
https://www.youtube.com/playlist?list=PLw7-XsDGUXTja9v4IvEA2zfB0cP73DToZ
https://i.ytimg.com/vi/Q5DIivSnGb0/hqdefault.jpg?sqp=-oaymwEXCNACELwBSFryq4qpAwkIARUAAIhCGAE=&rs=AOn4CLBPavTtQvM_Qhyk0Yi4beE0kYXuMQ
DUSP MIT
14 videos 292 views Last updated on 19 Dec 2022

Urban Studies and Planning
https://oge.mit.edu/programs/urban-studies-and-planning/
OGE = Office of Graduate Education
Website:
Urban Studies and Planning goes to: https://dusp.mit.edu/ (above)

City Design and Development
https://dusp.mit.edu/city-design-and-development

-->

# Lecture Notes

![](https://ocw.mit.edu/static_shared/images/ocw_logo_orange.png){ width=50% }

+ MIT delivers [OpenCourseWare (OCW)](https://ocw.mit.edu), publishing learning materials as _open content_.
+ Possible to find free lecture notes, videos, assignments and exams, all available online.
+ All resources are under **Creative Commons License** (CCL): free to share, adapt, must give credit and not-for-profit.
+ Search for "AI", "Machine Learning" or "Deep Learning" for such open courseware.
+ Search for "Urban Design", or "Urban Analytics" for course and seminars in urban studies.


----

+ Check out this MIT OCW: [Introduction to Urban Design and Development (2006)](https://ocw.mit.edu/courses/11-001j-introduction-to-urban-design-and-development-spring-2006/)
  + The course has 25 _Question of the Day_, all with readings and lectures, with 3 assignments.
  + Course instructor is Professor [Susan Silberberg](http://susansilberberg.com/), a city planner, urban designer, arts and cultural planner, architect, author, and educator.
  + [Want Global Change? Get Local! | Susan Silberberg](https://youtu.be/IZSQsXkj95k)【14:04】cc\
    ![](http://img.youtube.com/vi/IZSQsXkj95k/0.jpg){ width=20% }\
    A TEDx talk that is well-prepared and skillfully articulated.
+ See also [Beijing Urban Design Studio (2006)](https://ocw.mit.edu/courses/11-307-beijing-urban-design-studio-summer-2006/)
  + There are no lecture notes, but an image gallery and 6 student project presentations.
+ More info in [Urban Design Seminar (2005)](https://ocw.mit.edu/courses/11-333-urban-design-seminar-spring-2005/) and [Urban Design Seminar (2016)](https://ocw.mit.edu/courses/11-333-urban-design-seminar-spring-2016/).
+ Another series is given by the Indian Institute of Technology (IIT) at Roorkee:
  + [Introduction to Urban Planning](https://www.youtube.com/playlist?list=PLLy_2iUCG87AAaDRVrD02Y1z44OXt5shB) (playlist)\
    ![](https://i.ytimg.com/vi/q_XmlG3CwNk/hqdefault.jpg){ width=20% }\
    41 video lectures presented by Professor Harshit Sosan Lakra in July 2018.


<!--

"urban planning lecture notes"

How to Learn Machine Learning
Your repository of resources to learn Machine Learning
https://howtolearnmachinelearning.com/articles/mit-opencourseware/
https://howtolearnmachinelearning.com/wp-content/uploads/2021/06/MIT_OPEN_COURSEWARE_LOGO.png
https://ocw.mit.edu/static_shared/images/ocw_logo_orange.png

MIT Open Courseware
https://ocw.mit.edu
search "Urban Analytics".

11.001J | Spring 2006 | Undergraduate
Introduction to Urban Design and Development
https://ocw.mit.edu/courses/11-001j-introduction-to-urban-design-and-development-spring-2006/
by Professor Susan Silberberg
Lecture Notes, Assignements.

Susan Silberberg
http://susansilberberg.com/
Her TEDx talk “Want Global Change? Get Local!” can be viewed here.

Want Global Change? Get Local! | Susan Silberberg | TEDxBeaconStreet【14:04】cc
https://www.youtube.com/watch?v=IZSQsXkj95k
A talk that is well-prepared, skillfully articulated.


11.307 | Summer 2006 | Graduate
Beijing Urban Design Studio
https://ocw.mit.edu/courses/11-307-beijing-urban-design-studio-summer-2006/
5 professors, no lecture notes, but assignments and project presentations.


11.333 | Spring 2016 | Graduate
Urban Design Seminar
https://ocw.mit.edu/courses/11-333-urban-design-seminar-spring-2016/
Seminar resources are given in Readings.


11.333 | Spring 2005 | Graduate
Urban Design Seminar
https://ocw.mit.edu/courses/11-333-urban-design-seminar-spring-2005/
Only Readings and student project presentations


Introduction to Urban Planning
IIT Roorkee July 2018
https://www.youtube.com/playlist?list=PLLy_2iUCG87AAaDRVrD02Y1z44OXt5shB
41 videos
Prof. Harshit Sosan Lakra
Department of Architecture & Planning
IIT Roorkee (Indian Institute of Technology, Roorkee)

-->

# Turing 2.0

![](https://pbs.twimg.com/media/FW1ZuBjXwAIJ3Vi?format=jpg&name=medium){ width=50% }

+ A project from [The Alan Turing Institute](https://www.turing.ac.uk/), UK’s national institute for data science and artificial intelligence.
+ Published in 21 March 2023: [Turing 2.0 - Changing the world for the better with data science and AI](https://www.turing.ac.uk/sites/default/files/2023-03/turing_2.0_-_institute_strategy_-_final.pdf)
  + _Our ambition in Turing 2.0 is to harness the data science and AI revolution and direct its energy towards solving some of the thorniest challenges we face as a society. We think there is untapped potential for the public good and a national institute is key to unlocking it._

---

![](https://www.turing.ac.uk/sites/default/files/styles/teaser/public/2023-07/ma_thumbnail.png){ width=50% }

+ The Institute provides [The Turing Lectures](https://www.turing.ac.uk/events/the-turing-lectures).
+ A series of inspiring talks by leading figures in data science and AI.
+ Speakers include the lead scientist of Google Deepmind’s Machine Learning team.
+ Lectures are released to public about 6 months later:
  + [Turing Lectures: The Alan Turing Institute](https://www.youtube.com/playlist?list=PLuD_SqLtxSdVDcrCYIHayTL91DapuIHrO) (playlist)\
    ![](https://i.ytimg.com/vi/Ug8uvqKMRqg/hqdefault.jpg){ width=20% }\
    49 videos available in September 2023.

---

![](https://yt3.googleusercontent.com/ov2Lirri_okq3TdN5TgKOMaFfJiPI_w5PHqdb1ShMfTNsqQDG6OLSgXIfALTWlAWPTHqXKAcDG0=w2560-fcrop64=1,00005a57ffffa5a8-k-c0xffffffff-no-nd-rj){ width=50% }

+ The Institute has a [YouTube Channel](https://www.youtube.com/c/TheAlanTuringInstituteUK/), with many playlists for the large collection of videos.
+ [Towards urban analytics 2.0](https://www.youtube.com/playlist?list=PLuD_SqLtxSdVvfm_1AV2jTXc9FBT9pf6H) (playlist)\
  ![](https://i.ytimg.com/vi/waiyxVeQ_kI/hqdefault.jpg){ width=20% }\
  Includes 23 videos, with two 1-hour keynote speeches, followed by lots of 5-minute lightning talks.
+ [UA 2.0 – Towards greener and sustainable cities](https://www.youtube.com/playlist?list=PLuD_SqLtxSdVsrQ2uPGB633XwWtqJJ8-x) (playlist)\
  ![](https://i.ytimg.com/vi/aCgS8DoUVGs/hqdefault.jpg){ width=20% }\
  Includes 17 videos, with three 40-minute keynote speeches, others are 10-minute talks.

---

![](https://file-eu.clickdimensions.com/turingacuk-ap7zg/files/updatedeventassets-final8.jpg){ width=40% }

+ The Institute also organised [Research programmes](https://www.turing.ac.uk/research/research-programmes), with articles, projects and publications.
  + [Urban analytics (2018)](https://www.turing.ac.uk/research/research-programmes/urban-analytics)
    + Developing data science and AI focused on the process, structure, interactions and evolution of agents, technology and infrastructure within and between cities.
  + [Data science at scale (2016-2021)](https://www.turing.ac.uk/research/research-programmes/data-science-scale)
    + Building upon advances in high-performance computer architectures, through algorithm-architecture co-design, with applications including health and life science.
  + [Artificial intelligence (2022)](https://www.turing.ac.uk/research/research-programmes/artificial-intelligence)
    + Advancing world-class research into AI, its applications and its implications for society.
+ One of its founding universities, University College London (UCL), launched [Research Programmes Showcase](https://www.ucl.ac.uk/alan-turing-institute/news/2021/jan/research-programme-showcase) from January 2021. 

<!--

https://www.turing.ac.uk/sites/default/files/2023-03/turing%5F2.0%5F-%5Finstitute_strategy%5F-%5Ffinal.pdf

The Alan Turing Institute
https://www.turing.ac.uk/
We are the UK’s national institute for data science and artificial intelligence.

About The Turing Lectures
https://www.turing.ac.uk/events/the-turing-lectures
Speaking to packed audiences, the lectures have already included the lead scientist of Google Deepmind’s Machine Learning team, Nando de Freitas; Buzzfeed News Editor, Craig Silverman; computer scientist, academic and social entrepreneur Sue Black OBE; and mathematician, author and broadcaster, Hannah Fry. 

Turing Lectures
The Alan Turing Institute
52 videos 8,684 views Last updated on 15 Sept 2023
https://www.youtube.com/playlist?list=PLuD_SqLtxSdVDcrCYIHayTL91DapuIHrO

The Alan Turing Institute
https://www.youtube.com/c/TheAlanTuringInstituteUK/playlists
23 · Towards urban analytics 2.0

21 March 2023
Turing 2.0: - Changing the world for the better with data science and AI
https://www.turing.ac.uk/sites/default/files/2023-03/turing_2.0_-_institute_strategy_-_final.pdf
Our ambition in Turing 2.0 is to harness the data science and AI revolution and direct its energy towards solving some of the thorniest challenges we face as a society. We think there is untapped potential for the public good and a national institute is key to unlocking it.

*** Towards urban analytics 2.0
The Alan Turing Institute (23 videos)
https://www.youtube.com/playlist?list=PLuD_SqLtxSdVvfm_1AV2jTXc9FBT9pf6H

UA 2.0 – Towards greener and sustainable cities (2022)
https://www.turing.ac.uk/events/ua-20-towards-greener-and-sustainable-cities
UA 2.0 – Towards greener and sustainable cities
https://www.youtube.com/playlist?list=PLuD_SqLtxSdVsrQ2uPGB633XwWtqJJ8-x
17 videos, 3 keynotes, others 10-miniutes talks.

Towards urban analytics 2.0 (2021)
https://www.turing.ac.uk/events/towards-urban-analytics-20

Research programmes
https://www.turing.ac.uk/research/research-programmes
Significant programmes of strategic research, typically led by a Programme Director or Directors and with involvement from industry, public sector or third sector partners

Research programme showcase
18 January 2021
https://www.ucl.ac.uk/alan-turing-institute/news/2021/jan/research-programme-showcase
The Turing has launched its first Research Programmes Showcase, a new event series connecting the Turing’s research community to its research programmes.
UCL = University College London

Urban analytics (2018)
https://www.turing.ac.uk/research/research-programmes/urban-analytics
Developing data science and AI focused on the process, structure, interactions and evolution of agents, technology and infrastructure within and between cities
(programme articles, projects and publications)

Artificial intelligence (2022)
https://www.turing.ac.uk/research/research-programmes/artificial-intelligence
Advancing world-class research into AI, its applications and its implications for society, building on our academic network’s wealth of expertise
(programme articles, projects and publications)

Data science at scale (2016-2021)
https://www.turing.ac.uk/research/research-programmes/data-science-scale
Building upon advances in high-performance computer architectures, through algorithm-architecture co-design, with applications including health and life science – this programme has now ended.
(programme articles, projects and publications)

Fundamental AI
https://www.turing.ac.uk/research/research-programmes/fundamental-ai
Advancing world-class research into AI’s foundations, applications and implications for society
Undertake world-class research into the foundations of AI and its applications, including foundation models and the large language models that power chatbots such as ChatGPT and Bard.
(ongoing)

>>>

-->

<!--
pandoc -t revealjs -s --mathjax urban.md -o urban.html
-->
