/* ------------------------------------- *
 * Javascript for animation.             *
 *                                       *
 * Joseph Chan, May 2024.                *
 * ------------------------------------- */

/**
 Animate script

 Ideas taken from:
 * Markdeep:  https://casual-effects.com/markdeep/latest/markdeep.min.js
 * MathIsFun: https://mathsisfun.com/geometry/images/geom-triangle.js
 */

/**

 Documentation:

 Use a Panel for <animate>:
 * a DIV with position:relative, width and height.
 * have a Board of square size.
 * have Title and Info lines on top.
 * have an Animate/Reset button at the bottom.
 * other properties are local, e.g. the pieces.

 */

/**
 Common Global Functions
*/
// convert list to an array
function toArray(list) {
    return Array.prototype.slice.call(list);
}
// Round to decimal places
function round1(x) {
    return Math.round(x * 10) / 10;
}
function round2(x) {
    return Math.round(x * 100) / 100;
}
function round3(x) {
    return Math.round(x * 1000) / 1000;
}
// can also use: +(num.toFixed(places)), toFixed gives string, + converts to number.
// trigonometry functions
var sin = Math.sin;
var cos = Math.cos;
var tan = Math.tan;
var atan = Math.atan;
var atan2 = Math.atan2;
var PI = Math.PI;
// convert angle in degree to radian
function toRadian(x) {
    return x * PI / 180;
}
// convert angle in radian to degree
function toDeg(x) {
    return x * 180 / PI;
}
// convert angle in radian to degree with round-off
function toDegree(x) {
    return Math.round(x * 180 / PI);
}
// ensure val is within min to max
function constrain(min, val, max) {
    return Math.min(Math.max(min, val), max);
}
/* get a random number within min to max */
function getRandom(min, max) {
    return Math.random() * (max - min) + min;
}

/* Note: cannot be placed outside:
String.prototype.parse = function ...
as this would cause function ... to be evaluated, then assign to parse.
In that case, this = the function object, and it has no match method.
Once inside braces {}, the function is defined, not evaluated.
The function is assigned to String.prototype, and 'this' will be a string on execution.
*/

// single function application for parser
(function() {
'use strict';
/**
 A simple string parser.
 Syntax: parse(pattern), where pattern is a string of indicators:
 * w for word, of alphabets and numbers, underscore. \w+
 * c for char, a single alphabet
 * i for integers, [0-9]+
 * n for numbers, possibly with decimal. \d+\.?\d*
 * Some tips in https://stackoverflow.com/questions/2811031/
 * () for a group, giving a sublist, numbers will use parseInt or parseFloat.
 * The components are separated by whitespaces, with optional leading and ending spaces.
 */
String.prototype.parse = function (pattern) {
    // split this string into words, cleanup whitespaces.
    var words = this.match(/\b([\w\.]+)\b/g);
    // parse using local pattern
    function parseWords(pat) {
        var list = [];
        while (pat.length > 0 && words.length > 0) {
            // consume local pattern
            var ch = pat[0];
            pat = pat.slice(1);
            if (ch == '(') { // recursive call
                var j = pat.indexOf(')');
                if (j !== -1) {
                    var result = parseWords(pat.slice(0,j));
                    if (result.length > 0) list.push(result);
                    pat = pat.slice(j); // advance local pat
                }
            }
            else if (ch == ')') { // ignore
            }
            else { // consume words
                var item = words.shift();
                switch (ch) {
                case 'w' : if (item.match(/^\w+$/)) list.push(item); break;
                case 'c' : if (item.match(/^[A-Za-z]$/)) list.push(item); break;
                case 'i' : if (item.match(/^[0-9]+$/)) list.push(parseInt(item)); break;
                case 'n' : if (item.match(/^\d+\.?\d*$/)) list.push(parseFloat(item)); break;
                default : break; // discard item and ch
                }
            }
        }
        // alert('list :' + list.toString());
        return list;
    }
    // main call: ensure pattern not null.
    return parseWords(pattern || '');
}
// convert string to JSON object
String.prototype.toObject = function() {
    // split this string into words, cleanup whitespaces.
    var words = this.match(/\b([\w\.]+)\b/g);
    // remember indices pointing to a word that begins with an alphabet
    var alpha = [];
    for (var j = 0; j < words.length; j++) {
         if (words[j].match(/^[A-Za-z]/)) alpha.push(j);
    }
    // form the JSON string
    var s = [];
    while (alpha.length > 0) {
        var j = alpha.shift(); // alpha[0] = alpha.peek()
        var k = alpha.length > 0 ? alpha[0] : words.length;
        if (k == j+1) { // two consecutive words
            s.push('"' + words[j] + '":"' + (k < words.length ? words[k] : 'null') + '"');
            alpha.shift(); // discard index k
        }
        else if (k == j+2) { // a word and a number
            s.push('"' + words[j] + '":' + words[j+1]);
        }
        else { // a word and more numbers
            s.push('"' + words[j] + '":[' + words.slice(j+1, k).join(',') + ']');
        }
    }
    s = '{ ' + s.join(', ') + ' }'; // the JSON string
    // alert('toObject: ' + this + '\n' + s);
    return JSON.parse(s); // object of JSON string
}
})(); // invoke single function

/**
 Part 4: Origami, using the same idea.
 * set up stylesheet for Board and elements,
 * find all nodes of origami class or tag,
 * parse the node content and generate a Board node,
 * embed the Board node inside a Panel node with title and button,
 * replace each <origami> node by a Panel node.
 *
 * Elements of the board are pieces.
 * Animation of pieces is done by CSS, via keyframes.
 */

// single function application for <origami>
(function() {
'use strict';

// Constants
var size = 500; // original 200
// Safari CSS animation needs the -webkit- prefix, but not for FireFox and others.
var isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
// This uses negative look-arounds to exclude Chrome, Edge, and all Android browsers
// that include the Safari name in their user agent.
// See https://stackoverflow.com/questions/7944460/detect-safari-browser

// load style sheet for animate
function loadAnimateStyle() {
    var s = '';
    s += '<style>'
//    s += '.frame { position:relative; width:360px; height:280px; border: 1px solid blue; border-radius: 9px; margin:auto; display:block;}'
// increase frame width and height
    s += '.frame { position:relative; width:800px; height:600px; border: 1px solid blue; border-radius: 9px; margin:auto; display:block;}'
    s += '.title { font: 12pt arial; font-weight: bold; position:absolute; top:10px; left:0px; width:360px; text-align:center; pointer-events: none;}'
    s += '.info { font: 10pt arial; font-weight: bold; color: #6600cc; position:absolute; top:31px; left:0px; width:360px; text-align:center; pointer-events: none;}'
// bigger button and margin
    s += '.btn { display: inline-block; position: relative; text-align: center; margin: 50px; text-decoration: none; font: bold 22px/25px Arial, sans-serif; color: #268; border: 1px solid #88aaff; border-radius: 10px;cursor: pointer; background: linear-gradient(to top right, rgba(170,190,255,1) 0%, rgba(255,255,255,1) 100%); outline-style:none;}'
    s += '.btn:hover { background: linear-gradient(to top, rgba(255,255,0,1) 0%, rgba(255,255,255,1) 100%); }'
//    s += '.board { position:relative; width:'+size+'px; height:'+size+'px; border:1px solid coral; display:block;}'
// remove border
    s += '.board { position:relative; width:'+size+'px; height:'+size+'px; display:block;}'
    s += '.piece { position:absolute; width:100%; height:100%; top:0%; left:0%; }'
//    s += '.txtfont { font: 12pt arial; } '
// change font for greeting
    s += '.txtfont { font: 22pt Brush Script MT; } '
// add fading for banner
    s += '#fading {opacity: 1; transition: opacity 6s; } '
    s += '#fading.fade { opacity: 0; }'
    s += '</style>'
    document.write(s);
}


/*************************
 * Utilities for Origami *
 *************************/

// prefix fixing for Safari CSS animation
function fix(word) {
    return (isSafari ? '-webkit-' : '') + word;
}
// coordinate transform: (x, 100 - y) means [x,y]
function modify(pair) {
    return '' + pair[0] + '% ' + (100 - pair[1]) + '%';
}
// polygon with coordinate transforms
function polygon(points) {
    var poly = [];
    points.forEach(function (pair) {
        poly.push(modify(pair));
    });
    return 'polygon(' + poly.join(', ') + ');';
}
// polygon(0% 100%, 100% 100%, 100% 0%);


// compute a point vertically above
function above(point, h) {
    return [point[0], point[1] + h];
}

// compute a point horizontally right
function right(point, h) {
    return [point[0] + h, point[1]];
}

// compute a point reflected about a line of slope tan θ
// flip matrix:
//    [ cos 2θ   sin 2θ] [x] = [x cos 2θ + y sin 2θ]
//    [ sin 2θ  -cos 2θ] [y]   [x sin 2θ - y cos 2θ]
function reflect(point, angle) {
    var x = point[0], y = point[1];
    var c = cos(2 * angle), s = sin(2 * angle);
    return [x * c + y * s, x * s - y * c];
}

// compute points for a piece with angle
function polyPoints(angle) {
    var points = [];
    points.push([0,0]);
    points.push([100,0]);
    points.push([100, 100 * tan(angle)]); // y/100 = tan θ, so y = 100 * tan θ
    // the div dimensions will clip the shape if the third point is out of range.
    return points;
}
// compute points for a thin line with endpoints p and q, with above h and right w.
function polyLine(p, q, h, w) {
    h = h || 0; // default 0
    w = w || 0; // default 0
    if (w == 0) return [p, above(p,h), above(q,h), q];
    if (h == 0) return [p, right(p,w), right(q,w), q];
    return [p, right(above(p,h), w), right(above(q,h), w), q];
}
// construct a piece
function makePiece(points, color) {
    var piece = document.createElement('div');
    piece.className = 'piece';
    var poly = polygon(points);
    // alert('points: ' + points.toString() + '\npoly: ' + poly);
    piece.setAttribute('style', 'background-color: ' + color + '; ' + fix('clip-path: ') + poly);
    return piece;
}
// construct a dot at point (x,y)
function makeDot(x, y, color) {
    color = color || 'black'; // default black
    var span = document.createElement('span');
    span.className = 'txtfont';
    span.setAttribute('style', 'position:absolute; left: ' + x + '%; top: ' + (100 - y) + '%;');
    span.style.color = color;
    // &#9679;  = black circle
    // &#9899;  = medium black circle
    // &#11044; = black large circle
    span.innerHTML = '&#9679;';
    return span;
}
// construct a label at point (x,y)
function makeLabel(x, y, label) {
    var span = document.createElement('span');
    span.className = 'txtfont';
    span.setAttribute('style', 'position:absolute; left: ' + x + '%; top: ' + (100 - y) + '%;');
    span.innerHTML = label;
    return span;
}
// specify style with folding
// name of animation, color of background, duration, delay,
//         poly 100%, optional poly2 50%,
//   arg3: flag = true for poly2 0%
//   arg3: poly3 25% and 75%
function makeFold (name, color, duration, delay, poly1, poly2, arg3) {
    if (typeof arg3 == 'object') {
        var poly3 = arg3;
        document.write('<style>' +
        '.' + name + ' {' +
        ' animation-name:' + name + ';' +
        ' animation-duration:' + duration + 's;' +
        (delay == 0 ? '' : ' animation-delay: ' + delay + 's;') +
        ' animation-fill-mode:forwards;' +
        ' transform-origin:' + modify(poly1[0]) + ';' +
        ' }' +
        '@' + fix('keyframes ') + name + ' {' +
        '  0% {background-color: ' + color + '}' +
        ' 25% {' + fix('clip-path: ') + polygon(poly3) + '}' +
        ' 50% {' + fix('clip-path: ') + polygon(poly2) + '}' +
        ' 75% {' + fix('clip-path: ') + polygon(poly3) + '}' +
        '100% {' + fix('clip-path: ') + polygon(poly1) + '}' +
        ' }' +
        '</style>');
    }
    else {
        var flag = arg3 || false; // default false
        document.write('<style>' +
        '.' + name + ' {' +
        ' animation-name:' + name + ';' +
        ' animation-duration:' + duration + 's;' +
        (delay == 0 ? '' : ' animation-delay: ' + delay + 's;') +
        ' animation-fill-mode:forwards;' +
        ' transform-origin:' + modify(poly1[0]) + ';' +
        ' }' +
        '@' + fix('keyframes ') + name + ' {' +
        '  0% {background-color: ' + color + '}' +
        (poly2 && !flag ? '0% {' + fix('clip-path: ') + polygon(poly1) +'}' : '' ) + // Safari needs 0%
        (poly2 ? (' ' + (flag ? 0 : 50) + '% {' + fix('clip-path: ') + polygon(poly2) + '}') : '') +
        '100% {' + fix('clip-path: ') + polygon(poly1) +'}' +
        ' }' +
        '</style>');
    }
}
// specify style with fade-in.
// name of animation, duration, delay,
function makeFade (name, duration, delay) {
    document.write('<style>' +
    '.' + name + ' {' +
    ' animation-name:' + name + ';' +
    ' animation-duration:' + duration + 's;' +
    (delay == 0 ? '' : ' animation-delay: ' + delay + 's;') +
    ' animation-fill-mode:forwards;' + // to stay on
    ' }' +
    '@' + fix('keyframes ') + name + ' {' +
    '  0% {opacity: 0; }' +
    '100% {opacity: 1; }' +
    ' }' +
    '</style>');
}

/****************************
 * Utilities for Spirograph *
 ****************************/

/* original idea:
<spiro>
spiro R=115, r=46, d=41 end  (five-point star)
spiro R=13, r=-39, d=46 end  (heart)
wheel alpha=1, beta=6, gamma=14, a=160, b=160/2, c=160/3 end (5-fold symmetry)
</spiro>
*/

// Generate a random number within range
function getRandomNumber(start, end) {
    return start + Math.floor(Math.random() * (end - start))
}
// Generate a random color
function getRandomColor(start, end) {
    return "rgba("  + getRandomNumber(start, end)+ 
               ", " + getRandomNumber(start, end) +
               ", " + getRandomNumber(start, end)+", 1)"
}
// gcd of two numbers
function gcd(x, y) {
    x = Math.abs(x)
    y = Math.abs(y)
    var t
    while(y) {
       t = y
       y = x % y
       x = t
    }
    return x
}
// get style for multi-coloured path
function getRandomStyle(param) {
    var red = Math.round(Math.sin(param + 0) * 127 + 128)
    var green = Math.round(Math.sin(param + 2) * 127 + 128)
    var blue = Math.round(Math.sin(param + 4) * 127 + 128)
    return "rgb(" + red + "," + green + "," + blue + ")"
}


// plot spirograph (assume canvas context is cartesian)
function plotSpiroGraph(canvas, R, r, d, factor, style, callback) {
    style = style || 'multi' // default style is multi color 
    var ctx = canvas.getContext('2d') // assume cartesian, (0,0) at center
    var toRadian = Math.PI / 180;  // convert angle from degree to radian
    var beta = (R - r) / r; // for new version

    // none of these parameters can be zero
    // var R = eval(document.getElementById("spiro").value) || 55
    // var r = eval(document.getElementById("wheel").value) || 40
    // var d = eval(document.getElementById("pen").value) || 8

    // compute how many rounds
    var n = Math.abs(r) / gcd(R, r) | 0;  // integer division
    // n = 1;
    // deduce f-fold symmetry
    var f = Math.abs(R) / gcd(R, r) | 0;  // integer division
    
    /* Draw n rounds, with differnt colors
    var s = R - r, t, x, y;
    for (var j = 0; j < 2; j++) {
        ctx.beginPath();
        ctx.strokeStyle = getRandomColor(65, 210)
        for (var theta = 0; theta <= 360; theta += 1) {
            t = theta * toRadian + j * 2 * Math.PI; // to radian
            x = s * Math.cos(t) + d * Math.cos(beta * t);
            y = s * Math.sin(t) - d * Math.sin(beta * t);
            ctx.lineTo(x, y);
        }
        ctx.stroke();
        ctx.closePath();
    }
    */

    /* Draw n rounds, all at once */
    // ctx.beginPath();
    ctx.strokeStyle = style

    // draw n rounds
    var s = R - r, t, x, y
    /* break up the for loop by requestAnimateFrame
    ctx.moveTo(s + d, 0); // for theta = 0, then t = 0
    for (var theta = 1; theta <= n * 360; theta += 1) {
        t = theta * toRadian; // to radian
        x = s * Math.cos(t) + d * Math.cos(beta * t);
        y = s * Math.sin(t) - d * Math.sin(beta * t);
        ctx.lineTo(x, y);
    }
    */

    /* The for loop has only one parameter: theta, from 1 to n * 360,
       The ideal or standard browser frame rate is 60 frames per second.
       Use theta += 1 as the for loop to see how fast or slow this is.
     */
    var theta = 1, limit = n * 360, speed = 2 // for loop parameters
    s = s * factor, d = d * factor // enlarge spiro parameters (s, d) by factor
    x = s + d, y = 0 // initial state (x,y)
    requestAnimationFrame(animate) // start first frame of next function

    function animate() {
        for (var j = 0; j < speed; j++) {
            ctx.beginPath()
            ctx.moveTo(x, y)
            t = theta * toRadian // to radian
            x = s * Math.cos(t) + d * Math.cos(beta * t)
            y = s * Math.sin(t) - d * Math.sin(beta * t)
            ctx.lineTo(x, y)
            if (style == 'multi') ctx.strokeStyle = getRandomStyle(t / n)
            ctx.stroke()
            ctx.closePath()
            theta += 1
        }
        if (theta < limit) requestAnimationFrame(animate) // call itself
        else callback()
    }

    // ctx.stroke();
    // ctx.closePath();
}

/**
 A Panel is like the frame, having:
 * a title bar
 * an information bar
 * a board, square piece for paper folding
 * a button, for Animate/Reset.
 */

// construct a Panel
function Panel (mode, params, w, h) {
    w = w || size;  // default size, even 0 is size.
    h = h || size;  // default size, even 0 is size.
    // create the DOM elements for Panel
    var panel = document.createElement('div');
    panel.className = 'frame';
    var title = document.createElement('div');
    title.className = 'title';
    var info = document.createElement('div');
    info.className = 'info';
    //info.innerHTML = '&copy; 2021, based on MathsIsFun and MarkDeep.';
    title.innerHTML = 'Panel Title';
    info.innerHTML = '&copy; 2021, using CSS animation by keyframes.';
    var button = document.createElement('button');
    button.className = 'btn';
    // override some style of btn
    button.setAttribute('style', 'z-index:2; position:absolute; right:3px; bottom:3px;');
    // pass title, info and button to board
    var args = {
        title: title,
        info: info,
        button: button
    };
    var board = new Board(args, mode, params, w, h);

    // link up elements
    panel.appendChild(title);
    panel.appendChild(info);
    panel.appendChild(board);
    panel.appendChild(button);

    return panel;
}

// Pieces Manager
function Pieces () {
    // store pieces
    var pieces = [];
    // put in a piece
    this.put = function (p) {
        pieces.push(p);
        return p;
    }
    // clear all pieces
    this.clear = function () {
        pieces.forEach(function (p) {
            if (p.parentNode) p.parentNode.removeChild(p);
        });
    }
}

// Timers Manager
function Timers () {
    // store timers
    var timers = [];
    // put in a timer
    this.put = function (wait, action) {
        var t = setTimeout(action, wait * 1000); // convert to milliseconds
        timers.push(t);
        return t;
    }
    // clear all timers
    this.clear = function () {
        timers.forEach(function (t) {
            clearTimeout(t);
        });
    }
}

// A static identifier generator, for CSS.
var ID = {
    seed: 1,  // seed value
    next: function() {
        return this.seed++;
    }
};
// first time: ID.seed    gives 1.
// first call: ID.next()  gives 1.
// second call: ID.next() gives 2, etc.

// construct the Board
function Board (args, mode, params, w, h) {
    // attributes
    // alert('Board params: ' + params)
    mode = mode || 'pythag'; // default pythag
    // angle = angle || 75;   // default 75 degree, even 0 is 75
    // angle = toRadian(angle); // convert to radian for trigonometry calls

    // create the DOM elements for scene
    var board = document.createElement('div');
    board.className = 'board';
    board.setAttribute('style', 'position:absolute; left:50px; top:60px;');

    // handle arguments
    doModes(mode, params);

    /* ----------------------------------------------------- *
       All handling routines.
     * ----------------------------------------------------- */

    function doModes(mode, params) {
        // animation based on mode
        switch(mode) {
        case 'square': doSquare(board, size, args, makePiece, makeLabel, makeFold, makeFade); break;
        case 'spiro': doSpiro(board, size, args, makeLabel, makeFade, getRandomNumber, getRandomColor, plotSpiroGraph); break;
        default : doPythag(); break; // mode: pythag
        }
    }

    /* ----------------------------------------------------- *
       All drawing modes.
     * ----------------------------------------------------- */


    /* Note: color names:
       https://www.w3schools.com/colors/colors_names.asp
     */

    /* ----------------------------------------------------- *
       Pythagoras flip

    0,0                         100,0
     ............................
     |        B        K = flip of A about BC, collinear: A H K
     |   20,10 *    H
     |       a |    .c
     |         |      0  .
     |   20,50 *..........70,50
   0,100      A      b       C 100,100

      a = 50 - 10 = 40,
      b = 70 - 20 = 50,
      c = sqrt(40^2 + 50^2) = 64.0312423743
      small angle θ = arctan(a/b) = arctan(4/5) = 0.674740942 rad = 38.6598083 degrees
      height from c = h = a cos θ, intersect c at H.
      x-coordinate of H = h sin θ + 20 = a cos θ sin θ + 20
                        = 20 + 40 cos(arctan(4/5)) sin(arctan(4/5)) = 39.512195122
      y-coordinate of H = 50 - h cos θ = 50 - a cos θ cos θ
                        = 50 - 40 cos(arctan(4/5)) cos(arctan(4/5)) = 25.6097560976
      a = 40, b = 50, tan θ = a/b, sin θ = a/sqrt(a^2 + b^2), cos θ = b/sqrt(a^2 + b^2)
      h = a cos θ,
      H at (a cos θ sin θ, a cos θ cos θ) = (a/2 sin 2θ,  a/2 (1 + cos 2θ))
      H: ( 20 + a * ab/(a^2 + b^2), 50 + a * b^2/(a^2 + b^2) ) for [a * 20/41, a * 25/41]
      K is the flip of H about hypotenuse BC, so A H K are collinear, and AH = HK,
      therefore, AK = 2 * AH, or K = [20 + 2 h.x, 50 + 2 h.y] for [a * 40/41, a * 50/41]
      flip matrix about a line making angle φ = π - θ to x-axis:
           [ cos 2φ   sin 2φ] = [ cos 2θ  -sin 2θ]
           [ sin 2φ  -cos 2φ]   [-sin 2θ  -cos 2θ]
      With φ = π - θ, this is:
      cos(2(π - θ)) = cos(2π - 2θ) = cos 2θ = cos(2(arctan(4/5))) = 0.21951219512 = 9/41
      sin(2(π - θ)) = sin(2π - 2θ) = - sin 2θ = - sin(2(arctan(4/5))) = -0.97560975609 = - 40/41
      cos 2θ = cos(2(atan(a/b))) = (b^2 - a^2)/(a^2 + b^2) = (25 - 16)/(25 + 16) = 9/41
      sin 2θ = sin(2(atan(a/b))) = 2ab/(a^2 + b^2) = 2(4)(5)/(25 + 16) = 40/41
      [[9/41, -40/41], [-40/41, -9/41]] . [x,y] = [(9x - 40y)/41, -(40x + 9y)/41]
      [[9/41, -40/41], [-40/41, -9/41]] . [a * 20/41, a * 25/41] = [-20a/41, -25a/41]

    flip y-axis: φ = π/2, so [[0, 1], [1, 0]] . [x, y] = [y, x]
    flip x-axis: φ = 0, so [[1,0],[0,-1]] . [x, y] = [x, -y]
    [[1,0],[0,-1]] . [20,10] = [20,-10]
    [[1,0],[0,-1]] . [ 0,-40] = [0,40]
    transform-origin: 20% 40%; // default: 50% 50% 0
     * ----------------------------------------------------- */

    // for mode: pythag
    function doPythag() {
        var A = [20 +  0, 50 +  0]; // the right-angle vertex [0,0]
        var B = [20 +  0, 50 + 40]; // the top vertex [0, a]
        var C = [20 + 50, 50 +  0]; // the right vertex [b, 0]
        var H = [20 + 40 * 20/41, 50 + 40 * 25/41];
        // H: 39.51219512195122,74.39024390243902
        var K = [20 + 2 * 40 * 20/41, 50 + 2 * 40 * 25/41];
        // K: 59.02439024390244, 1.2195121951219505
        // polygon([B, A, C]) = polygon(20% 10%, 20% 50%, 70% 50%);
        // polygon([B, A, H]) = polygon(20% 10%, 20% 50%, 39.51219512195122% 25.609756097560975%);
        // polygon([H, A, C]) = polygon(39.51219512195122% 25.609756097560975%, 20% 50%, 70% 50%);
        // polygon([B, K, C]) = polygon(20% 10%, 59.02439024390244% 1.2195121951219505%, 70% 50%);
        // alert('poly: ' + polygon([B, K, C]));

        // make the piece and link it up
        // with only 3 pieces, no need for a manager
        var piece1 = makePiece([B, A, C], 'hotpink');
        var piece2 = makePiece([A, H, B], 'darkviolet');
        var piece3 = makePiece([A, H, C], 'darkturquoise');
        board.appendChild(piece1); // hypotenuse flip
        board.appendChild(piece2); // vertical flip
        board.appendChild(piece3); // horizontal flip

        // add style with flip
        // piece1: BAC -> BKC
        makeFold('flip', 'hotpink', 3, 2, [B, K, C]);
        // piece2: AHB -> AH'B
        makeFold('flip-y', 'darkviolet', 3, 0, [A, [20 - 40 * 20/41, 50 + 40 * 25/41], B]);
        // piece3: AHC -> AH''C
        makeFold('flip-x', 'darkturquoise', 3, 0, [A, [20 + 40 * 20/41, 50 - 40 * 25/41], C]);

        // set up animation
        args.title.innerHTML = 'Pythagoras Theorem by Folding';
        args.info.innerHTML = 'The fan-out triangles are similar.';
        args.button.innerHTML = 'Animate';
        args.button.onclick = doRun;

        // start animation
        function doRun () {
            // alert('doRun!');
            piece1.classList.add('flip');
            piece2.classList.add('flip-y');
            piece3.classList.add('flip-x');
            args.button.innerHTML = 'Reset';
            args.button.onclick = doReset;
        }
        // reset animation
        function doReset () {
            // alert('doReset!');
            piece1.classList.remove('flip');
            piece2.classList.remove('flip-y');
            piece3.classList.remove('flip-x');
            args.button.innerHTML = 'Animate';
            args.button.onclick = doRun;
        }
    } // doPythag

    /* ----------------------------------------------------- *
       Square flip

  0,100                               100,100
     .................................
     |        B            C
     |   20,70 *-----------* 70,70
     |         |           |
     |       a |     M     |
     |         |           |
     |   20,30 *-----------* 70,30
     |         A      b    D 
   0,0.................................100,0

      a = 50 - 10 = 40,
      b = 70 - 20 = 50,
      offset: (20 + x, 30 + y)
      without offset:
      A: (0,0), B:(0,a), C:(b,a), D:(b,0)
      M = middle-center of rectangle: (b/2,a/2)
      M1 = flip of M about BD = (b/2,a + a/2)
      M2 = flip of M about BA = (-b/2,a/2)

    flip y-axis: φ = π/2, so [[0, 1], [1, 0]] . [x, y] = [y, x]
    flip x-axis: φ = 0, so [[1,0],[0,-1]] . [x, y] = [x, -y]
    [[1,0],[0,-1]] . [20,10] = [20,-10]
    [[1,0],[0,-1]] . [ 0,-40] = [0,40]
    transform-origin: 20% 40%; // default: 50% 50% 0
     * ----------------------------------------------------- */

    return board;
}

// transform the node
function transform(node) {
    var source = node.innerHTML;
    // parse the source, determine mode and up to 5 numbers
    var list = source.parse('wnnnnn');
    if (list.length < 2) {
        alert('Invalid syntax: ' + source + '\nIgnored!');
        list = ['pyth', 75]; // default values
    }
    var mode = list.shift()
    // alert('mode: ' + mode + ':' + (typeof mode) + ', ' + list + ':' + (typeof list));
    return new Panel(mode, list);
}

// process all animate nodes
function processAnimate() {
   // Collect all nodes that will receive origami processing
   var origamiNodeArray = toArray(document.getElementsByClassName('animate'))
                  .concat(toArray(document.getElementsByTagName('animate')));

   // Process all nodes, replacing them as we progress
   origamiNodeArray.forEach(function (node) {
       node.parentNode.replaceChild(transform(node), node);
   });
}

// Start from here.
loadAnimateStyle(); // load the style CSS for frame, canvas, title, info, button
processAnimate();   // find and process all <animate> nodes

})(); // invoke single function

/** End of Animate Script processing.
 ** Author: Joseph Chan
 ** Date: 25 May, 2024.
 */
