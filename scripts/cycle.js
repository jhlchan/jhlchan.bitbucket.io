/**
 Cycle script

 Ideas taken from:
 * Markdeep: https://casual-effects.com/markdeep/latest/markdeep.min.js
 */

/**

 Documentation:

 Use a Panel for <cycle>:
 * a DIV with position:relative, width and height.
 * have a Title on top.
 * have a Board with canvas.
 * have canvas to plot cycles, draw objects.

 */

/**
 Common Global Functions
*/
// convert list to an array
function toArray(list) {
    return Array.prototype.slice.call(list);
}

/**
Cycle, using the same idea.
 * set up stylesheet for Board and elements,
 * find all nodes of cycle class or tag,
 * parse the node content and generate a Board node,
 * embed the Board node inside a Panel node with title,
 * replace each <cycle> node by a Panel node.
 */

// single function application for parser
(function() {
'use strict';
/**
 A simple string parser.
 Syntax: parse(ch), where ch is a char indicator:
 * L for list of numbers :signed decimals, can be '(3,-4,1)', or '3,-4,1'
 * N for number
 * B for boolean
 * Q for quote string
 */
String.prototype.parse = function (ch) {
    switch (ch) {
    case 'L': // parse a list
        if (this.length == 0) return [];
        // extract number strings: optional sign, then digits, optional dot digits
        var nums = this.match(/[-+]?\d+(\.\d+)?/g); // group by signed decimals
        // '1,2.5,-3'.match(/[-+]?\d+(\.\d+)?/g) gives [ '1', '2.5', '-3' ]
        return nums.map(Number); //  ['1','9'].map(Number);  gives [1, 9]
    case 'N': // parse a number
        return Number(this);
    case 'B': // parse a boolean: yes or no
        return this.toLowerCase().charAt(0) == 'y';
    case 'Q': // parse a quote
        var quote = this.charAt(0);
        return this.slice(1, this.indexOf(quote, 1));
    default: return this; // no change
    }
}
// convert string to JSON object
String.prototype.toObject = function() {
    // split this string into words, cleanup whitespaces.
    var words = this.match(/\b([\w\.]+)\b/g);
    // remember indices pointing to a word that begins with an alphabet
    var alpha = [];
    for (var j = 0; j < words.length; j++) {
         if (words[j].match(/^[A-Za-z]/)) alpha.push(j);
    }
    // form the JSON string
    var s = [];
    while (alpha.length > 0) {
        var j = alpha.shift(); // alpha[0] = alpha.peek()
        var k = alpha.length > 0 ? alpha[0] : words.length;
        if (k == j+1) { // two consecutive words
            s.push('"' + words[j] + '":"' + (k < words.length ? words[k] : 'null') + '"');
            alpha.shift(); // discard index k
        }
        else if (k == j+2) { // a word and a number
            s.push('"' + words[j] + '":' + words[j+1]);
        }
        else { // a word and more numbers
            s.push('"' + words[j] + '":[' + words.slice(j+1, k).join(',') + ']');
        }
    }
    s = '{ ' + s.join(', ') + ' }'; // the JSON string
    // alert('toObject: ' + this + '\n' + s);
    return JSON.parse(s); // object of JSON string
}
})(); // invoke single function

/* ------------------------------------- *
 * Cycle Processing                      *
 * ------------------------------------- */

// single function application for <cycle>
(function() {
'use strict';

// Constants
var size = 300;

// load style sheet for cycle
// for .title and .info, remove: pointer-events: none;
function loadCycleStyle() {
    var width = 660; // 660 px instead of 360px
    var s = '';
    s += '<style>'
    // s += '.frame { position:relative; width:360px; height:280px; border: 1px solid blue; border-radius: 9px; margin:auto; display:block;}'
    s += '.frame { position:relative; width:'+(2*size+60)+'px; height:'+(size+80)+'px; border: 1px solid blue; border-radius: 9px; margin:auto; display:block;}'
    s += '.board { position:relative; width:'+(2*size)+'px; height:'+size+'px; border:1px solid coral; display:block;}'
    s += '.piece { position:absolute; width:100%; height:100%; top:0%; left:0%; }'
    s += '.txtfont { font: 12pt arial; } '
    s += '.frame table { border-collapse:collapse; line-height:140%; page-break-inside:avoid; } '
    s += '.frame th { color:#FFF; background-color:#AAA; border:1px solid #888; padding:8px 15px 8px 15px; } '
    s += '.frame td { border:1px solid #888; padding:5px 15px 5px 15px; } '
    s += '.frame tr:nth-child(even) { background:#EEE; } '
    s += '.title { font: 12pt arial; font-weight: bold; position:absolute; top:10px; left:0px; width:'+width+'px; text-align:center;}'
    s += '.info { font: 10pt arial; font-weight: bold; color: #6600cc; position:absolute; top:31px; left:0px; width:300px; text-align:left;}'
    s += '.btn { display: inline-block; position: relative; text-align: center; margin: 2px; text-decoration: none; font: bold 14px/25px Arial, sans-serif; color: #268; border: 1px solid #88aaff; border-radius: 10px;cursor: pointer; background: linear-gradient(to top right, rgba(170,190,255,1) 0%, rgba(255,255,255,1) 100%); outline-style:none;}'
    s += '.btn:hover { background: linear-gradient(to top, rgba(255,255,0,1) 0%, rgba(255,255,255,1) 100%); }'
    s += '.noborder td {border: none}'
    s += '</style>'
    document.write(s);
}

/**
 A Panel is like the frame, having:
 * a title bar
 * a board, square piece for plotting
 */

// construct a Panel
function Panel (args, w, h) {
    w = w || size;  // default size, even 0 is size.
    h = h || size;  // default size, even 0 is size.
    // create the DOM elements for Panel
    var panel = document.createElement('div');
    panel.className = 'frame';
    var title = document.createElement('div');
    title.className = 'title';
    title.innerHTML = args.param ||'Panel Title'; // default title
    args.panel = panel;
    args.title = title;

    var board = new Board(args, w, h);

    // panel will have:
    //          title
    //          board
    // link up elements
    panel.appendChild(title);
    panel.appendChild(board);

    return panel;
}

// construct the Board
function Board (args, w, h) {

    // create the DOM elements for scene
    var board = document.createElement('div');
    board.className = 'board';
    // board.setAttribute('style', 'position:absolute; left:80px; top:60px;');
    board.setAttribute('style', 'position:absolute; left:30px; top:60px;');

    // handle arguments
    var canvas = document.createElement('canvas');
    doModes(args.mode, args.lines);
    board.appendChild(canvas);

    /* ----------------------------------------------------- *
       All handling routines.
     * ----------------------------------------------------- */

    function doModes(mode, lines) {
        // cycle based on mode
        switch(mode) {
        case 'basic' : BasicCycle(canvas, lines); break;
        case 'cyclic' : CyclicCycle(canvas, lines, args); break;
        case 'random' : RandomNumber(canvas, lines, args); break;
        default : SampleCycle(canvas, lines); break;
        }
    }

    // final return
    return board; // the board
}

/* ------------------------------------- *
 * Sample Cycle                          *
 * ------------------------------------- */

// Sample cycle
// * just a random sample to show gradient on canvas.
function SampleCycle(canvas, lines) {
    // sample ignore lines

    // A sample canvas with gradient
    var ctx = canvas.getContext("2d");
    // Create gradient
    var grd = ctx.createLinearGradient(0, 0, 200, 0);
    grd.addColorStop(0, "red");
    grd.addColorStop(1, "white");
    // Fill with gradient
    ctx.fillStyle = grd;
    ctx.fillRect(10, 10, 150, 80);
} // end of SampleCycle

/* ------------------------------------- *
 * Basic Cycle                           *
 * ------------------------------------- */

// Basic cycle
// * demo of drawing circle and polygen
function BasicCycle(canvas, lines) {
    // should parse the following from lines
    var ratio = 0.8; // ratio for radius
    var numberOfSides = 7; // cycle period

    // set canvas dimensions
    canvas.width = size;
    canvas.height = size;
    // good for debugging
    canvas.style.border = "1px solid #88aaff";
    // get context
    var ctx = canvas.getContext("2d");

    // center and radius (size = 300)
    var center = {x: size/2, y: size/2}; // at middle
    var radius = (size/2)* ratio; // max radius is size/2

    // local abbreviations
    var pi = Math.PI, cos = Math.cos, sin = Math.sin;

    // Draw circle
    // circle = arc covering angle 2 * pi
    ctx.beginPath();
    // arc(x,y,r,startangle,endangle
    ctx.arc(center.x, center.y, radius, 0, 2 * pi);
    ctx.strokeStyle = 'black';
    ctx.lineWidth = 1;
    ctx.stroke();

    // Drawing regular polygon
    var start = -pi/2; // start vertex at top, angle is clockwise
    var increment = 2*pi/numberOfSides; // increment angle
    ctx.beginPath();
    // initial moveTo
    ctx.moveTo(center.x +  radius * cos(start),
               center.y +  radius * sin(start));
    // subsequent lineTo
    for (var j = 1; j <= numberOfSides; j++) {
        ctx.lineTo(center.x + radius * cos(start + j * increment),
                   center.y + radius * sin(start + j * increment));
    }
    ctx.strokeStyle = 'blue';
    ctx.lineWidth = 2;
    ctx.stroke();

    // add labels
    ctx.font = 'bold 15px Arial';
    ctx.fillStyle = 'green';
    ctx.fillText("Hello", 100, 100);
    ctx.fillText("Bye", 60, 50);
    drawVertex(ctx, 50, 100, 'red');
    drawLabel(ctx, 50,100, "C = 5");

    function drawVertex(ctx, x, y, color, r){
        var pointSize = r || 3; // Change according to the size of the point.
        ctx.beginPath(); //Start path
        ctx.arc(x, y, pointSize, 0, 2 * pi, true); // Draw small circle, true = counterclockwise, default false = clockwise
        ctx.save(); // save and restore later
        ctx.fillStyle = color;
        ctx.fill(); // Close the path and fill.
        ctx.restore();
    }

    function drawLabel(ctx, x, y, txt) {
       var w = ctx.measureText(txt).width;
       ctx.fillText(txt, x - w/2, y);
    }
} // end of BasicCycle

/* ------------------------------------- *
 * Cyclic Cycle                          *
 * ------------------------------------- */

// Example syntax of lines:
// poly 7
// path (0,2,4,6,0,1,3,5,0) ; indexes
// radius 0.8 ; optional max = 1
// speed 0.7  ; optional max = 1
// wait 0.05  ; optional in seconds
// thick 2    ; optional
// type add | type power ; optional
// mark no  | mark yes   ; optional
// can add vcolor, lcolor, button text, etc.
function parseCyclic(lines) {
    var debug = false; // set true to debug
    var poly = 0;
    var path = [];
    var radius = 0.8, speed = 0.7, wait = 0.05, thick = 2, type = 'none', mark = 'no'; // defaults
    // parse lines for Difference engine simulation
    lines.forEach(function(line) {
        // var parts = line.split(' ');
        // split parts by blanks, but keep blanks within quotes (see above)
        var parts = line.match(/(?:[^\s"]+|"[^"]*")+/g);
        // all with at least 2 parts
        if (parts.length > 1) {
            var key = parts.shift();
            if (debug) db.add(key);
            //db.add(parts);
            switch (key) {
            case 'poly' : // cycle n
               poly = parts[0].parse('N');
               break;
            case 'path' : // a list of indexes
               path = parts[0].parse('L');
               break;
            case 'radius' : // radius fraction
               radius = parts[0].parse('N');
               break;
            case 'speed' : // speed fraction
               speed = parts[0].parse('N');
               break;
            case 'wait' : // wait seconds
               wait = parts[0].parse('N');
               break;
            case 'thick' : // thick value
               thick = parts[0].parse('N');
               break;
            case 'type' : // type name
               type = parts[0] || 'none';
               break;
            case 'mark' : // mark boolean
               mark = parts[0].parse('B');
               break;
            default: alert('Cyclic - Invalid key: ' + key);
            }
            if (debug) db.add('!!!');
        }
    });

    // verify configuration
    var err = 0;
    if (poly != (poly | 0)) err++;
    if (path.length == 0) err++;
    // check all indexes are valid
    path.forEach(function(n) { if (!(n == (n|0) && n >= 0 && n < poly)) err++; });
    if (!(radius > 0 && radius <= 1)) err++;
    if (!(speed > 0 && speed <= 1)) err++;
    if (isNaN(wait) || wait < 0) err++
    if (thick != (thick | 0)) err++;
    if (err !== 0) { alert('Wrong syntax for Cyclic: ' + lines); return null; }
    // return the configuration
    return {poly: poly,
            path: path,
            radius: radius,
            speed: speed,
            wait: wait,
            thick: thick,
            type: type,
            mark: mark};
}

// superscript static object
var Super = new function() {
    // Unicode superscripts
    // https://en.wikipedia.org/wiki/Unicode_subscripts_and_superscripts
    var sup = ['\u2070', // <sup>0</sup>
               '\u00b9', // <sup>1</sup>
               '\u00b2', // <sup>2</sup>
               '\u00b3', // <sup>3</sup>
               '\u2074', // <sup>4</sup>
               '\u2075', // <sup>5</sup>
               '\u2076', // <sup>6</sup>
               '\u2077', // <sup>7</sup>
               '\u2078', // <sup>8</sup>
               '\u2079'];// <sup>9</sup>
    // get superscript
    this.get = function(n) {
       var s = []; // as queue, use shift and unshift.
       do {
          s.unshift(sup[n % 10]); // javascript remainer
          n = (n/10) | 0; // javascript quotient
       } while (n !== 0);
       return s.join('');
    }
}
// Cyclic cycle
// * for cyclic walk and hop
function CyclicCycle(canvas, lines, args) {
    // default configuration
    var numberOfSides = 7; // cycle period
    var indexes = [0,1,2,3,4,5,6,0]; // path
    // var indexes = [0,2,4,6,0,1,3,5,0]; // path
    var ratio = 0.8; // ratio for radius
    var speed = 0.7; // 1 = max speed (0.2)
    var wait = 50; // in milliseconds (100)
    var thick = 2; // thickness (6)
    var type = 'none'; // no type
    var mark = true; // have mark labels
    // parse lines for Cyclic configuration
    try {
        var config = parseCyclic(lines);
        numberOfSides = config.poly;
        indexes = config.path;
        ratio = config.radius;
        speed = config.speed;
        wait = config.wait * 1000; // convert to millsecond
        thick = config.thick;
        type = config.type;
        mark = config.mark;
    } catch (err) {
        alert('Cyclie Lines parse error: ' + err + '\nUse default configuartion.');
    }

    // set canvas dimensions
    canvas.width = size * 2; // for long list of numbers!
    canvas.height = size;
    // good for debugging
    // canvas.style.border = "1px solid #88aaff";

    // center and radius (size = 300)
    var center = {x: canvas.width/2, y: canvas.height/2}; // at middle
    var radius = (size/2)* ratio; // max radius is size/2

    // local abbreviations
    var pi = Math.PI, cos = Math.cos, sin = Math.sin;

    // set the button
    var button = document.createElement('button');
    button.className = 'btn';
    // override some style of btn
    button.setAttribute('style', 'z-index:2; position:absolute; right:3px; bottom:3px;');
    button.innerHTML = 'Draw Again';
    button.onclick = animate; // works!
    args.panel.appendChild(button); // button is outside

    // setup and animate
    var ctx = canvas.getContext("2d");
    ctx.lineWidth = thick;
    // ctx.fillStyle = 'white'; // default is black
    ctx.fillStyle = window.getComputedStyle(document.body)['background-color'];
    // for labels
    ctx.font = 'bold 15px Arial';

    var points = getPolygon(center, radius, numberOfSides);
    var locals = getPolygon(center, radius + 15, numberOfSides);
    for (var j = 0; j < numberOfSides; j++) {
       points[j].index = j; // later labels[j];
       points[j].local = locals[j]; // can discard locals
    }
    animate();

    // ----------------------------------
    // Utilities
    // ----------------------------------

    // points on regular polygon of n sides
    // around center c, on circle or radius r
    function getPolygon(c, r, n) {
       var points = [];
       var delta = 2*pi/n;
       var angle = -pi/2; // angle 0 is horizontal
       for (var j = 0; j < n; j++) {
          points.push({x: c.x + r * cos(angle + j*delta),
                       y: c.y + r * sin(angle + j*delta)});
       }
       return points;
    }

    // make a path from a list and indexes
    function makePath(list, indexes) {
       return indexes.map((j) => { return list[j]; });
    }

    // draw a point at p with radius r, color c
    function drawDot(p, r, c) {
      r = r || 2; // default radius 2
      c = c || 'red'; // default red
      ctx.save();
      ctx.beginPath();
      ctx.arc(p.x, p.y, r, 0, 2*pi);
      ctx.fillStyle = c;
      ctx.fill();
      ctx.restore();
    }

    // draw a line from p to q with color c
    function drawLine(p, q, c) {
      c = c || 'blue'; // default blue
      ctx.save();
      ctx.beginPath();
      ctx.lineCap = "round"; // line with rounded end caps
      ctx.lineJoin = "round"; // rounded corner when two lines meet
      ctx.moveTo(p.x, p.y);
      ctx.lineTo(q.x, q.y);
      ctx.closePath();
      ctx.strokeStyle = c;
      ctx.stroke();
      ctx.restore();
    }

    // draw circle = arc covering angle 2 * pi
    function drawCycle(c, r, color, width) {
      color = color || 'blank'; // default is black
      width = width || 1; // default 1
      ctx.save();
      ctx.beginPath();
      // arc(x,y,r,startangle,endangle
      ctx.arc(c.x, c.y, r, 0, 2*pi);
      ctx.lineWidth = width;
      ctx.strokeStyle = color;
      ctx.stroke();
      ctx.restore();
    }

    // draw a label txt at p, with color c
    function drawLabel(txt, p, c) {
       var w = ctx.measureText(txt).width;
       c = c || 'blue'; // default blue
       ctx.save();
       ctx.fillStyle = c;
       ctx.fillText(txt, p.x - w/2, p.y + 5);
       ctx.restore();
    }

    // ----------------------------------
    // Animation
    // ----------------------------------

    var timer; // for moveAlong

    // animation
    function animate() {
      // clear canvas
      ctx.fillRect(0, 0, canvas.width, canvas.height);
      clearInterval(timer);
      drawCycle(center, radius);
      points.forEach((p) => {
         drawDot(p, 2*thick);
         drawLabel('' + p.index, p.local);
      });
      walk(makePath(points, indexes));
    }

    // walk along a list of points
    function walk(points) {
       /* Idea:
        * as long as there are two points in the list,
        * get the first two points p, q.
        * compute an intermediate point t between p, q, by speed.
        * draw the short line from p to t.
        * update p by t, set timer to compute next t.
        * when t is close enough to q, stop timer.
        * throw out first point, repeat walking the rest.
        */
      if (points.length < 2) {
         // final label after last
         if (type == 'add') {
            var h = (numberOfSides/2 | 0);
            var k = numberOfSides - h;
            drawLabel(h + ' + ' + k + ' \u2261 0', // \equiv
                      pointAdd(center, 0, 40), 'violet');
         }
         if (mark && type == 'power')
            drawLabel('\u2261 x' + Super.get(numberOfSides), // \equiv
                      pointAdd(points[0].local, 50, 0), 'green');
         if (!mark && type == 'power')
            drawLabel('The ' + indexes.slice(0,-1).length +
                      ' numbers are: ' + indexes.slice(0,-1) + '.',
                      pointAdd(center, 0, 136), 'green');
         return; // no more walk
      }
      var p, q, c;
      p = points.shift(); // first point, points is shorter
      q = points[0]; // second point at front
      c = 'blue'; // default color
      // for type == 'power'
      p.label = 'x' + Super.get(numberOfSides - points.length);
      // if (type == 'add' && p.index >= 3) c = 'green'; // correct!
      if (type == 'add' && p.index >= (numberOfSides/2 | 0)) c = 'green';
      // waiting for type add
      if (type == 'add' && p.index == (numberOfSides/2 | 0)) {
         drawLabel('Return to starting 0, how?',
                   pointAdd(center, 0, -20), 'violet');
         drawLabel('Turn back, or ...', center, 'violet');
         var id = setTimeout(function() {
                clearTimeout(id);
                drawLabel('Keep walking!', pointAdd(center, 0, 20), 'violet');
                moveAlong(p, q, c, function() { walk(points); });
             }, 2000); // wait 2 seconds
      }
      else {
         moveAlong(p, q, c, function() { walk(points); });
      }
      // use callback rather than direct recursion
      // walk(points)
    }

    // move along from point p to point q with color c
    function moveAlong(p, q, c, callback) {
       // drawLine(p, q); // direct, no timer
       var dx = (q.x - p.x) * speed;
       var dy = (q.y - p.y) * speed;
       var t1, t2; // two moving points
       var k = 0;
       t1 = p;
       timer = setInterval(proceed, wait);
       // draw label for type power
       if (mark && type == 'power') {
          var less = p.local.x < p.x
          var label = less ? (p.label + ' \u2261') : ('\u2261 ' + p.label);
          var off = less ? -20 : 20;
          drawLabel(label, pointAdd(p.local, off, 0), 'green');
       }
       // continuation
       function proceed() {
          if (eqPoint(t1, q)) {
             clearInterval(timer);
             return callback();
          }
          t2 = pointAdd(t1, dx, dy);
          k++;
          if (k * speed >= 1) t2 = q; // prevent overshooting
          drawLine(t1, t2, c);
          t1 = t2;
       }
    }

    // check if two points are equal
    function eqPoint(p, q) {
       return (p.x == q.x && p.y == q.y)
    }

    // compute a point with increment
    function pointAdd(p, dx, dy) {
       return {x: p.x + dx, y: p.y + dy}
    }

} // end of CyclicCycle

/* ------------------------------------- *
 * Random Number                         *
 * ------------------------------------- */

/*

Linear congruential generator
https://en.wikipedia.org/wiki/Linear_congruential_generator
A linear congruential generator (LCG) is an algorithm that yields a sequence of pseudo-randomized numbers calculated with a discontinuous piecewise linear equation. The method represents one of the oldest and best-known pseudorandom number generator algorithms.
The generator is defined by the recurrence relation: X_{n+1} = (a X_{n} + c) mod m.
There are three common families of parameter choice:
* m prime, c = 0 (Lehmer random number generator, published in 1951)
The period is m−1 if the multiplier a is chosen to be a primitive element of the integers modulo m. Seed must be nonzero.
* m a power of 2, c = 0
This allows the modulus operation to be computed by simply truncating the binary representation. There are, however, disadvantages. This form has maximal period m/4, achieved if a ≡ 3 or a ≡ 5 (mod 8). The initial state X0 must be odd, and the low three bits of X alternate between two states and are not useful.
* c ≠ 0 (first published in 1958 by W. E. Thomson and A. Rotenberg.)
When c ≠ 0, correctly chosen parameters allow a period equal to m, for all seed values. This will occur if and only if:
1. m and c are relatively prime,
2. a − 1 is divisible by all prime factors of m,
3. a - 1 is divisible by 4 if m is divisible by 4.
These three requirements are referred to as the Hull–Dobell Theorem.

Parameters in common use:
gcc library: m = 2^{31}, a = 1103515245, c = 12345.
Turbo Pascal: m = 2^{32}, a = 134775813, c = 1.

$ node
> a = []; a[0] = 7;
> for (var j = 0; j < 10; j++) { a[j+1] = (5 * a[j] + 3) % 100; }
> a
[ 7, 38, 93, 68, 43, 18, 93, 68, 43, 18, 93 ]
> a.join(',')
'7,38,93,68,43,18,93,68,43,18,93'
> function lcg(a,c,m,s,k) {var j = 0, v=[]; v[0] = s; for (; j < k; j++) { v[j+1] = (a * v[j] + c) % m; }; return v.join(',');}
> lcg(5,3,100,7,20);
'7,38,93,68,43,18,93,68,43,18,93,68,43,18,93,68,43,18,93,68,43'
> lcg(7,1,100,3,20);
'3,22,55,86,3,22,55,86,3,22,55,86,3,22,55,86,3,22,55,86,3'
> lcg(7,1,97,3,20);
'3,22,58,19,37,66,75,41,94,77,55,95,84,7,50,60,33,38,73,27,93'
> function lcg(a,c,m,s,k) {var j = 0, x = s, v='' + x; for (; j < k; j++) { y = (a * x + c) % m; v += ',' + y; x = y; }; return v;}
> lcg(11,7,100,3,20);
'3,40,47,24,71,88,75,32,59,56,23,60,67,44,91,8,95,52,79,76,43'
> lcg(11,7,100,3,30);
'3,40,47,24,71,88,75,32,59,56,23,60,67,44,91,8,95,52,79,76,43,80,87,64,11,28,15,72,99,96,63'
> lcg(11,7,100,2,30);
'2,29,26,93,30,37,14,61,78,65,22,49,46,13,50,57,34,81,98,85,42,69,66,33,70,77,54,1,18,5,62'
> lcg(11,1,100,13,30);
'13,44,85,36,97,68,49,40,41,52,73,4,45,96,57,28,9,0,1,12,33,64,5,56,17,88,69,60,61,72,93'
> lcg(11,1,100,17,30);
'17,88,69,60,61,72,93,24,65,16,77,48,29,20,21,32,53,84,25,76,37,8,89,80,81,92,13,44,85,36,97'

Note that 88 + 1 = 89, a prime, so GF(89) is a finite field.
Let p be a prime, then C_p is a finite field, with C*_p a cyclic group with (p-1) elements.
Any number a coprime to (p-1) is a generator of the cyclic group?

Note that 8 + 1 = 9, a prime power, so GF(9) is a finite field.
Actually, the strip uses only A to G, H is for the true/false lamp.
Note that 7 + 1 = 8, a prime power, so GF(8) is a finite field.
F a b D G b D c E G E c D a G c D F E b G F a E F c G E  (length = 28, a b c are false signals)

How to find a generator of a cyclic group?
https://math.stackexchange.com/questions/786452/how-to-find-a-generator-of-a-cyclic-group
Criterion: An element 𝑔 of multiplicative group of order (𝑝−1) in ℤ/𝑝ℤ with prime 𝑝 is a generator, iff for each prime factor 𝑞 in the factorization of 𝑝−1, g^((p-1)/q) <> 1 holds.

Every cyclic group is isomorphic to either ℤ or ℤ/𝑛ℤ if it is infinite or finite. If it is infinite, it'll have generators ±1. If it is finite of order 𝑛, any element of the group with order relatively prime to 𝑛 is a generator. The number of relatively prime numbers can be computed via the Euler Phi Function 𝜙(𝑛).

*/

/* Random Number Generator as an object */
function Random(multiplier, constant, modulus, seed) {
    // X_{n+1} = (a X_{n} + c) mod m.
    // attributes
    var a = multiplier || 21; // default a = 21
    var c = constant || 1;   // default c = 1
    var m = modulus || 100;  // default m = 100
    var value = seed || 17;  // default seed = 17
    // set the value
    this.set = function(seed) {
       value = seed;
    }
    // get the value
    this.get = function() {
       return value;
    }
    // change the parameters
    this.change = function(multiplier, constant, modulus) {
       if (multiplier == (multiplier|0)) a = multiplier;
       if (constant == (constant|0)) c = constant;
       if (modulus == (modulus|0)) m = modulus;
    }
    // string representation
    this.toString = function() {
       return 'y = (' + a + 'x' + (c == 0 ? '' : ' + ' + c) + ') mod ' + m;
    }
    // next value
    this.next = function() {
       value = (a * value + c) % m;
       return value;
    }
    // explain next value (but not getting it)
    this.explain = function() {
       var text = [];
       text.push('old number is: ' + value);
       text.push('multiply by ' + a + ':' + (a * value));
       if (c !== 0) text.push('adding ' + c + ':' + (a * value + c));
       if (m == 100) {
           // special hack for modulus = 100
          text.push('last two digits give new number:' + ((a * value + c) % m));
       }
       else {
          text.push('divide by ' + m + ', remainder becomes:new number');
          text.push('new number is:' + ((a * value + c) % m));
       }
       return text;
    }
    return this;
}

// Example syntax of lines:
// param (11,1,100) ; (a,c,m) for y = (a * x + c) mod m
// seed 17  ; starting value, optional
// wait 0.5 ; in seconds, optional
function parseRandom(lines) {
    var param = [], grid = [];
    var seed = 1, wait = 0.05; // defaults
    // parse lines for Difference engine simulation
    lines.forEach(function(line) {
        // var parts = line.split(' ');
        // split parts by blanks, but keep blanks within quotes (see above)
        var parts = line.match(/(?:[^\s"]+|"[^"]*")+/g);
        // all with at least 2 parts
        if (parts.length > 1) {
            var key = parts.shift();
            switch (key) {
            case 'param' : // a list of parameters
               param = parts[0].parse('L');
               break;
            case 'seed' : // the seed value, whole number
               seed = parts[0].parse('N');
               break;
            case 'wait' : // wait seconds
               wait = parts[0].parse('N');
               break;
            default: alert('Random - Invalid key: ' + key);
            }
        }
    });

    // verify configuration
    var err = 0;
    if (param.length !== 3) err++;
    if (seed != (seed | 0)) err++; // seed whole number
    // check all parameters are valid: positive whole numbers
    param.forEach(function(n) { if (!(n == (n|0) && n >= 0)) err++; });
    if (isNaN(wait) || wait < 0) err++
    if (err !== 0) { alert('Wrong syntax for Random: ' + lines); return null; }
    // return the configuration
    return {param: param,
            seed: seed,
            wait: wait};
}

// range in pure JavaScript
function range(start, end) {
   return (new Array(end - start + 1)).fill(undefined).map((_, i) => i + start);
}

// Random Number
// * for random number display
function RandomNumber(canvas, lines, args) {
    // default configuration
    var multiplier = 11;
    var constant = 1;
    var modulus = 100;
    var seed = 17;
    var wait = 500; // in milliseconds
    // parse lines for Random configuration
    try {
        var config = parseRandom(lines);
        multiplier = config.param[0];
        constant = config.param[1];
        modulus = config.param[2];
        seed = config.seed;
        wait = config.wait * 1000; // convert to millsecond
    } catch (err) {
        alert('Random Lines parse error: ' + err + '\nUse default configuartion.');
    }
    var cols = 10; // number of columns (fixed)
    var rows = Math.ceil(modulus / cols); // number of rows

    // make Random object
    var random = new Random(multiplier, constant, modulus, seed);
    var boxSize = size/cols; // set box size
    // say('Generator: ' + random);

    // set canvas dimensions (size = 300)
    canvas.width = size;
    canvas.height = size;
    canvas.setAttribute('style', 'position:absolute;top:0px;left:0px;');
    // good for debugging
    canvas.style.border = "1px solid #88aaff";

    // setup button for Restart/New/Done
    var button = document.createElement('button');
    button.className = 'btn';
    // override some style for button
    button.setAttribute('style', 'z-index:2; position:absolute; right:3px; bottom:3px;');
    button.innerHTML = 'Restart';
    button.onclick = doRestart;
    args.panel.appendChild(button); // button is outside

    // setup explanation
    var explain = document.createElement('div');
    explain.className = 'piece';
    // override some style for explain
    explain.setAttribute('style', 'top:60px;left:340px;width:290px;height:300px;');
    explain.innerHTML = 'Explain';
    args.panel.appendChild(explain); // explain is outside

    // setup button for Run/Pause
    var run = document.createElement('button');
    run.className = 'btn';
    // override some style for run
    run.setAttribute('style', 'z-index:2; position:absolute;right:100px; bottom:3px;');
    run.innerHTML = 'Pause';
    args.panel.appendChild(run); // run is outside

    // setup parameter configuration
    var piece = document.createElement('div');
    piece.className = 'piece';
    piece.setAttribute('style', 'z-index:2;left:10px;top:100px;width:300px;height:180px;background-color:#bbff22;border:2px solid #73AD21;border-radius:25px;padding:20px;text-align:right;');
    setupParameter(piece);
    args.panel.appendChild(piece); // piece is outside
    piece.style['display'] = 'none'; // hide it

    // setup and animate
    var ctx = canvas.getContext("2d");
    ctx.fillStyle = 'rgba(225,225,225,0.5)'; // not entirely black
    ctx.font = 'bold 15px Arial'; // for labels

    var cells = getGrid(cols, rows);
    // canvas.addEventListener('click', handleClick);
    animate();

    // ----------------------------------
    // Utilities
    // ----------------------------------

    // draw a label txt at p, with color c
    function drawLabel(txt, p, c) {
       var w = ctx.measureText(txt).width;
       c = c || 'blue'; // default blue
       ctx.save();
       ctx.fillStyle = c;
       ctx.fillText(txt, p.x - w/2, p.y + 5);
       ctx.restore();
    }

    // a grid of cells, with p columns, q rows
    function getGrid(p, q) {
       var cells = [];
       for (var row = 0; row < q; row++) {
          for (var column = 0; column < p; column++) {
             cells.push({x: column, y: row, id: (column + row * p) % modulus,
                         p: {x: column * boxSize + boxSize/2, y: row * boxSize + boxSize/2}});
          }
       }
       return cells;
    }

    // get cell identifier by coordinates x and y
    function getId(x, y) {
       return (x + y * cols) % modulus;
    }

    // update id of cells when modulus is changed
    function updateId(cells) {
       cells.forEach((item, i) => {
          item.id = i % modulus;
       });

    }

    // mark a cell with color
    function markCell(cell, color) {
       color = color || "palegreen" // default green
       ctx.save();
       ctx.fillStyle = color;
       ctx.fillRect(cell.x * boxSize, cell.y * boxSize, boxSize, boxSize);
       drawLabel('' + cell.id, cell.p);
       // cell.mark = true;
       ctx.restore();
    }

    // draw a grid of p columns, q rows
    function drawGrid(cells) {
       ctx.save();
       ctx.beginPath();
       ctx.lineWidth = 2;
       ctx.strokeStyle = 'black';
       ctx.fillStyle = "white";
       cells.forEach((item, i) => {
          ctx.rect(item.x * boxSize, item.y * boxSize, boxSize, boxSize);
          ctx.fill();
          ctx.stroke();
       });
       ctx.closePath();
       ctx.restore();
    }

    // show and clear all cells
    function showGrid(cells) {
       cells.forEach((item, i) => {
          // item.mark = false;
          drawLabel('' + item.id, item.p);
       });
    }

    // handle mouse click event on canvas
    function handleClick(e) {
       // mouse coordinates: e.offsetX, e.offsetY
       var x = Math.floor(e.offsetX / boxSize);
       var y = Math.floor(e.offsetY / boxSize);
       var cell = cells[getId(x, y)];
       markCell(cell);
       args.title.innerHTML = 'Cell clicked: ' + cell.id;
    }

    // say something for debugging
    function say(message) {
       args.title.innerHTML = message;
    }
    // format an explanation into a table
    function format(explain) {
       var table = [];
       table.push('<table>');
       explain.forEach((line) => {
          var parts = line.split(':');
          table.push('<tr><td align="left">' + parts[0] + '</td><td align="right">' + parts[1] + '</td></tr>')
       });
       table.push('</table>');
       table.push('<small>Click grid (any cell) for next number</small>')
       return table.join('');
    }

    // ----------------------------------
    // Parameter Selector
    // ----------------------------------

    // setup param selector
    function setupParameter(piece) {
        // first parameter: multiplier
        var piece1 = makePiece('Choose a multiplier: ');
        var param1 = makeParam(range(2,99), multiplier);
        piece1.appendChild(param1);
        param1.onchange = () => multiplier = Number(param1.value);
        // second parameter: constant
        var piece2 = makePiece('then adding: ');
        var param2 = makeParam(range(0,99), constant);
        piece2.appendChild(param2);
        param2.onchange = () => constant = Number(param2.value);
        // third parameter: modulus
        var piece3 = makePiece('for remainder, divide by: ');
        var param3 = makeParam(range(7,100), modulus);
        piece3.appendChild(param3);
        param3.onchange = () => modulus = Number(param3.value);
        // the Done button
        var button = document.createElement('button');
        button.className = 'btn';
        button.innerHTML = 'Done';
        // append all pieces
        piece.appendChild(makePiece('<b>Random Number Generator</b><p>', 'text-align:center'));
        piece.appendChild(piece1);
        piece.appendChild(piece2);
        piece.appendChild(piece3);
        piece.appendChild(makePiece('<p>'));
        piece.appendChild(button);
        // Done action
        button.onclick = function() {
           piece.style['display'] = 'none';
           doRestart(true);
        };

        // make a piece
        function makePiece(text, style) {
            var piece = document.createElement('div');
            piece.innerHTML = text;
            if (style) piece.setAttribute('style', style);
            return piece;
        }
        // make a parameter
        function makeParam(options, select) {
           select = select || 0; // default is 0
           var param = document.createElement('select');
           param.setAttribute('style','width:50px;text-align:right;font-size:16px;');
           /* no good!
           param.onfocus = function() { this.size = 10; }
           param.onblur = function() { this.size = 1; }
           param.onchange = function() { this.size = 1; this.blur(); }
           */
           var opt;
           options.forEach((item, i) => {
              opt = document.createElement("option");
              opt.text = item;
              param.add(opt);
           });
           param.selectedIndex = options.indexOf(select); // index count from 0
           return param;
        }
    }

    // ----------------------------------
    // Animation
    // ----------------------------------

    var timer; // for setInterval and clearInterval
    var count; // for period

    // animation, start with seed
    function animate() {
       // clear canvas
       ctx.clearRect(0, 0, canvas.width, canvas.height);
       drawGrid(cells);
       random.set(seed); // seed can be changed by pickSeed
       count = 0;
       stepping();
    }

    // stepping through generator
    function stepping() {
       markCell(cells[random.get()]);
       count++;
       explain.style['display'] = 'block';
       explain.innerHTML = 'Current number is ' + random.get() +
                           '<p><small>Click the grid (any cell) for next number.</small>';
       canvas.addEventListener('click', doStep);
       button.style['display'] = 'block'; // show Reset
       run.style['display'] = 'block'; // show Run
       run.innerHTML = 'Run';
       run.onclick = doRun;
    }

    // do one step
    function doStep() {
       markCell(cells[random.get()], 'yellow');
       if (explain.style['display'] == 'block') {
          explain.innerHTML = format(random.explain());
       }
       var next = random.next();
       if (next == seed) { // abort
           clearInterval(timer);
           explain.style['display'] = 'block';
           explain.innerHTML = 'Numbers repeat!</br>period = ' + count +
                               '<p>for generator with formula:<p>' +
                               random.toString().replace('mod', 'in cycle');
           run.style['display'] = 'none'; // hide Run
       }
       else {
          markCell(cells[next]);
          count++;
       }
    }

    // do all steps
    function doRun() {
       canvas.removeEventListener('click', doStep);
       // use a timer with wait, rather than relying on browser refresh rate
       timer = setInterval(doStep, wait);
       explain.style['display'] = 'none';
       run.innerHTML = 'Pause';
       run.onclick = function() {
          clearInterval(timer);
          stepping();
       };
    }

    // do Restart, picking seed or New
    function doRestart(flag) {
       clearInterval(timer);
       // clear canvas
       ctx.clearRect(0, 0, canvas.width, canvas.height);
       if (flag) { // new changes
           random.change(multiplier, constant, modulus);
           // update title
           say('<b>Random Numbers less than ' + modulus + '</b>');
           // say('Generator: ' + random);
           // re-establish the cells
           rows = Math.ceil(modulus / cols);
           cells = getGrid(cols, rows);
       }
       drawGrid(cells);
       showGrid(cells);
       run.style['display'] = 'none'; // hide Run
       // change Restart to New
       button.style['display'] = 'block'; // show Button
       button.innerHTML = 'New';
       button.onclick = getParameters;
       explain.style['display'] = 'block'; // show explanation
       explain.innerHTML = 'Pick a number to start' +
                           '<p>The generator uses this formula:<p>' +
                           random.toString().replace('mod', 'in cycle') +
                           '<p>where x is the old number,' +
                           '</br>and y is the new number.' +
                           '<p><small>Press "New" to change formula.</small>';
       canvas.removeEventListener('click', doStep);
       canvas.addEventListener('click', pickSeed);
    }

    // pick a seed from mouse click, then animate
    function pickSeed(e) {
       canvas.removeEventListener('click', pickSeed);
       // mouse coordinates: e.offsetX, e.offsetY
       var x = Math.floor(e.offsetX / boxSize);
       var y = Math.floor(e.offsetY / boxSize);
       seed = getId(x, y);
       // change New back to Restart
       button.innerHTML = 'Restart';
       button.onclick = doRestart;
       animate();
    }

    // get parameters for a new generator
    function getParameters() {
       canvas.removeEventListener('click', pickSeed);
       button.style['display'] = 'none'; // hide New
       piece.style['display'] = 'block'; // show Selector
    }
} // end of RandomNumber


/* ------------------------------------- *
 * Node transformation and processing    *
 * ------------------------------------- */

// transform the node
function transform(node) {
    var source = node.innerHTML;
    // alert('source: ' + source + '!');
    // minimal processing
    var lines = [];
    source.split('\n').forEach(function(line) {
        // remove comments after ;
        if (line && line.indexOf(';') !== -1) line = line.slice(0,line.indexOf(';'));
        // remove blank lines
        if (line && line.length !== 0) lines.push(line);
    });
    // alert('filtered lines: ' + lines + ', count: ' + lines.length);
    // first line is mode, with optional param separate by one space
    var first = lines.shift().match(/\w+|\s.*/g);
    // match either pattern: \w+ a word, \s.* blank then anything
    var args =
       {mode: first.length > 0 ? first[0] : '',           // start word
        param: first.length > 1 ? first[1].slice(1) : '', // no leading blank
        lines: lines }
    return new Panel(args);
}

// process all <cycle> nodes
function processCycle() {
   // Collect all nodes that will receive <cycle> processing
   // Process all nodes, replacing them as we progress
   toArray(document.getElementsByClassName('cycle'))
       .concat(toArray(document.getElementsByTagName('cycle')))
       .forEach(function (node) {
           node.parentNode.replaceChild(transform(node), node);
       });
}

// Start from here.
loadCycleStyle(); // load the style CSS for panel, board, title, info, button
processCycle();   // find and process all <cycle> nodes

})(); // invoke single function

/** End of Cycle Script processing.
 ** Author: Joseph Chan
 ** Date: 22 January, 2023.
 */
