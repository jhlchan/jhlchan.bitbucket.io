/**
 Fourier script

 Ideas taken from:
 * Markdeep: https://casual-effects.com/markdeep/latest/markdeep.min.js
 *
 * Understanding JavaScript's requestAnimationFrame() method for smooth animations
 * Updated: Nov 1st, 2017
 * http://www.javascriptkit.com/javatutors/requestanimationframe.shtml
 */

/**

 Documentation:

 Use a Canvas for <fourier>:
 * have a Button near bottom to start/top animation
 * use requestAnimationFrame to start animation
 * use cancelAnimationFrame to stop animation

 to investigate:

HTML canvas getImageData() Method
https://www.w3schools.com/tags/canvas_getimagedata.asp
put the image data back onto the canvas with putImageData().

Foreground/Background Demo
https://jsfiddle.net/gtTNR/3/

new path2D object
https://developer.mozilla.org/en-US/docs/Web/API/Path2D
Browser compatibility looks good.

 */

/**
 Common Global Functions
*/
// convert list to an array (JavaScript arrow syntax)
toArray = (list) => Array.prototype.slice.call(list);

/**
 Fourier, using the same idea as MarkDeep.
 * set up stylesheet for Fourier elements,
 * find all nodes of Fourier class or tag,
 * parse the node content and generate a Canvas node,
 * replace each <fourier> node by Canvas node.
 */

// single function application for parser
(function() {
'use strict';
/**
 A simple string parser.
 Syntax: parse(ch), where ch is a char indicator:
 * L for list of numbers :signed decimals, can be '(3,-4,1)', or '3,-4,1'
 * N for number
 * Q for quote string
 */
String.prototype.parse = function (ch) {
    switch (ch) {
    case 'L': // parse a list
        if (this.length == 0) return [];
        // extract number strings: optional sign, then digits, optional dot digits
        var nums = this.match(/[-+]?\d+(\.\d+)?/g); // group by signed decimals
        // '1,2.5,-3'.match(/[-+]?\d+(\.\d+)?/g) gives [ '1', '2.5', '-3' ]
        return nums.map(Number); //  ['1','9'].map(Number);  gives [1, 9]
    case 'N': // parse a number
        return Number(this);
    case 'Q': // parse a quote
        var quote = this.charAt(0);
        return this.slice(1, this.indexOf(quote, 1));
    default: return this; // no change
    }
}
// convert string to JSON object
String.prototype.toObject = function() {
    // split this string into words, cleanup whitespaces.
    var words = this.match(/\b([\w\.]+)\b/g);
    // remember indices pointing to a word that begins with an alphabet
    var alpha = [];
    for (var j = 0; j < words.length; j++) {
         if (words[j].match(/^[A-Za-z]/)) alpha.push(j);
    }
    // form the JSON string
    var s = [];
    while (alpha.length > 0) {
        var j = alpha.shift(); // alpha[0] = alpha.peek()
        var k = alpha.length > 0 ? alpha[0] : words.length;
        if (k == j+1) { // two consecutive words
            s.push('"' + words[j] + '":"' + (k < words.length ? words[k] : 'null') + '"');
            alpha.shift(); // discard index k
        }
        else if (k == j+2) { // a word and a number
            s.push('"' + words[j] + '":' + words[j+1]);
        }
        else { // a word and more numbers
            s.push('"' + words[j] + '":[' + words.slice(j+1, k).join(',') + ']');
        }
    }
    s = '{ ' + s.join(', ') + ' }'; // the JSON string
    // alert('toObject: ' + this + '\n' + s);
    return JSON.parse(s); // object of JSON string
}
// fallback for requestAnimationFrame
window.requestAnimationFrame = window.requestAnimationFrame
	|| window.mozRequestAnimationFrame
	|| window.webkitRequestAnimationFrame
	|| window.msRequestAnimationFrame
	|| function(f){return setTimeout(f, 1000/60)} // simulate 60 frames in 1 second = 1000 milliseconds
// fallback for cancelAnimationFrame
window.cancelAnimationFrame = window.cancelAnimationFrame
	|| window.mozCancelAnimationFrame
	|| function(requestID){clearTimeout(requestID)}

})(); // invoke single function

/* ------------------------------------- *
 * Fourier Processing                    *
 * ------------------------------------- */

// single function application for <fourier>
(function() {
'use strict';

// load style sheet for Fourier
function loadFourierStyle() {
    let s = ''; // class attributes should avoid width/height, or positions: left/right, or top/bottom.
    s += '<style>'  // uncomment zborder to check positioning.
    s += '.panel {position:relative; border: 1px solid blue; border-radius: 9px; margin:auto; display:block; background-color:white}'
    s += '.title {font: 12pt arial; font-weight: bold; top:10px; left:0px; text-align:center; margin-top:5px}'
    s += '.canvas {zborder:1px solid violet;}'
    s += '.button {display: inline-block; position: relative; text-align: center; margin: 2px; text-decoration: none; font: bold 14px/25px Arial, sans-serif; color: #268; border: 1px solid #88aaff; border-radius: 10px;cursor: pointer; background: linear-gradient(to top right, rgba(170,190,255,1) 0%, rgba(255,255,255,1) 100%); outline-style:none;}'
    s += '.button:hover {background: linear-gradient(to top, rgba(255,255,0,1) 0%, rgba(255,255,255,1) 100%);}'
    s += '</style>'
    document.write(s);
}

// global constants
const PI = Math.PI;
const cos = function (angle) { return Math.cos(angle/180 * PI); };
const sin = function (angle) { return Math.sin(angle/180 * PI); };

/* ------------------------------------- *
 * Extension of 2D context of Canvas     *
 * ------------------------------------- */

// draw a dot at p, with color and size
CanvasRenderingContext2D.prototype.dot = function (p, color, size) {
    color = color || 'red'; // default 'red'
    size = (typeof size  == 'number') ? size : 5; // default 5, allow 0.
    this.save();
    this.fillStyle = color;
    this.beginPath();
    this.arc(p.x, p.y, size, 0, 2 * PI);
    this.fill();
    this.restore();
};
// draw a line from p to q, with color and width
CanvasRenderingContext2D.prototype.line = function (p, q, color, width) {
    color = color || 'black'; // default 'black'
    width = width || 1; // default 1, even 0 is 1.
    this.save();
    this.strokeStyle = color;
    this.lineWidth = width;
    this.beginPath();
    this.moveTo(p.x, p.y);
    this.lineTo(q.x, q.y);
    this.stroke();
    this.restore();
};
// draw a arrow from p to q with color, width, and tip size
CanvasRenderingContext2D.prototype.arrow = function (p, q, color, width, size) {
    color = color || 'blue'; // default 'blue'
    width = width || 2; // default 5, even 0 is 2.
    size = size || 20; // default arrow head size = 20

    //variables to be used when creating the arrow
    var fromx = p.x, fromy = p.y;
    var tox = q.x, toy = q.y;
    var angle = Math.atan2(toy-fromy,tox-fromx);
    var tipx = tox - size * Math.cos(angle-PI/7);
    var tipy = toy - size * Math.sin(angle-PI/7);
    var tiph = tox - size * Math.cos(angle+PI/7);
    var tipk = toy - size * Math.sin(angle+PI/7);

    this.save();
    this.strokeStyle = color;
    this.lineWidth = width;
    this.fillStyle = color;

    //starting path of the arrow from the start to end
    //the line
    this.beginPath();
    this.moveTo(fromx, fromy);
    this.lineTo(tox, toy);
    this.stroke();

    //starting a new path from the head of the arrow to one of the sides of
    //the arrowhead
    this.beginPath();
    this.moveTo(tox, toy);
    this.lineTo(tipx, tipy);
    //path from the side point of the arrow, to the other side point
    this.lineTo(tiph, tipk);
    //path from the side point back to the tip of the arrow, and then
    //back to starting point
    this.closePath();
    //draws the paths created above
    this.stroke();
    this.fill();
    this.restore();
};
// draw a circle at center with radius, color and width
CanvasRenderingContext2D.prototype.circle = function (center, radius, color, width) {
    color = color || 'black'; // default 'black'
    width = width || 1; // default 1, even 0 is 1.
    this.save();
    this.strokeStyle = color;
    this.lineWidth = width;
    this.beginPath();
    this.arc(center.x, center.y, radius, 0, 2 * PI); // default: false = clockwise
    this.stroke();
    this.restore();
};
// draw a label at p with text, offset x and y and font.
CanvasRenderingContext2D.prototype.label = function (p, text, x, y, font) {
    x = (x == null) ? -15 : x; // default x offset, accept 0
    y = (y == null) ? 15 : y;  // default y offset, accept 0
    font = font || "16px Georgia"; // default font
    let w = this.measureText(text).width;
    this.save();
    this.font = font;
    this.fillText(text, p.x - w/2 + x, p.y - y);
    this.restore();
};
// add two point p, q, taking center c
function addPoints(c, p, q) {
    // x = c.x + (p.x - c.x) + (q.x - c.x)
    // y = c.y + (p.y - c.y) + (q.y - c.y)
    return {x: p.x + q.x - c.x, y: p.y + q.y - c.y};
}

/**
 A Panel has:
 * a title
 * a canvas
 * a button
 */

// construct a Panel
function Panel (args) {
    // create the DOM elements for Panel
    let title = document.createElement('div');
    title.className = 'title';
    title.innerHTML = args.param; // no default title, null if no param
    // title.width = 200; // no effect on actual width, need style width in px
    title.setAttribute('style', 'width:500px; zborder: 1px solid green;');

    let canvas = document.createElement('canvas');
    canvas.className = 'canvas';
    canvas.width = 800;  // in px, affects style
    canvas.height = 300; // in px, affects style

    let button = document.createElement('button');
    button.className = 'button';
    // override some style of button for positioning
    button.setAttribute('style', 'position:absolute; right:30px; bottom:10px;');

    let panel = document.createElement('div');
    panel.className = 'panel';
    // set DIV dimensions, assign numbers to panel.width or panel.height has no effect.
    panel.style.width = (canvas.width + 20) + 'px'; // a string with px, not a number
    panel.style.height = (canvas.height + 20) + 'px'; // a string with px, not a number

    /* As far as width and height goes, the following works!
       This works a bit (need more with z-index and background-colors)
       for 'canvas' and 'button', etc.
       Some DOM elements has direct width and height, others simply don't,
       But don't use programming tricks!!!
    let panel = document.createElement('button');
    panel.className = 'panel';
    panel.width = 820;
    panel.height = 320;
    */
    // put components in panel
    panel.appendChild(title);
    panel.appendChild(canvas);
    panel.appendChild(button);

    // handle args
    doModes(args.mode, args.lines);

    /* ----------------------------------------------------- *
       All handling routines.
     * ----------------------------------------------------- */
    function doModes(mode, lines) {
        // chart based on mode
        switch(mode) {
        case 'plot' : PlotPoly(title, canvas, button, lines); break;
        case 'separate' : SeparateWaves(title, canvas, button, lines); break;
        case 'combine' : CombineWaves(title, canvas, button, lines); break;
        case 'mixed' : MixedWaves(title, canvas, button, lines); break;
        case 'basic' : // fall to default
        default : BasicWave(title, canvas, button, lines); break;
        }
    }
    // final return
    return panel;
}

/* ------------------------------------- *
 * Parsing Plots                         *
 * ------------------------------------- */

// Syntax of lines:
// poly ([list of coefficients, highest first, comma separated]) [optional color]
// xspan ([xmin],[xmax]) [optional units in x]
// yspan ([ymin],[ymax]) [optional units in y]
// point ([x],[y]) xoff yoff [optional color]
//
// example:
// poly (1,4,3)    ; y = x^2 + 4x + 3
// xspan (-2,2) 1  ; from -2 to 2, marks in 1 units
// yspan (-5,15) 5 ; from -5 to 15, marks in 5 units
// point (0,3) 10 0 red; a point (0,3), offset 10 0, color red.
function parsePlots(lines) {
    let poly = [], // a list of polynomial coefficients
        xspan = [-10,10], xmark = 1, // default x from -10 to 10, interval 1
        yspan = [-10,10], ymark = 1, // default y from -10 to 10, interval 1
        point = []; // a list of points.
    // parse lines for wave simulation
    lines.forEach(function(line) {
        // var parts = line.split(' ');
        // split parts by blanks, but keep blanks within quotes (see above)
        let parts = line.match(/(?:[^\s"]+|"[^"]*")+/g);
        // all with at least 2 parts
        if (parts.length > 1) {
            var key = parts.shift();
            switch (key) {
            case 'poly' : poly.push({h: parts[0].parse('L'), // a list of poly in coefficients
                                     k: parts[1] || 'black'}); // default curve color: black.
               break;
            case 'xspan' : xspan = parts[0].parse('L'); // min and max
                           xmark = (parts[1] || '1').parse('N'); // interval
                break;
            case 'yspan' : yspan = parts[0].parse('L'); // min and max
                           ymark = (parts[1] || '1').parse('N'); // interval
                break;
            case 'point' : point.push({d: parts[0].parse('L'), // a list of points, or dot (x,y)
                                       x: Number(parts[1] || '0'), // default x offset 0
                                       y: Number(parts[2] || '0'), // default y offset 0
                                       c: parts[3] || 'black', // default dot color: black
                                      });
                break;
            default: // alert('Plots - Invalid key: ' + key);
            }
        }
    });

    // verify configuration
    let err = 0;
    // if (poly.length == 0) err++; // can be no poly
    // if (point.length == 0) err++; // can be no points
    poly.forEach(function(item) {
        item.h.forEach((c) => { if (isNaN(c)) err++; });
    });
    xspan.forEach((c) => { if (isNaN(c)) err++; });
    yspan.forEach((c) => { if (isNaN(c)) err++; });
    if (isNaN(xmark) || isNaN(ymark)) err++;
    // origin (0,0) is within xspan and yspan
    if (xspan[0] > 0 || yspan[0] > 0) err++;
    point.forEach(function(item) {
        item.d.forEach((c) => { if (isNaN(c)) err++; });
        if (isNaN(item.x) || isNaN(item.y)) err++;
    });
    if (err !== 0) { alert('Wrong syntax for Plots: ' + lines); return null; }
    // return the configuration
    return {poly: poly, xspan: xspan, yspan: yspan, xmark: xmark, ymark: ymark, point: point};
} // end of parsePlots

// debug in FireFox:
// console.log(JSON.stringify(parsePlots(['poly (1,4,3)'])));
// {"poly":[{"h":[1,4,3],"k":"black"}],"xspan":[-10,10],"yspan":[-10,10],"xmark":1,"ymark":1,"point":[]}

// get a proper plot configuration
function getPlotConfig(lines) {
    var config = null;
    try { // parse lines for configuration
        config = parsePlots(lines);
    } catch (err) {
        alert('Plot Lines parse error: ' + err + '\nUse default waves.');
    }
    // use default configuration
    if (config == null) config = parsePlots(['poly (1,4,3)']);

    return config;
}

/* ------------------------------------- *
 * Plot a Polynomial                     *
 * ------------------------------------- */

// Polynomial Plot
function PlotPoly(title, canvas, button, lines) {
    title.innerHTML = title.innerHTML || 'Plotting Polynomials';
    const ctx = canvas.getContext("2d");

    let config = getPlotConfig(lines);
    // obtain from configuration
    const poly = config.poly;
    const xmin = config.xspan[0], xmax = config.xspan[1];
    const ymin = config.yspan[0], ymax = config.yspan[1];
    const xmark = config.xmark, ymark = config.ymark;
    const dots = config.point;

    // hide button
    button.innerHTML = 'Hello';
    button.style.display = 'none';
    // modify canvas dimension
    canvas.width = 500;  // in px, affects style
    canvas.height = 500; // in px, affects style
    // canvas.style.border = '1px solid violet';
    // modify panel dimension
    canvas.parentNode.style.width = (canvas.width + 20) + 'px'; // a string with px, not a number
    canvas.parentNode.style.height = (canvas.height + 30) + 'px'; // a string with px, not a number

    /* The layout is:

             canvas.width
       +---------------------------------+
       |                                 |
       |        ^ ymax                   |
       |        |                        |
       |        |                        | canvas.height
       |        |                        |
       |        |                        |
       | -------+--------------------->  |
       | xmin   0                   xmax |
       |        |ymin                    |
       +---------------------------------+

       So, divide width into h blocks, h = canvas.width/(xmax - xmin)
       and divide height into k blocks, k = canvas.height/(ymax - ymin)
       The origin oo at (0 - xmin) * h, (ymax - 0) * k.
       Use a factor 31/32 for slight adjustment, otherwise at boundary,
    */

    // define positions
    const width = canvas.width * 31/32;
    const height = canvas.height * 31/32;
    const oo = {x: width * xmin/(xmin - xmax),
                y: height * ymax/(ymax - ymin)};
    const ot = {x: oo.x, y: height/100};
    const ob = {x: oo.x, y: height};
    const ol = {x: width/100, y: oo.y};
    const or = {x: width, y: oo.y};
    const xscale = (or.x - ol.x) / (xmax - xmin);
    const yscale = (ob.y - ot.y) / (ymax - ymin);
    // alert('xscale, yscale: ' + xscale + ', ' + yscale);
    // xspan -5,5, yspan -1,20 gives  xscale, yscale: 49, 17.857142857142858

    // polynomial values by Horner's method
    // ((0 * x + a) x + b) x + c = a x^2 + b x + c = c + b x + a x^2
    function horner(coeff, x) {
        let y = 0;
        coeff.forEach((item, i) => {
            y = y * x + item;
        });
        return y;
    }

    // clear and draw both axes
    ctx.clearRect(0, 0, canvas.width, canvas.height); // new frame
    ctx.arrow(ob, ot, 'grey', 1, 10);
    ctx.arrow(ol, or, 'grey', 1, 10);
    ctx.label(or, 'x', 2, 10);
    ctx.label(ot, 'y', 10, -12);
    // plot mark lines
    for (let x = xmin; x <= xmax; x += xmark) {
        ctx.line({x:oo.x + x * xscale, y:ob.y}, {x:oo.x + x * xscale, y:ot.y}, 'grey', 0.2);
    }
    for (let y = ymin; y <= ymax; y += ymark) {
        ctx.line({x:ol.x, y:oo.y - y * yscale}, {x:or.x, y:oo.y - y * yscale}, 'grey', 0.2);
    }
    // plot polynomials
    poly.forEach((item, i) => {
        ctx.save();
        ctx.strokeStyle = item.k;
        ctx.beginPath();
        ctx.moveTo(ol.x, ol.y - yscale * horner(item.h, (ol.x - oo.x)/xscale));
        for (let x = ol.x; x <= or.x; x++) {
            ctx.lineTo(x, ol.y - yscale * horner(item.h, (x - oo.x)/xscale));
        }
        ctx.stroke();
        ctx.restore();
    });
    // plot points
    dots.forEach((item, i) => {
        let p = {x:oo.x + item.d[0] * xscale, y:oo.y - item.d[1] * yscale};
        ctx.dot(p, item.c);
        ctx.label(p, '(' + item.d[0] + ',' + item.d[1] + ')', item.x, item.y);
    });

} // end of PlotPoly

/* ------------------------------------- *
 * Parsing Waves                         *
 * ------------------------------------- */

// Syntax of lines:
// wave [cycle] [radius][theta] [color]
// time [optional, how many seconds for entire graph]
// fast [optional, factor for angle increment]
// dot [optional, color for combined wave]
//
// example:
// wave 2 100 -60 blue
// time 2
// fast 1
// dot  white
function parseWaves(lines) {
    let waves = [], // a list of waves
        time = 2, // default 2 seconds
        fast = 1, // default speed
        dot = 'white'; // default combine dot
    // parse lines for wave simulation
    lines.forEach(function(line) {
        // var parts = line.split(' ');
        // split parts by blanks, but keep blanks within quotes (see above)
        let parts = line.match(/(?:[^\s"]+|"[^"]*")+/g);
        // all with at least 2 parts
        if (parts.length > 1) {
            var key = parts.shift();
            switch (key) {
            case 'wave' : // a list of (radius) (cycle) (theta) (color)
               if (parts.length == 4) {
                  waves.push({cycle: parts[0].parse('N'),
                             radius: parts[1].parse('N'),
                              theta: parts[2].parse('N'),
                              color: parts[3]});
               }
               break;
            case 'time' : time = parts[0].parse('N');
                break;
            case 'fast' : fast = parts[0].parse('N');
                break;
            case 'dot' : dot = parts[0];
                break;
            default: // alert('Waves - Invalid key: ' + key);
            }
        }
    });

    // verify configuration
    let err = 0;
    if (waves.length == 0) err++;
    waves.forEach(function(item) {
        if (isNaN(item.radius) || isNaN(item.cycle) || isNaN(item.theta)) err++; // some are not numbers
        if (item.color.length == 0) err++; // invalid color
    });
    if (isNaN(time) || isNaN(fast)) err++; // not number
    if (err !== 0) { alert('Wrong syntax for Waves: ' + lines); return null; }
    // return the configuration
    return {waves: waves, time: time, fast: fast, dot: dot};
} // end of parseWaves

// debug in FireFox:
// console.log(JSON.stringify(parseWaves(['wave 2 100 -60 blue'])));
// {"waves":[{"cycle":2,"radius":100,"theta":-60,"color":"blue"}],"time":2,"fast":1,"dot":"white"}

// get a proper wave configuration
function getWaveConfig(lines) {
    var config = null;
    try { // parse lines for configuration
        config = parseWaves(lines);
    } catch (err) {
        alert('Wave Lines parse error: ' + err + '\nUse default waves.');
    }
    // use default configuration
    if (config == null) config = parseWaves(['wave 2 100 -60 blue']);

    // ensure wave amplitudes are positive values, and find the sum
    let sum = 0;
    config.waves.forEach((item, i) => {
        item.radius = Math.abs(item.radius);
        sum += item.radius;
    });
    // scale wave amplitudes so that sum matches 100 pixels
    config.waves.forEach((item, i) => {
        item.radius *= 100 / sum;
    });

    // ensure optional paratmeters with default
    config.time = config.time || 2;
    config.fast = config.fast || 1;
    config.dot = config.dot || 'black';
    return config;
}

/* ------------------------------------- *
 * Basic Wave                            *
 * ------------------------------------- */

// Assume (oo.x to om.x) represent n seconds
// so at time t, x = (t/n) * (om.x - oo.x)
// and           y = radius * sin (theta + t/period * 360)
// or            y = radius * sin (theta + t * cycle * 360)
// eliminate t,  y = radius * sin (theta + (x - oo.x)/(om.x - oo.x) * n * cycle * 360)
// now       angle = theta +/- t * cycle * 360,  + clockwise, - anticlockwise
// so            t = (angle - theta) / (cycle * 360)
// and           x = (angle - theta) / (n * cycle * 360) * (om.x - oo.x)
//
// Let f = frame count, each call to requestAnimationFrame.
// then     angle = theta +/- f * step, + clockwise, - anticlockwise
// and       step = k * cycle, so that faster increment for higher frequency
// so        t * cycle * 360 = f * k * cycle
// giving                  t = f * k / 360, independent of any cycle
// so        x = (om.x - oo.x) * f * k / (n * 360)         for frame f
// and       y = radius * sin (theta +/- cycle * f * k/ n) for frame f
// to keep x within range, x/(om.x - oo.x) <= 1,
// which is equivalent to  f * k <= n * 360
// Once f > 360 * n/k, f should reset to 0.

// Basic wave
function BasicWave(title, canvas, button, lines) {
    title.innerHTML = title.innerHTML || 'Basic Wave';
    const ctx = canvas.getContext("2d");

    let config = getWaveConfig(lines);
    const wave = config.waves.pop(); // only the first wave, ignore others
    const n = config.time; // number of seconds for span of x-axis (small for curvy)
    const k = config.fast; // multiplier of cycle to give step (small for smooth)

    // define positions of wall and axes
    const c = {x: canvas.width / 8, y: canvas.height / 2}; // center
    const st = {x: canvas.width /4 + 10, y: canvas.height * 1/8};
    const sb = {x: st.x, y: canvas.height * 7/8};
    const oo = {x: canvas.width /4 + 30, y: canvas.height / 2};
    const ot = {x: oo.x, y: canvas.height * 1/8};
    const ob = {x: oo.x, y: canvas.height * 7/8};
    const om = {x: canvas.width * 99/100, y: oo.y};
    const h = (om.x - oo.x) / (n * 360);

    // start and stop animation
    let handle; // frame reference handle
    function setHandle() {
      button.innerHTML = 'Pause';
      button.onclick = clearHandle;
      handle = window.requestAnimationFrame(animation);
    }
    function clearHandle() {
      button.innerHTML = 'Resume';
      button.onclick = setHandle;
      window.cancelAnimationFrame(handle);
    }

    let f = 0;   // start frame count
    setHandle(); // start animation

    function animation() {
      handle = requestAnimationFrame(animation); // update handle
      ctx.clearRect(0, 0, canvas.width, canvas.height); // new frame
      // rotating vector
      let angle = wave.theta - f * k * wave.cycle; // + clockwise, - anticlockwise
      const cp = {x: c.x + (wave.radius - 5) * cos(angle),
                  y: c.y + (wave.radius - 5) * sin(angle)};
      const cq = {x: c.x + wave.radius * cos(angle),
                  y: c.y + wave.radius * sin(angle)};
      ctx.circle(c, wave.radius, wave.color, 0.4); // center at c
      ctx.arrow(c, cp, wave.color, 1.8, 10); // from center to cp
      ctx.dot(cq, wave.color); // dot at cq
      // shadow
      ctx.line(st, sb);
      ctx.dot({x: st.x, y: cq.y}, wave.color);
      // wave
      ctx.arrow(ob, ot, 'grey', 1, 10);
      ctx.arrow(oo, om, 'grey', 1, 10);
      ctx.label(om, 'time');
      ctx.save();
      ctx.beginPath();
      ctx.moveTo(oo.x, oo.y + wave.radius * sin(wave.theta));
      for (let x = oo.x; x <= om.x; x++) {
          ctx.lineTo(x, oo.y + wave.radius * sin(wave.theta - wave.cycle * (x - oo.x) / h));
      }
      ctx.stroke();
      ctx.restore();
      ctx.dot({x:oo.x + f * k * h, y:cq.y}, wave.color);// moving dot on wave
      f++; // update frame count
      if (f * k > n * 360) f = 0; // reset frame count
    }
} // end of BasicWave

/* ------------------------------------- *
 * Separate Waves                        *
 * ------------------------------------- */

// Separate waves
function SeparateWaves(title, canvas, button, lines) {
    title.innerHTML = title.innerHTML || 'Separate Waves';
    const ctx = canvas.getContext("2d");

    let config = getWaveConfig(lines);
    const waves = config.waves;
    const m = waves.length;
    const n = config.time; // number of seconds for span of x-axis (small for curvy)
    const k = config.fast; // multiplier of cycle to give step (small for smooth)

    // define positions of wall and axes
    const c = {x: canvas.width / 8, y: canvas.height / 2}; // center
    const st = {x: canvas.width /4 + 10, y: canvas.height * 1/8};
    const sb = {x: st.x, y: canvas.height * 7/8};
    const oo = {x: canvas.width /4 + 30, y: canvas.height / 2};
    const ot = {x: oo.x, y: canvas.height * 1/8};
    const ob = {x: oo.x, y: canvas.height * 7/8};
    const om = {x: canvas.width * 99/100, y: oo.y};
    const h = (om.x - oo.x) / (n * 360);

    // start and stop animation
    let handle; // frame reference handle
    function setHandle() {
      button.innerHTML = 'Pause';
      button.onclick = clearHandle;
      handle = window.requestAnimationFrame(animation);
    }
    function clearHandle() {
      button.innerHTML = 'Resume';
      button.onclick = setHandle;
      window.cancelAnimationFrame(handle);
    }

    let f = 0;   // start frame count
    setHandle(); // start animation

    function animation() {
      handle = requestAnimationFrame(animation); // update handle
      ctx.clearRect(0, 0, canvas.width, canvas.height); // new frame
      // draw wall and axes
      ctx.line(st, sb);
      ctx.arrow(ob, ot, 'grey', 1, 10);
      ctx.arrow(oo, om, 'grey', 1, 10);
      ctx.label(om, 'time');
      // draw waves without dot
      waves.forEach((item, i) => {
          ctx.save();
          ctx.beginPath();
          ctx.moveTo(oo.x, oo.y + item.radius * sin(item.theta));
          for (let x = oo.x; x <= om.x; x++) {
              ctx.lineTo(x, oo.y + item.radius * sin(item.theta - item.cycle * (x - oo.x) / h));
          }
          ctx.stroke();
          ctx.restore();
      });
      waves.forEach((item, i) => {
          // rotating vector
          ctx.circle(c, item.radius, item.circle, 0.4); // center at c
          let angle = item.theta - f * k * item.cycle, // + clockwise, - anticlockwise
              cq = {x: c.x + item.radius * cos(angle),
                    y: c.y + item.radius * sin(angle)};
          ctx.arrow(c, cq, item.color, 1.8, 10); // arrow from c to cq
          ctx.dot(cq, item.color);
          // shadow dot
          ctx.dot({x: st.x, y: cq.y}, item.color);
          // dot on wave
          ctx.dot({x:oo.x + (om.x - oo.x) * f * k / (n * 360), y:cq.y}, item.color);
      });
      f++; // update frame count
      if (f * k > n * 360) f = 0; // reset frame count
    }
} // end of SeparateWaves

/* ------------------------------------- *
 * Combine Waves                         *
 * ------------------------------------- */

// Combine waves
function CombineWaves(title, canvas, button, lines) {
    title.innerHTML = title.innerHTML || 'Combine Waves';
    const ctx = canvas.getContext("2d");

    let config = getWaveConfig(lines);
    const waves = config.waves;
    const m = waves.length;
    const n = config.time; // number of seconds for span of x-axis (small for curvy)
    const k = config.fast; // multiplier of cycle to give step (small for smooth)
    const dot = config.dot; // color of final dot

    // define positions of wall and axes
    const c = {x: canvas.width / 8, y: canvas.height / 2}; // center
    const st = {x: canvas.width /4 + 10, y: canvas.height * 1/8};
    const sb = {x: st.x, y: canvas.height * 7/8};
    const oo = {x: canvas.width /4 + 30, y: canvas.height / 2};
    const ot = {x: oo.x, y: canvas.height * 1/8};
    const ob = {x: oo.x, y: canvas.height * 7/8};
    const om = {x: canvas.width * 99/100, y: oo.y};
    const h = (om.x - oo.x) / (n * 360);

    // start and stop animation
    let handle; // frame reference handle
    function setHandle() {
      button.innerHTML = 'Pause';
      button.onclick = clearHandle;
      handle = window.requestAnimationFrame(animation);
    }
    function clearHandle() {
      button.innerHTML = 'Resume';
      button.onclick = setHandle;
      window.cancelAnimationFrame(handle);
    }

    let f = 0;   // start frame count
    setHandle(); // start animation

    function animation() {
      handle = requestAnimationFrame(animation); // update handle
      ctx.clearRect(0, 0, canvas.width, canvas.height); // new frame
      // draw wall and axes
      ctx.line(st, sb);
      ctx.arrow(ob, ot, 'grey', 1, 10);
      ctx.arrow(oo, om, 'grey', 1, 10);
      ctx.label(om, 'time');
      // rotating vector
      let cp = c; // start cp at center
      waves.forEach((item, i) => {
          ctx.circle(cp, item.radius, item.circle, 0.4); // circle center at cp
          let angle = item.theta - f * k * item.cycle, // + clockwise, - anticlockwise
              cq = {x: c.x + item.radius * cos(angle),
                    y: c.y + item.radius * sin(angle)};
          if (cp.x !== c.x) ctx.circle(c, item.radius, 'grey', 0.2); // shadow circle always at c
          ctx.arrow(c, cq, 'grey', 1.8, 8); // shadow arrow from c to cq
          cq = addPoints(c, cp, cq); // new cq
          ctx.arrow(cp, cq, item.color, 1.8, 10); // arrow from cp to new cq
          cp = cq; // advance cp
      });
      ctx.dot(cp, dot); // yes! the final dot
      // shadow dot of final
      ctx.dot({x: st.x, y: cp.y}, dot);
      // wave
      // compute the moveTo location
      let y = 0; // get sum of radius * sin(theta)
      waves.forEach((item, i) => {
          y += item.radius * sin(item.theta);
      });
      ctx.save();
      ctx.beginPath();
      ctx.moveTo(oo.x, oo.y + y);
      for (let x = oo.x; x <= om.x; x++) {
          let y = 0; // get sum of radius * sin(angle)
          waves.forEach((item, i) => {
              y += item.radius * sin(item.theta - item.cycle * (x - oo.x) / h);
          });
          ctx.lineTo(x, oo.y + y);
      }
      ctx.stroke();
      ctx.restore();
      ctx.dot({x:oo.x + (om.x - oo.x) * f * k / (n * 360), y:cp.y}, dot); // looks good!
      f++; // update frame count
      if (f * k > n * 360) f = 0; // reset frame count
    }
} // end of CombineWaves

/* ------------------------------------- *
 * Mixed Waves                           *
 * ------------------------------------- */

// MixedWaves
function MixedWaves(title, canvas, button, lines) {
    // add another button with switching modes.
    let flip = document.createElement('button');
    flip.className = 'button';
    // override some style of button for positioning
    flip.setAttribute('style', 'position:absolute; right:120px; bottom:10px;');
    button.parentNode.appendChild(flip);

    // separate mode
    function doSeparate() {
        // pause animation before switching
        if (button.innerHTML == 'Pause') button.click();
        SeparateWaves(title, canvas, button, lines);
        title.innerHTML = 'Mixed Waves (separate)'; // override title
        flip.innerHTML = 'Combine';
        flip.onclick = doCombine;
    }
    // combine mode
    function doCombine() {
        // pause animation before switching
        if (button.innerHTML == 'Pause') button.click();
        CombineWaves(title, canvas, button, lines);
        title.innerHTML = 'Mixed Waves (combine)'; // override title
        flip.innerHTML = 'Separate';
        flip.onclick = doSeparate;
    }
    doSeparate();
} // end of MixedWaves

/* ------------------------------------- *
 * Node transformation and processing    *
 * ------------------------------------- */

// transform the node
function transform(node) {
    var source = node.innerHTML;
    // minimal processing
    var lines = [];
    source.split('\n').forEach(function(line) {
        // remove comments after ;
        if (line && line.indexOf(';') !== -1) line = line.slice(0,line.indexOf(';'));
        // remove blank lines
        if (line && line.length !== 0) lines.push(line);
    });
    // first line is mode, with optional param separate by one space
    var first = lines.shift().match(/\w+|\s.*/g);
    // match either pattern: \w+ a word, \s.* blank then anything
    var args =
       {mode: first.length > 0 ? first[0] : '',          // start word
       param: first.length > 1 ? first[1].slice(1) : '', // no leading blank
       lines: lines }
    return new Panel(args);
}

// process all <fourier> nodes
function processFourier() {
   // Process all <fourier> nodes, replacing them as we progress
   toArray(document.getElementsByClassName('fourier'))
      .concat(toArray(document.getElementsByTagName('fourier')))
      .forEach(function (node) {
          node.parentNode.replaceChild(transform(node), node);
       });
}

// Start from here.
loadFourierStyle(); // load the style CSS for panel, title, canvas, button
processFourier();   // find and process all <fourier> nodes

})(); // invoke single function

/** End of Fourier Script processing.
 ** Author: Joseph Chan
 ** Date: 2 November, 2022.
 */
