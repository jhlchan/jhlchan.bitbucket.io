/*

Games in Javascript

*/

// ImageLoader object
ImageLoader = function() {
    this.images = [] // list of images
    this.names = {}  // to access image by name
}

// add image to ImageLoader by name and url
ImageLoader.prototype.addImage = function(name, url) {
    this.names[name] = {'url':url}
    this.images.push(this.names[name])
}

// get image from ImageLoader by name
ImageLoader.prototype.getImage = function(name) {
    return this.names[name].img
}

// load image to ImageLoader
ImageLoader.prototype.loadImages = function(callback) {
    console.log('load images')
    console.log(this.images)
    var max = this.images.length, image, count = 0
    for (var i = 0; i < max; i++) {
        image = new Image()
        image.addEventListener('load', onLoaded)
        image.addEventListener('error', onError)
        image.src = this.images[i].url
        console.log('loading:' + image.src)
        this.images[i].img = image // append attribute
    }

    // onLoad handler
    function onLoaded(e) {
       count++
       if (count >= max) callback()
    }

    // onError handler
    function onError(e) {
       console.log(e)
    }
}

// Hopover object
Hopover = function(pegs, wins, context, loader) {
    // JC: for n = 1
    // pegs = [1, 0, 2]
    // wins = [2, 0, 1]
    // JC: for n = 1, this works, and isGameOver can detect a win
    // pegs = [0, 0, 1, 0, 2, 0, 0]
    // wins = [0, 0, 2, 0, 1, 0, 0]
    // JC: for n = 2
    // pegs = [1, 1, 0, 2, 2]
    // wins = [2, 2, 0, 1, 1]
    // JC: for n = 3
    // pegs = [1, 1, 1, 0, 2, 2, 2]
    // wins = [2, 2, 2, 0, 1, 1, 1]
	this.init = pegs // initial pegs
	this.wins = wins // winning pegs
    this.ctx = context // canvas context
    this.loader = loader // image loader
    this.reset()
}

// reset puzzle for Hopover
Hopover.prototype.reset = function() {
    document.getElementById('txtGameover').innerHTML = ""
    this.pegs = this.init.slice() // copy the array
    this.draw()
}

// draw frogs for Hopover, matching pegs
Hopover.prototype.drawFrogs = function() {
    var ctx = this.ctx, loader = this.loader
    for (var i = 0; i < this.pegs.length; i++) {
        if (this.pegs[i] > 0) {
            // context.drawImage(img, x, y, width, height)
            ctx.drawImage(loader.getImage(this.pegs[i] == 1 ? 'frog' : 'toad'), (i * 85) + 24, 180, 60, 60)
        }
    }
}

// draw the puzzle for Hopover
Hopover.prototype.draw = function() {
    var ctx = this.ctx, loader = this.loader
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height)
    // context.drawImage(img, x, y)
    ctx.drawImage(loader.getImage('pond'), 0, 0)
    this.drawFrogs()
}

// move a frog for Hopover, (x,y) is mouse click relative to context
Hopover.prototype.moveFrog = function(x, y) {
    var col = Math.floor(x / 85) // get pegs index
    var dir = 0

    if (this.pegs[col] > 0) { // move frog or toad
        dir = this.pegs[col] == 1 ? 1 : -1 // right = 1, left = -1
        // delta = 1 is slide, delta = 2 is jump over
        for (var delta = 1; delta < 3; delta++) {
            if (this.pegs[col + (dir * delta)] == 0) {
                // destination is an empty rock
                this.pegs[col + (dir * delta)] = this.pegs[col] // update destination
                this.pegs[col] = 0 // remove frog or toad to expose the rock
            }
        }

        this.draw()
        if (this.isGameOver()) this.gameOver()
    }
}

// check if game over for Hopover
Hopover.prototype.isGameOver = function() {
    if (this.wins.toString() == this.pegs.toString()) return true
    var dir = 0

    for (var i = 0; i < this.pegs.length; i++) {
        if (this.pegs[i] > 0) { // check frog or toad
            dir = this.pegs[i] == 1 ? 1 : -1 // right = 1, left = -1
            // delta = 1 is slide, delta = 2 is jump over
            for (var delta = 1; delta < 3; delta++) {
                if (this.pegs[i + (dir * delta)] == 0) {
                    // there is a rock at destination
                    return false
                }
            }
        }
    }  

    return true
}

// open a dialog for Hopover
Hopover.prototype.openDialog = function(msg, color) {
    var oCtl = document.getElementById('txtGameover')
    oCtl.innerHTML = msg
    oCtl.style.color = color
}

// puzzle game over for Hopover
Hopover.prototype.gameOver = function() {
    var msg
    
    if (this.wins.toString() == this.pegs.toString()) {
        msg = 'Congratulations, you solved the puzzle!'
        this.openDialog(msg, 'purple')
    } else {
        msg = 'Sorry, the frogs are stuck - try again!'
        this.openDialog(msg, 'red')
    }

}

// get mouse click position within context
function getMousePos(mouseEvent, context) {
    var rect = context.canvas.getBoundingClientRect()
    return {
        x: mouseEvent.clientX - rect.left,
        y: mouseEvent.clientY - rect.top
    }
}

// toggle quiz answer
function DoToggle(pId) {
   var oCtlkey = document.getElementById(pId + "key")
   var oCtlbtn = document.getElementById(pId + "btn")

   if (oCtlkey.className == "cssQuizKey") {
      oCtlkey.className = "cssQuizKeyHide"
      oCtlbtn.value = "Show Answer"
   }
   else {
      oCtlkey.className = "cssQuizKey"
      oCtlbtn.value = "Hide Answer"
   }
}
