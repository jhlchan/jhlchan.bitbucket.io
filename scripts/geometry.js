/**
 Geometry script

 Ideas taken from:
 * Markdeep:  https://casual-effects.com/markdeep/latest/markdeep.min.js
 * MathIsFun: https://mathsisfun.com/geometry/images/geom-triangle.js
 */

/**

 Documentation:

 Use a Frame for <geometry>:
 * a DIV with position:relative, width and height.
 * have a Canvas of the same size.
 * have Title and Info lines on top.
 * have a Reset button at the bottom.
 * other properties are local, e.g. the 3 points.

 Use a Panel for <origami>:
 * a DIV with position:relative, width and height.
 * have a Board of square size.
 * have Title and Info lines on top.
 * have an Animate/Reset button at the bottom.
 * other properties are local, e.g. the pieces.

 Use a Span for <drawing>:
 * a SPAN with innerHTML an SVG specification.
 * have width, height and viewbox.
 * have SVG specification with definitions.
 * have elements <path>, <circle>, <text>, <g>, etc.
 * initial coordinates are converted to raw for drawing.

 */

/**
 Common Global Functions
*/
// convert list to an array
function toArray(list) {
    return Array.prototype.slice.call(list);
}
// Round to decimal places
function round1(x) {
    return Math.round(x * 10) / 10;
}
function round2(x) {
    return Math.round(x * 100) / 100;
}
function round3(x) {
    return Math.round(x * 1000) / 1000;
}
// can also use: +(num.toFixed(places)), toFixed gives string, + converts to number.
// trigonometry functions
var sin = Math.sin;
var cos = Math.cos;
var tan = Math.tan;
var atan = Math.atan;
var atan2 = Math.atan2;
var PI = Math.PI;
// convert angle in degree to radian
function toRadian(x) {
    return x * PI / 180;
}
// convert angle in radian to degree
function toDeg(x) {
    return x * 180 / PI;
}
// convert angle in radian to degree with round-off
function toDegree(x) {
    return Math.round(x * 180 / PI);
}
// ensure val is within min to max
function constrain(min, val, max) {
    return Math.min(Math.max(min, val), max);
}
/* get a random number within min to max */
function getRandom(min, max) {
    return Math.random() * (max - min) + min;
}

/* Note: cannot be placed outside:
String.prototype.parse = function ...
as this would cause function ... to be evaluated, then assign to parse.
In that case, this = the function object, and it has no match method.
Once inside braces {}, the function is defined, not evaluated.
The function is assigned to String.prototype, and 'this' will be a string on execution.
*/

// single function application for parser
(function() {
'use strict';
/**
 A simple string parser.
 Syntax: parse(pattern), where pattern is a string of indicators:
 * w for word, of alphabets and numbers, underscore. \w+
 * c for char, a single alphabet
 * i for integers, [0-9]+
 * n for numbers, possibly with decimal. \d+\.?\d*
 * Some tips in https://stackoverflow.com/questions/2811031/
 * () for a group, giving a sublist, numbers will use parseInt or parseFloat.
 * The components are separated by whitespaces, with optional leading and ending spaces.
 */
String.prototype.parse = function (pattern) {
    // split this string into words, cleanup whitespaces.
    var words = this.match(/\b([\w\.]+)\b/g);
    // parse using local pattern
    function parseWords(pat) {
        var list = [];
        while (pat.length > 0 && words.length > 0) {
            // consume local pattern
            var ch = pat[0];
            pat = pat.slice(1);
            if (ch == '(') { // recursive call
                var j = pat.indexOf(')');
                if (j !== -1) {
                    var result = parseWords(pat.slice(0,j));
                    if (result.length > 0) list.push(result);
                    pat = pat.slice(j); // advance local pat
                }
            }
            else if (ch == ')') { // ignore
            }
            else { // consume words
                var item = words.shift();
                switch (ch) {
                case 'w' : if (item.match(/^\w+$/)) list.push(item); break;
                case 'c' : if (item.match(/^[A-Za-z]$/)) list.push(item); break;
                case 'i' : if (item.match(/^[0-9]+$/)) list.push(parseInt(item)); break;
                case 'n' : if (item.match(/^\d+\.?\d*$/)) list.push(parseFloat(item)); break;
                default : break; // discard item and ch
                }
            }
        }
        // alert('list :' + list.toString());
        return list;
    }
    // main call: ensure pattern not null.
    return parseWords(pattern || '');
}
// convert string to JSON object
String.prototype.toObject = function() {
    // split this string into words, cleanup whitespaces.
    var words = this.match(/\b([\w\.]+)\b/g);
    // remember indices pointing to a word that begins with an alphabet
    var alpha = [];
    for (var j = 0; j < words.length; j++) {
         if (words[j].match(/^[A-Za-z]/)) alpha.push(j);
    }
    // form the JSON string
    var s = [];
    while (alpha.length > 0) {
        var j = alpha.shift(); // alpha[0] = alpha.peek()
        var k = alpha.length > 0 ? alpha[0] : words.length;
        if (k == j+1) { // two consecutive words
            s.push('"' + words[j] + '":"' + (k < words.length ? words[k] : 'null') + '"');
            alpha.shift(); // discard index k
        }
        else if (k == j+2) { // a word and a number
            s.push('"' + words[j] + '":' + words[j+1]);
        }
        else { // a word and more numbers
            s.push('"' + words[j] + '":[' + words.slice(j+1, k).join(',') + ']');
        }
    }
    s = '{ ' + s.join(', ') + ' }'; // the JSON string
    // alert('toObject: ' + this + '\n' + s);
    return JSON.parse(s); // object of JSON string
}
})(); // invoke single function

// single function application for <geometry>
(function() {
'use strict';

/**
 Part 1: from MathIsFun.
 * object Point is vertex, with angle-in and angle-out,
 * object Line is segment, with end-points,
 * object Triangle has 3 vertices,
 * object Shape is a point, with color and hit-test,
 * object Frame is a content-division <div> container, with a canvas,
 * the 2D-context of canvas is where graphics are drawn,
 * the 2D-context has listeners for mouse and touch events,
 * the event handlers checks for hits on dot Shape, which covers a vertex Point,
 * any change in the position of a dot affects the vertex, and the triangle is re-drawn,
 * there are several pre-defined types of triangle, see getABC(type),
 * there are fixed pre-defined modes for drawings, like 'incircle' and 'circum'.
 */

// Global constant
// types of triangles
var types = ['any', 'acute', 'obtuse', 'equi', 'iso', 'right'];
// drawing mode for triangles
var modes = ['type', 'median', 'circum', 'incircle', 'ortho', 'area', 'perim', 'inequal', 'angles', 'choose']
// scale factor for all objects
var scaleFactor = 1; // for Line and Triangle
// ratio = 2; actually, the finally display is independent of ratio.
var ratio = 2; // for Frame: setTransform, onMouseDown, onMouseMove, doPointer

/**
 The Utilities for Geometry

*/
// norm of (dx, dy), the distance squared
function norm(dx, dy) {
    return (dx * dx + dy * dy);
}
// distance of (dx,dy) from origin
function dist(dx, dy) {
    return (Math.sqrt(dx * dx + dy * dy));
}
// loop around increment
function loop(currNo, minNo, maxNo, incr) {
    currNo += incr;
    var was = currNo;
    var range = maxNo - minNo + 1;
    if (currNo < minNo) {
        currNo = maxNo - (-currNo + maxNo) % range;
    }
    if (currNo > maxNo) {
        currNo = minNo + (currNo - minNo) % range;
    }
    return currNo;
}
// convert 0, 1, 2 to angle names A, B, C
function toAngleName(i) {
    // return String.fromCharCode(65 + i)
    return ['A', 'B', 'C'][i];
}
// convert 0, 1, 2 to side names a, b, c
function toSideName(i) {
    // return String.fromCharCode(97 + i);
    return ['a', 'b', 'c'][i];
}
// get the three points A, B, C of a triangle
function getABC(type) {
    switch (type) {
    case "acute":
        return [
            [157, 97, "A"],
            [85, 196, "B"],
            [233, 236, "C"]
        ];
    case "equi":
        return [
            [195, 88, "A"],
            [85, 196, "B"],
            [233, 236, "C"]
        ];
    case "iso":
        return [
            [188, 110, "A"],
            [85, 196, "B"],
            [233, 236, "C"]
        ];
    case "obtuse":
        return [
            [77, 95, "A"],
            [96, 206, "B"],
            [247, 221, "C"]
        ];
    case "right":
        return [
            [104, 82, "A"],
            [90, 203, "B"],
            [247, 221, "C"]
        ];
    case "any":
        return [
            [250, 90, "A"],
            [66, 186, "B"],
            [263, 234, "C"]
        ];
    default:
        return parseABC(type);
    }
}
// parse the type string as line for 3 vertices
function parseABC(line) {
    var vertices = []; // collect the vertices
    var parts = line.parse('(cnn)(cnn)(cnn)');
    if (parts.length == 3) {
        parts.forEach(function (triple) {
            var ch = triple.shift();
            triple.push(ch); // rotate ch back to the end
            if (ch == 'A' || ch == 'B' || ch == 'C') vertices.push(triple);
        });
    }
    // alert('parseABC: ' + vertices.length + ', ' + vertices.toString());
    if (vertices.length == 3) return vertices;
    alert('Invalid syntax: ' + line + '\nExpect 3 vertices. Ignored!');
    return getABC('any');
}

/**
 The Vertex Point Object

 */
// construct a vertex point (ix, iy)
function Point(ix, iy) {
    // attributes: coordinates and angles
    this.x = ix || 0; // default x = 0
    this.y = iy || 0; // default y = 0
    this.angleIn = 0;
    this.angleOut = 0;
    // a vertex is the intersection of two edges,
    // like so:        / out
    //                * <--- in
    // the in-edge makes angleIn,
    // the out-edge makes angleOut, and vertex angle = angleOut - angleIn.
};
// representation of vertex
Point.prototype.toString = function () {
    // round coordinates to two decimals
    return '(' + Math.round(this.x * 100)/100 +
          ', ' + Math.round(this.y * 100)/100 + ')';
};
// set the in and out angles for this vertex
Point.prototype.setAngle = function (ain, aout) {
    this.angleIn = ain;
    this.angleOut = aout;
};
// get the angle at this vertex
Point.prototype.getAngle = function () {
    return this.angleOut - this.angleIn;
};
// set this point as the given point
Point.prototype.set = function (pt) {
    this.x = pt.x;
    this.y = pt.y;
};
// set this point as (ix,iy)
Point.prototype.setxy = function (ix, iy) {
    this.x = ix;
    this.y = iy;
};
// clone this point
Point.prototype.clone = function () {
    return new Point(this.x, this.y);
};
// get the norm of this point with another point pt
Point.prototype.norm = function (pt) {
    return norm(this.x - pt.x, this.y - pt.y);
};
// get the distance of this point with another point pt
Point.prototype.dist = function (pt) {
    return dist(this.x - pt.x, this.y - pt.y);
};
// draw this point on context g
Point.prototype.drawMe = function (g) {
    g.drawPoint(this, 'rgba(0, 0, 255, 0.3)', 20);
};
// set this point as the average of a list of points pts
Point.prototype.averageMe = function (pts) {
    var xSum = 0;
    var ySum = 0;
    var total = pts.length;
    for (var i = 0; i < total; i++) {
        xSum += pts[i].x;
        ySum += pts[i].y;
    }
    this.setxy(xSum / total, ySum / total);
};
// set this point as the interpolation of points pt1, pt2 with factor f : (1 - f)
Point.prototype.interpolateMe = function (pt1, pt2, f) {
    this.x = pt1.x * f + pt2.x * (1 - f);
    this.y = pt1.y * f + pt2.y * (1 - f);
};
// get the translation of this point by vector pt, addQ = true/false for forward/backward
Point.prototype.translate = function (pt, addQ) {
    addQ = (typeof addQ == 'undefined') ? true : addQ; // default: true, no OR for boolean
    var t = this.clone();
    if (addQ) {
        t.x += pt.x;
        t.y += pt.y;
    } else {
        t.x -= pt.x;
        t.y -= pt.y;
    }
    return t;
};
// set this point as the translation by vector pt, addQ = true/false for forward/backward
Point.prototype.translateMe = function (pt, addQ) {
    this.set(this.translate(pt, addQ));
};
// get the scalar multiple of this point by a factor
Point.prototype.multiply = function (factor) {
    return new Point(this.x * factor, this.y * factor);
};
// set this point as its scalar multiple by a factor
Point.prototype.multiplyMe = function (factor) {
    this.x *= factor;
    this.y *= factor;
};
// get the rotation of this point by an angle (in radians)
Point.prototype.rotate = function (angle) {
    var cosa = cos(angle);
    var sina = sin(angle);
    var xPos = this.x * cosa + this.y * sina;
    var yPos = -this.x * sina + this.y * cosa;
    return new Point(xPos, yPos);
};
// set this point as the rotation by an angle (in radians)
Point.prototype.rotateMe = function (angle) {
    this.set(this.rotate(angle));
};

/**
 The Line Segment Object

 */
// construct a line segment with begin pt1 and end pt2
function Line(pt1, pt2) {
    // attributes
    this.a = pt1;
    this.b = pt2;
};
// compute the line segment length
Line.prototype.getLength = function () {
    var dx = this.b.x - this.a.x;
    var dy = this.b.y - this.a.y;
    return Math.sqrt(dx * dx + dy * dy) * scaleFactor;
};
// compute the intersection of this line segment with another line ln
Line.prototype.getIntersection = function (ln, asSegmentsQ) {
    asSegmentsQ = (typeof asSegmentsQ == 'undefined') ? false : asSegmentsQ;
    // default false, no OR for boolean
    var A = this.a;
    var B = this.b;
    var E = ln.a;
    var F = ln.b;
    var a1 = B.y - A.y;
    var b1 = A.x - B.x;
    var c1 = B.x * A.y - A.x * B.y;
    var a2 = F.y - E.y;
    var b2 = E.x - F.x;
    var c2 = F.x * E.y - E.x * F.y;
    var denom = a1 * b2 - a2 * b1;
    if (denom == 0) {
        return null;
    }
    var ip = new Point((b1 * c2 - b2 * c1) / denom, (a2 * c1 - a1 * c2) / denom);
    if (asSegmentsQ) {
        if (ip.norm(B) > A.norm(B)) return null;
        if (ip.norm(A) > A.norm(B)) return null;
        if (ip.norm(F) > E.norm(F)) return null;
        if (ip.norm(E) > E.norm(F)) return null;
    }
    return ip;
};
// get the midpoint of this line segment
Line.prototype.getMidPoint = function () {
    return new Point((this.a.x + this.b.x) / 2, (this.a.y + this.b.y) / 2);
};
// set this line as rotated by angle, about midpoint
Line.prototype.rotateMidMe = function (angle) {
    var mid = this.getMidPoint();
    this.a.translateMe(mid, false);
    this.b.translateMe(mid, false);
    this.a.rotateMe(angle);
    this.b.rotateMe(angle);
    this.a.translateMe(mid);
    this.b.translateMe(mid);
};
// set this line as rotated by angle, about a point pt
Line.prototype.rotatePointMe = function (pt, angle) {
    // this avoid the intermediate new points
    this.a.x -= pt.x;
    this.a.y -= pt.y;
    this.b.x -= pt.x;
    this.b.y -= pt.y;
    this.a.rotateMe(angle);
    this.b.rotateMe(angle);
    this.a.x += pt.x;
    this.a.y += pt.y;
    this.b.x += pt.x;
    this.b.y += pt.y;
};
// get the closet point from this point to toPt, inSegmentQ = true to ensure it is within segment
Line.prototype.getClosestPoint = function (toPt, inSegmentQ) {
    var AP = toPt.translate(this.a, false);
    var AB = this.b.translate(this.a, false);
    var ab2 = AB.x * AB.x + AB.y * AB.y;
    var ap_ab = AP.x * AB.x + AP.y * AB.y;
    var t = ap_ab / ab2;
    if (inSegmentQ) {
        t = constrain(0, t, 1);
    }
    var closest = this.a.translate(AB.multiply(t));
    return closest;
};
// set the length of this segment to newLen, fromMidQ = boolean
Line.prototype.setLength = function (newLen, fromMidQ) {
    fromMidQ = typeof fromMidQ == 'undefined' ? true : fromMidQ; // default true
    var len = this.getLength();
    if (fromMidQ) {
        var mid = this.getMidPoint();
        var half = new Point(this.a.x - mid.x, this.a.y - mid.y);
        half.multiplyMe(newLen / len);
        this.a = mid.translate(half);
        this.b = mid.translate(half, false);
    } else {
        var diff = new Point(this.a.x - this.b.x, this.a.y - this.b.y);
        diff.multiplyMe(newLen / len);
        this.b = this.a.translate(diff, false);
    }
};
// get angle of this line segment
Line.prototype.getAngle = function () {
    return atan2(this.b.y - this.a.y, this.b.x - this.a.x);
};
// return a clone of this line segment, with clone end points
Line.prototype.clone = function () {
    // not: new Line(this.a, this.b), as points are not cloned
    var ln = new Line(new Point(), new Point());
    ln.a.x = this.a.x;
    ln.a.y = this.a.y;
    ln.b.x = this.b.x;
    ln.b.y = this.b.y;
    return ln;
};

/**
 The Triangle Object

 */
// construct a triangle, initially empty
function Triangle() {
    // constants
    var numPoints = 3; // three vertices
    var defaultAngleLabels = ["A", "B", "C"];
    var defaultSideLabels = ["c", "a", "b"];

    // attributes
    this.pts = new Array(numPoints); // the three vertex points
    for (var k = 0; k < numPoints; k++) {
        this.pts[k] = new Point();
    }
    this.sides = [3, 4, 5]; // default to 3,4,5 right-triangle
    this.sideLabels = defaultSideLabels;
    this.angleLabels = defaultAngleLabels;
    this.isAngleKnownQ = [false, false, false];
    this.isSideKnownQ = [false, false, false];
};
// get the 3 angles from vertices
Triangle.prototype.getAngles = function () {
    // var numPoints = this.pts.length; /* use hard-coding for 3, 2 */
    var angs = [];
    var angSum = 0;
    for (var i = 0; i < 3; i++) {
        var ang = toDegree(this.pts[i].getAngle());
        if (i < 2) {
            angSum += ang;
        } else {
            ang = 180 - angSum;
        }
        angs[i] = ang;
    }
    return angs
};
// get the 3 sides from vertices
Triangle.prototype.getSides = function () {
    var numPoints = this.pts.length;
    var sides = [];
    for (var i = 0; i < numPoints; i++) {
        var pt = this.pts[i];
        var ptp1 = this.pts[loop(i, 0, numPoints - 1, 1)];
        sides.push(dist(ptp1.x - pt.x, ptp1.y - pt.y));
    }
    return (sides);
};
// determine if A, B, C is clockwise (+1), linear (0), or anti-clockwise (-1)
Triangle.prototype.getClockwise = function () {
    var numPoints = this.pts.length;
    var count = 0;
    for (var i = 0; i < numPoints; i++) {
        var pt = this.pts[i];
        var ptm1 = this.pts[loop(i, 0, numPoints - 1, -1)];
        var ptp1 = this.pts[loop(i, 0, numPoints - 1, 1)];
        var z = 0;
        z += (pt.x - ptm1.x) * (ptp1.y - pt.y);
        z -= (pt.y - ptm1.y) * (ptp1.x - pt.x);
        if (z < 0) {
            count--;
        } else if (z > 0) {
            count++;
        }
    }
    if (count > 0) return (1);
    if (count == 0) return (0);
    return (-1);
};
/* set the angles of this triangle from vertices */
Triangle.prototype.setAngles = function () {
    var CW = this.getClockwise();
    var numPoints = this.pts.length;
    // set the in and out angles of all points
    for (var i = 0; i < numPoints; i++) {
        var pt = this.pts[i];
        var ptm1 = this.pts[loop(i, 0, numPoints - 1, -1)];
        var ptp1 = this.pts[loop(i, 0, numPoints - 1, 1)];
        // from this point on, the angles are in radian
        var a1 = atan2(ptm1.y - pt.y, ptm1.x - pt.x);
        var a2 = atan2(ptp1.y - pt.y, ptp1.x - pt.x);
        if (CW == 1) {
            var temp = a1;
            a1 = a2;
            a2 = temp;
        }
        if (a1 > a2) a2 += 2 * PI;
        pt.angleIn = a1;
        pt.angleOut = a2;
        // It is better to keep these angles as in radian.
        // If they are in degree, trigonometry calls need to convert degree to radian, then compute.
    }
};
// update this triangle, after changes in vertices
Triangle.prototype.updateMe = function () {
    this.setAngles(); // reset the angles
    this.sides = this.getSides(); // reset the sides
};
// get the index number of the variable
Triangle.prototype.getNum = function (varName) {
    switch (varName) {
    case "A":
        return 0;
    case "B":
        return 1;
    case "C":
        return 2;
    case "a":
        return 1;
    case "b":
        return 2;
    case "c":
        return 0;
    }
    return -1;
};
// get the value of the variable in this triangle
Triangle.prototype.getValue = function (varName) {
    switch (varName) {
    case "A":
    case "B":
    case "C":
        return this.pts[this.getNum(varName)].getAngle();
    case "a":
    case "b":
    case "c":
        return this.sides[this.getNum(varName)];
    default:
        return 0;
    }
};
// set the labels of this triangle
Triangle.prototype.setLabels = function (angleA, angleB, angleC, sidea, sideb, sidec) {
    this.setLabel("A", angleA);
    this.setLabel("B", angleB);
    this.setLabel("C", angleC);
    this.setLabel("a", sidea);
    this.setLabel("b", sideb);
    this.setLabel("c", sidec);
};
// set the label of a variable of this triangle
Triangle.prototype.setLabel = function (varName, labelStr) {
    var lblNum = this.getNum(varName);
    if (lblNum < 0) return;
    if (labelStr == null) return;
    switch (varName) {
    case "A":
    case "B":
    case "C":
        this.angleLabels[lblNum] = labelStr;
        break;
    case "a":
    case "b":
    case "c":
        this.sideLabels[lblNum] = labelStr;
        break;
    default:
    }
};
// set point at ptNum as (ix, iy)
Triangle.prototype.setxy = function (ptNum, ix, iy) {
    this.pts[ptNum].setxy(ix, iy);
};
// set all angles and sides as known
Triangle.prototype.setAllKnown = function (knownQ) {
    this.isAngleKnownQ = [knownQ, knownQ, knownQ];
    this.isSideKnownQ = [knownQ, knownQ, knownQ];
};
// set a variable as known or not-known (knownQ = false)
Triangle.prototype.setKnown = function (varName, knownQ) {
    switch (varName) {
    case "A":
    case "B":
    case "C":
        this.isAngleKnownQ[this.getNum(varName)] = knownQ;
        break;
    case "a":
    case "b":
    case "c":
        this.isSideKnownQ[this.getNum(varName)] = knownQ;
        break;
    default:
    }
};
// test if variable is known
Triangle.prototype.isKnown = function (varName) {
    switch (varName) {
    case "A":
    case "B":
    case "C":
        return this.isAngleKnownQ[this.getNum(varName)];
        break;
    case "a":
    case "b":
    case "c":
        return this.isSideKnownQ[this.getNum(varName)];
        break;
    default:
        return false;
    }
};
// get length of side with index i
Triangle.prototype.userSide = function (i) {
    return Math.round(this.sides[i] * scaleFactor);
};
// get user string of the variable in this triangle
Triangle.prototype.getUserStr = function (varName) {
    switch (varName) {
    case "A":
    case "B":
    case "C":
        if (this.isKnown(varName)) {
            return (toDegree(this.pts[this.getNum(varName)].getAngle()).toString() + "º");
        } else {
            return (this.angleLabels[this.getNum(varName)]);
        }
        break;
    case "a":
    case "b":
    case "c":
        if (this.isKnown(varName)) {
            return (this.userSide(this.getNum(varName)).toString());
        } else {
            return (this.sideLabels[this.getNum(varName)]);
        }
        break;
    default:
        return "";
    }
};
// draw the 3 sides
Triangle.prototype.drawSides = function (g) {
    // compute the centroid
    var ptC = new Point();
    ptC.averageMe(this.pts);
    var ptM = new Point();
    for (var i = 0; i < 3; i++) {
        ptM.averageMe([this.pts[i], this.pts[loop(i, 0, 2, 1)]]);
        ptM.interpolateMe(ptM, ptC, 1.4); // 1.4 = 7/5
        var side = Math.round(this.getValue(toSideName(loop(i + 1, 0, 2, 1))));
        g.fillText(side.toString(), ptM.x - 10, ptM.y + 5, 100);
    }
};
// draw the 3 angles
Triangle.prototype.drawAngles = function (g) {
    var angSum = 0;
    var angDescr = "";
    var angs = [];
    for (var i = 0; i < 3; i++) {
        var d = 30;
        var ang = toDegree(this.pts[i].getAngle());
        if (i < 2) {
            angSum += ang;
        } else {
            ang = 180 - angSum;
        }
        angs[i] = ang;
        angDescr += ang + "° + ";
        if (ang == 90) {
            g.drawBox(this.pts[i], 25, this.pts[i].angleOut - PI / 2);
        } else {
            if (ang > 90) {
                d = 30 - (ang - 90) / 6;
            } else {}
            g.drawAngle(this.pts[i], d, 'rgba(0, 0, 255, 0.3)');
        }
        var aMid = (this.pts[i].angleIn + this.pts[i].angleOut) / 2;
        var txtPt = new Point();
        txtPt.x = this.pts[i].x + (d + 15) * cos(aMid) - 0;
        txtPt.y = this.pts[i].y + (d + 15) * sin(aMid) - 0;
        g.textAlign = 'left';
        g.fillStyle = "rgba(0, 0, 255, 1)";
        g.fillText(ang + "°", txtPt.x - 10, txtPt.y + 5, 100);
    }
    // angDescr is not used!
};
// draw the 3 perpendicular bisectors
Triangle.prototype.drawPerpBisectors = function (g) {
    var lns = [];
    for (var i = 0; i < 3; i++) {
        var ln = new Line(this.pts[i], this.pts[loop(i, 0, 2, 1)]);
        var perpLn = ln.clone();
        perpLn.rotateMidMe(PI / 2);
        perpLn.setLength(1400);
        lns.push(perpLn);
        g.drawLine(perpLn.a, perpLn.b);
        g.drawBox(ln.getMidPoint(), 15, perpLn.getAngle());
    }
    return lns[0].getIntersection(lns[1]);
}
// draw the 3 medians
Triangle.prototype.drawMedians = function (g) {
    var lns = [];
    for (var i = 0; i < 3; i++) {
        var ln = new Line(this.pts[i], this.pts[loop(i, 0, 2, 1)]);
        var mid = ln.getMidPoint();
        var medianLn = new Line(mid, this.pts[loop(i + 1, 0, 2, 1)]);
        lns.push(medianLn);
        g.drawLine(medianLn.a, medianLn.b);
        g.drawPoint(mid, 'black', 2);
    }
    return lns[0].getIntersection(lns[1]);
}
// get the altitude of vertex ptNum
Triangle.prototype.getAltitude = function (ptNum) {
    var aa = this.pts[loop(ptNum, 0, 2, 1)];
    var ln = new Line(aa, this.pts[loop(ptNum, 0, 2, 2)]);
    var iPt = ln.getClosestPoint(this.pts[ptNum]);
    var htLn = new Line(this.pts[ptNum], iPt);
    return Math.round(htLn.getLength() * scaleFactor);
};
// draw the altitude from vertex ptNum.
Triangle.prototype.drawAltitude = function (g, ptNum, drawAsSegmentQ, showTextQ) {
    var drawAsSegmentQ = typeof drawAsSegmentQ !== 'undefined' ? drawAsSegmentQ : true;
    var showTextQ = typeof showTextQ !== 'undefined' ? showTextQ : true;
    var ln = new Line(this.pts[loop(ptNum, 0, 2, 1)], this.pts[loop(ptNum, 0, 2, 2)]);
    var iPt = ln.getClosestPoint(this.pts[ptNum]);
    var htLn = new Line(this.pts[ptNum], iPt);
    g.drawBox(htLn.b, 10, htLn.getAngle() + PI); // the perpendicular right-angles
    if (showTextQ) {
        var htMid = htLn.getMidPoint();
        g.fillText(Math.round(htLn.getLength() * scaleFactor), htMid.x + 2, htMid.y + 10)
    }
    var iSegPt = ln.getClosestPoint(this.pts[ptNum], true);
    var extendedLn = new Line(iSegPt, iPt);
    if (extendedLn.getLength() > 20) {
        extendedLn.setLength(extendedLn.getLength() - 20);
        extendedLn.setLength(extendedLn.getLength() + 30, false);
        g.drawLine(extendedLn.a, extendedLn.b);
    }
    if (!drawAsSegmentQ) htLn.setLength(1400);
    g.drawLine(htLn.a, htLn.b, 'blue');
    return htLn;
};
// draw the orthocenter
Triangle.prototype.drawOrthocenter = function (g) {
    var ln0 = this.drawAltitude(g, 0, true, false);
    var ln1 = this.drawAltitude(g, 1, true, false);
    var ln2 = this.drawAltitude(g, 2, true, false);
    var iPt = ln0.getIntersection(ln1)
    g.drawPoint(iPt);
    g.setLineDash([5, 15]);
    g.drawLine(ln0.b, iPt);
    g.drawLine(ln1.b, iPt);
    g.drawLine(ln2.b, iPt);
    g.setLineDash([]);
    return iPt;
}
// get the angle bisector of vertex i
Triangle.prototype.getAngleBisector = function (i) {
    // get a horizontal line
    var p = this.pts[i].clone();
    var ln = new Line(p.clone(), new Point(p.x + 100, p.y));
    // half the angle at vertex + angleIn = (out - in)/2 + in = (out + in)/2
    var angle = -(this.pts[i].angleIn + this.pts[i].angleOut) / 2;
    // rotate horizontal line about vertex by this angle, clockwise by negative
    ln.rotatePointMe(p, angle);
    return ln;
}
// get the incenter, center of inscribed circle
Triangle.prototype.getInCenter = function () {
    var line0 = this.getAngleBisector(0);
    var line1 = this.getAngleBisector(1);
    return line0.getIntersection(line1);
    // note: another method is to use the formula:
    // if ΔABC has angles at A, B, C, and opposite sides a, b, c,
    // then in-center = (aA + bB + cC)/(a + b + c).
    // see: https://en.wikipedia.org/wiki/Incircle_and_excircles_of_a_triangle
}
// draw the inscribed circle, or incircle
Triangle.prototype.drawInCircle = function (g) {
    var mid = this.getInCenter();
    var radius = 2 * this.getHeronArea() / this.getPerimeter();
    g.drawCircle(mid, radius);
}
// draw the 3 angle bisectors
Triangle.prototype.drawAngleBisectors = function (g) {
    var lns = [];
    for (var i = 0; i < 3; i++) {
        var ln = this.getAngleBisector(i);
        ln.setLength(1400);
        lns.push(ln);
        g.drawLine(ln.a, ln.b);
    }
    return lns[0].getIntersection(lns[1]);
}
// draw the 3 angle bisectors from vertex, return in-center
Triangle.prototype.drawSimpleBisectors = function (g) {
    var mid = this.getInCenter();
    for (var i = 0; i < 3; i++) {
        g.drawLine(this.pts[i], mid);
    }
    return mid;
}
// get the angle trisectors of vertex i
Triangle.prototype.getAngleTrisectors = function (i) {
    // get two horizontal line
    var p = this.pts[i].clone();
    var ln1 = new Line(p.clone(), new Point(p.x + 100, p.y));
    var ln2 = new Line(p.clone(), new Point(p.x + 100, p.y));
    // rotate both by angleIn, anti-clockwise
    ln1.rotatePointMe(p, - this.pts[i].angleIn);
    ln2.rotatePointMe(p, - this.pts[i].angleIn);
    // one third of angle at vertex
    var angle = this.pts[i].getAngle() / 3;
    // rotate first line by this angle, anti-clockwise
    ln1.rotatePointMe(p, - angle);
    // rotate second line by double this angle, anti-clockwise
    ln2.rotatePointMe(p, - 2 * angle);
    return [ln1, ln2];
}
// draw the angle trisectors from vertex, return 3 points
Triangle.prototype.drawSimpleTrisectors = function (g) {
    var pair0 = this.getAngleTrisectors(0);
    var pair1 = this.getAngleTrisectors(1);
    var pair2 = this.getAngleTrisectors(2);
    // find intersections and draw them
    // for vertices 0 and 1, pair0[1] intersect with pair1[0]
    var pt1 = pair0[1].getIntersection(pair1[0]);
    g.drawLine(this.pts[0], pt1);
    g.drawLine(this.pts[1], pt1);
    // for vertices 1 and 2, pair1[1] intersect with pair2[0]
    var pt2 = pair1[1].getIntersection(pair2[0]);
    g.drawLine(this.pts[1], pt2);
    g.drawLine(this.pts[2], pt2);
    // for vertices 2 and 0, pair2[1] intersect with pair0[0]
    var pt3 = pair2[1].getIntersection(pair0[0]);
    g.drawLine(this.pts[2], pt3);
    g.drawLine(this.pts[0], pt3);
    return [pt1, pt2, pt3]; // 3 intersection points
}
// get length of side with index n
Triangle.prototype.getLength = function (n) {
    var pt1 = this.pts[n];
    var pt2 = this.pts[loop(n, 0, 2, 1)];
    return pt2.dist(pt1) * scaleFactor;
};
// get perimeter of this triangle
Triangle.prototype.getPerimeter = function () {
    return this.getLength(0) + this.getLength(1) + this.getLength(2);
};
// get the area of this triangle
Triangle.prototype.getHeronArea = function () {
    var p = this.getPerimeter() / 2;
    var heron = p;
    for (var i = 0; i < 3; i++) {
        heron *= p - this.getLength(i);
    }
    return Math.sqrt(heron);
};
// make this triangle from source points srcPts
Triangle.prototype.fromPts = function (srcPts) {
    for (var i = 0; i < 3; i++) {
        this.pts[i].x = srcPts[i].x;
        this.pts[i].y = srcPts[i].y;
    }
    this.updateMe();
};

/**
 The Shape Object

 */
// construct a shape at (x,y) with color and label
function Shape(x, y, label, color) {
    color = color || 'pink'; // default: pink
    this.x = x;
    this.y = y;
    this.radius = 9; // radius of a hit
    this.label = label;
    this.color = color;
    this.keepX = false; // move vertically
    this.keepY = false; // move horizontally
};
// representation of shape
Shape.prototype.toString = function () {
    // round coordinates to three decimals
    return '(' + round3(this.x) + ', ' + round3(this.y) + ')';
};
// test if (mx, my) hits this shape
Shape.prototype.hitTest = function (mx, my) {
    var dx = mx - this.x;
    var dy = my - this.y;
    return (norm(dx, dy) < this.radius * this.radius);
};


/**
 The Geometry Script
 */

// load style sheet for geometry
function loadGeometryStyle() {
    var s = '';
    s += '<style>'
    s += '.frame { position:relative; width:360px; height:280px; border: 1px solid blue; border-radius: 9px; margin:auto; display:block;}'
    s += '.can { z-index:1; }'
    s += '.title { font: 12pt arial; font-weight: bold; position:absolute; top:10px; left:0px; width:360px; text-align:center; pointer-events: none;}'
    s += '.info { font: 10pt arial; font-weight: bold; color: #6600cc; position:absolute; top:31px; left:0px; width:360px; text-align:center; pointer-events: none;}'
    s += '.btn { display: inline-block; position: relative; text-align: center; margin: 2px; text-decoration: none; font: bold 14px/25px Arial, sans-serif; color: #268; border: 1px solid #88aaff; border-radius: 10px;cursor: pointer; background: linear-gradient(to top right, rgba(170,190,255,1) 0%, rgba(255,255,255,1) 100%); outline-style:none;}'
    s += '.btn:hover { background: linear-gradient(to top, rgba(255,255,0,1) 0%, rgba(255,255,255,1) 100%); }'
    s += '</style>'
    document.write(s);
};

/**
 The Frame Object

 */
// construct a frame with mode, type, width and height
function Frame(mode, type, w, h) {
    // attributes
    mode = mode || 'type'; // default: type
    type = type || 'any';  // default: any
    w = w || 360;          // default: 360, even 0 is 360
    h = h || 280;          // default: 280, even 0 is 280

    // constants
    var numShapes = 3; // for 3 vertices

    // locals
    var shapes;    // to cover 3 vertices
    var tri;       // the triangle
    var dragN = 0; // index of vertex being dragged
    var dragging = false; // indicates dragging
    var dragHoldX, dragHoldY; // dragging variables
    var circle = false; // no circle

    // create the DOM elements for Frame
    var frame = document.createElement('div');
    frame.className = 'frame';
    frame.style.width = '' + w + 'px';
    frame.style.height = '' + h + 'px';

    var canvas = document.createElement('canvas');
    canvas.className = 'can';
    canvas.width = w * ratio;
    canvas.height = h * ratio;
    canvas.style.width = w + "px";
    canvas.style.height = h + "px";

    var title = document.createElement('div');
    title.className = 'title';

    var info = document.createElement('div');
    info.className = 'info';
    info.innerHTML = '&copy; 2021, based on MathsIsFun and MarkDeep.';

    var button = document.createElement('button');
    button.className = 'btn';
    // override some style of btn
    button.setAttribute('style', 'z-index:2; position:absolute; right:3px; bottom:3px;');
    setButton();

    // link up elements
    frame.appendChild(canvas);
    frame.appendChild(title);
    frame.appendChild(info);
    frame.appendChild(button);

    // get context and draw shapes
    var g = canvas.getContext("2d");
    g.setTransform(ratio, 0, 0, ratio, 0, 0);
    // get the shapes
    tri = new Triangle();
    tri.setAllKnown(false);
    tri.setLabels("", "", "", "", "", "");
    makeShapes(type);
    drawShapes(mode);

    // add listener to Canvas
    canvas.addEventListener("mousedown", onMouseDown, false);
    canvas.addEventListener('touchstart', onTouchStart, false);
    canvas.addEventListener("mousemove", doPointer, false);

    /* ----------------------------------------------------- *
       All shape routines.
     * ----------------------------------------------------- */

    // make the shapes to cover the 3 vertices A, B, C.
    function makeShapes(type) {
        var tempColor = "rgb(" + 0 + "," + 0 + "," + 255 + ")";
        var pos = getABC(type);
        shapes = []; // reset shapes to empty
        for (var i = 0; i < numShapes; i++) {
            // tempColor is a constant.
            shapes.push(new Shape(pos[i][0], pos[i][1], pos[i][2], tempColor));
        }
    }

    // draw the shapes: the triangle with A, B, C, with mode.
    function drawShapes(mode) {
        g.clearRect(0, 0, g.canvas.width, g.canvas.height) // clean up
        // redraw the triangle
        // draw the triangle and fill with pale yellow
        g.drawPoly(shapes, 2, 'rgba(255, 255, 0, 0.1)', 'rgba(136, 136, 204, 1)');
        for (var i = 0; i < numShapes; i++) {
            // mark the shapes at vertices with labels
            g.drawMark(shapes[i], 2);
            // set triangle vertices from shapes
            tri.setxy(i, shapes[i].x, shapes[i].y);
        }
        tri.updateMe();
        // drawing based on mode
        switch(mode) {
        case 'median' : modeMedian(); break;
        case 'circum' : modeCircum(); break;
        case 'incircle' : modeInCircle(); break;
        case 'ortho' : modeOrtho(); break;
        case 'area' : modeArea(); break;
        case 'perim' : modePerim(); break;
        case 'inequal' : modeInEqual(); break;
        case 'angles' : modeAngles(); break;
        case 'choose' : modeChoose(); break;
        case 'bisect' : modeBisect(); break;
        case 'trisect' : modeTrisect(); break;
        case 'incenter' : modeIncenter(); break;
        case 'morley' : modeMorley(); break;
        default : modeType(); break; /* mode: type */
        }
    }

    // set the button according to mode
    function setButton() {
        // these modes need the Circle button
        var needCircle = mode == 'bisect' || mode == 'morley';
        button.innerHTML = needCircle ? 'Circle' : 'Reset'
        button.onclick = needCircle ? showCircle : resetDraw; // set onclick to a function
    }

    // show inscribed circle
    function showCircle() {
        circle = true;
        // draw the inscribed circle, or incircle
        tri.drawInCircle(g);
        // change button
        button.innerHTML = 'Reset';
        button.onclick = resetDraw; // set onclick to a function
    }

    // reset drawing
    function resetDraw() {
        circle = false;
        resetShapes();
        setButton();
    }

    /* ----------------------------------------------------- *
       All drawing mode, require: g, tri, and shapes[].
     * ----------------------------------------------------- */

    // for mode: median
    function modeMedian() { // g, tri
        var mid = tri.drawMedians(g);
        // mark the centroid
        g.drawPoint(mid);
        title.innerHTML = "Three Medians";
        info.innerHTML = "Meet at centroid: " + mid.toString();
    }

    // for mode: circum
    function modeCircum() { // g, tri, shapes[0]
        var mid = tri.drawPerpBisectors(g);
        // mark the circumcenter
        g.drawPoint(mid);
        // draw the circumscribed circle, or circumcirle
        var radLn = new Line(mid, new Point(shapes[0].x, shapes[0].y));
        g.drawCircle(mid, radLn.getLength());
        title.innerHTML = "Three Perpendicular Bisectors";
        info.innerHTML = "Meet at circum-center: " + mid.toString();
    }

    // for mode: incircle
    function modeInCircle() { // g, tri
        var mid = tri.drawAngleBisectors(g);
        // mark the incenter
        g.drawPoint(mid);
        // draw the inscribed circle, or incircle
        var radius = 2 * tri.getHeronArea() / tri.getPerimeter();
        g.drawCircle(mid, radius);
        title.innerHTML = "Three Angle Bisectors";
        info.innerHTML = "Meet at in-center: " + mid.toString();
    }

    // for mode: bisect
    function modeBisect() { // g, tri
        var mid = tri.drawSimpleBisectors(g);
        // mark the incenter
        g.drawPoint(mid, 'green', 3);
        title.innerHTML = "All Angle Bisectors";
        info.innerHTML = "Meet at a single point!";
        // info.innerHTML = 'A: ' + tri.pts[0].toString() + ', B: ' + tri.pts[1].toString() + ', C: ' + tri.pts[2].toString() + ', mid: ' + mid.toString();
        if (circle) tri.drawInCircle(g);
    }

    // for mode: incenter
    function modeIncenter() { // g, tri
        var mid = tri.drawSimpleBisectors(g);
        // mark the incenter
        g.drawPoint(mid, 'green', 3);
        tri.drawInCircle(g);
        // compute the touching points
        var touch = [];
        for (var j = 0; j < 3; j++) {
           var side = new Line(tri.pts[j], tri.pts[loop(j, 0, 2, 1)]);
           touch[j] = side.getClosestPoint(mid);
           g.drawLine(mid, touch[j]);
           // two perpendicular right-angles
           var angle = Math.atan2(mid.y - touch[j].y, mid.x - touch[j].x);
           g.drawBox(touch[j], 10, angle);
           g.drawBox(touch[j], 10, angle - PI/2);
        }
        // 3 pairs of congruent triangles
        g.drawPoly([tri.pts[0], touch[0], mid], 2, 'rgba(0, 255, 0, 0.3)');
        g.drawPoly([tri.pts[0], touch[2], mid], 2, 'rgba(0, 255, 0, 0.3)');
        g.drawPoly([tri.pts[1], touch[1], mid], 2, 'rgba(0, 0, 255, 0.2)');
        g.drawPoly([tri.pts[1], touch[0], mid], 2, 'rgba(0, 0, 255, 0.2)');
        g.drawPoly([tri.pts[2], touch[2], mid], 2, 'rgba(255, 255, 0, 0.5)');
        g.drawPoly([tri.pts[2], touch[1], mid], 2, 'rgba(255, 255, 0, 0.5)');
        // text
        title.innerHTML = "Incenter: meeting point of all angle bisectors";
        info.innerHTML = "The triangles in same color are congruent.";
    }

    // for mode: trisect
    function modeTrisect() { // g, tri
        g.clearRect(0, 0, g.canvas.width, g.canvas.height) // clean up
        // Triangle A, B, C as vertices: P, B, E
        var P = shapes[0], B = shapes[1], E = shapes[2];
        // P moves horizontally
        P.label = 'P'; P.keepY = true;
        // E moves vertically
        E.label = 'E'; E.keepX = true;
        // B is fixed
        B.keepX = true; B.keepY = true;

        // var x = 50, y = 80;
        // var x = B.x, y = P.y;
        // var width = w * 0.5;
        var width = B.y - P.y;
        // Square ABCD
        // var A = new Point(x, y);
        // var B = new Point(A.x, A.y + width);
        // var C = new Point(A.x + width , A.y + width); // for a square
        // var D = new Point(A.x + width , A.y);
        // treat B = [0,0], adjust all coordinate expressions.
        // B [0,0] -> [A.x, A.y + width]
        // C [100,0] -> [A.x + width , A.y + width]
        // A [0,100] -> [x, y]
        // D [100,100] -> [A.x + width , A.y]
        function mark(x, y, label) {
           return new Shape(B.x + x * width/100, P.y + (100 - y) * width/100, label);
        }
        // square ABCD
        var A = mark(  0, 100, 'A'),
         // B = mark(  0,   0, 'B'),
            C = mark(100,   0, 'C'),
            D = mark(100, 100, 'D');
        // Two creases: EF and GH
        // var rise = 28; // 2h almost any height
        // 2h/100 = (B.y - E.y)/(B.y - P.y) = (B.y - E.y) / width
        // h = 50 * (B.y - E.y)/width
        var rise = 50 * (B.y - E.y) / width;
        var // E = mark(0, 2 * rise, 'E'),
            F = mark(100, 2 * rise, 'F');
        var G = mark(  0, rise, 'G'),
            H = mark(100, rise, 'H');
        // P: a point along AD
        // use point P to define the angle
        var angle = atan2(width, P.x - B.x),
            alpha = angle/3,
            beta = 2 * alpha;
        // var P = mark([100, 100 * tan(angle)]); // if angle < pi/4, 45 degrees
        var J = mark(100, 100 * tan(beta), 'J'),
            K = mark(100, 100 * tan(alpha), 'K');
        if (J.y < P.y) J = mark(100/tan(beta), 100, 'J');
        // ST: vertical trisector
        var Tx = rise / sin(beta), Sy = Tx / tan(alpha),
            T = mark(Tx, 0, 'T'),
            S = mark(0, Sy, 'S');
        var Z = mark(Tx + Tx * cos(beta), rise, 'Z'); // [x + x cos 2α, h]
        // then EU = (y - 2h) * tan α = x1,
        var Ux = (Sy - 2 * rise) * tan(alpha),
            U = mark(Ux, 2 * rise, 'U'); // [x1, 2h]
        var X = mark(Ux + Ux * cos(beta), 2 * rise + Ux * sin(beta), 'X'); // [x1 + x1 cos 2α, 2h + x1 sin 2α]
        // and GV = (y - h) * tan α = x2,
        var Vx = (Sy - rise) * tan(alpha),
            V = mark(Vx, rise, 'V'); // [x2, h]
        var Y = mark(Vx + Vx * cos(beta), rise + Vx * sin(beta), 'Y'); // [x2 + x2 cos 2α, h + x2 sin 2α]
        var W = mark(Tx + Tx * cos(beta), 0, 'W'); // [Z.x, 0]
        // angle label, at B
        var aB = mark(0, 0, 'θ');

        // paint the square sheet
        g.drawPoly([A,B,C,D], 1, 'rgba(255, 99, 71, 0.3)', 'blue');
        g.drawMark(A);
        g.drawMark(B, 1, -12, 'green');
        g.drawMark(C);
        g.drawMark(D);
        // draw two parallel creases
        g.drawLine(E, F, 'yellow');
        g.drawLine(G, H, 'green');
        // draw other lines
        g.drawLine(B, P, 'blue', 2); // angle arm
        g.drawLine(B, J, 'green'); // trisector
        g.drawLine(B, K, 'lime'); // trisector
        g.drawLine(T, Z, 'green'); // parallel to BJ
        g.drawLine(E, X, 'lime'); // parallel to BK
        g.drawLine(G, Y, 'lime'); // parallel to BK
        g.drawLine(S, T, 'navy');
        g.drawLine(S, Z, 'navy');
        g.drawLine(Z, W, 'navy');
        // mark points
        g.drawMark(E, 1, -12, 'blue');
        g.drawMark(F);
        g.drawMark(G, 1, -12);
        g.drawMark(H);
        g.drawMark(P, 1, 5, 'blue');
        g.drawMark(J);
        g.drawMark(K);
        g.drawMark(S, 1, -12);
        g.drawMark(T, 1, -12, 'lime');
        g.drawMark(U, 1, -12);
        g.drawMark(V, 1, -12, 'lime');
        g.drawMark(W);
        g.drawMark(X);
        g.drawMark(Y);
        g.drawMark(Z);
        // mark right-angles
        g.drawBox(W, 10, PI);
        g.drawBox(Y, 10, - beta + PI/2);
        g.drawBox(Y, 10, - beta + PI);
        // mark congruent triangles
        g.drawPoly([B,Y,X], 1, 'rgba(255, 255, 0, 0.3)');
        g.drawPoly([B,Y,Z], 1, 'rgba(0, 255, 0, 0.3)');
        g.drawPoly([B,Z,W], 1, 'rgba(0, 0, 255, 0.2)');
        // mark angle
        g.drawMark(aB, 1, 12);

        title.innerHTML = "Angle Trisectors by Folding";
        info.innerHTML = 'angle: ' + round1(toDeg(angle)) + '°, move P along AD, move E along AB.';
        // info.innerHTML = 'B: ' + B + ', E: ' + E +  ', P: ' + P +
        //  ', angle: ' + round3(toDeg(angle)) + ', rise: ' + round3(rise) + ', width: ' + width;
    }

    // for mode: morley
    function modeMorley() { // g, tri
        var pts = tri.drawSimpleTrisectors(g);
        // mark the 3 points
        g.drawPoint(pts[0], 'red');
        g.drawPoint(pts[1], 'blue');
        g.drawPoint(pts[2], 'green');
        g.drawTriangle(pts[0], pts[1], pts[2], 'brown', 2);
        title.innerHTML = "All Angle Trisectors";
        info.innerHTML = "Meeting points form an equilateral triangle!";
        // info.innerHTML = 'A: ' + tri.pts[0].toString() + ', B: ' + tri.pts[1].toString() + ', C: ' + tri.pts[2].toString();
        if (circle) tri.drawInCircle(g);
    }

    // for mode: ortho
    function modeOrtho() { // g, tri
        var mid = tri.drawOrthocenter(g);
        // mark the orthocenter
        g.drawPoint(mid);
        title.innerHTML = "Three Altitudes";
        info.innerHTML = "Meet at ortho-center: " + mid.toString();
    }

    // for mode: area
    function modeArea() { // g, tri
        tri.setKnown("a", true);
        var base = Number(tri.getUserStr("a"));
        var ht = tri.getAltitude(0);
        var s = ''
        if (ht < 0.3 || base < 0.3) {
            s = "Is that a Triangle?";
        } else {
            s = "Area = &frac12; &times; ";
            s += base;
            s += " &times; ";
            s += ht;
            s += " = ";
            s += Math.round(0.5 * base * ht * 100) / 100;
        }
        tri.drawAltitude(g, 0);
        tri.drawSides(g);
        title.innerHTML = s;
        info.innerHTML = "By the formula shown.";
    }

    // for mode: perim
    function modePerim() { // g, tri
        tri.drawSides(g);
        var s = "Perimeter = ";
        var sum = 0;
        for (var i = 0; i < numShapes; i++) {
            var side = Math.round(tri.getValue(toSideName(i)));
            s += side + " + ";
            sum += side;
        }
        s = s.substring(0, s.length - 2);
        s += " = " + sum;
        title.innerHTML = s;
        info.innerHTML = "By adding 3 sides.";
    }

    // for mode: inequal
    function modeInEqual() { // g, tri
        tri.drawSides(g);
        var len0 = tri.userSide(0);
        var len1 = tri.userSide(1);
        var len2 = tri.userSide(2);
        var s = "";
        if (len0 >= Math.round(len1 + len2)) {} else {
            s += len0.toString();
            s += "&nbsp; <i>is less than</i> &nbsp;";
            s += len1.toString();
            s += " + ";
            s += len2.toString();
            s += " = ";
            s += Math.round(len1 + len2).toString();
        }
        title.innerHTML = s;
    }

    // for mode: angles
    function modeAngles() { // g, tri
        tri.drawAngles(g);
        var angs = tri.getAngles()
        var s = "";
        var okQ = true;
        for (var i = 0; i < numShapes; i++) {
            if (angs[i] == 180) okQ = false;
            s += angs[i] + "&deg; + ";
        }
        s = s.substring(0, s.length - 2);
        s += " = 180&deg;";
        if (!okQ) s = 'Is that a triangle?';
        title.innerHTML = s;
    }

    // for mode: choose
    function modeChoose() { // g, tri
        tri.drawAngles(g);
        var angs = tri.getAngles()
        var s = "";
        var okQ = true;
        for (var i = 0; i < numShapes; i++) {
            if (angs[i] == 180) okQ = false;
            s += angs[i] + "° + ";
        }
        s = s.substring(0, s.length - 2);
        s += " = 180°";
        if (!okQ) s = 'Is that a triangle?';
        title.innerHTML = s;
        info.innerHTML = 'A: ' + tri.pts[0].toString() +
                         ', B: ' + tri.pts[1].toString() +
                         ', C: ' + tri.pts[2].toString();
        // a good one: A 89 64, B 24 262 C 331 202
    }

    // for mode: type (default)
    function modeType() { // g, tri
        tri.drawAngles(g);
        tri.drawSides(g);
        var angs = tri.getAngles()
        var angleAbove90 = false;
        var angle90 = false;
        var angleSame = false;
        var angleEqual = -1;
        var angleAll60 = true;
        var angleZero = false;
        for (var i = 0; i < 3; i++) {
            if (angs[i] == 0) angleZero = true;
            if (angs[i] > 90) angleAbove90 = true;
            if (angs[i] == 90) angle90 = true;
            if (angs[i] == angs[loop(i, 0, 2, 1)]) {
                angleSame = true;
                angleEqual = i;
            }
            if (angs[i] != 60) angleAll60 = false;
        }
        var descr = '';
        var descrColor = '';
        var infom = '';
        if (angleZero) {
            descr = 'Is that a Triangle?';
            infom = 'An angle is Zero!';
        } else if (angleAbove90) {
            descrColor = '#ff0000';
            if (angleSame) {
                descr = "Obtuse Isosceles Triangle";
                infom = "Has an angle more than 90&deg;, and also two equal angles and two equal sides";
            } else {
                descr = "Obtuse Triangle";
                infom = "Has an angle more than 90&deg;";
            }
        } else if (angle90) {
            descrColor = '#00aa00';
            if (angleSame) {
                descr = "Right Isosceles Triangle";
                infom = "Has a right angle (90&deg;) and also two equal angles and two equal sides";
            } else {
                descr = "Right Triangle";
                infom = "Has a right angle (90&deg;)";
            }
        } else {
            descrColor = 'black';
            if (angleSame) {
                if (angleAll60) {
                    descr = "Equilateral Triangle";
                    infom = "Three equal sides and three equal angles of 60&deg; each";
                } else {
                    descr = "Acute Isosceles Triangle";
                    infom = "All angles are less than 90&deg; and has two equal sides and two equal angles";
                }
            } else {
                descr = "Acute Triangle";
                infom = "All angles are less than 90&deg;";
            }
        }
        title.innerHTML = descr;
        title.style.color = descrColor;
        info.innerHTML = infom;
    }

    // update the shapes
    function updateShapes() { // mode
        drawShapes(mode);
    }

    // reset the shapes
    function resetShapes() { // mode, type
        makeShapes(type);
        drawShapes(mode);
    }

    /* ----------------------------------------------------- *
       All action responeses.
     * ----------------------------------------------------- */

    // handle MouseDown
    function onMouseDown(evt) { // el, shapes[]
        var highestIndex = -1;
        var bRect = canvas.getBoundingClientRect();
        var mouseX = (evt.clientX - bRect.left) * (canvas.width / ratio / bRect.width);
        var mouseY = (evt.clientY - bRect.top) * (canvas.height / ratio / bRect.height);
        // update the 3 points
        for (var i = 0; i < numShapes; i++) {
            if (shapes[i].hitTest(mouseX, mouseY)) {
                dragging = true;
                if (i > highestIndex) {
                    dragHoldX = mouseX - shapes[i].x;
                    dragHoldY = mouseY - shapes[i].y;
                    highestIndex = i;
                    dragN = i;
                }
            }
        }
        if (dragging) {
            if (evt.touchQ) {
                window.addEventListener('touchmove', onTouchMove, false);
            } else {
                window.addEventListener("mousemove", onMouseMove, false);
            }
        }
        if (evt.touchQ) {
            canvas.removeEventListener("touchstart", onTouchStart, false);
            window.addEventListener("touchend", onTouchEnd, false);
        } else {
            canvas.removeEventListener("mousedown", onMouseDown, false);
            window.addEventListener("mouseup", onMouseUp, false);
        }
        if (evt.preventDefault) {
            evt.preventDefault();
        }
        else if (evt.returnValue) {
            evt.returnValue = false;
        }
        return false;
    }

    // handle MouseUp
    function onMouseUp() { // el
        canvas.addEventListener("mousedown", onMouseDown, false);
        window.removeEventListener("mouseup", onMouseUp, false);
        if (dragging) {
            dragging = false;
            window.removeEventListener("mousemove", onMouseMove, false);
        }
    }

    // handle MouseMove
    function onMouseMove(evt) { // el, shapes[], mode
        var posX;
        var posY;
        var shape = shapes[dragN]; // shape to be changed by mouse
        var shapeRad = shape.radius;
        var minX = shapeRad;
        var maxX = canvas.width - shapeRad;
        var minY = shapeRad;
        var maxY = canvas.height - shapeRad;
        var bRect = canvas.getBoundingClientRect();
        var mouseX = (evt.clientX - bRect.left) * (canvas.width / ratio / bRect.width);
        var mouseY = (evt.clientY - bRect.top) * (canvas.height / ratio / bRect.height);
        posX = mouseX - dragHoldX;
        posX = (posX < minX) ? minX : ((posX > maxX) ? maxX : posX);
        posY = mouseY - dragHoldY;
        posY = (posY < minY) ? minY : ((posY > maxY) ? maxY : posY);
        // modify shapes[]
        if (shape.keepX && shape.keepY) { } // no change
        else if (shape.keepY) shape.x = posX; // change X only
        else if (shape.keepX) shape.y = posY; // change Y only
        else { // change both X and Y
            shape.x = posX;
            shape.y = posY;
        }
        updateShapes();
    }

    // handle TouchStart
    function onTouchStart(evt) {
        var touch = evt.targetTouches[0];
        evt.clientX = touch.clientX;
        evt.clientY = touch.clientY;
        evt.touchQ = true;
        onMouseDown(evt)
    }

    // handle TouchEnd
    function onTouchEnd(evt) { // el
        canvas.addEventListener('touchstart', onTouchStart, false);
        window.removeEventListener("touchend", onTouchEnd, false);
        if (dragging) {
            dragging = false;
            window.removeEventListener("touchmove", onTouchMove, false);
        }
    }

    // handle TouchMove
    function onTouchMove(evt) {
        var touch = evt.targetTouches[0];
        evt.clientX = touch.clientX;
        evt.clientY = touch.clientY;
        evt.touchQ = true;
        onMouseMove(evt);
        evt.preventDefault();
    }

    // handle doPointer
    function doPointer(evt) { // el, shapes[]
        var bRect = canvas.getBoundingClientRect();
        var mouseX = (evt.clientX - bRect.left) * (canvas.width / ratio / bRect.width);
        var mouseY = (evt.clientY - bRect.top) * (canvas.height / ratio / bRect.height);
        var inQ = false;
        for (var i = 0; i < numShapes; i++) {
            if (shapes[i].hitTest(mouseX, mouseY)) {
                inQ = true;
            }
        }
        if (inQ) {
            document.body.style.cursor = "pointer";
        } else {
            document.body.style.cursor = "default";
        }
    }

    return frame; // return the frame for use
}; // object Frame

/**
 Extension of 2D context of canvas.
 */
// draw a point on the 2D context
CanvasRenderingContext2D.prototype.drawPoint = function (p, color, size) {
    color = color || 'navy'; // default 'navy'
    size = (typeof size  == 'number') ? size : 3; // default 3, allow 0.
    this.beginPath();
    this.fillStyle = color;
    this.arc(p.x, p.y, size, 0, 2 * PI);
    this.fill();
};
// draw a line on the 2D context
CanvasRenderingContext2D.prototype.drawLine = function (p, q, color, width) {
    color = color || 'black'; // default 'black'
    width = width || 1; // default 1, even 0 is 1.
    this.strokeStyle = color;
    this.lineWidth = width;
    this.beginPath();
    this.moveTo(p.x, p.y);
    this.lineTo(q.x, q.y);
    this.stroke();
};
// draw a triangle on the 2D context
CanvasRenderingContext2D.prototype.drawTriangle = function (p, q, r, color, width) {
    color = color || 'black'; // default 'black'
    width = width || 1; // default 1, even 0 is 1.
    this.strokeStyle = color;
    this.lineWidth = width;
    this.beginPath();
    this.moveTo(p.x, p.y);
    this.lineTo(q.x, q.y);
    this.lineTo(r.x, r.y);
    this.lineTo(p.x, p.y);
    this.stroke();
};
// draw an arc at vertex on the 2D context
CanvasRenderingContext2D.prototype.drawAngle = function (vertex, radius, color, width) {
    color = color || 'pink'; // default 'pink'
    width = width || 1; // default 1, even 0 is 1.
    this.fillStyle = color;
    this.lineWidth = width;
    this.beginPath();
    this.moveTo(vertex.x, vertex.y);
    this.arc(vertex.x, vertex.y, radius, vertex.angleIn, vertex.angleOut, false); // false = clockwise
    this.closePath();
    this.fill();
};
// draw a circle on the 2D context
CanvasRenderingContext2D.prototype.drawCircle = function (center, radius, color, width) {
    color = color || 'orange'; // default 'orange'
    width = width || 1; // default 1, even 0 is 1.
    this.strokeStyle = color;
    this.lineWidth = width;
    this.beginPath();
    this.arc(center.x, center.y, radius, 0, 2 * PI); // default: false = clockwise
    this.stroke();
};
// draw a box on the 2D context, at corner, rotate by angle anti-clockwise (up) from x-axis.
CanvasRenderingContext2D.prototype.drawBox = function (corner, size, angle, color, width) {
    color = color || 'blue'; // default 'blue'
    width = width || 1; // default 1, even 0 is 1.
    // compute the vertices of the rotated unit square
    // rotation matrix: [[cos θ, -sin θ], [sin θ, cos θ]]
    // [[cos θ, -sin θ], [sin θ, cos θ]] . [0, 0] = [0, 0]
    // [[cos θ, -sin θ], [sin θ, cos θ]] . [1, 0] = [cos θ, sin θ]
    // [[cos θ, -sin θ], [sin θ, cos θ]] . [1, 1] = [cos θ - sin θ, cos θ + sin θ]
    // [[cos θ, -sin θ], [sin θ, cos θ]] . [0, 1] = [-sin θ, cos θ]
    var c = cos(angle), s = sin(angle);
    var v = [[0, 0], [c, s], [c - s, c + s], [-s, c], [0, 0]];
    this.strokeStyle = color;
    this.lineWidth = width;
    this.beginPath();
    this.moveTo(corner.x, corner.y); // initial i = 0
    for (var i = 1; i < v.length; i++) {
        this.lineTo(corner.x + size * v[i][0], corner.y + size * v[i][1]);
    }
    this.stroke();
};
// draw a shape mark with haze on the 2D context
CanvasRenderingContext2D.prototype.drawMark = function (shape, radius, offset, haze) {
    radius = (typeof radius  == 'number') ? radius : 1; // default 1, allow 0.
    offset = (typeof offset  == 'number') ? offset : 5; // default 5, allow 0.
    haze = haze || "rgba(0, 0, 255, 0.3)"; // default blue haze
    // the blue haze around vertex, radius 8
    this.fillStyle = haze;
    this.beginPath();
    this.arc(shape.x, shape.y, 4 * radius, 0, 2 * PI, false);
    this.closePath();
    this.fill();
    // the black dot at vertex, radius 2
    this.fillStyle = "rgba(0, 0, 0, 0.8)"; // black, weight 0.8
    this.beginPath();
    this.arc(shape.x, shape.y, radius, 0, 2 * PI, false);
    this.closePath();
    this.fill();
    // the label at the vertex
    this.font = "14px Arial";
    this.fillText(shape.label, shape.x + offset, shape.y - offset, 100); // max width = 100
};
// draw a polygon on the 2D context
CanvasRenderingContext2D.prototype.drawPoly = function (vertices, width, color, stroke) {
    width = width || 1; // default 1, even 0 is 1.
    this.lineWidth = width;
    if (color) this.fillStyle = color;
    if (stroke) this.strokeStyle = stroke;
    var n = vertices.length;
    if (n > 0) {
        this.beginPath();
        this.moveTo(vertices[n-1].x, vertices[n-1].y); // initial last vertex
        for (var j = 0; j < vertices.length; j++) {
            this.lineTo(vertices[j].x, vertices[j].y);
        }
    }
    if (color) this.fill();
    if (stroke) this.stroke();
};

/**
 Part 2: from Markdeep.
 * Collect all nodes for geometry processing,
 * Process each node, replacing by a frame from Geometry.
 */

// transform the node
function transform(node) {
    var source = node.innerHTML;
    // parse the source, determine mode and type
    var words = source.parse('wcnncnncnn');
    if (words.length !== 10) {
        alert('Invalid syntax: ' + words.join(' ') + '\nExpect 10 words. Ignored!');
        words = ['type', 'any'];
    }
    var mode = words.shift(); // the first word
    var type = words.join(' '); // clean up whitespaces
    return new Frame(mode, type);
}

// process all geometry nodes
function processGeometry() {
   // Collect all nodes that will receive geometry processing
   var geometryNodeArray = toArray(document.getElementsByClassName('geometry'))
                   .concat(toArray(document.getElementsByTagName('geometry')));

   // Process all nodes, replacing them as we progress
   for (var i = 0; i < geometryNodeArray.length; ++i) {
       var oldNode = geometryNodeArray[i];
       var newNode = transform(oldNode);
       oldNode.parentNode.replaceChild(newNode, oldNode);
   }
}

/**
 Part 3: Main Action.
 * set up stylesheet for Frame and elements,
 * find all nodes of geometry class or tag,
 * parse the node content and generate a Frame node,
 * replace each <geometry> node by a Frame node.
 */

// Start from here.
loadGeometryStyle(); // load the style CSS for frame, canvas, title, info, button
processGeometry();   // find and process all <geometry> nodes

})(); // invoke single function

/**
 Part 4: Origami, using the same idea.
 * set up stylesheet for Board and elements,
 * find all nodes of origami class or tag,
 * parse the node content and generate a Board node,
 * embed the Board node inside a Panel node with title and button,
 * replace each <origami> node by a Panel node.
 *
 * Elements of the board are pieces.
 * Animation of pieces is done by CSS, via keyframes.
 */

// single function application for <origami>
(function() {
'use strict';

// Constants
var size = 200;
// Safari CSS animation needs the -webkit- prefix, but not for FireFox and others.
var isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
// This uses negative look-arounds to exclude Chrome, Edge, and all Android browsers
// that include the Safari name in their user agent.
// See https://stackoverflow.com/questions/7944460/detect-safari-browser

/**
 The Utilities for Origami

*/
// prefix fixing for Safari CSS animation
function fix(word) {
    return (isSafari ? '-webkit-' : '') + word;
}
// coordinate transform: (x, 100 - y) means [x,y]
function modify(pair) {
    return '' + pair[0] + '% ' + (100 - pair[1]) + '%';
}
// polygon with coordinate transforms
function polygon(points) {
    var poly = [];
    points.forEach(function (pair) {
        poly.push(modify(pair));
    });
    return 'polygon(' + poly.join(', ') + ');';
}
// polygon(0% 100%, 100% 100%, 100% 0%);


// load style sheet for geometry
function loadOrigamiStyle() {
    var s = '';
    s += '<style>'
    s += '.board { position:relative; width:'+size+'px; height:'+size+'px; border:1px solid coral; display:block;}'
    s += '.piece { position:absolute; width:100%; height:100%; top:0%; left:0%; }'
    s += '.txtfont { font: 12pt arial; } '
    s += '</style>'
    document.write(s);
}

// compute a point vertically above
function above(point, h) {
    return [point[0], point[1] + h];
}

// compute a point horizontally right
function right(point, h) {
    return [point[0] + h, point[1]];
}

// compute a point reflected about a line of slope tan θ
// flip matrix:
//    [ cos 2θ   sin 2θ] [x] = [x cos 2θ + y sin 2θ]
//    [ sin 2θ  -cos 2θ] [y]   [x sin 2θ - y cos 2θ]
function reflect(point, angle) {
    var x = point[0], y = point[1];
    var c = cos(2 * angle), s = sin(2 * angle);
    return [x * c + y * s, x * s - y * c];
}

// compute points for a piece with angle
function polyPoints(angle) {
    var points = [];
    points.push([0,0]);
    points.push([100,0]);
    points.push([100, 100 * tan(angle)]); // y/100 = tan θ, so y = 100 * tan θ
    // the div dimensions will clip the shape if the third point is out of range.
    return points;
}
// compute points for a thin line with endpoints p and q, with above h and right w.
function polyLine(p, q, h, w) {
    h = h || 0; // default 0
    w = w || 0; // default 0
    if (w == 0) return [p, above(p,h), above(q,h), q];
    if (h == 0) return [p, right(p,w), right(q,w), q];
    return [p, right(above(p,h), w), right(above(q,h), w), q];
}
// construct a piece
function makePiece(points, color) {
    var piece = document.createElement('div');
    piece.className = 'piece';
    var poly = polygon(points);
    // alert('points: ' + points.toString() + '\npoly: ' + poly);
    piece.setAttribute('style', 'background-color: ' + color + '; ' + fix('clip-path: ') + poly);
    return piece;
}
// construct a dot at point (x,y)
function makeDot(x, y, color) {
    color = color || 'black'; // default black
    var span = document.createElement('span');
    span.className = 'txtfont';
    span.setAttribute('style', 'position:absolute; left: ' + x + '%; top: ' + (100 - y) + '%;');
    span.style.color = color;
    // &#9679;  = black circle
    // &#9899;  = medium black circle
    // &#11044; = black large circle
    span.innerHTML = '&#9679;';
    return span;
}
// construct a label at point (x,y)
function makeLabel(x, y, label) {
    var span = document.createElement('span');
    span.className = 'txtfont';
    span.setAttribute('style', 'position:absolute; left: ' + x + '%; top: ' + (100 - y) + '%;');
    span.innerHTML = label;
    return span;
}
// specify style with folding
// name of animation, color of background, duration, delay,
//         poly 100%, optional poly2 50%,
//   arg3: flag = true for poly2 0%
//   arg3: poly3 25% and 75%
function makeFold (name, color, duration, delay, poly1, poly2, arg3) {
    if (typeof arg3 == 'object') {
        var poly3 = arg3;
        document.write('<style>' +
        '.' + name + ' {' +
        ' animation-name:' + name + ';' +
        ' animation-duration:' + duration + 's;' +
        (delay == 0 ? '' : ' animation-delay: ' + delay + 's;') +
        ' animation-fill-mode:forwards;' +
        ' transform-origin:' + modify(poly1[0]) + ';' +
        ' }' +
        '@' + fix('keyframes ') + name + ' {' +
        '  0% {background-color: ' + color + '}' +
        ' 25% {' + fix('clip-path: ') + polygon(poly3) + '}' +
        ' 50% {' + fix('clip-path: ') + polygon(poly2) + '}' +
        ' 75% {' + fix('clip-path: ') + polygon(poly3) + '}' +
        '100% {' + fix('clip-path: ') + polygon(poly1) + '}' +
        ' }' +
        '</style>');
    }
    else {
        var flag = arg3 || false; // default false
        document.write('<style>' +
        '.' + name + ' {' +
        ' animation-name:' + name + ';' +
        ' animation-duration:' + duration + 's;' +
        (delay == 0 ? '' : ' animation-delay: ' + delay + 's;') +
        ' animation-fill-mode:forwards;' +
        ' transform-origin:' + modify(poly1[0]) + ';' +
        ' }' +
        '@' + fix('keyframes ') + name + ' {' +
        '  0% {background-color: ' + color + '}' +
        (poly2 && !flag ? '0% {' + fix('clip-path: ') + polygon(poly1) +'}' : '' ) + // Safari needs 0%
        (poly2 ? (' ' + (flag ? 0 : 50) + '% {' + fix('clip-path: ') + polygon(poly2) + '}') : '') +
        '100% {' + fix('clip-path: ') + polygon(poly1) +'}' +
        ' }' +
        '</style>');
    }
}

/**
 A Panel is like the frame, having:
 * a title bar
 * an information bar
 * a board, square piece for paper folding
 * a button, for Animate/Reset.
 */

// construct a Panel
function Panel (mode, angle, w, h) {
    w = w || size;  // default size, even 0 is size.
    h = h || size;  // default size, even 0 is size.
    // create the DOM elements for Panel
    var panel = document.createElement('div');
    panel.className = 'frame';
    var title = document.createElement('div');
    title.className = 'title';
    var info = document.createElement('div');
    info.className = 'info';
    //info.innerHTML = '&copy; 2021, based on MathsIsFun and MarkDeep.';
    title.innerHTML = 'Panel Title';
    info.innerHTML = '&copy; 2021, using CSS animation by keyframes.';
    var button = document.createElement('button');
    button.className = 'btn';
    // override some style of btn
    button.setAttribute('style', 'z-index:2; position:absolute; right:3px; bottom:3px;');
    // pass title, info and button to board
    var args = {
        title: title,
        info: info,
        button: button
    };
    var board = new Board(args, mode, angle, w, h);

    // link up elements
    panel.appendChild(title);
    panel.appendChild(info);
    panel.appendChild(board);
    panel.appendChild(button);

    return panel;
}

// Pieces Manager
function Pieces () {
    // store pieces
    var pieces = [];
    // put in a piece
    this.put = function (p) {
        pieces.push(p);
        return p;
    }
    // clear all pieces
    this.clear = function () {
        pieces.forEach(function (p) {
            if (p.parentNode) p.parentNode.removeChild(p);
        });
    }
}

// Timers Manager
function Timers () {
    // store timers
    var timers = [];
    // put in a timer
    this.put = function (wait, action) {
        var t = setTimeout(action, wait * 1000); // convert to milliseconds
        timers.push(t);
        return t;
    }
    // clear all timers
    this.clear = function () {
        timers.forEach(function (t) {
            clearTimeout(t);
        });
    }
}

// A static identifier generator, for CSS.
var ID = {
    seed: 1,  // seed value
    next: function() {
        return this.seed++;
    }
};
// first time: ID.seed    gives 1.
// first call: ID.next()  gives 1.
// second call: ID.next() gives 2, etc.

// construct the Board
function Board (args, mode, angle, w, h) {
    // attributes
    mode = mode || 'pythag'; // default pythag
    angle = angle || 75;   // default 75 degree, even 0 is 75
    angle = toRadian(angle); // convert to radian for trigonometry calls

    // create the DOM elements for scene
    var board = document.createElement('div');
    board.className = 'board';
    board.setAttribute('style', 'position:absolute; left:50px; top:60px;');

    // handle arguments
    doModes(mode, angle);

    /* ----------------------------------------------------- *
       All handling routines.
     * ----------------------------------------------------- */

    function doModes(mode, angle) {
        // animation based on mode
        switch(mode) {
        case 'bisect' : doBisect(angle); break;
        case 'trisect1': doTrisect(angle, 1); break;
        case 'trisect2': doTrisect(angle, 2); break;
        case 'trisect3': doTrisect(angle, 3); break;
        case 'trisect' : doTrisect(angle); break;
        default : doPythag(); break; // mode: pythag
        }
    }

    /* ----------------------------------------------------- *
       All drawing modes.
     * ----------------------------------------------------- */


    /* ----------------------------------------------------- *
       Angle Bisection.   100 means 100%
       (x, 100 - y) corresponds to [x,y]

     D (0,0) [0,100]                  (100,0) [100,100]
       +-------------------------------+
       |                               + P [100,100 * tan θ]
       |                            /  |
       |                         /     |
       |                   C  /        |
       |                   /           | Q [100,100 * tan (θ/2)]
       |                /          .   |
       |             /        .        |
       |          /      .             |
       |       /    .                  |
       |    /   .                      |
       | /  θ .                        |
     A +-------------------------------+ B
       (0,100) [0,0]                  (100,100) [100,0]

       Point B folds to C, along the edge, distance 100 from origin.
       C: [100 * cos θ, 100 * sin θ]
     * ----------------------------------------------------- */

    // for mode: bisect
    function doBisect(angle) {
        var A = [0, 0];
        var B = [100, 0];
        var P = [100, 100 * tan(angle)];
        var Q = [100, 100 * tan(angle/2)]
        var C = [100 * cos(angle), 100 * sin(angle)];
        var D = [0, 100];
        // the points for the flipped half angle: A C Q.
        // make the pieces
        var pieces = new Pieces();
        var fold1s = pieces.put(makePiece([A, D, P, Q], 'orange')); // stationary piece
        var fold1m = pieces.put(makePiece([A, B, Q], 'orange')); // the moving piece
        var ray = pieces.put(makePiece(polyLine(A, P, 3), 'blue'));
        var base = pieces.put(makePiece(polyLine(A, B, 1), 'green'));
        var crease = pieces.put(makePiece(polyLine(A, Q, 1), 'black'));
        var text1 = pieces.put(makeLabel(10, 16, 'θ'));
        var text2 = pieces.put(makeLabel(20, 40, 'θ/2'));
        var text3 = pieces.put(makeLabel(30, 20, 'θ/2'));

        // link them up at doReset()

        // animation
        var time = 3; // unit in seconds
        var id = ID.next(); // CSS names are global, need id to distinguish.
        // Fold 1: ABQA -> ACQA -> ABQA
        makeFold('bflip-' + id, 'blue', time, 0, [A, B, Q], [A, C, Q]);
        // Fold 1: AB -> AC -> AB
        makeFold('eflip-' + id, 'green', time, 0, polyLine(A, B, 1), polyLine(A, C, 1));

        // set up animation
        args.title.innerHTML = 'Angle Bisection by Folding';
        doReset();

        // start animation
        function doRun () {
            // alert('doRun!');
            fold1m.classList.add('bflip-' + id);
            // need reset to remove, and add again for repeat animation
            fold1m.addEventListener("animationend", doAfter, false);
            base.classList.add('eflip-' + id);
            board.appendChild(crease);
            args.info.innerHTML = 'Fold the green base to the blue arm, then press flat.';
            args.button.innerHTML = 'Reset';
            args.button.onclick = doReset;
        }
        // after animation
        function doAfter () {
            board.appendChild(text2);
            board.appendChild(text3);
            args.info.innerHTML = 'The crease bisects the original angle.';
        }
        // reset animation
        function doReset () {
            // alert('doReset!');
            pieces.clear(); // remove all pieces
            // add pieces
            board.appendChild(fold1s); // fixed piece
            board.appendChild(fold1m); // flipping piece
            fold1m.appendChild(base); // the base on flipping piece
            board.appendChild(ray); // the full angle boundary
            board.appendChild(text1); // the label
            // clear animations
            base.classList.remove('eflip-' + id);
            fold1m.classList.remove('bflip-' + id);
            fold1m.removeEventListener("animationend", doAfter, false);
            args.info.innerHTML = 'An angle between a blue arm and a green base.';
            args.button.innerHTML = 'Animate';
            args.button.onclick = doRun;
        }
    } // doBisect

    /* ----------------------------------------------------- *
       Angle Trisection.   100 means 100%

      A [0,100]                       D [100,100]
       +-------------------------------+
       |                               | P [100,100 * tan θ]
       |                            /  |
       |                         /     |
       |                      /        |
       |                   /           | J [100,100 * tan (2θ/3)]
       |                /          +   |
       |             /       +         |
       |          /     +          .   | K [100,100 * tan (θ/3)]
       |       /    +      .           |
       |    /   +  .                   |
       | /  θ                          |
       +-------------------------------+ C
      B [0,0]                          [100,0]

       First fold: gives EF parallel AD.
       Second fold: gives GH exactly between EF and BC.
       Third fold: meet E along BP at X, B along GH at Z, so that XYZ has Y on EF.
       The inclined crease is SUVT, making angle α to vertical, where θ = 3α.
       UX, VY, TZ all make angle 2α to horizontal, U on GH, V on EF, T on BC.

       Robert Lang's Geometric constructions in origami
       https://langorigami.com/wp-content/uploads/2015/09/origami_constructions.pdf
       Page 33: Figure 25. Hisashi Abe’s trisection of an arbitrary acute angle.

       Origami and Mathematics
       New Wave Mathematics 2018
       Andrew Kei Fong LAM (slide, 94 pages)
       https://www.math.cuhk.edu.hk/sites/default/files/community-outreach/new-wave/newwave2018.pdf
       Tsune Abe's method (page 40) ABE Tsune

     * ----------------------------------------------------- */

    // for mode: trisect
    function doTrisect(angle, stage) {
        // Stage 1: get two equidistant horizontal lines, for E and GH.
        // Stage 2: make a fold landing on the two equidistant horizontals, vertical θ/3.
        // Stage 3: complete the creases for two trisectors, transpose to horizontal θ/3.
        stage = stage || 0; // default: 0
        var all = (stage == 0); // boolean: all stages?
        if (all) stage = 1; // all stage, start from stage 1
        var alpha = angle/3, beta = 2 * alpha;
        var A = [0, 100];
        var B = [0, 0];
        var C = [100, 0];
        var D = [100, 100];
        var P = [100, 100 * tan(angle)];
        // the trisectors intersect the far side at:
        var J = [100, 100 * tan(beta)];
        var K = [100, 100 * tan(alpha)];
        var rise = 27; // 2h = almost any height from bottom
        var Q = [2 * rise / tan(angle), 2 * rise];
        var R = [rise / tan(angle), rise]; // angle arm = BRQP
        var E = [0, 2 * rise], F = [100, 2 * rise];
        var G = [0, rise], H = [100, rise];
        // XYZ = reflection of EB about an inclined crease, such that X on BP, Z on GH.
        // Let SUVT be the crease of the EB fold, and let angle θ = 3α.
        // It can be shown that UX, VY, TZ all make angle 2α to horizontal.
        // Let x = BT, then TZ = x, and x sin 2α = h, so x = h/(sin 2α).
        // It can be shown that crease SUVT makes angle α to vertical.
        // Let y = BS, then x/y = tan α, so y = x/tan α = h/(tan α)(sin 2α) = h/(2(sin α)(sin α)).
        var T = [rise / sin(beta), 0]; // [x, 0]
        var S = [0, T[0] / tan(alpha)];     // [0, y]
        var Z = [T[0] + T[0] * cos(beta), rise]; // [x + x cos 2α, h]
        // then EU = (y - 2h) * tan α = x1,
        var U = [(S[1] - 2 * rise) * tan(alpha), 2 * rise]; // [x1, 2h]
        var X = [U[0] + U[0] * cos(beta), 2 * rise + U[0] * sin(beta)]; // [x1 + x1 cos 2α, 2h + x1 sin 2α]
        // and GV = (y - h) * tan α = x2,
        var V = [(S[1] - rise) * tan(alpha), rise]; // [x2, h]
        var Y = [V[0] + V[0] * cos(beta), rise + V[0] * sin(beta)]; // [x2 + x2 cos 2α, h + x2 sin 2α]
        // MN = reflection of AD about EF.
        var M = [0, 4 * rise - 100], N = [100, M[1]];
        // BW = 1/2 BZ, W the perpendicular intersection of ST and BZ.
        var W = [Z[0]/2, Z[1]/2];
        // I = intersection of EF and BJ, BM = double of BV
        var I = [2 * V[0], 2 * V[1]];
        // L = intersection of ray BP and vertical trisector ST.
        // equation of BP:  y = x tan θ
        // equation of ST:  x/Tx + y/Sy = 1
        // solving: x (1/Tx + tan θ/Sy) = 1, so x = Tx Sy / (Sy + Tx tan θ)
        // and y = x tan θ = Tx Sy tan θ / (Sy + Tx tan θ)
        var Lx = T[0] * S[1] / (S[1] + T[0] * tan(angle)),
            L = [Lx, Lx * tan(angle)];

        // make the pieces
        var pieces = new Pieces();
        // basic pieces
        var top = pieces.put(makePiece(polyLine(A, D, 1), 'yellow')); // top AD
        var base = pieces.put(makePiece(polyLine(B, C, 1), 'green')); // base BC
        var ray = pieces.put(makePiece(polyLine(B, P, 2), 'blue')); // angle BP
        // stage 1: two creases EF and GH
        var fold1m = pieces.put(makePiece([E, F, D, A], 'orange')); // the moving piece
        var fold1s = pieces.put(makePiece([G, H, F, E], 'orange')); // the middle piece
        var fold1n = pieces.put(makePiece([G, H, C, B], 'orange')); // the moving piece
        var ray1m = pieces.put(makePiece(polyLine(Q, P, 2), 'blue')); // angle line QP
        var ray1s = pieces.put(makePiece(polyLine(R, Q, 2), 'blue')); // angle line RQ
        var ray1n = pieces.put(makePiece(polyLine(B, R, 2), 'blue')); // angle line BR
        var lineEF = pieces.put(makePiece(polyLine(E, F, 1), 'yellow'));
        var lineGH = pieces.put(makePiece(polyLine(G, H, -1), 'green'));
        // stage 2: vertical trisector ST
        var fold2m = pieces.put(makePiece([S, T, B], 'orange')); // the moving piece
        var fold2s = pieces.put(makePiece([S, T, C, P, A], 'orange')); // the stationary piece
        var ray2m = pieces.put(makePiece(polyLine(B, L, 2), 'blue')); // angle line BL
        var ray2s = pieces.put(makePiece(polyLine(L, P, 2), 'blue')); // angle line LP
        var lineST = pieces.put(makePiece(polyLine(S, T, -1), 'navy'));  // the crease ST
        var lineEU = pieces.put(makePiece(polyLine(E, U, 1), 'yellow')); // EU:UF, first part
        var lineUF = pieces.put(makePiece(polyLine(U, F, 1), 'yellow')); // EU:UF, second part
        var lineGV = pieces.put(makePiece(polyLine(G, V, -1), 'green'));  // GV:VH, first part
        var lineVH = pieces.put(makePiece(polyLine(V, H, -1), 'green'));  // GV:VH, second part
        // stage 3: actual trisectors BK and BJ
        var fold3m = pieces.put(makePiece([B, K, C], 'orange')); // the moving piece
        var fold3n = pieces.put(makePiece([B, J, K], 'orange')); // the next moving piece
        var fold3s = pieces.put(makePiece([B, J, P, A], 'orange')); // the stationary piece
        var lineBK = pieces.put(makePiece(polyLine(B, K, 1), 'navy')); // BK trisector
        var lineBJ = pieces.put(makePiece(polyLine(B, J, 1), 'navy')); // BJ trisector
        var lineEI = pieces.put(makePiece(polyLine(E, I, 1), 'yellow')); // EI:IF, first part
        var lineIF = pieces.put(makePiece(polyLine(I, F, 1), 'yellow')); // EI:IF, second part
        var lineVZ = pieces.put(makePiece(polyLine(V, Z, -1), 'green')); // VZ:ZH, first part
        var lineZH = pieces.put(makePiece(polyLine(Z, H, -1), 'green')); // VZ:ZH, second part
        // the angle labels
        var text1 = pieces.put(makeLabel(10, 12, 'θ'));
        var text2 = pieces.put(makeLabel(22, 46, 'θ/3'));
        var text3 = pieces.put(makeLabel(30, 30, 'θ/3'));
        var text4 = pieces.put(makeLabel(38, 14, 'θ/3'));
        // essential points
        var dotE = pieces.put(makeDot(E[0] - 2.4, E[1] + 5.4, 'blue'));
        var lblE = pieces.put(makeLabel(E[0] - 8, E[1] + 2, 'E'));
        var dotB = pieces.put(makeDot(B[0] - 2.4, B[1] + 5, 'green'));
        var lblB = pieces.put(makeLabel(B[0] - 8, B[1] + 2, 'B'));
        var dotX = pieces.put(makeDot(X[0] - 2.4, X[1] + 5, 'blue')); // E flip to X
        var lblX = pieces.put(makeLabel(X[0] + 6, X[1] + 2, 'X'));
        var dotZ = pieces.put(makeDot(Z[0] - 2.4, Z[1] + 5, 'green')); // B flip to Z
        var lblZ = pieces.put(makeLabel(Z[0] + 6, Z[1] + 2, 'Z'));
        var dotV = pieces.put(makeDot(V[0] - 2.4, V[1] + 5, 'lime'));
        var lblV = pieces.put(makeLabel(V[0] - 8, V[1] + 2, 'V'));
        var dotT = pieces.put(makeDot(T[0] - 2.4, T[1] + 5, 'lime'));
        var lblT = pieces.put(makeLabel(T[0] + 2, T[1] - 1, 'T'));
        // non-essential points
        var dotS = makeDot(S[0] - 2.4, S[1] + 5);
        var lblS = makeLabel(S[0] - 8, S[1] + 2, 'S');
        var dotU = makeDot(U[0] - 2.4, U[1] + 5);
        var lblU = makeLabel(U[0] - 8, U[1] + 2, 'U');
        var dotY = makeDot(Y[0] - 2.4, Y[1] + 5);
        var lblY = makeLabel(Y[0] + 6, Y[1] + 2, 'Y');
        var dotW = makeDot(W[0] - 2.4, W[1] + 5);
        var lblW = makeLabel(W[0] + 6, W[1] + 2, 'W');
        // P and J can be outside!
        var dotK = makeDot(K[0] - 2.4, K[1] + 5);
        var lblK = makeLabel(K[0] + 6, K[1] + 5, 'K');
        var dotQ = makeDot(Q[0] - 2.4, Q[1] + 5);
        var lblQ = makeLabel(Q[0] + 2, Q[1] + 2, 'Q');
        var dotR = makeDot(R[0] - 2.4, R[1] + 5);
        var lblR = makeLabel(R[0] + 2, R[1] + 2, 'R');
        var dotI = makeDot(I[0] - 2.4, I[1] + 5);
        var lblI = makeLabel(I[0] + 2, I[1] + 2, 'I');
        var dotL = makeDot(L[0] - 2.4, L[1] + 5);
        var lblL = makeLabel(L[0] + 2, L[1] + 2, 'L');

        // link them up during doReset().

        // CSS animations
        var timers = new Timers();
        var time = 3; // unit in seconds
        var id = ID.next(); // CSS names are global, need id to distinguish.
        // Fold 1: EFDAE -> EFNME -> EFDAE
        makeFold('fold01-' + id, 'blue', time, 0, [E, F, D, A], [E, F, N, M]);
        // Fold 2: GHCBG -> GHFEG -> GHCBG
        makeFold('fold02-' + id, 'blue', time, 0, [G, H, C, B], [G, H, F, E]);
        // Fold 3: STBS -> STZS -> STBS
        makeFold('fold03-' + id, 'blue', 2 * time, 0, [S, T, B], [S, T, Z]);
        // Fold 4: BKC -> BKC' -> BK'C' -> BKC' -> BKC
        makeFold('fold04-' + id, 'blue', 4 * time, 0, [B, K, C], [B, reflect(K, beta), reflect(C, alpha)], [B, K, reflect(C, alpha)]); // [B,K,C] -> [B,K,C'] -> [B,K',C']
        // Fold 5: BJK -> BJK' -> BJK
        makeFold('fold05-' + id, 'blue', 2 * time, time, [B, J, K], [B, J, reflect(K, beta)]); // delay = 3, duration = 6
        // Fold 6: BKC' -> BKC not used
        // makeFold('fold06-' + id 'blue', time, 0, [B, K, C], [B, K, reflect(C, alpha)], true);

        // set up animation
        args.title.innerHTML = 'Angle Trisection by Folding';
        doReset();

        // restart animation for stage n
        function restart (n) {
            stage = n; // set stage
            timers.put(time/2, doSetup);
            timers.put(time, doRun);
        }
        // set up piecies
        function doSetup () {
            // remove all pieces before adding
            pieces.clear();
            // add pieces for each stage
            if (stage == 1) {
                board.appendChild(fold1s); // stationary first
                board.appendChild(fold1m); // append after to show folding
                board.appendChild(fold1n); // append after to show folding
                fold1m.appendChild(ray1m);
                fold1s.appendChild(ray1s);
                fold1n.appendChild(ray1n);
                board.appendChild(top);
                board.appendChild(base);
                board.appendChild(ray); // cover up broken parts
                board.appendChild(text1); // the angle label
            }
            if (stage == 2) {
                board.appendChild(fold2s); // stationary first
                board.appendChild(fold2m); // append after to show folding
                fold2m.appendChild(lineEU); // break up EF as EU:UF
                fold2s.appendChild(lineUF);
                fold2m.appendChild(lineGV); // break up GH as GV:VH
                fold2s.appendChild(lineVH);
                board.appendChild(lineEF); // cover up broken parts
                board.appendChild(lineGH); // cover up broken parts
                board.appendChild(top);
                board.appendChild(base);
                fold2m.appendChild(ray2m);
                fold2s.appendChild(ray2s);
                board.appendChild(ray); // cover up broken parts
                board.appendChild(text1); // the angle label
                board.appendChild(dotB); // attach to board, for showing
                board.appendChild(lblB);
                board.appendChild(dotE); // attach to board, for showing
                board.appendChild(lblE);
            }
            if (stage == 3) {
                // add pieces
                board.appendChild(fold3s); // stationary first
                board.appendChild(fold3n); // append after to show folding
                board.appendChild(fold3m); // append after to show folding
                fold3s.appendChild(lineGV); // break up GH as GV:VZ:ZH
                fold3n.appendChild(lineVZ);
                fold3m.appendChild(lineZH);
                board.appendChild(lineGH); // cover up broken parts
                board.appendChild(top);
                board.appendChild(base);
                board.appendChild(ray);
                board.appendChild(text1); // the angle label
                board.appendChild(dotB);
                board.appendChild(lblB);
                board.appendChild(lineST); // the vertical trisector
                board.appendChild(dotV);
                board.appendChild(lblV);
                board.appendChild(dotT);
                board.appendChild(lblT);
            }
        }
        // start animation
        function doRun () {
            // alert('doRun!');
            if (stage == 1) { // make crease EF, then GH, by chaining.
                board.removeChild(ray);
                fold1m.classList.add('fold01-' + id); // for stage = 1
                fold1m.addEventListener("animationend", FoldTwo, false);
                args.info.innerHTML = 'Fold yellow top down to intersect blue arm.';
                board.appendChild(lineEF);
            }
            if (stage == 2) { // make crease ST
                board.removeChild(ray);
                board.removeChild(lineEF);
                board.removeChild(lineGH);
                fold2m.classList.add('fold03-' + id); // for stage = 2
                fold2m.addEventListener("animationend", FoldFour, false);
                args.info.innerHTML = 'Fold E to blue arm, with B along green crease.';
                board.appendChild(lineST);
                timers.put(time, function() { // time at middle of animation
                    board.appendChild(dotX); // no label
                    board.appendChild(dotZ); // no label
                    board.removeChild(lblE); // remove label
                    board.removeChild(lblB); // remove label
                });
            }
            if (stage == 3) { // make crease BK, then BJ, by delay timing.
                board.removeChild(lineGH);
                fold3m.classList.add('fold04-' + id); // for stage = 3
                fold3n.classList.add('fold05-' + id); // start with delay 3s
                fold3n.addEventListener("animationstart", FoldFive, false); // at 3s
                fold3n.addEventListener("animationend", FoldSix, false); // at 9s
                fold3m.addEventListener("animationend", FoldSeven, false); // at 12s
                args.info.innerHTML = 'Fold edge BT to BV, the crease is a trisector.';
            }
            args.button.innerHTML = 'Reset';
            args.button.onclick = doReset;
        }

        // make crease GH
        function FoldTwo () {
            fold1m.removeEventListener("animationend", FoldTwo, false);
            fold1n.classList.add('fold02-' + id); // for stage = 1
            fold1n.addEventListener("animationend", FoldThree, false);
            // args.info.innerHTML = 'Step 2: Another equidistant crease in between.';
            args.info.innerHTML = 'Fold green base up to yellow crease.';
            board.appendChild(lineGH);
        }

        // show points B and E
        function FoldThree () {
            fold1n.removeEventListener("animationend", FoldThree, false);
            board.appendChild(ray); // full arm
            board.appendChild(dotB); // B
            board.appendChild(dotE); // E
            board.appendChild(lblB); // B
            board.appendChild(lblE); // E
            // args.info.innerHTML = 'The creases EF and GH are shown.';
            args.info.innerHTML = 'Note the blue point E and green vertex B.';
            if (all) restart(2); // next stage
        }

        // show points V and T
        function FoldFour () {
            fold2m.removeEventListener("animationend", FoldFour, false);
            // remove dots
            board.removeChild(dotE);
            board.removeChild(dotX);
            board.removeChild(dotB);
            board.removeChild(dotZ);
            // remove EU:UF
            fold2m.removeChild(lineEU);
            fold2s.removeChild(lineUF);
            // add points
            board.appendChild(ray); // full arm
            board.appendChild(dotV); // V
            board.appendChild(lblV); // V
            board.appendChild(dotT); // T
            board.appendChild(lblT); // T
            args.info.innerHTML = 'New crease cuts green crease at V, base at T.';
            if (all) restart(3); // next stage
        }

        // make crease BJ
        function FoldFive () {
            fold3n.removeEventListener("animationstart", FoldFive, false);
            board.appendChild(lineBJ);
            board.removeChild(lblV); // remove label
            board.removeChild(lblT); // remove label
            args.info.innerHTML = 'Fold trisector to arm, the crease is another trisector.';
        }

        // unfold first part
        function FoldSix () {
            fold3n.removeEventListener("animationend", FoldSix, false);
            board.appendChild(lineBK);
            board.removeChild(dotV); // remove dot
            board.removeChild(dotT); // remove dot
            args.info.innerHTML = 'Unfold fully to reveal the trisectors.';
        }

        // unfold last part
        function FoldSeven () {
            fold3m.removeEventListener("animationend", FoldSeven, false);
            fold3s.removeChild(lineGV);
            fold3n.removeChild(lineVZ);
            fold3m.removeChild(lineZH);
            board.removeChild(lineST);
            board.appendChild(text2); // the angle labels
            board.appendChild(text3); // the angle labels
            board.appendChild(text4); // the angle labels
            args.info.innerHTML = 'The original angle is trisected!';
        }

        // reset animation
        function doReset () {
            // alert('doReset!');
            // remove all timers
            timers.clear();
            fold1m.classList.remove('fold01-' + id);
            fold1n.classList.remove('fold02-' + id);
            fold2m.classList.remove('fold03-' + id);
            fold3m.classList.remove('fold04-' + id);
            fold3n.classList.remove('fold05-' + id);
            fold1m.removeEventListener("animationend", FoldTwo, false);
            fold1n.removeEventListener("animationend", FoldThree, false);
            fold2m.removeEventListener("animationend", FoldFour, false);
            fold3n.removeEventListener("animationstart", FoldFive, false);
            fold3n.removeEventListener("animationend", FoldSix, false);
            fold3m.removeEventListener("animationend", FoldSeven, false);
            args.info.innerHTML = 'Modified from the method of Hisashi Abe.';
            if (stage == 1) args.info.innerHTML = 'Stage 1: Get two horizontal folds.';
            if (stage == 2) args.info.innerHTML = 'Stage 2: Make a vertical trisector.';
            if (stage == 3) args.info.innerHTML = 'Stage 3: Produce actual trisectors. angle: ' + toDegree(angle);
            if (all) args.info.innerHTML = 'An angle between a blue arm and a green base.';
            if (all) stage = 1;
            doSetup(); // when stage is set
            args.button.innerHTML = 'Animate';
            args.button.onclick = doRun;
        }

    } // doTrisect

    /* Note: color names:
       https://www.w3schools.com/colors/colors_names.asp
     */

    /* ----------------------------------------------------- *
       Pythagoras flip

    0,0                         100,0
     ............................
     |        B        K = flip of A about BC, collinear: A H K
     |   20,10 *    H
     |       a |    .c
     |         |      0  .
     |   20,50 *..........70,50
   0,100      A      b       C 100,100

      a = 50 - 10 = 40,
      b = 70 - 20 = 50,
      c = sqrt(40^2 + 50^2) = 64.0312423743
      small angle θ = arctan(a/b) = arctan(4/5) = 0.674740942 rad = 38.6598083 degrees
      height from c = h = a cos θ, intersect c at H.
      x-coordinate of H = h sin θ + 20 = a cos θ sin θ + 20
                        = 20 + 40 cos(arctan(4/5)) sin(arctan(4/5)) = 39.512195122
      y-coordinate of H = 50 - h cos θ = 50 - a cos θ cos θ
                        = 50 - 40 cos(arctan(4/5)) cos(arctan(4/5)) = 25.6097560976
      a = 40, b = 50, tan θ = a/b, sin θ = a/sqrt(a^2 + b^2), cos θ = b/sqrt(a^2 + b^2)
      h = a cos θ,
      H at (a cos θ sin θ, a cos θ cos θ) = (a/2 sin 2θ,  a/2 (1 + cos 2θ))
      H: ( 20 + a * ab/(a^2 + b^2), 50 + a * b^2/(a^2 + b^2) ) for [a * 20/41, a * 25/41]
      K is the flip of H about hypotenuse BC, so A H K are collinear, and AH = HK,
      therefore, AK = 2 * AH, or K = [20 + 2 h.x, 50 + 2 h.y] for [a * 40/41, a * 50/41]
      flip matrix about a line making angle φ = π - θ to x-axis:
           [ cos 2φ   sin 2φ] = [ cos 2θ  -sin 2θ]
           [ sin 2φ  -cos 2φ]   [-sin 2θ  -cos 2θ]
      With φ = π - θ, this is:
      cos(2(π - θ)) = cos(2π - 2θ) = cos 2θ = cos(2(arctan(4/5))) = 0.21951219512 = 9/41
      sin(2(π - θ)) = sin(2π - 2θ) = - sin 2θ = - sin(2(arctan(4/5))) = -0.97560975609 = - 40/41
      cos 2θ = cos(2(atan(a/b))) = (b^2 - a^2)/(a^2 + b^2) = (25 - 16)/(25 + 16) = 9/41
      sin 2θ = sin(2(atan(a/b))) = 2ab/(a^2 + b^2) = 2(4)(5)/(25 + 16) = 40/41
      [[9/41, -40/41], [-40/41, -9/41]] . [x,y] = [(9x - 40y)/41, -(40x + 9y)/41]
      [[9/41, -40/41], [-40/41, -9/41]] . [a * 20/41, a * 25/41] = [-20a/41, -25a/41]

    flip y-axis: φ = π/2, so [[0, 1], [1, 0]] . [x, y] = [y, x]
    flip x-axis: φ = 0, so [[1,0],[0,-1]] . [x, y] = [x, -y]
    [[1,0],[0,-1]] . [20,10] = [20,-10]
    [[1,0],[0,-1]] . [ 0,-40] = [0,40]
    transform-origin: 20% 40%; // default: 50% 50% 0
     * ----------------------------------------------------- */

    // for mode: pythag
    function doPythag() {
        var A = [20 +  0, 50 +  0]; // the right-angle vertex [0,0]
        var B = [20 +  0, 50 + 40]; // the top vertex [0, a]
        var C = [20 + 50, 50 +  0]; // the right vertex [b, 0]
        var H = [20 + 40 * 20/41, 50 + 40 * 25/41];
        // H: 39.51219512195122,74.39024390243902
        var K = [20 + 2 * 40 * 20/41, 50 + 2 * 40 * 25/41];
        // K: 59.02439024390244, 1.2195121951219505
        // polygon([B, A, C]) = polygon(20% 10%, 20% 50%, 70% 50%);
        // polygon([B, A, H]) = polygon(20% 10%, 20% 50%, 39.51219512195122% 25.609756097560975%);
        // polygon([H, A, C]) = polygon(39.51219512195122% 25.609756097560975%, 20% 50%, 70% 50%);
        // polygon([B, K, C]) = polygon(20% 10%, 59.02439024390244% 1.2195121951219505%, 70% 50%);
        // alert('poly: ' + polygon([B, K, C]));

        // make the piece and link it up
        // with only 3 pieces, no need for a manager
        var piece1 = makePiece([B, A, C], 'hotpink');
        var piece2 = makePiece([A, H, B], 'darkviolet');
        var piece3 = makePiece([A, H, C], 'darkturquoise');
        board.appendChild(piece1); // hypotenuse flip
        board.appendChild(piece2); // vertical flip
        board.appendChild(piece3); // horizontal flip

        // add style with flip
        // piece1: BAC -> BKC
        makeFold('flip', 'hotpink', 3, 2, [B, K, C]);
        // piece2: AHB -> AH'B
        makeFold('flip-y', 'darkviolet', 3, 0, [A, [20 - 40 * 20/41, 50 + 40 * 25/41], B]);
        // piece3: AHC -> AH''C
        makeFold('flip-x', 'darkturquoise', 3, 0, [A, [20 + 40 * 20/41, 50 - 40 * 25/41], C]);

        // set up animation
        args.title.innerHTML = 'Pythagoras Theorem by Folding';
        args.info.innerHTML = 'The fan-out triangles are similar.';
        args.button.innerHTML = 'Animate';
        args.button.onclick = doRun;

        // start animation
        function doRun () {
            // alert('doRun!');
            piece1.classList.add('flip');
            piece2.classList.add('flip-y');
            piece3.classList.add('flip-x');
            args.button.innerHTML = 'Reset';
            args.button.onclick = doReset;
        }
        // reset animation
        function doReset () {
            // alert('doReset!');
            piece1.classList.remove('flip');
            piece2.classList.remove('flip-y');
            piece3.classList.remove('flip-x');
            args.button.innerHTML = 'Animate';
            args.button.onclick = doRun;
        }
    } // doPythag

    return board;
}

// transform the node
function transform(node) {
    var source = node.innerHTML;
    // parse the source, determine mode and type
    // var list = source.match(/\b([\w.]+)\b/g)
    var list = source.parse('wn(cin)w');
    // alert('transform: list = ' + list.toString() + ', length: ' + list.length);
    // alert('list[0]:' + list[0] + ', list[1]:' + list[1] + ', list[2]:' + list[2].toString());
    // alert('Types, list[0]:' + (typeof list[0]) + ', list[1]:' + (typeof list[1]) + ', list[2]:' + (typeof list[2]));
    if (list.length < 2) {
        alert('Invalid syntax: ' + source + '\nIgnored!');
        list = ['pyth', 75]; // default values
    }
    var mode = list[0];
    var angle = list[1];
    // alert('source: ' + mode + ':' + (typeof mode) + ', ' + angle + ':' + (typeof angle));
    return new Panel(mode, angle);
}

// process all origami nodes
function processOrigami() {
   // Collect all nodes that will receive origami processing
   var origamiNodeArray = toArray(document.getElementsByClassName('origami'))
                  .concat(toArray(document.getElementsByTagName('origami')));

   // Process all nodes, replacing them as we progress
   origamiNodeArray.forEach(function (node) {
       node.parentNode.replaceChild(transform(node), node);
   });
}

// Start from here.
loadOrigamiStyle(); // load the style CSS for frame, canvas, title, info, button
processOrigami();   // find and process all <origami> nodes

})(); // invoke single function

/**
 Part 5: Drawing by SVG, using the same idea.
 * find all nodes of drawing class or tag,
 * parse the node content and generate an SVG node,
 * replace each <drawing> node by the SVG node.
 *
 * There are two ways to make the SVG node:
 * Method 1, using createElementNS, create element with specified namespace:
 *   var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
 *       svg.setAttribute("width", "500");
 *       svg.setAttribute("height", "140");
 *       var ellipse = document.createElementNS("http://www.w3.org/2000/svg", "ellipse");
 *       ellipse.setAttribute("cx", "200");
 *       ellipse.setAttribute("cy", "80");
 *       ellipse.setAttribute("rx", "100");
 *       ellipse.setAttribute("ry", "50");
 *       ellipse.setAttribute("style", "fill:yellow;stroke:purple;stroke-width:2");
 *       svg.appendChild(ellipse);
 *       return svg;
 * Method 2, as <diagram> in markdeep, just create DIV with innerHTML with <svg>:
 *   var svg = document.createElement('div');
 *       svg.innerHTML =
 *     '<svg width="500" height="140">' +
 *     '<ellipse cx="200" cy="80" rx="100" ry="50" style="fill:yellow;stroke:purple;stroke-width:2" />' +
 *     '</svg>';
 *     return svg;
 * Here method 2 is used, for simplicity.
 * Also, SPAN is used instead of DIV, so that <drawing> can be side-by-side.
 */

// single function application for <drawing>
(function() {
'use strict';

/**
 The Mark Object (simplified Shape)

 */
// construct a mark at p = [x,y] with color and label
function Mark(p, label, color) {
    color = color || 'pink'; // default pink
    this.x = p[0];
    this.y = p[1];
    this.label = label;
    this.color = color;
};
// string representation of a mark
Mark.prototype.toString = function () {
    return this.x + ' ' + this.y;
};
// set x and y of a mark
Mark.prototype.setxy = function (x, y) {
    this.x = x;
    this.y = y;
    return this;
};
// have a mark offset by dx, dy
Mark.prototype.offset = function (dx, dy) {
    return new Mark([this.x + dx, this.y + dy], this.label, this.color);
};
// hava a mark rotated by angle
// rotate matrix:
//    [ cos θ  sin θ] [x] = [ x cos θ + y sin θ]
//    [-sin θ  cos θ] [y]   [-x sin θ + y cos θ]
Mark.prototype.rotate = function (angle) {
    var c = cos(angle), s = sin(angle);
    return new Mark([this.x * c + this.y * s, -this.x * s + this.y * c], this.label, this.color);
};
// have a mark reflected about a line of slope tan θ
// flip matrix:
//    [ cos 2θ   sin 2θ] [x] = [x cos 2θ + y sin 2θ]
//    [ sin 2θ  -cos 2θ] [y]   [x sin 2θ - y cos 2θ]
Mark.prototype.reflect = function (angle) {
    var c = cos(2 * angle), s = sin(2 * angle);
    return new Mark([this.x * c + this.y * s, this.x * s - this.y * c], this.label, this.color);
}

// a manager for marks
function Marks() {
    // keep a stack
    var marks = [];
    // put in a mark
    this.put = function (x, y, label, color) {
        var m = new Mark([x, y], label, color);
        marks.push(m);
        return m;
    };
    // apply a function to all marks
    this.apply = function (map) {
        marks.forEach(function(m) {
            map(m);
        });
    };
}
// put a polygon from marks, with fill and stroke
function putPoly(marks, fill, stroke, width) {
    // fill = fill || 'rgba(0, 0, 255, 0.3)'; // default blue haze
    fill = fill || 'orange'; // default orange
    stroke = stroke || 'blue'; // default blue lines
    width = width || 1; // default 1, even 0 is 1.
    var s = [];
    marks.forEach(function(m) {
        s.push(m.toString());
    });
    // M = moveto, L = lineto, Z = closepath
    return '<path d="M ' + s.join('L ') + ' Z"' +
           ' fill="' + fill + '"' +
           ' stroke="' + stroke + '"' +
           ' stroke-width="' + width + '" />';
}
// put a curve from 3 marks.
function putCurve(marks, stroke, width, arrow, dash) {
    stroke = stroke || 'blue'; // default blue lines
    width = width || 1; // default 1, even 0 is 1.
    arrow = arrow || false; // default no arrow
    dash = dash || false; // default no dash
    // M = moveto, C = curveto (start control end)
    var A = marks[0], B = marks[1], C = marks[2];
    // "M ' + A + ' C ' + A + ' ' + B + ' ' + C + '"  for one direction
    // "M ' + A + ' C ' + B + ' ' + C + ' ' + C + '"  is the same effect
    var s = '<path d="M ' + A + ' C ' + A + ' ' + B + ' ' + C + '" fill="none"' +
            ' stroke="' + stroke + '"' +
            ' stroke-width="' + width + '"' +
            (dash ? ' stroke-dasharray="10,10"' : '') +
            // marker-start has poor support, in Safari orientation is always 0.
            // (arrow ? ' marker-start="url(#startarrow)"' : '') +
            (arrow ? ' marker-end="url(#pointer)"' : '') + ' />';
    // alert('s: ' + s);
    return s;
}
// put a line from mark p to mark q.
function putLine(p, q, stroke, width, dash) {
    stroke = stroke || 'blue'; // default blue lines
    width = width || 1; // default 1, even 0 is 1.
    dash = dash || false; // default no dash
    // M = moveto, L = lineto
    return '<path d="M ' + p + ' L ' + q + '"' +
           ' stroke="' + stroke + '"' +
           ' stroke-width="' + width + '"' +
           (dash ? ' stroke-dasharray="2,1"' : '') +' />';
}
// put a mark p with size s.
function putMark(p, s, fill) {
    fill = fill || 'black'; // default black dot
    s = (typeof s  == 'number') ? s : 3; // default 3, allow 0.
    return '<circle cx="' + p.x + '" cy="' + p.y + '" r="' + s + '"' +
           ' stroke="' + fill + '" fill="' + fill + '" />';
}
// put a lable for p with offset dx, dy
function putLabel(p, dx, dy) {
    dx = (typeof dx  == 'number') ? dx : -10; // default -10, allow 0.
    dy = dy || 0; // default 0
    return '<text x="' + p.x + '" y="' + p.y +
              '" dx="' + dx + '" dy="' + dy + '">' + p.label + '</text>';
}

// load SVG definitions
function loadDrawingDefs() {
    // SVG definitions
    document.write(
        '<svg width="0" height="0">' +
           '<defs>' +
           '<marker id="startarrow" markerWidth="10" markerHeight="7" ' +
           ' refX="2" refY="4" orient="auto">' +
           ' <polygon points="10 0, 10 7, 0 3.5" fill="black" />' +
           '</marker>' +
           '<marker id="endarrow" markerWidth="10" markerHeight="7" ' +
           ' refX="8" refY="4" orient="auto">' +
           '<polygon points="0 0, 10 3.5, 0 7" fill="black" />' +
           '</marker>' +
           '<marker id="pointer" markerWidth="10" markerHeight="10" ' +
           ' refX="9.5" refY="5.1" orient="auto" markerUnits="userSpaceOnUse">' +
           ' <polyline points="1 1, 9 5, 1 7" />' +
           '</marker>' +
           '</defs>' +
        '</svg>');
    // if refX and refY are to be adjusted each time,
    // ID.next() must be used to give unique ids, like class in CSS,
    // and move this definition to the relevant location for use.
}

/*
See: Making Arrows in SVG
http://thenewcode.com/1068/Making-Arrows-in-SVG
*/

// convert drawing command to SVG
function doDrawing(scale, args) {
    scale = scale || 1; // default 1, even 0 is 1.
    var size = 250; // 250x250 fixed, for 3 svg's in a row mobile landscape.
    // var size = 280; // 280x280 fixed, for 4 svg's in a row.
    // var size = 300; // 300x300 fixed, for 3 svg's in a row.
    // form the SVG string
    var s = [];
    // start with scale, for border, add style="border:1px solid black"
    s.push('<svg width="' + size + '" height="' + size + '"' +
               ' viewbox="0 0 ' + size/scale + ' ' + size/scale + '" >');
    switch(args.mode) {
    case 'bisect' : s.push(drawBisect(args)); break;
    case 'trisect' : s.push(drawTrisect(args)); break;
    default : s.push(drawEllipse(args)); break;
    }
    // ending tag
    s.push('</svg>');
    // return the SVG element
    var svg = document.createElement('span');
    svg.innerHTML = s.join();
    return svg;
}
// drawing to show angle bisection
function drawBisect(args, stage) {
    var origin = args.origin;
    var ox = origin[0] || 50, // default 50, even 0 is 50.
        oy = origin[1] || 250, // default 250, even 0 is 250.
        angle = args.angle || 72, // default 72 degree
        width = args.width || 180, // default 180, even 0 is 180.
        stage = args.stage || 3, // default 3, even 0 is 3.
        label = args.label == 'true', // default false.
        mark = args.mark == 'true'; // default false.
    angle = toRadian(angle); // angle in radian
    var alpha = angle/2;
    var small = (tan(angle) < 1); // angle < 45 degree

    // define all required points, using 0-100
    var marks = new Marks();
    // square ABCD and P
    var A = marks.put(  0, 100, 'A');
    var B = marks.put(  0,   0, 'B');
    var C = marks.put(100,   0, 'C');
    var D = marks.put(100, 100, 'D');
    var P = small ? // that is, 100 * tan(angle) < 100, P along CD
                marks.put(100, 100 * tan(angle), 'P') :
                marks.put(100/tan(angle), 100, 'P'); // BP = angle arm
    var H = marks.put(100, 100 * tan(alpha), 'H'); // BH = angle bisector
    var K = marks.put(100 * cos(angle), 100 * sin(angle), 'K'); // reflect of C about BH
    var J = marks.put(100 * cos(alpha), 100 * sin(alpha), 'J'); // reflect of A about BP
    var M = marks.put(K.x/2, K.y/2, 'M'); // midpoint between B and K
    var N = marks.put(J.x/2, J.y/2, 'N'); // midpoint between B and J
    var aB = marks.put(  0, 0, 'θ');
    var bB = marks.put(  0, 0, 'θ/2');
    // convert all marks to raw scale for drawing
    marks.apply(function(m) {
        m.setxy(ox + m.x * width/100, oy - m.y * width/100);
    })
    // form the SVG string content
    var s = [];
    s.push(putPoly([A, B, C, D]));
    s.push(putLine(B, P));
    // keep stage 1 clean
    // if (stage == 1) {
    //    s.push(putPoly([B, P, A], 'white'));
    //    s.push(putPoly([B, P, J], 'rgba(255, 255, 0, 0.5)'));
    //    s.push(putCurve([B.offset(0, -100), B.offset(70, -70 * tan(alpha)), N], 'blue', 1, true, true));
    // }
    if (stage == 2) {
        s.push(putPoly([B, H, C], 'white'));
        s.push(putPoly([B, H, K], 'rgba(255, 255, 0, 0.5)'));
        s.push(putCurve([B.offset(100,0), B.offset(100, -100 * tan(alpha)), M], 'blue', 1, true, true));
    }
    if (stage == 3) {
        s.push(putLine(B, H, 'black'));
    }
    // Mark relevant points
    if (mark) s.push('<g stroke="black" stroke-width="3" fill="black">' +
                     putMark(B) + putMark(C) + putMark(P) + '</g>');
    // Label the points
    s.push('<g font-size="18" font-family="sans-serif" fill="black" stroke="none" text-anchor="middle">' +
           (label ? putLabel(P,10, small ? 8 : -8) + putLabel(B) + putLabel(C,10) : '') +
           (label && stage == 1 ? putLabel(A,10) : '') +
           (label && stage == 1 ? putLabel(D,10) : '') +
           (stage == 1 ? putLabel(aB, small ? 40 : 20, -10) : '') +
           (stage == 3 ? putLabel(bB, small ? 80 : 55, -8) : '') + // lowest
           (stage == 3 ? putLabel(bB, small ? 60 : 36, -30) : '') + '</g>');
    return s.join('');
}
// drawing to show angle trisection
function drawTrisect(args) {
    var origin = args.origin;
    var ox = origin[0] || 50, // default 50, even 0 is 50.
        oy = origin[1] || 250, // default 250, even 0 is 250.
        angle = args.angle || 72, // default 72 degree
        width = args.width || 180, // default 180, even 0 is 180.
        rise = args.rise || 28, // default 28, even 0 is 28.
        stage = args.stage || 6, // default 6, even 0 is 3.
        label = args.label == 'true', // default false.
        mark = args.mark == 'true'; // default false.
    angle = toRadian(angle); // angle in radian
    var alpha = angle/3,
        beta = 2 * alpha;
    var small = (tan(angle) < 1); // angle < 45 degree

    // define all required points, using 0-100
    var marks = new Marks();
    // square ABCD and P, E
    var A = marks.put(  0, 100, 'A');
    var B = marks.put(  0,   0, 'B');
    var C = marks.put(100,   0, 'C');
    var D = marks.put(100, 100, 'D');
    var P = small ? // that is, 100 * tan(angle) < 100, P along CD
                marks.put(100, 100 * tan(angle), 'P') :
                marks.put(100/tan(angle), 100, 'P'); // BP = angle arm
    var E = marks.put(0, 2 * rise, 'E');
    var F = marks.put(100, 2 * rise, 'F');
    var G = marks.put(  0, rise, 'G');
    var H = marks.put(100, rise, 'H');
    // for angle = 72 deg, beta = 2 * 72/3 = 48, tan(beta) is false.
    // for angle = 63 deg, beta = 2 * 63/3 = 42, tan(beta) is true.
    var J = tan(beta) < 1 ? // that is, 100 * tan(beta) < 100, J along CD
               marks.put(100, 100 * tan(beta), 'J') :
               marks.put(100/tan(beta), 100, 'J');
    var K = marks.put(100, 100 * tan(alpha), 'K');
    var T = marks.put(rise / sin(beta), 0, 'T');
    var Sy = T.x / tan(alpha); // if under, Sy > 100.
    var S = Sy < 100 ? // if Sy < 100, S is along BA.
               marks.put(0, T.x / tan(alpha), 'S'):
               marks.put((Sy - 100) * tan(alpha), 100, 'S');
    var Sz = marks.put((Sy - 100) * tan(beta), 100, 'Sz');
    var Z = marks.put(T.x + T.x * cos(beta), rise, 'Z'); // [x + x cos 2α, h]
    // then EU = (y - 2h) * tan α = x1,
    var U = marks.put((Sy - 2 * rise) * tan(alpha), 2 * rise, 'U'); // [x1, 2h]
    var X = marks.put(U.x + U.x * cos(beta), 2 * rise + U.x * sin(beta), 'X'); // [x1 + x1 cos 2α, 2h + x1 sin 2α]
    // and GV = (y - h) * tan α = x2,
    var V = marks.put((Sy - rise) * tan(alpha), rise, 'V'); // [x2, h]
    var Y = marks.put(V.x + V.x * cos(beta), rise + V.x * sin(beta), 'Y'); // [x2 + x2 cos 2α, h + x2 sin 2α]
    var W = marks.put(T.x + T.x * cos(beta), 0, 'W'); // [Z.x, 0]
    var aC = marks.put(100 * cos(beta), 100 * sin(beta), 'aC'); // reflect C along BK
    var bK = marks.put(100 / cos(alpha) * cos(angle), 100 / cos(alpha) * sin(angle), 'bK'); // reflect K along BJ
    var bD = marks.put(J.x + (100 - J.x) * cos(2 * beta), 100 + (100 - J.x) * sin(2 * beta), 'bD'); // reflect D along BJ
    // angle label, at B
    var aB = marks.put(0, 0, 'θ');
    var bB = marks.put(0, 0, 'θ/3');

    // convert all marks to raw scale for drawing
    marks.apply(function(m) {
        m.setxy(ox + m.x * width/100, oy - m.y * width/100);
    })
    // form the SVG string content
    var s = [];
    s.push(putPoly([A, B, C, D]));
    s.push(putLine(B, P));
    if (stage == 2) {
        s.push(putPoly([G, H, C, B], 'white'));
        s.push(putPoly([G, H, F, E], 'rgba(255, 255, 0, 0.5)'));
        s.push(putLine(E, F, 'yellow', 2));
        s.push(putLine(G, H, 'green'));
        s.push(putCurve([B.offset(80,0), G.offset(120, 0), E.offset(80, 0)], 'blue', 1, true, true));
    }
    if (stage == 3) {
        s.push(putLine(E, F, 'yellow', 2));
        s.push(putLine(G, H, 'green'));
        s.push(putLine(S, T));
        s.push(putPoly(Sy < 100 ? [S, T, B] : [S, T, B, A], 'white'));
        s.push(putPoly(Sy < 100 ? [S, T, Z] : [S, T, Z, Sz], 'rgba(255, 255, 0, 0.5)'));
        s.push(putCurve([B, V, Z], 'blue', 1, true, true));
        s.push(putCurve([E, U.offset(-20 * tan(alpha), -20), X], 'blue', 1, true, true));
    }
    if (stage == 4) {
        s.push(putLine(G, H, 'green'));
        s.push(putLine(S, T));
        s.push(putPoly([B, K, C], 'white'));
        s.push(putPoly([B, K, aC], 'rgba(255, 255, 0, 0.5)'));
        s.push(putCurve([T, V.offset(30, 30 * tan(alpha)), V], 'blue', 1, true, true));
    }
    if (stage == 5) {
        s.push(putLine(G, H, 'green'));
        s.push(putLine(S, T));
        s.push(putPoly([B, J, D, C], 'white'));
        s.push(putPoly(tan(beta) < 1 ? [B, J, bK] : [B, J, bD, bK], 'rgba(255, 255, 0, 0.5)'));
        s.push(putPoly([B, bK, aC], 'rgba(255, 255, 0, 0.2)'));
        s.push(putLine(B, K, 'blue', 1, true));
        s.push(putCurve([Z.offset(small ? -50 : 0, small ? 50 * tan(alpha) : 0),
                         Y.offset(small ? -20 : 20, small ? 20 * tan(beta) : -20 * tan(beta)),
                         X.offset(small ? -50 : 0, small ? 50 * tan(angle) : 0)], 'blue', 1, true, true));
    }
    if (stage == 6) {
        s.push(putLine(B, J, 'black'));
        s.push(putLine(B, K, 'black'));
    }
    // Mark relevant points
    s.push('<g stroke="black" stroke-width="3" fill="black">' +
           (mark ? putMark(B) + putMark(C) + putMark(P) : '') +
           (stage == 2 ? putMark(E,3,'blue') : '') +
           (stage == 2 ? putMark(B,3,'green') : '') +
           (stage == 3 ? putMark(V,3,'lime') : '') +
           (stage == 3 ? putMark(T,3,'lime') : '') + '</g>');
    // Label the points
    s.push('<g font-size="18" font-family="sans-serif" fill="black" stroke="none" text-anchor="middle">' +
           (label ? putLabel(P,10,-8) + putLabel(B) + putLabel(C,10) : '') +
           (stage == 1 ? putLabel(aB, small ? 40 : 20,-10) : '') +
           (stage == 2 ? putLabel(E) : '') +
           (stage == 2 ? putLabel(B) : '') +
           (stage == 3 ? putLabel(V) : '') +
           (stage == 3 ? putLabel(T) : '') +
           (stage == 6 ? putLabel(bB, small? 100 : 65,-8) : '') + // lowest
           (stage == 6 ? putLabel(bB, small? 90 : 55,-30) : '') +
           (stage == 6 ? putLabel(bB, small? 80 : 36,-50) : '') + '</g>');
    return s.join('');
}
// drawing to show ellipse
function drawEllipse(args) {
    // args is ignored
    return '<ellipse cx="150" cy="150" rx="100" ry="50" style="fill:pink;stroke:purple;stroke-width:2" />';
}
function ydrawEllipse(args) {
    return "<defs>" +
    "<marker id='head' orient='auto' markerWidth='2' markerHeight='4'" +
    " refX='0.1' refY='2'>" +
    "<path d='M0,0 V4 L2,2 Z' fill='red' />" +
    "</marker>" +
    "</defs>" +
    "<path d='M50,50 C95,95 95,15 140,50' stroke-width='5' fill='none' stroke='black' marker-end='url(#head)' />";
}
function zdrawEllipse(args) {
  return '<defs>' +
  '<style>' +
  'path, polyline { fill: none; stroke: #231F20; vector-effect: non-scaling-stroke; stroke-width: 2; }' +
  'path { stroke-dasharray: 11, 5; }' +
  '</style>' +
  '<marker id="pointer" markerWidth="10" markerHeight="8" ' +
  'refX="9.5" refY="5.1" orient="-45" markerUnits="userSpaceOnUse">' +
  '<polyline points="1 1, 9 5, 1 7" />' +
  '</marker>' +
  '</defs>' +
  '<path d="M16.7,178 c87.6-46.9,162.9-185.4,227-136.4C307.2,90.1,195,158.5,111,108.9C71,85.2,92.2,30.7,126,7" ' +
  'marker-end="url(#pointer)"/>';
}

// transform the node
function transform(node) {
    var source = node.innerHTML;
    // check optional scale
    var j = source.indexOf('[');
    var k = source.indexOf(']');
    var scale = 1; // default
    if (j == 0 && k > 0) {
        scale = parseFloat(source.slice(1, k)); // update scale
        source = source.slice(k+1); // remove scale option
    }
    // convert source to an object by JSON.parse
    var args = source.toObject();
    return doDrawing(scale, args);
}

// process all drawing nodes
function processDrawing() {
   // Collect all nodes that will receive drawing processing
   var drawingNodeArray = toArray(document.getElementsByClassName('drawing'))
                  .concat(toArray(document.getElementsByTagName('drawing')));

   // Process all nodes, replacing them as we progress
   drawingNodeArray.forEach(function (node) {
       node.parentNode.replaceChild(transform(node), node);
   });
}


// Start from here.
loadDrawingDefs(); // load the SVG definitions
processDrawing();  // find and process all <drawing> nodes

})(); // invoke single function

/** End of Geometry Script processing.
 ** Author: Joseph Chan
 ** Date: 08 April, 2021.
 */
