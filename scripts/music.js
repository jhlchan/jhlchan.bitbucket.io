/**
 Music script

 Ideas taken from:
 * Markdeep:  https://casual-effects.com/markdeep/latest/markdeep.min.js
 * ABCjs: https://www.abcjs.net/ by Paul Rosen (https://paulrosen.github.io/abcjs/)
 */

/**

 Documentation:

 Use a DIV for <music>:
 * the tag includes a tune in ABC music notation.
 * this will be replaced by a DIV called player
 * Inside the player DIV are two DIVs: paper and audio.
 * ABCjs will put the music engraving (sheet music) in paper.
 * ABCjs will put the audio media player with controls in audio.
 * The cursor to indicate tune progression is defined in this script.
 */

/**
 Common Global Functions
*/
// convert list to an array
function toArray(list) {
    return Array.prototype.slice.call(list);
}

// JC: innerHTML will duplicate <...> inside to the end, e.g.
// '\nX:39\nT:Are You Sleeping?\nO:Old French\n%R: air, march\nB:"The Everyday Song Book", 1927
//  \nF:http://www.library.pitt.edu/happybirthday/pdf/The_Everyday_Song_Book.pdf\nZ:2017 John Chambers <jc:trillian.mit.edu>
//  \nM:4/4\nL:1/8\nK:Cmaj\n"^1"C2 D2 E2 C2| C2 D2 E2 C2|"^2"E2 F2 G4| E2 F2 G4|\nw:~Are you sleep-ing, are you sleep-ing, Bro-ther John, Bro-ther John,
//  \nw:Fr\\`e-re Jac-ques, Fr\\`e-re Jac-ques, Dor-mez-vous, dor-mez-vous?\n%\n"^3"G A G F E2 C2| G A G F E2 C2|"^4"C2 G,2 C4| C2 G,2 C4 |]
//  \nw:Morn-ing bells are ring-ing, Morn-ing bells are ring-ing; Ding, ding, dong, ding, ding, dong.
//  \nw:Son-nez les ma-ti-nes, son-nez les ma-ti-nes, Bim, bim, bom, bim, bim, bom.\n</jc:trillian.mit.edu>'

// fix a string by keeping between first and last \n
function stripNewLine(line) {
   var j = line.indexOf('\n'),
       k = line.lastIndexOf('\n')
   return line.slice(j+1, k)
}

// single function application for <music>
(function() {
'use strict';

// Media routines

// Cursor control constructor
function CursorControl(paper) {

   this.onReady = function () {
      // JC: no need to have download or explanation
   }
   this.onStart = function () {
      var cursor = document.createElementNS("http://www.w3.org/2000/svg", "line");
      cursor.setAttribute("class", "abcjs-cursor");
      cursor.setAttributeNS(null, 'x1', 0)
      cursor.setAttributeNS(null, 'y1', 0)
      cursor.setAttributeNS(null, 'x2', 0)
      cursor.setAttributeNS(null, 'y2', 0)
      var svg = paper.querySelector("svg")
      svg.appendChild(cursor)
   }
   this.onEvent = function (event) {
      if (event.measureStart && event.left === null) return
      // Above is the second part of a tie across a measure line. Just ignore it.
      var lastSelection = paper.querySelectorAll("svg .highlight")
      for (var k = 0; k < lastSelection.length; k++) lastSelection[k].classList.remove("highlight")
		for (var i = 0; i < event.elements.length; i++ ) {
			 var note = event.elements[i];
			 for (var j = 0; j < note.length; j++) note[j].classList.add("highlight");
		}

      var cursor = paper.querySelector("svg .abcjs-cursor")
      if (cursor) {
         cursor.setAttribute("x1", event.left - 2)
         cursor.setAttribute("x2", event.left - 2)
         cursor.setAttribute("y1", event.top)
         cursor.setAttribute("y2", event.top + event.height)
      }
   }
   this.onFinished = function () {
      var elements = paper.querySelectorAll("svg .highlight")
      for (var i = 0; i < elements.length; i++) elements[i].classList.remove("highlight")

      var cursor = paper.querySelector("svg .abcjs-cursor")
      if (cursor) {
         cursor.setAttribute("x1", 0)
         cursor.setAttribute("x2", 0)
         cursor.setAttribute("y1", 0)
         cursor.setAttribute("y2", 0)
      }
   }
}

// Load ABC song to paper with audio

function abcLoad(paper, audio, song) {
   var cursor = new CursorControl(paper)
   var controlOptions = {
      displayLoop: true,
      displayRestart: true,
      displayPlay: true,
      displayProgress: true,
      displayWarp: true
   }
   var synthControl = new ABCJS.synth.SynthController()
   synthControl.load(audio, cursor, controlOptions)

   // JC: listen for note clicked, will be marked in red
   // JC: see function highlight(klass, color), and function unhighlight(klass, color).
   function clickListener(abcElement, tuneNumber, classes, analysis, drag, mouseEvent) {
      var lastClicked = abcElement.midiPitches
      lastClicked && // JC: quick form to have: if (lastClicked) ...
      ABCJS.synth.playEvent(lastClicked, abcElement.midiGraceNotePitches, synthControl.visualObj.millisecondsPerMeasure())
                 .then(function (response) { console.log("JC: note played") })
                 .catch (function (error) { console.log("JC: error playing note", error) })
   }

   // JC: for var visualObj = ABCJS.renderAbc(paper, song, abcOptions)[0]
   var abcOptions = {
      add_classes: true,
      clickListener: clickListener,
      responsive: "resize"
   }
   setTune(song, false) // JC: initialize controls

   // JC: set a tune, flag = true to update controls, false to initialize controls.
   function setTune(song, flag) {
      synthControl.disable(true)
      var visualObj = ABCJS.renderAbc(paper, song, abcOptions)[0]

      // TODO-PER: This will allow the callback function to have access to timing info - this should be incorporated into the render at some point.
      // JC: This combines both the visualObj and synthControl
      var midi = new ABCJS.synth.CreateSynth()
      midi.init({
         //audioContext: new AudioContext(),
         visualObj: visualObj,
         // sequence: [],
         // millisecondsPerMeasure: 1000,
         // debugCallback: function(message) { console.log(message) },
         options: {
            // soundFontUrl: "https://paulrosen.github.io/midi-js-soundfonts/FluidR3_GM/" ,
            // sequenceCallback: function(noteMapTracks, callbackContext) { return noteMapTracks; },
            // callbackContext: this,
            // onEnded: function(callbackContext),
            // pan: [ -0.5, 0.5 ]
         }
      })
      .then(function (response) {
         // console.log(response); // JC: original log
         // JC: connnect visualObj to synthControl
         // JC: therefore, if song is changed, have another visualObj, and do another setTune.
         synthControl.setTune(visualObj, flag)
                     .then(function (response) { console.log("JC: Audio successfully loaded.") })
                     .catch (function (error) { console.warn("JC: Audio problem in setune:", error) })
      })
      .catch (function (error) {
         console.warn("Audio problem in init:", error)
      })
   }

   // return a function to update song
   return function (song) {
      setTune(song, true) // JC: reuse controls
   }
}

// do media
function doMedia(source) {
   // console.log('JC: song = ', song)
   var paper = document.createElement('div')
   var audio = document.createElement('div')
   // isolate the first line ending with \n
   var k = source.indexOf('\n')
   var style = source.slice(0, k)
   // var song = source.slice(k + 1)
   var song = stripNewLine(source)
   abcLoad(paper, audio, song) // JC: ignore return

   var player = document.createElement('div')
   if (style) player.setAttribute('style', style)
   else player.className = 'player'
   player.appendChild(paper)
   player.appendChild(audio)
   return player
}

// transform the node
function transform(node) {
   var source = node.innerHTML
   return doMedia(source)
}

// load script and styles for ABC music, with onload callback
function loadMusicParts(callback) {
   // user-defined CSS
   document.write(
      '<style type="text/css">' +
      '.player { max-width: 800px; margin: auto; }' +
      '.abcjs-cursor { stroke: red }' +
      '.highlight { zfill: #0a9ecc; /* dark green */ fill: #0acc7b;  /* bright green */ }' +
      '</style>')
   // styles
   // <link rel="stylesheet" type="text/css" href="../css/abcjs-audio.css">
   // document.write('<link rel="stylesheet" type="text/css" href="../css/abcjs-audio.css">')
   // JC: this work, as well as the one below
   var css = document.createElement('link')
   css.rel = "stylesheet"
   css.type = "text/css"
   css.href = "../css/abcjs-audio.css"
   document.head.appendChild(css)
   // scripts
   // <script src="../scripts/abcjs-melody.js" type="text/javascript"></ script>
   // document.write('<script src="../scripts/abcjs-melody.js" type="text/javascript""></' + 'script>')
   // JC: this works, but there is no onload (see below), so processAbcMusic() will get: ABCJS is not defined
   var script = document.createElement('script')
   script.type = "text/javascript"
   script.src = "../scripts/abcjs-melody.js"
   script.onload = callback // JC: otherwise processAbcMusic() gets: ABCJS is not defined
   document.head.appendChild(script)
}

// process all <music> nodes
function processAbcMusic() {
   // Check if ABC audio is supported
   if (!ABCJS.synth.supportsAudio()) {
      alert("Audio is not supported in this browser.")
      return
   }
   // process any music not marked by <music>
   if (typeof main == 'function') main(abcLoad)

   // Collect all nodes that will receive music processing
   var abcNodeArray = toArray(document.getElementsByClassName('music'))
              .concat(toArray(document.getElementsByTagName('music')))

   // Process all nodes, replacing them as we progress
   abcNodeArray.forEach(function (node) {
      node.parentNode.replaceChild(transform(node), node)
   })
}

// Start from here.
// loadMusicParts(); // load the required information
// processAbcMusic(); // find and process all <music> nodes
// achieve this via script.onload callback.
loadMusicParts(processAbcMusic)

})(); // invoke single function
