/**
 Simulation script

 Ideas taken from:
 * Markdeep:  https://casual-effects.com/markdeep/latest/markdeep.min.js
 * MathIsFun: https://mathsisfun.com/geometry/images/geom-triangle.js
 */

/**

 Documentation:

 Use a Panel for <simulate>:
 * a DIV with position:relative, width and height.
 * have a Board of square size.
 * have Title and Info lines on top.
 * have an Animate/Reset button at the bottom.
 * other properties are local, e.g. the pieces.

 */

/**
 Common Global Functions
*/
// convert list to an array
function toArray(list) {
    return Array.prototype.slice.call(list);
}
// ensure val is within min to max
function constrain(min, val, max) {
    return Math.min(Math.max(min, val), max);
}
/* get a random number within min to max */
function getRandom(min, max) {
    return Math.random() * (max - min) + min;
}

/* Note: cannot be placed outside:
String.prototype.parse = function ...
as this would cause function ... to be evaluated, then assign to parse.
In that case, this = the function object, and it has no match method.
Once inside braces {}, the function is defined, not evaluated.
The function is assigned to String.prototype, and 'this' will be a string on execution.
*/

// single function application for parser
(function() {
'use strict';
/**
 A simple string parser.
 Syntax: parse(pattern), where pattern is a string of indicators:
 * w for word, of alphabets and numbers, underscore. \w+
 * c for char, a single alphabet
 * i for integers, [0-9]+
 * n for numbers, possibly with decimal. \d+\.?\d*
 * Some tips in https://stackoverflow.com/questions/2811031/
 * () for a group, giving a sublist, numbers will use parseInt or parseFloat.
 * The components are separated by whitespaces, with optional leading and ending spaces.
 */
String.prototype.parse = function (pattern) {
    // split this string into words, cleanup whitespaces.
    var words = this.match(/\b([\w\.]+)\b/g);
    // parse using local pattern
    function parseWords(pat) {
        var list = [];
        while (pat.length > 0 && words.length > 0) {
            // consume local pattern
            var ch = pat[0];
            pat = pat.slice(1);
            if (ch == '(') { // recursive call
                var j = pat.indexOf(')');
                if (j !== -1) {
                    var result = parseWords(pat.slice(0,j));
                    if (result.length > 0) list.push(result);
                    pat = pat.slice(j); // advance local pat
                }
            }
            else if (ch == ')') { // ignore
            }
            else { // consume words
                var item = words.shift();
                switch (ch) {
                case 'w' : if (item.match(/^\w+$/)) list.push(item); break;
                case 'c' : if (item.match(/^[A-Za-z]$/)) list.push(item); break;
                case 'i' : if (item.match(/^[0-9]+$/)) list.push(parseInt(item)); break;
                case 'n' : if (item.match(/^\d+\.?\d*$/)) list.push(parseFloat(item)); break;
                default : break; // discard item and ch
                }
            }
        }
        // alert('list :' + list.toString());
        return list;
    }
    // main call: ensure pattern not null.
    return parseWords(pattern || '');
}
// convert string to JSON object
String.prototype.toObject = function() {
    // split this string into words, cleanup whitespaces.
    var words = this.match(/\b([\w\.]+)\b/g);
    // remember indices pointing to a word that begins with an alphabet
    var alpha = [];
    for (var j = 0; j < words.length; j++) {
         if (words[j].match(/^[A-Za-z]/)) alpha.push(j);
    }
    // form the JSON string
    var s = [];
    while (alpha.length > 0) {
        var j = alpha.shift(); // alpha[0] = alpha.peek()
        var k = alpha.length > 0 ? alpha[0] : words.length;
        if (k == j+1) { // two consecutive words
            s.push('"' + words[j] + '":"' + (k < words.length ? words[k] : 'null') + '"');
            alpha.shift(); // discard index k
        }
        else if (k == j+2) { // a word and a number
            s.push('"' + words[j] + '":' + words[j+1]);
        }
        else { // a word and more numbers
            s.push('"' + words[j] + '":[' + words.slice(j+1, k).join(',') + ']');
        }
    }
    s = '{ ' + s.join(', ') + ' }'; // the JSON string
    // alert('toObject: ' + this + '\n' + s);
    return JSON.parse(s); // object of JSON string
}
})(); // invoke single function


/**
 Simulate, using the same idea.
 * set up stylesheet for Board and elements,
 * find all nodes of simulate class or tag,
 * parse the node content and generate a Board node,
 * embed the Board node inside a Panel node with title and button,
 * replace each <simulate> node by a Panel node.
 *
 * Elements of the board are pieces.
 * Animation of pieces is done by CSS, via keyframes.
 */

/**
 The Simulate Script
 */

// single function application for <simulate>
(function() {
'use strict';

// Constants
var size = 200;
// Safari CSS animation needs the -webkit- prefix, but not for FireFox and others.
var isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
// This uses negative look-arounds to exclude Chrome, Edge, and all Android browsers
// that include the Safari name in their user agent.
// See https://stackoverflow.com/questions/7944460/detect-safari-browser

/**
 The Utilities for Simulate

*/
// prefix fixing for Safari CSS animation
function fix(word) {
    return (isSafari ? '-webkit-' : '') + word;
}
// coordinate transform: (x, 100 - y) means [x,y]
function modify(pair) {
    return '' + pair[0] + '% ' + (100 - pair[1]) + '%';
}
// polygon with coordinate transforms
function polygon(points) {
    var poly = [];
    points.forEach(function (pair) {
        poly.push(modify(pair));
    });
    return 'polygon(' + poly.join(', ') + ');';
}
// polygon(0% 100%, 100% 100%, 100% 0%);


// load style sheet for simulate
// for .title and .info, remove: pointer-events: none;
function loadSimulateStyle() {
    var width = 660; // 660 px instead of 360px
    var s = '';
    s += '<style>'
    // s += '.frame { position:relative; width:360px; height:280px; border: 1px solid blue; border-radius: 9px; margin:auto; display:block;}'
    s += '.frame { position:relative; width:'+width+'px; height:280px; border: 1px solid blue; border-radius: 9px; margin:auto; display:block;}'
    s += '.board { position:relative; width:'+size+'px; height:'+size+'px; border:1px solid coral; display:block;}'
    s += '.piece { position:absolute; width:100%; height:100%; top:0%; left:0%; }'
    s += '.txtfont { font: 12pt arial; } '
    s += '.frame table { border-collapse:collapse; line-height:140%; page-break-inside:avoid; } '
    s += '.frame th { color:#FFF; background-color:#AAA; border:1px solid #888; padding:8px 15px 8px 15px; } '
    s += '.frame td { border:1px solid #888; padding:5px 15px 5px 15px; } '
    s += '.frame tr:nth-child(even) { background:#EEE; } '
    s += '.title { font: 12pt arial; font-weight: bold; position:absolute; top:10px; left:0px; width:'+width+'px; text-align:center;}'
    s += '.info { font: 10pt arial; font-weight: bold; color: #6600cc; position:absolute; top:31px; left:0px; width:300px; text-align:left;}'
    s += '.btn { display: inline-block; position: relative; text-align: center; margin: 2px; text-decoration: none; font: bold 14px/25px Arial, sans-serif; color: #268; border: 1px solid #88aaff; border-radius: 10px;cursor: pointer; background: linear-gradient(to top right, rgba(170,190,255,1) 0%, rgba(255,255,255,1) 100%); outline-style:none;}'
    s += '.btn:hover { background: linear-gradient(to top, rgba(255,255,0,1) 0%, rgba(255,255,255,1) 100%); }'
    s += '.noborder td {border: none}'
    s += '.blink { animation:blinking 0.8s infinite; }'
    s += '@keyframes blinking {'
    s += '      0% { color: red;         }'
    s += '     50% { color: transparent; }'
    s += '    100% { color: red;         }'
    s += '}'
    s += '.instep table { width: 90%; border-collapse: collapse; margin: 10px 0 0 10px; text-align:right; }'
    s += '.instep table tr td {border: none; }'
    s += '.instep tr { opacity:0; animation:fadein 2s steps(60, end) forwards; }'
    s += '.instep tr:nth-child(2) { animation-delay: 0.5s; }'
    s += '.instep tr:nth-child(3) { animation-delay: 1.0s; }'
    s += '@keyframes fadein {'
    s += '      0% { opacity: 0;}'
    s += '    100% { opacity: 1;}'
    s += '}'
    s += '.bird { position:absolute; width:38px; height:20px; left:160px; top:96px;'
    s += '   text-align:right; font-size:12pt; padding:5px 15px 5px 15px; background:lawngreen; }'
    s += '</style>'
    document.write(s);
}

// compute a point vertically above
function above(point, h) {
    return [point[0], point[1] + h];
}

// compute a point horizontally right
function right(point, h) {
    return [point[0] + h, point[1]];
}
// paint a div with color
function paint(div, color) {
    div.style['background-color'] = color;
}
// display a div with a flag
function display(div, flag) {
    div.style['display'] = flag ? 'block' : 'none';
}

// compute a point reflected about a line of slope tan θ
// flip matrix:
//    [ cos 2θ   sin 2θ] [x] = [x cos 2θ + y sin 2θ]
//    [ sin 2θ  -cos 2θ] [y]   [x sin 2θ - y cos 2θ]
function reflect(point, angle) {
    var x = point[0], y = point[1];
    var c = cos(2 * angle), s = sin(2 * angle);
    return [x * c + y * s, x * s - y * c];
}

// compute points for a piece with angle
function polyPoints(angle) {
    var points = [];
    points.push([0,0]);
    points.push([100,0]);
    points.push([100, 100 * tan(angle)]); // y/100 = tan θ, so y = 100 * tan θ
    // the div dimensions will clip the shape if the third point is out of range.
    return points;
}
// compute points for a thin line with endpoints p and q, with above h and right w.
function polyLine(p, q, h, w) {
    h = h || 0; // default 0
    w = w || 0; // default 0
    if (w == 0) return [p, above(p,h), above(q,h), q];
    if (h == 0) return [p, right(p,w), right(q,w), q];
    return [p, right(above(p,h), w), right(above(q,h), w), q];
}
// construct a piece
function makePiece(points, color) {
    var piece = document.createElement('div');
    piece.className = 'piece';
    var poly = polygon(points);
    // alert('points: ' + points.toString() + '\npoly: ' + poly);
    piece.setAttribute('style', 'background-color: ' + color + '; ' + fix('clip-path: ') + poly);
    return piece;
}
// make a square piece with bottom-left corner at p, size s, with color c.
function makeSquare(p, s, c) {
    c = c || 'white'; // default white
    var x = p[0], y = p[1];
    return makePiece([p, [s + x, y], [s + x, s + y], [x, s + y]], c);
}
// make a head piece under square with bottom-left corner at p, size s, with color c.
function makeHead(p, s, c) {
    c = c || 'blue'; // default blue
    var x = p[0], y = p[1];
    return makePiece([[x + s/2,y], [x, y - s], [x + s/2,  y - 2 * s/3], [x + s, y - s]], c);
}
// make a line segment at p, length s, width w, color c.
function makeLine(p, s, w, c) {
    c = c || 'black'; // default black
    w = w || 1; // default 1
    var x = p[0], y = p[1];
    return makePiece(polyLine([x, y], [x + s, y], w, 0), c);
}
// make a bar at p, height h, width w, color c.
function makeBar(p, h, w, c) {
    c = c || 'black'; // default black
    w = w || 1; // default 1
    var x = p[0], y = p[1];
    return makePiece(polyLine([x, y], [x, y + h], 0, w), c);
}

/**
 A Panel is like the frame, having:
 * a title bar
 * an information bar
 * a board, square piece for paper folding
 * a button, for Animate/Reset.
 */

// construct a Panel
function Panel (args, w, h) {
    w = w || size;  // default size, even 0 is size.
    h = h || size;  // default size, even 0 is size.
    // create the DOM elements for Panel
    var panel = document.createElement('div');
    panel.className = 'frame';
    var title = document.createElement('div');
    title.className = 'title';
    title.innerHTML = 'Panel Title';
    var info1 = document.createElement('div');
    var info2 = document.createElement('div');
    var info3 = document.createElement('div');
    var info4 = document.createElement('div');
    info1.className = 'info';
    info2.className = 'info';
    info3.className = 'info';
    info4.className = 'info';
    //info.innerHTML = '&copy; 2021, based on MathsIsFun and MarkDeep.';
    info1.setAttribute('style', 'position:absolute; left:100px; top:32px;');
    info2.setAttribute('style', 'position:absolute; left:400px; top:32px;');
    info3.setAttribute('style', 'position:absolute; left:400px; top:220px;');
    // info4.setAttribute('style', 'position:absolute; left:450px; top:208px; font:22pt arial;');
    info4.setAttribute('style', 'position:absolute; left:450px; top:208px;' +
     ' text-align:center; font:12pt arial; width:36px; height:36px;' +
     ' border-radius: 50%; background: yellow;');
    var button = document.createElement('button');
    button.className = 'btn';
    // override some style of btn
    button.setAttribute('style', 'z-index:2; position:absolute; right:3px; bottom:3px;');
    // configuration
    var table = document.createElement('div');
    table.setAttribute('style', 'position:absolute; left:10px; top:60px;');
    // pass some elements via fields in args for Board
    args.panel = panel;
    args.title = title;
    args.info1 = info1;
    args.info2 = info2;
    args.info3 = info3;
    args.info4 = info4;
    args.table = table;
    args.button = button;

    var board = new Board(args, w, h);

    // panel will have:
    //          title
    //   info1        info2
    //   table        board
    //                info3 info4    button
    // link up elements
    panel.appendChild(title);
    panel.appendChild(info1);
    panel.appendChild(info2);
    panel.appendChild(table);
    panel.appendChild(board);
    panel.appendChild(info3); // info3 covers board
    panel.appendChild(info4); // info4 covers info3
    panel.appendChild(button); // button is outside

    return panel;
}

// Pieces Manager
function Pieces () {
    // store pieces
    var pieces = [];
    // put in a piece
    this.put = function (p) {
        pieces.push(p);
        return p;
    }
    // clear all pieces
    this.clear = function () {
        pieces.forEach(function (p) {
            if (p.parentNode) p.parentNode.removeChild(p);
        });
    }
}

// Timers Manager
function Timers () {
    // store timers
    var timers = [];
    // put in a timer
    this.put = function (wait, action) {
        var t = setTimeout(action, wait * 1000); // convert to milliseconds
        timers.push(t);
        return t;
    }
    // clear all timers
    this.clear = function () {
        timers.forEach(function (t) {
            clearTimeout(t);
        });
    }
}

// A static identifier generator, for CSS.
var ID = {
    seed: 1,  // seed value
    next: function() {
        return this.seed++;
    }
};
// first time: ID.seed    gives 1.
// first call: ID.next()  gives 1.
// second call: ID.next() gives 2, etc.

// generate fixed table style for n columns, each column is w pixels
function tableStyle(n, w) {
    // need a unique identifier here
    var id = ID.next();
    var s = '<style type="text/css">' +
            'table.fixed'+id+' {table-layout:fixed; width:' + (n * w) + 'px;}' + // entire table width
            'table.fixed'+id+' td {width:' + w + 'px; border:1px solid black; overflow:hidden;}' + // fixed, hide text outside cell
            '</style>';
    document.write(s);
    return 'fixed' + id;
}

/* ------------------------------------- *
 * Turing Machine                        *
 * ------------------------------------- */

// convert boolean true to 1, false to 0
function bool(flag) {
    return flag ? 1 : 0;
}
// convert 1 to black, 0 to white
function toBW(num) {
    return num == 0 ? 'white' : 'black';
}
// convert 1 to black char, 0 to white char
function toBWC(num) {
    return num == 0 ? '◻' : '◼';
}
// convert flag to Right or Left
function toRL(flag) {
    return flag ? 'right' : 'left'
}

// parse lines for Turing configuration, return configuration or null
// will be wrapped by try and catch for any error.
//
// Example syntax of lines:
// tape BWBWW
// rule 1 W W R 2
// rule 1 B B R 1
// rule 2 W B R 0
// rule 2 B W L 2
//
// example rule
// if in state ...  , read ..., write ..., move ..., next state
// rules[state] = [when read 0, when read 1]
function parseTuring(lines) {
    var tape = []; // the initial tape
    var rules = []; // set of rules
    // parse lines for Turing machine definition
    lines.forEach(function(line) {
        var key = line.slice(0,4);
        var value = line.slice(5);
        switch (key) {
        case 'tape': // parse tape
            for (var j = 0; j < value.length; j++) tape[j] = bool(value.charAt(j) == 'B');
            break;
        case 'rule': // parse rule
            var list = value.parse('ncccn');
            // alert('rule: ' + value + ', list: ' + list);
            if (list.length == 5) {
                if (typeof rules[list[0]] == 'undefined') rules[list[0]] = [];
                rules[list[0]][bool(list[1] == 'B')] =
                    [bool(list[2] == 'B'), list[3] == 'R', list[4]];
            } else alert('Invalid rule: ' + value)
            break;
        default: alert('Turing - Invalid key: ' + key);
        }
    });
    // verify the rules of states, each rule has rule[0] and rule[1], length 2.
    var err = 0;
    for (var j = 1; j < rules.length; j++) if (rules[j].length !== 2) err++;
    if (err !== 0) { alert('Missing ' + err + ' rule(s)!'); return null; }
    // return the configuration
    return {tape: tape, rules: rules};
}

// row index given state and sym = tape[head]
// state = 1, tape[head] = 1, index = 1
// state = 1, tape[head] = 0, index = 2
// state = 2, tape[head] = 1, index = 3
// state = 2, tape[head] = 0, index = 4
function rowidx(state, sym) {
    return 2 * state - sym; // same as: 2 * state + (1 - sym) - 1;
}

// construct table from rules
function makeTable(table, rules) {
    var s = [], rule;
    s.push('<table>');
    s.push('<tr><th>at</br>state</th><th>if</br>read</th>');
    s.push('<th>then</br>write</th><th>and</br>move</th><th>next</br>state</th></tr>');
    for (var j = 1; j < rules.length; j++) {
        rule = rules[j][1];
        s.push('<tr><td>' + j + '</td><td>' + toBWC(1) + ' </td>' +
               '<td>' + toBWC(rule[0]) + '</td>' +
               '<td>' + toRL(rule[1]) + '</td><td>' + rule[2] + '</td></tr>');
        rule = rules[j][0];
        s.push('<tr><td>' + j + '</td><td>' + toBWC(0) + '</td>' +
               '<td>' + toBWC(rule[0]) + '</td>' +
               '<td>' + toRL(rule[1]) + '</td><td>' + rule[2] + '</td></tr>');
    }
    s.push('</table>');
    table.innerHTML = s.join('');
    return table;
}

// Turing object
// * a model of machine: rules, tape, head, state.
// * start = initial tape, tape will be updated through action.
// * model provides display, stepping action, and rule table indication.
function Turing (lines) {
    var start, rules = [];
    // parse lines for Turing machine definition
    try {
        var config = parseTuring(lines);
        start = config.tape;
        rules = config.rules;
    } catch (err) {
        alert('Turing lines parse error: ' + err + '\nUse default configuration.');
        // use default configuration
        start = [1, 0, 1, 0, 0]; // BWBWW
        rules[1] = [[0, true, 2], [1, true, 1]];
        rules[2] = [[1, true, 0], [0, false, 2]];
    }

    var n = start.length, head, state, tape = [];
    var mark = null, color;

    // mark state and sym = tape[head]
    function getMark(rows, state, sym) {
        mark = rows[rowidx(state, sym)];
        color = mark.style['background-color'];
        mark.style['background-color'] = 'orange';
    }
    // restore mark state
    function clearMark() {
        mark.style['background-color'] = color;
    }
    // show state
    function showState(info) {
        if (state == 0) {
            info.style['font-size'] = '9pt';
            info.style['font-weight'] = 'bold';
            info.style['line-height'] = '100%';
            info.style['border'] = '3px solid red';
            info.innerHTML = '<br>HALT';
        }
        else {
            info.style['font-size'] = '22pt';
            info.style['font-weight'] = 'normal';
            info.style['line-height'] = '120%';
            info.style['border'] = '3px solid green';
            info.innerHTML = '' + state;
        }
    }

    // get length of tape
    this.getTapeLength = function() {
        return n;
    }
    // get configuration rules
    this.getRules = function() {
        return rules;
    }
    // get current state
    this.getState = function() {
        return state;
    }
    // reset machine
    this.reset = function(info) {
        for (var j = 0; j < n; j++) tape[j] = start[j]; // clone tape
        head = 0; // head position
        state = 1; // starting state
        showState(info);
    }
    // show tape and head
    this.showMachine = function(sq, hd, rows) {
        for (var j = 0; j < n; j++) {
            paint(sq[j], toBW(tape[j]));
        }
        display(hd[head], true);
        // mark current state in table
        getMark(rows, state, tape[head]);
    }
    // machine action, return true when HALT.
    this.action = function(sq, hd, rows, info) {
        if (state == 0) return true;
        //alert('state: ' + state + ', head: ' + head);
        var move = rules[state][tape[head]];
        // write symbol move[0]
        tape[head] = move[0];
        paint(sq[head], toBW(tape[head]));
        display(hd[head], false);
        // head move by move[1]
        head = move[1] ? head + 1 : head - 1;
        // alert('after, head: ' + head);
        clearMark();
        if (head == n || head < 0) {
            alert('Machine crash!');
            state = 0; // force HALT
        }
        else {
            display(hd[head], true);
            // next state is move[2]
            state = move[2];
            getMark(rows, state, tape[head]);
        }
        showState(info);
        return false;
    }
} // end of Turing

/* ------------------------------------- *
 * Difference Table                      *
 * ------------------------------------- */

// parse lines for Difference Table generator setup, return setup or null
// will be wrapped by try and catch for any error.
//
// Example syntax of lines:
// title HTML-string-for-title
// columns 10       number of columns for the final Difference Table
// values 0 1 8 27  function values (e.g. cubes) to form initial Difference Table
function parseDTable(lines) {
    var title = ''; // title for display
    var max = 0; // max columns
    var start = []; // start values for first row
    // parse lines for Difference engine simulation
    lines.forEach(function(line) {
        var parts = line.split(' ');
        if (parts.length > 0) {
            var key = parts.shift();
            switch (key) {
            case 'title':
                title = line.slice(6); // skip title keyword
                break;
            case 'columns' :
                if (parts.length == 1) max = parseInt(parts[0]); // must be integer
                break;
            case 'values' :
                // 1 1 for Bernoulli numbers
                // otherwise only give Bernoulli-like numbers
                parts.forEach(function(c) { start.push(parseInt(c)); });
                break;
            default: alert('DTable - Invalid key: ' + key);
            }
        }
    });
    // verify max and start
    var err = 0, n = start.length;
    if (title.length == 0) err++;
    if (Number(max) !== max) err++;
    if (max == 0) err++;
    if (n == 0) err++;
    if (max < n) err++;
    start.forEach(function(c) { if (Number(c) !== c) err++; });
    if (err !== 0) { alert('Wrong syntax for DTable: ' + err + '|' + lines); return null; }
    // form initial portion of difference table from the first row
    for (var j = 0; j < n; j++) {
        if (j == 0) {
            start[0] = start.slice(0);
        }
        else {
            start[j] = [];
            for (var k = 0; k < n; k++) {
                if (k >= j) start[j][k] = start[j-1][k] - start[j-1][k-1];
            }
        }
    }
    // return the configuration
    return {title: title, max: max, start: start};
}

// DTable object
// * a model of difference table: cells.
// * start = initial cells, other cells will be updated through next.
// * model provides display, next action, and table cell indication.
function DTable (lines) {
    var n, m, start, title; // title for display only
    // parse lines for Difference Table model
    try {
        var setup = parseDTable(lines);
        title = setup.title;
        start = setup.start;
        n = start.length;
        m = setup.max;
    } catch (err) {
        alert('DTable Lines parse error: ' + err + '\nUse default setup.');
        // use default setup
        title = 'Computing cubes (top row)'
        start = // initial dtable
        [[0, 1, 8, 27],
        [ , 1, 7, 19],
        [ ,  , 6, 12],
        [ ,  ,  ,  6]];
        n = 4, m = 10; // 4 difference rows, 10 columns
    }
    var dtable; // the difference table

    // get order of dtable
    this.getOrder = function() {
        return n;
    }
    // get the max width of dtable
    this.getSize = function() {
        return m;
    }
    // get title from setup
    this.getTitle = function() {
        return title;
    }
    // set up the dtable, from start
    this.setup = function() {
        dtable = [];
        for (var j = 0; j < n; j++) {
            dtable[j] = start[j].slice(0);
        }
    }
    // make difference table, n rows, m columns, all blanks
    this.makeTable = function(n, m) {
        var s = [];
        s.push('<table width="100%">');
        for (var j = 0; j < n; j++) {
            s.push('<tr>');
            for (var k = 0; k < m; k++) {
                s.push('<td align="right" style="border:none">&nbsp;</td>');
            }
            s.push('</tr>');
        }
        s.push('</table>');
        return s.join('');
    }
    // initialize the table
    this.initTable = function(cell) {
        var twidth = start[0].length; // width of difference table at start
        for (var j = 0; j < n; j++) {
            for (var k = 0; k < m; k++) {
                cell[j][k].innerHTML =
                    (j <= k && k < twidth) ? '' + dtable[j][k] : '&nbsp;';
            }
        }
        /*
        cell[0][0].innerHTML = '' + 0;
        cell[0][1].innerHTML = '' + 1;
        cell[0][2].innerHTML = '' + 8;
        cell[0][3].innerHTML = '' + 27;
        cell[1][1].innerHTML = '' + 1;
        cell[1][2].innerHTML = '' + 7;
        cell[1][3].innerHTML = '' + 19;
        cell[2][2].innerHTML = '' + 6;
        cell[2][3].innerHTML = '' + 12;
        cell[3][3].innerHTML = '' + 6;
        */
    }
    // make column j of dtable
    this.makeColumn = function(j) {
        // from bottom up
        var k = n;
        k--;
        dtable[k][j] = dtable[k][j-1]; // the constant
        for (; k > 0;) {
            k--; // the difference rule
            dtable[k][j] = dtable[k][j-1] + dtable[k+1][j];

        }
        /*
        dtable[3][j] = dtable[3][j-1]; // the constant
        dtable[2][j] = dtable[2][j-1]+dtable[3][j]; // the difference rule
        dtable[1][j] = dtable[1][j-1]+dtable[2][j]; // the difference rule
        dtable[0][j] = dtable[0][j-1]+dtable[1][j]; // the difference rule
        */
    }

    var timer, colors = [];
    function nocolor() {
        clearTimeout(timer);
        while (colors.length > 0) {
            colors.pop().style['background-color'] = '';
        }
    }
    // show column j of cell, need animation
    this.showColumn = function(cell, j) {
        cell[n-1][j].innerHTML = '' + dtable[n-1][j]; // constant
        // self-invoking loop for delay
        (function loop(k) {
            timer = setTimeout(function() {
                while (colors.length > 0) {
                    colors.pop().style['background-color'] = '';
                }
                cell[k][j].innerHTML = '' + dtable[k][j];
                cell[k+1][j].style['background-color'] = '#f5c71a'; // deep lemon
                cell[k][j-1].style['background-color'] = '#f5c71a'; // deep lemon
                cell[k][j].style['background-color'] = 'lawngreen';
                colors.push(cell[k+1][j]);
                colors.push(cell[k][j-1]);
                colors.push(cell[k][j]);
                k--;
                if (k >= 0) {
                    loop(k); // go up
                }
                else { // k < 0, column complete
                    timer = setTimeout(nocolor, 1000); // 1 second for top green
                }
            }, k == (n-2) ? 0 : 1000); // 1000ms = 1s
        })(n-2); // start from k = n-2
    }

    var self = this, dtimer; // delay timer
    // run all columns
    this.run = function(cell) {
        // self-invoking loop for delay
        (function loop(j) {
            dtimer = setTimeout(function() {
                self.makeColumn(j);
                self.showColumn(cell, j);
                j++;
                if (j < m) loop(j);
                else clearTimeout(dtimer);
            }, j ==  n ? 0 : 1000 * n) // 1000ms = 1s, for each cells in column
        })(n); // start from j = n
    }
    // clear all timers
    this.clearTimer = function() {
        // clearTimeout(timer);
        nocolor(); // this will clear timer
        clearTimeout(dtimer);
    }
} // end of DTable

/* ------------------------------------- *
 * Difference Engine                     *
 * ------------------------------------- */

// make a wheel at p, radius r, label c
function makeWheel(p, r, c) {
    r = r || 30; // default 30
    c = c || 0;  // default 0
    var x = p[0], y = p[1];
    var svg = document.createElement('div');
    svg.className = 'piece';
    svg.path = '<path d="m'+x+','+y+' a'+r+','+(r/3)+' 0 0,0 '+(2*r)+',0 a'+r+','+(r/3)+' 0 0,0 '+(-2*r)+',0 l0,'+(r/2)+' a'+r+','+(r/3)+',0 0,0 '+(2*r)+',0 l0,'+(-r/2)+'" style="stroke:#660000;fill:lightyellow;"/>';
    svg.text = '<text x="'+(x+0.9*r)+'" y="'+(y+0.75*r)+'" font-family="Verdana" font-size="12" font-weight="bold" fill="black">';
    svg.label = c;
    svg.innerHTML = '<svg><g>' + svg.path + svg.text + svg.label + '</text></g></svg>';
    return svg;
}

// parse lines for Difference engine configuration, return configuration or null
// will be wrapped by try and catch for any error.
//
// Example syntax of lines:
// digits 6
// initial 0 1 6 6
function parseDifference(lines) {
    var digits = 0; // number of digits
    var initial = []; // start differences
    // parse lines for Difference engine simulation
    lines.forEach(function(line) {
        var parts = line.split(' ');
        if (parts.length > 0) {
            var key = parts.shift();
            switch (key) {
            case 'digits' :
                if (parts.length == 1) digits = parseInt(parts[0]); // must be integer
                break;
            case 'initial' :
                // map Number to parts, allow sign or decimal,
                // although wheels of simple engine admit only unsigned integer
                parts.forEach(function(c) { initial.push(Number(c)); });
                break;
            default: alert('Difference - Invalid key: ' + key);
            }
        }
    });
    // verify digits and initial
    var err = 0;
    if (Number(digits) !== digits) err++;
    if (digits == 0) err++;
    if (initial.length == 0) err++;
    initial.forEach(function(c) { if (Number(c) !== c) err++; });
    if (err !== 0) { alert('Wrong syntax for engine: ' + lines); return null; }
    // return the configuration
    return {digits: digits, initial: initial};
}

// Difference object
// * a model of engine: difference list (dlist), columns, number of digits.
// * start = initial dlist, dlist will be updated through turns.
// * model provides display, turning action, and difference table indication.
function Difference (lines) {
    var digit, start;
    // parse lines for difference engine
    try {
        var config = parseDifference(lines);
        digit = config.digits;
        start = config.initial;
    } catch (err) {
        alert('Engine Lines parse error: ' + err + '\nUse default configuration.');
        // use default configuration
        digit = 6; // number of digits
        start = [0, 1, 6, 6]; // start dlist
    }
    var n = start.length;
    var dlist = [];
    var error = false, self = this;
    var max = Math.pow(10, digit) - 1;
    // alert('max: ' + max); 999999
    // max = 999; // set for now

    // get the maximum value of a column
    function getMax() { return max; }
    // get the wheel column value
    function getColumn(column) {
        var value = 0;
        for (var k = 0; k < digit; k++) {
           value = 10 * value + column[digit-k-1].label; // reverse for significant digits
        }
        return value;
    }
    // change the wheel column value c
    function changeColumn(column, c) {
        // value c can be negative, or exceed max, but column cannot show these
        error = error || (c < 0) || (c > max);
        if (error) return false; // column not updated
        var r; // remainder
        var svg; // element
        for (var k = 0; k < digit; k++) {
           r = c % 10; // integer remainder
           c = (c / 10)|0; // integer quotient
           svg = column[k];
           if (r !== svg.label) {
              svg.label = r; // update label
              svg.innerHTML = '<svg><g>' + svg.path + svg.text + svg.label + '</text></g></svg>';
           }
        }
        return true; // column updated
    }

    // call methods

    // get the engine size
    this.getSize = function () { return n; }
    // get the engine precision
    this.getPrecision = function() { return digit; }
    // get the status
    this.getStatus = function() { return error; }
    // reset engine
    this.reset = function() {
        error = false;
        for (var j = 0; j < n; j++) dlist[j] = start[j]; // clone dlist
    }
    // turn engine
    this.turn = function() {
        for (var j = 0; j < n-1; j++) dlist[j] += dlist[j+1];
    }
    // make wheels with anchor [0][0] at p, each of width w, height h.
    this.makeWheels = function(p, w, h) {
        w = w || 50; // default 50
        h = h || 20; // default 20
        var x = p[0], y = p[1];
        var r = w/2 - 1; // slightly less to prevent overlapping
        var wheel = [];
        for (var j = 0; j < n; j++) {
            wheel[j] = []; // all values 0
            for (var k = 0; k < digit; k++) {
                wheel[j][k] = makeWheel([x + j * w, y - k * h], r);
            }
        }
        return wheel;
    }
    // initialize the wheels
    this.initWheels = function(wheel) {
        error = false;
        // set initial values from start[]
        for (var j = 0; j < n; j++) changeColumn(wheel[j], start[j]);
        // if any overflow or underflow, error = true
        return error;
    }
    // update the wheels
    this.updateWheels = function(wheel) {
        for (var j = 0; j < n; j++) changeColumn(wheel[j], dlist[j]);
        // if any overflow or underflow, error = true
        return error;
    }

    // fixed table class
    var fixed = tableStyle(n, 20); // table of n columns, each column 20px
    // make 2*n buttons, and append to info
    this.makeButtons = function(info) {
        // make a table with n columns, 2 rows for info.
        var s = [];
        s.push('<table class="'+fixed+'">');
        for (var j = 0; j < 2; j++) {
            s.push('<tr>');
            for (var k = 0; k < n; k++) // #defab1 is pale lime green
                s.push('<td style="background-color: #defab1; border:none;"></td>');
            s.push('</tr>');
        }
        s.push('</table>');
        var table = document.createElement('div');
        table.style['border'] = '1px solid blue';
        table.innerHTML = s.join('');
        info.appendChild(table);
        var rows = table.getElementsByTagName('tr');
        var pcells = rows[0].getElementsByTagName('td'); // plus
        var mcells = rows[1].getElementsByTagName('td'); // minus
        var btn, button = [];
        // plus buttons
        for (var j = 0; j < n; j++) {
            btn = document.createElement('button');
            btn.className = 'btn';
            btn.innerHTML = '+';
            btn.style['margin'] = '0px';
            // font-size here has no effect for FireFox
            btn.style['zfont-size'] =  '7.2pt'; // not FireFox
            pcells[j].appendChild(btn);
            button[j] = btn;
        }
        // minus buttons
        for (var j = 0; j < n; j++) {
            btn = document.createElement('button');
            btn.className = 'btn';
            btn.innerHTML = '-';
            btn.style['margin'] = '0px';
            mcells[j].appendChild(btn);
            button[n + j] = btn;
        }
        return button;
    }
    // update buttons according to wheels, update also cells, with callback for error
    this.updateButtons = function(button, wheel, cell, callback) {
        var column, btn, value, max = getMax();
        // update value of wheel[j]
        function updateValue(j, value) {
            // return a function for onclick to run
            return function() {
                dlist[j] = value; // update the model dlist
                self.updateWheels(wheel); // will changeColumn
                self.updateTable(cell, false); // update initial difference table
                // after each button click, refresh all buttons!
                if (error) callback();
                else self.updateButtons(button, wheel, cell, callback); // call itself
            }
        }
        for (var j = 0; j < n; j++) {
            value = dlist[j];
            // the '+' button
            btn = button[j];
            if (value < max) {
                btn.style['display'] = 'block'; // show this
                btn.onclick = updateValue(j, value + 1);
            }
            else {
                btn.style['display'] = 'none'; // hide this
            }
            // the '-' button
            btn = button[n + j];
            if (value > 0) {
                btn.style['display'] = 'block'; // show this
                btn.onclick = updateValue(j, value - 1);
            }
            else {
                btn.style['display'] = 'none'; // hide this
            }
        }
    }
    // update info
    this.updateInfo = function() {
        var s = [];
        s.push('<table width="100%">');
        s.push('<tr>');
        for (var j = 0; j < n-1; j++) {
            s.push('<td align="right" style="border:none">');
            s.push(dlist[j] + '</br>+ ' + dlist[j+1] + '</td>');
        }
        s.push('<td align="right" style="border:none">');
        s.push(dlist[n-1] + '</br>+ 0</td>');
        s.push('</tr>');
        s.push('</table>');
        return s.join('');
    }
    var twidth; // width of difference table
    // make difference table, width w, all blanks
    this.makeTable = function(w) {
        if (w < n + 2) {
            alert('Width: ' + w + ', too small! Set to: ' + (n + 2));
            w = n + 2;
        }
        // w = 7; // for now, 7 = 4+3
        twidth = w;
        var s = [];
        s.push('<table width="100%">');
        for (var j = 0; j < n; j++) {
            s.push('<tr>');
            for (var k = 0; k < w; k++) {
                s.push('<td align="right" style="border:none">&nbsp;</td>');
            }
            s.push('</tr>');
        }
        s.push('</table>');
        return s.join('');
    }
    var offset = -1; // iteration offset, so that first increment is 0
    var pairs = []; // for cells and background colors
    // update difference table from dlist, flag = false will only show dlist.
    // quick version, only indicates the sum
    this.updateTable = function(cell, flag) {
        var c, v, pair;
        if (!flag) {
            for (var j = 0; j < n; j++) {
                c = cell[j][j];
                c.innerHTML = '' + dlist[j];
                c.style['font-size'] = '12pt';
                c.style['font-weight'] = 'normal';
            }
            offset = -1;
        }
        else {
            // restore text colors
            while (pairs.length > 0) {
                pair = pairs.pop();
                c = pair[0];
                c.style['font-size'] = '12pt';
                c.style['font-weight'] = 'normal';
                if (c.style['color'] !== 'red') c.style['color'] = pair[1];
            }
            offset++;
            for (var j = 0; j < n; j++) {
                v = dlist[j];
                c = cell[j][offset + j];
                c.innerHTML = '' + v;
                pairs.push([c, c.style['color']]);
                c.style['color'] = (v < 0 || v > max) ? 'red' : '#12AD2B'; // parrot green
                c.style['font-size'] = '18pt';
                c.style['font-weight'] = 'bold';
            }
            if (offset + n == twidth) self.slideTable(cell);
        }
    }
    // update difference table from dlist, flag = false will only show dlist.
    // slow version, indicates the formation of the sum
    this.zzupdateTable = function(cell, flag) {
        var c, v, pair;
        if (!flag) {
            for (var j = 0; j < n; j++) {
                c = cell[j][j];
                c.innerHTML = '' + dlist[j];
                c.style['font-size'] = '12pt';
                c.style['font-weight'] = 'normal';
            }
            offset = -1;
        }
        else {
            // restore text color and font
            while (pairs.length > 0) {
                pair = pairs.pop();
                c = pair[0];
                c.style['font-size'] = '12pt';
                c.style['font-weight'] = 'normal';
                if (c.style['color'] !== 'red') c.style['color'] = pair[1];
            }
            offset++;
            // break this for loop as body with setTimeout
            for (var j = 0; j < n; j++) {
                v = dlist[j];
                c = cell[j][offset + j];
                c.innerHTML = '' + v;
                pairs.push([c, c.style['color']]);
                c.style['color'] = (v < 0 || v > max) ? 'red' : '#12AD2B'; // parrot green
                c.style['font-size'] = '18pt';
                c.style['font-weight'] = 'bold';
            }
            if (offset + n == twidth) self.slideTable(cell);
        }
    }
    // difference table, sliding
    this.slideTable = function(cell) {
        // update cell[j][k] from cell[j][twidth-n+k]
        function updateCell(j, k) {
            cell[j][k].innerHTML = cell[j][twidth-n+k].innerHTML;
        }
        // blank cell[j, k]
        function blankCell(j, k) {
            var c = cell[j][k];
            c.innerHTML = '&nbsp;';
            c.style['font-size'] = '12pt';
            c.style['font-weight'] = 'normal';
            c.style['color'] = 'black';
        }
        // slide: copy column 0 to (n-1), blank column n to twidth
        for (var j = 0; j < n; j++) {
            for (var k = 0; k < twidth; k++) {
                if (k > j) blankCell(j, k); else updateCell(j, k);
            }
        }
        offset = -1;
        self.updateTable(cell, true);
    }

} // end of Difference

/* ------------------------------------- *
 * Analytical Engine                     *
 * ------------------------------------- */

// parse lines for Analytical engine setup, return setup or null
// will be wrapped by try and catch for any error.
//
// Example syntax of lines:
// variable 0.2 0.8    weights for exams, will be v0 v1.
// variable 45.6 78.4  marks for exams, will be v2 v3.
// variable . .        working variables, will be v4 v5.
// ; program to compute, the cards. Format: vx op vy = vz
// card v0 * v2 = v4
// card v1 * v3 = v5
// card v4 + v5 = v4
function parseAnalytical(lines) {
    var start = [], program = [];
    // parse lines for Analytical engine simulation
    lines.forEach(function(line) {
        var parts = line.split(' ');
        if (parts.length > 0) {
            var key = parts.shift();
            switch (key) {
            case 'variable' :
                parts.forEach(function(c) {
                    start.push(c == '.' ? '' : parseFloat(c));
                });
                break;
            case 'card' :
                if (parts.length !== 5) alert('Analytical - Invalid card: ' + line);
                else { // the equal sign is ignored, can be any char = : | > < etc.
                    program.push([parts[1].slice(0,1), // operation
                                  parseInt(parts[0].slice(1)),
                                  parseInt(parts[2].slice(1)),
                                  parseInt(parts[4].slice(1))]);
                }
                break;
            default: alert('Analytical - Invalid key: ' + key);
            }
        }
    });
    // verify start and program
    var err = 0;
    if (start.length == 0) err++;
    start.forEach(function(c) { if (c !== '' && Number(c) !== c) err++; });
    if (program.length == 0) err++;
    program.forEach(function(c) {
        if (c.length !== 4) err++;
        if (c[0].length !== 1) err++;
        if (c[0].indexOf('+-*/') == 0) err++;
        if (Number(c[1]) !== c[1]) err++;
        if (Number(c[2]) !== c[2]) err++;
        if (Number(c[3]) !== c[3]) err++;
    });
    if (err !== 0) { alert('Wrong syntax for Analytical: ' + lines); return null; }
    // return the configuration
    return {start: start, program: program};
}

// Analytical object
// * a model of Analytical engine: cards, mill, store.
// * start = initial configuration.
// * model provides display, next action, and card indication.
function Analytical (lines) {
    var start, program;
    // parse lines for Analytical Engine
    try {
        var setup = parseAnalytical(lines);
        start = setup.start;
        program = setup.program;
    } catch (err) {
        alert('Analytical Lines parse error: ' + err + '\nUse default setup.');
        // use default setup
        start = [0.2, 0.8, 45.6, 78.4, '', '']; // initial memory cells
        program = // cards for program
        [['*', 0, 2, 4],
         ['*', 1, 3, 5],
         ['+', 4, 5, 4]];
    }
    var n = start.length, m = program.length; // 6 memory cells for store, 3 cards
    // cells for store, registers for mill, index for cards
    var cell = [], reg = [], idx = 0;

    // name of op
    function name(op) {
        switch (op) {
        case '+': return 'add';
        case '-': return 'subtract';
        case '*': return 'multiply';
        case '/': return 'divide';
        default : return 'unknown';
        }
    }
    // symbol of op
    function symbol(op) {
        switch (op) {
        case '+': return '&plus;';
        case '-': return '&minus;';
        case '*': return '&times;';
        case '/': return '&divide;';
        default: return '?';
        }
    }
    // flying styles
    function flyStyles(bird) {
        var s = [];
        s.push('<style>');
        // the get styles for r0: r0 <- j
        for (var j = 0; j < n; j++) {
            s.push('.get-0' + j + '{');
            s.push('animation-name: fly-0'+ j +';');
            s.push('animation-duration: 0.5s;');
            s.push('animation-fill-mode: forwards;');
            s.push('}');
            s.push('@keyframes fly-0'+ j +'{');
            s.push('0%   {left:'+(bird[j].style['left'])+'; top:'+(bird[j].style['top'])+'; opacity: 1;}');
            s.push('100% {left:'+(60)+'px; top:'+(40)+'px; opacity: 1;}'); // for r0
            s.push('}');
        }
        // the get styles for r1: r1 <- j
        for (var j = 0; j < n; j++) {
            s.push('.get-1' + j + '{');
            s.push('animation-name: fly-1'+ j +';');
            s.push('animation-duration: 0.5s;');
            s.push('animation-delay: 0.5s;'); // for no change in value
            s.push('animation-fill-mode: forwards;');
            s.push('}');
            s.push('@keyframes fly-1'+ j +'{');
            s.push('0%   {left:'+(bird[j].style['left'])+'; top:'+(bird[j].style['top'])+'; opacity: 1;}');
            s.push('100% {left:'+(60)+'px; top:'+(80)+'px; opacity: 1;}'); // for r1
            s.push('}');
        }
        // the put styles for r2: r2 -> j
        for (var j = 0; j < n; j++) {
            s.push('.put-2' + j + '{');
            s.push('animation-name: fly-2'+ j +';');
            s.push('animation-duration: 0.5s;');
            s.push('animation-delay: 0.8s;'); // let result visible
            s.push('animation-fill-mode: forwards;');
            s.push('}');
            s.push('@keyframes fly-2'+ j +'{');
            s.push('0%     {left:'+(60)+'px; top:'+(120)+'px; opacity: 1;}'); // for r2
            s.push('100%   {left:'+(bird[j].style['left'])+'; top:'+(bird[j].style['top'])+'; opacity: 1;}');
            s.push('}');
        }
        s.push('</style>');
        document.write(s.join(''));
    }

    // reset the engine
    this.reset = function() {
        cell = start.slice(0);
        while (cell.length < n) cell.push('');
        // op: reg[0], args: reg[1], reg[2], result: reg[3].
        reg = ['', '1st', '2nd', 'ans'];
        idx = 0;
    }
    // make cards from program
    this.makeCards = function() {
        var s = [];
        s.push('<table width="100%">');
        s.push('<tr><th>Card</th><th>Action</th></tr>');
        for (var j = 0; j < program.length; j++) {
            s.push('<tr>');
            s.push('<td>'+name(program[j][0])+'</td>');
            s.push('<td>v<sub>'+(program[j][1])+'</sub> '+symbol(program[j][0])+
                      ' v<sub>'+(program[j][2])+'</sub> &#8594;' +
                      ' v<sub>'+(program[j][3])+'</sub></td>');
            // thin arrow: &#8594;  thick arrow: &#10132;
            s.push('</tr>');
        }
        s.push('</table>');
        return s.join('');
    }

    // fixed table class
    var fixed = tableStyle(n, 70); // table of n columns, each column 70px
    // caller: board.innerHTML = engine.makeStore('position:relative; left:160px; top:50px; text-align:right; font-size:12pt;');
    // make store cells
    this.makeStore = function(style) {
        var s = [];
        s.push('<table class="'+fixed+'" style="' + style + '">');
        s.push('<tr>');
        for (var j = 0; j < n; j++) {
            s.push('<th>v<sub>'+j+'</sub></th>');
        }
        s.push('</tr>');
        s.push('<tr>');
        for (var j = 0; j < n; j++) {
            s.push('<td>&nbsp;</td>');
        }
        s.push('</tr>');
        s.push('</table>');
        return s.join('');
    }
    // update store from cells
    this.updateStore = function(store) {
        for (var j = 0; j < n; j++) {
            store[j].innerHTML = '' + cell[j];
        }
    }
    // make the mill registers
    this.makeMill = function(style) {
        var s = [];
        s.push('<table width="50%" class="noborder" style="' + style + '">');
        s.push('<tr><td>&nbsp;</td><td>&nbsp;</td></tr>');
        s.push('<tr style="background:none;"><td>&nbsp;</td><td>&nbsp;</td></tr>');
        s.push('<tr><td>&nbsp;</td><td>&nbsp;</td></tr>');
        s.push('</table>');
        return s.join('');
    }
    // update the mill
    this.updateMill = function(mill) {
        mill[0][1].innerHTML = '&nbsp;' + 'x';
        mill[1][1].innerHTML = '&nbsp;' + 'y';
        mill[1][0].innerHTML = '&nbsp;';
        mill[2][1].innerHTML = '&nbsp;' + 'z';
    }
    // make the birds
    this.makeBirds = function(board) {
        // the birds to fly
        // have a DIV covering each cell, for later movemnt
        var bird = [];
        for (var j = 0; j < n; j++) {
           bird[j] = document.createElement('div');
           bird[j].className = 'bird';
           bird[j].style['left'] = (160 + j*70) + 'px';
           bird[j].innerHTML = 'z'+j;
           board.appendChild(bird[j]);
        }
        flyStyles(bird);
        return bird;
    }
    // hide the birds
    this.hideBirds = function(bird) {
        for (var j = 0; j < n; j++) {
            bird[j].style['opacity'] = '0'; // for CSS animation, rather than display: none
        }
    }
    // clear the cards background
    this.clearCards = function(card) {
        // card[0] is header, no forEach for getElementsByTagName array.
        for (var j = 0; j < card.length; j++) {
            card[j].style['background-color'] = '';
        }
    }
    // next step through the program, return true = done.
    this.next = function(card, mill, store, bird, button) {
        if (idx == program.length) return true;
        // hide the button
        button.style['display'] = 'none';
        // indicate the working card, card[0] is header
        card[idx].style['background-color'] = '';
        card[idx+1].style['background-color'] = 'lawngreen';
        var cmd = program[idx];
        var op = cmd[0], idx1 = cmd[1], idx2 = cmd[2], idx3 = cmd[3];
        // load the birds
        bird[idx1].innerHTML = '' + cell[idx1];
        bird[idx2].innerHTML = '' + cell[idx2];
        // fly the birds
        bird[idx1].fly = 'get-0' + idx1;
        bird[idx2].fly = 'get-1' + idx2;
        bird[idx1].classList.add(bird[idx1].fly);
        bird[idx2].classList.add(bird[idx2].fly);
        // load the registers
        reg[0] = symbol(op);
        reg[1] = cell[idx1];
        reg[2] = cell[idx2];
        mill[1][0].innerHTML = reg[0]; // show the op
        var timer = setTimeout(calculate, 2000); // delay 2 seconds
        idx++; // ready for next card

        // calculate when args are ready
        function calculate() {
            clearTimeout(timer);
            // load the registers
            mill[0][1].innerHTML = '' + reg[1];
            mill[1][1].innerHTML = '' + reg[2];
            // remove animation for args, put opacity: 0
            bird[idx1].classList.remove(bird[idx1].fly);
            bird[idx2].classList.remove(bird[idx2].fly);
            // no need to move the hidden bird, that will shift the fly-back down a bit
            //bird[idx3].style['left'] = '60px';
            //bird[idx3].style['top'] = '120px';
            //bird[idx3].style['top'] = '96px'; // shift up to compensate
            switch (op) {
            case '+' : reg[3] = reg[1] + reg[2]; break;
            case '-' : reg[3] = reg[1] - reg[2]; break;
            case '*' : reg[3] = reg[1] * reg[2]; break;
            case '/' : reg[3] = reg[1] / reg[2]; break;
            default: reg[3] = 0/0; // NaN
            }
            // update answer
            reg[3] = Math.round(reg[3] * 1000) / 1000; // round to 3 decimals max
            mill[2][1].innerHTML = '' + reg[3];
            bird[idx3].innerHTML = '' + reg[3];
            // fly the bird
            bird[idx3].fly = 'put-2' + idx3;
            bird[idx3].classList.add(bird[idx3].fly);
            timer = setTimeout(tidyup, 2000); // delay 2 seconds
        }

        // tidy up after calculation
        function tidyup() {
            clearTimeout(timer);
            // update memory
            cell[idx3] = reg[3]; // internal model
            store[idx3].innerHTML = '' + reg[3]; // external value
            // remove animation for result, put opacity: 0
            bird[idx3].classList.remove(bird[idx3].fly);
            // make result visible
            bird[idx3].style['opacity'] = '1';
            // next iteration will hideBirds() by caller, opacity: 0
            // show the button
            button.style['display'] = 'block';
        }

        return (idx == program.length); // true = last card, no next().
    }

} // end Analytical

/* ------------------------------------- *
 * Rational Numbers                      *
 * ------------------------------------- */

// The Rational -- a static object Q
// * a quick-and-dirty implementation
// * no error check, fraction a/b is a pair [a, b]
// * all methods expect arguments as pair fractions
// * assume fraction a/b has b positive.
var Q = new function() {
    var self = this;

    // compute greatest common divisor
    function gcd(a, b) {
        // alert('gcd(' + a + ',' + b + ')');
        if (a < 0) a = -a;
        if (b < 0) b = -b;
        // both a, b are non-negative
        if (b == 0) return a;
        if (a == 0) return b;
        if (a < b) return gcd(b, a);
        // so b <= a, both nonzero
        var r = a % b; // remainder by b
        while (r !== 0) {
            a = b; // the smaller of a, b
            b = r; // the remainder r < b
            r = a % b; // divide again
        }
        return b;
    }

    // static methods

    // print a fraction q
    this.print = function(q) {
        return (q[1] == 1) ? q[0] : (q[0] + '/' + q[1]);
    }
    // negate a fraction q
    this.neg = function(q) {
        return [-q[0], q[1]];
    }
    // reduce a fraction q
    function reduce(q) {
        var d = gcd(q[0], q[1]);
        return [q[0]/d, q[1]/d];
    }
    // add two fractions, compute p + q
    this.add = function(p, q) {
        return reduce([p[0] * q[1] + q[0] * p[1], // numerator
                       p[1] * q[1]]);              // denominator
    }
    // subtract two fractions, compute p - q
    this.sub = function(p, q) {
        return self.add(p, self.neg(q));
    }
    // multiply two fractions, compute p * q
    this.mul = function(p, q) {
        return reduce([p[0] * q[0], p[1] * q[1]]);
    }
    // divide two fractions, compute p/q
    this.div = function(p, q) {
        return reduce([p[0] * q[1], p[1] * q[0]]);
    }
    // gcd exposed for testing
    // this.gcd = function(a, b) { return gcd(a, b); };
    this.gcd = gcd;
}

/* ------------------------------------- *
 * Bernoulli Numbers                     *
 * ------------------------------------- */

// parse lines for Bernoulli generator setup, return setup or null
// will be wrapped by try and catch for any error.
//
// Example syntax of lines:
// max 8        this is only pass to user for display, not used internally
// start 1 1    other start patterns will give Bernoulli-like numbers
function parseBernoulli(lines) {
    var max = 0; // max index
    var start = []; // start pattern
    // parse lines for Difference engine simulation
    lines.forEach(function(line) {
        var parts = line.split(' ');
        if (parts.length > 0) {
            var key = parts.shift();
            switch (key) {
            case 'max' :
                if (parts.length == 1) max = parseInt(parts[0]); // must be integer
                break;
            case 'start' :
                // 1 1 for Bernoulli numbers
                // otherwise only give Bernoulli-like numbers
                parts.forEach(function(c) { start.push(parseInt(c)); });
                break;
            default: alert('Bernoulli - Invalid key: ' + key);
            }
        }
    });
    // verify max and start
    var err = 0;
    if (Number(max) !== max) err++;
    if (max == 0) err++;
    if (start.length == 0) err++;
    start.forEach(function(c) { if (Number(c) !== c) err++; });
    if (err !== 0) { alert('Wrong syntax for Bernoulli: ' + lines); return null; }
    // return the configuration
    return {max: max, start: start};
}

// Bernoulli object
// * a model of Bernoulli numbers: pattern, equation, solve.
// * start = initial pattern, pattern will be updated through next.
// * model provides display, next action, and pattern table indication.
function Bernoulli (lines) {
    var max, start;
    // parse lines for Bernoulli generator
    try {
        var setup = parseBernoulli(lines);
        max = setup.max;
        start = setup.start;
    } catch (err) {
        alert('Bernoulli Lines parse error: ' + err + '\nUse default setup.');
        // use default setup
        max = 8; // B_0 to B_7
        start = [1, 1]; // start pattern
    }
    var pattern; // set in reset

    // The Bernoulli numbers
    var B = [];
    // B_0 = 1
    // B_1 = 1/2
    // B_2 = 1/6
    // B_3 = 0
    // B_4 = -1/30
    // B_5 = 0
    // B_6 = 1/42
    // B_7 = 0
    // B_8 = -1/30
    // B_9 = 0
    // B_10 = 5/66
    // B_11 = 0
    // B_12 = -691/2730

    // form next pattern
    function nextPattern() {
        // shift and add
        var n = pattern.length;
        pattern.unshift(0); // put zero at front
        for (var j = 0; j < n; j++) pattern[j] += pattern[j+1];
    }
    // show the pattern
    function show(pat) {
        return '<span style="color:#c34a2c">' + pat.join('&nbsp;&nbsp;') + '</span>';
    }
    // form equation of pattern
    function equation(pat) {
        var eq = [];
        var m = pat.length - 1;
        for (var j = 0; j < m; j++) {
            eq.push('<span style="color:#c34a2c">' + pat[j] + '</span>B<sub>' + j + '</sub>');
        }
        return eq.join(' + ') + ' = ' + '<span style="color:#c34a2c">' + pat[m] + '</span>x' + m;
    }
    // solve equation for Bernoulli number
    function solve(pat) {
        var n = pat.length;
        // var sum = 0;
        var sum = [0, 1]; // 0/1 = 0
        if (n > 2) {
            // add all the known multiples
            for (var j = 0; j < n-2; j++) {
                // sum += pat[j] * B[j]; // rational add, multiply
                sum = Q.add(sum, Q.mul([pat[j], 1], B[j]));
            }
        }
        /*
        var p = pat[n-1] * (n-1) - sum; // numerator, rational subtract
        var q = pat[n-2]; // denomintor
        // return {num: p, den: q};
        return p / q;
        */
        return Q.div(Q.sub(Q.mul([pat[n-1], 1], [n-1, 1]), sum), [pat[n-2], 1]);
    }
    // value of B[j]
    function value(j) {
        // return 'B<sub>' + j + '</sub> = -' + j + '/0' + j; // should be B[j]
        // return 'B<sub>' + j + '</sub> = ' + Math.round(B[j] * 100)/100; // rational show
        return 'B<sub>' + j + '</sub> = ' + Q.print(B[j]); // rational show
    }

    // call methods

    // reset generator
    this.reset = function() {
        pattern = start.slice(0); // reset pattern
    }
    // get the max number from setup
    this.getMax = function() {
        return max;
    }
    // get the pattern
    this.getPattern = function() {
        return pattern.slice(0); // return a copy
    }
    // make pattern table, with n rows, values blank
    this.makeTable = function(n) {
        //alert('pattern: [' + pattern.join(',') + ']');
        var s = [];
        s.push('<table width="100%">');
        for (var j = 0; j < n; j++) {
            s.push('<tr>');
            // the pattern
            // s.push('<td align="center" style="border:none">' + show(pattern) + '</td>');
            s.push('<td align="center" style="border:none">&nbsp;</td>');
            // the equation from pattern
            // s.push('<td align="center" style="border:none">' + equation(pattern) + '</td>');
            s.push('<td align="center" style="border:none">&nbsp;</td>');
            // B[j] = solve(pattern);
            s.push('</tr>');
            // nextPattern();
        }
        s.push('</table>');
        return s.join('');
    }
    // make value table, with n rows, values blank
    this.makeValue = function(n) {
        var s = [];
        s.push('<table width="100%">');
        for (var j = 0; j < n; j++) {
            s.push('<tr>');
            // the value
            // s.push('<td align="left" style="border:none">' + value(j) + '</td>');
            // s.push('<td align="left" style="border:none">&nbsp;</td>');
            s.push('<td></td>');
            s.push('</tr>');
        }
        s.push('</table>');
        return s.join('');
    }
    // update row table and cell table.
    this.update = function(row, cell) {
        var j = pattern.length - 2;
        var unit = row[j].getElementsByTagName('td');
        // the pattern
        // s.push('<td align="center" style="border:none">' + show(pattern) + '</td>');
        unit[0].innerHTML = show(pattern);
        // the equation from pattern
        // s.push('<td align="center" style="border:none">' + equation(pattern) + '</td>');
        unit[1].innerHTML = equation(pattern);
        B[j] = solve(pattern);
        // if (j == 12) alert('B_12: ' + Q.print(B[j]));
        // the value
        // s.push('<td align="left" style="border:none">' + value(j) + '</td>');
        cell[j].innerHTML = value(j);
    }
    // explain the next pattern.
    this.explain = function(info) {
        // the info
        var a = pattern.slice(0), b = a.slice(0), n = pattern.length;
        a.push(0); b.unshift(0);
        var c = [];
        for (var j = 0; j < n+1; j++) c.push(a[j] + b[j]);
        a.pop(); b.shift();
        // animate the next pattern
        var s = [];
        s.push('<table>');
        // first row
        s.push('<tr>');
        for (var j = 0; j < n; j++) s.push('<td>' + a[j] + '</td>');
        s.push('<td></td>'); // trailing space
        s.push('</tr>');
        // second row
        s.push('<tr>');
        s.push('<td></td>'); // leading space
        for (var j = 0; j < n; j++) s.push('<td>' + b[j] + '</td>');
        s.push('</tr>');
        // third row
        s.push('<tr>');
        for (var j = 0; j < n+1; j++) s.push('<td>' + c[j] + '</td>');
        s.push('</tr>');
        s.push('</table>');
        info.innerHTML = s.join('');
    }
    // next Bernoulli number by update pattern
    this.next = function() {
        nextPattern();
    }

} // end of Bernoulli

// construct the Board
function Board (args, w, h) {

    // create the DOM elements for scene
    var board = document.createElement('div');
    board.className = 'board';
    // board.setAttribute('style', 'position:absolute; left:80px; top:60px;');
    board.setAttribute('style', 'position:absolute; left:380px; top:60px;');

    // handle arguments
    doModes(args.mode, args.auto, args.lines);

    /* ----------------------------------------------------- *
       All handling routines.
     * ----------------------------------------------------- */

    function doModes(mode, auto, lines) {
        // animation based on mode
        switch(mode) {
        case 'test' : doTest(lines); break;
        case 'turing' : doTuring(lines, auto); break;
        case 'dtable' : doDTable(lines, auto); break;
        case 'difference' : doDifference(lines, auto); break;
        case 'analytical' : doAnalytical(lines, auto); break;
        case 'bernoulli' : doBernoulli(lines, auto); break;
        case 'sudoku' : doSudoku(lines, auto); break;
        default : doTuring(lines, true); break;
        }
    }

    /* ----------------------------------------------------- *
       All simulation modes.
     * ----------------------------------------------------- */

    // for mode: test
    function doTest(lines) {
        // ignore lines
        // show board
        board.style['background-color'] = 'lavender'; // works!
        board.innerHTML = 'I am a Board';
        board.onclick = function() { alert('Board!'); }; // works!
        // show button
        args.button.innerHTML = 'Test';
        args.button.onclick = function() { alert('Click!'); }; // works!
        // show title
        args.title.style['background-color'] = 'lightblue'; // lightblue
        args.title.onclick = function() { alert('Title!'); }; // works! remove: pointer-events: none
        // show info1
        args.info1.style['background-color'] = 'lightgreen'; // lightgreen
        args.info1.innerHTML = 'information #1';
        args.info1.onclick = function() { alert('Info1!'); }; // works!
        // show info2
        args.info2.style['background-color'] = 'lightpink'; // lightpink, overshoots!
        args.info2.innerHTML = 'information #2';
        args.info2.onclick = function() { alert('Info2!'); }; // works!
        // show info3
        args.info3.style['background-color'] = 'palegreen'; // palegreen, overshoots!
        args.info3.innerHTML = 'information #3';
        args.info3.onclick = function() { alert('Info3!'); }; // works now
        // show info4
        args.info4.innerHTML = '#4';
        args.info4.onclick = function() { alert('Info4!'); }; // works now
        // add button
        var button = document.createElement('button');
        button.className = 'btn';
        button.setAttribute('style', 'z-index:2; position:absolute; right:3px; bottom:40px;');
        button.innerHTML = 'New';
        // alert('Add button!');
        args.panel.appendChild(button);
        // test gcd
        args.info2.innerHTML = 'gcd(12, 15) = ' + Q.gcd(12, 15); // gcd = 3
        args.info2.innerHTML = 'gcd(323, 187) = ' + Q.gcd(323, 187); // gcd = 17
    } // end test

    // for mode: turing
    function doTuring(lines, auto) {
        auto = auto || false; // default false
        args.title.innerHTML = 'Turing Machine';
        args.info1.innerHTML = 'Rules';
        args.info3.innerHTML = 'State: ';
        // make panel wider
        args.panel.style['width'] = '680px'; // original 660px
        args.panel.style['height'] = '300px'; // original 280px
        // move board
        board.style['left'] = '420px';
        board.style['top'] = '60px';
        // move info2 info3 and info4
        args.info2.style['left'] = '440px'; // original 400px
        args.info3.style['left'] = '430px'; // original 400px
        args.info4.style['left'] = '480px'; // original 450px

        // the turing machine
        var turing = new Turing(lines);
        var rows; // set in doReset

        // for up: 80 = top, 50 = middle, 20 = low
        var up = 60;
        var n = turing.getTapeLength(), ww = 100/n;
        var p = [], sq = [], hd = [], ln = [];

        // make the pieces
        var pieces = new Pieces();
        for (var j = 0; j < n; j++) {
            p[j] = [j * ww, up]; // reference points for squares
            sq[j] = pieces.put(makeSquare(p[j], ww)); // tape square
            hd[j] = pieces.put(makeHead(p[j], ww)); // tape head
            ln[j] = pieces.put(makeBar(p[j], ww, 1)); // tape line vertical
        }
        var upper = pieces.put(makeLine([0, up + ww], n * ww, 1));
        var lower = pieces.put(makeLine([0, up], n * ww, -1));
        var right = pieces.put(makeBar([n * ww, up], ww, -1));

        // set up animation for Turing
        var timer;
        doReset();

        // start animation
        function doRun () { // Turing
            // alert('doRun!');
            var halt = turing.action(sq, hd, rows, args.info4);
            if (halt || turing.getState() == 0) {
                args.info2.innerHTML = 'Machine stops.';
                args.button.innerHTML = 'Reset';
                args.button.onclick = doReset;
            } else {
                args.info2.innerHTML = 'Tape changes, Head moves.';
                if (auto) {
                    timer = setTimeout(doRun, 500); // 500 milliseconds = 0.5 second
                    args.button.innerHTML = 'Reset';
                    args.button.onclick = doReset;
                }
                else {
                    args.button.innerHTML = 'Step';
                    args.button.onclick = doRun;
                }
            }
        }
        // reset animation
        function doReset () { // Turing
            // alert('doReset!');
            clearTimeout(timer);
            pieces.clear(); // remove all pieces
            // add pieces
            for (var j = 0; j < n; j++) {
                board.appendChild(sq[j]);
                board.appendChild(ln[j]);
                display(hd[j], false); // hide head
                board.appendChild(hd[j]);
            }
            board.appendChild(upper);
            board.appendChild(lower);
            board.appendChild(right);
            // reset table and rows
            rows = makeTable(args.table, turing.getRules())
                   .getElementsByTagName('tr');
            // initial tape
            turing.reset(args.info4);
            turing.showMachine(sq, hd, rows);
            args.info2.innerHTML = 'Tape and read/write Head.';
            args.button.innerHTML = 'Start';
            args.button.onclick = doRun;
        }
    } // doTuring

    // for mode: dtable
    function doDTable(lines, auto) {
        auto = auto || false; // default false
        args.title.innerHTML = 'Difference Table';

        // the difference table model
        var model = new DTable(lines); // get engine from input lines
        var n = model.getOrder(),
            m = model.getSize(); // n = 4 rows, m = 10 columns,
        args.info1.innerHTML = model.getTitle();
        args.info2.innerHTML = // #f5c71a = deep lemon
        'Rule: <span style="background:lawngreen">green</span> is the sum of two <span style="background:#f5c71a">yellow</span> values.';
        args.info2.style['left'] = '100px';
        args.info2.style['top'] = '250px';
        // hide these
        args.info3.style['display'] = 'none';
        args.info4.style['display'] = 'none';
        board.style['display'] = 'none';

        args.table.innerHTML = model.makeTable(n, m);
        var row = args.table.getElementsByTagName('tr');
        var cell = []; // the table cells
        for (var j = 0; j < n; j++) {
            cell[j] = row[j].getElementsByTagName('td');
        }
        var timer;
        doReset();

        // start animation
        function doRun () { // DTable
            // alert('doRun!');
            // reset button
            args.button.innerHTML = 'Reset';
            args.button.onclick = doReset;
            model.run(cell);
        }
        // reset animation
        function doReset () { // DTable
            // alert('doReset!');
            clearTimeout(timer); // no use!
            model.clearTimer();
            // set initial model
            model.setup();
            model.initTable(cell);
            args.button.innerHTML = 'Run';
            args.button.onclick = doRun;
        }

    } // doDTable

    // for mode: difference
    function doDifference(lines, auto) {
        auto = auto || false; // default false
        args.title.innerHTML = 'Difference Engine';
        args.info1.innerHTML = 'Difference Table';
        // alter args.info3 for buttons
        args.info3.setAttribute('style', 'position:absolute; left:380px; top:200px; width:200px;');
        args.info3.style['background-color'] = 'yellow';
        // error message
        args.info4.style['background-color'] = 'palegreen';
        args.info4.style['font-size'] = '18pt';
        args.info4.style['font-weight'] = 'bold';
        args.info4.style['width'] = '120px';
        args.info4.style['padding'] = '10px';
        args.info4.classList.add("blink"); // add class
        args.info4.innerHTML = 'Error!';
        // turn button
        var bTurn = document.createElement('button');
        bTurn.className = 'btn';
        bTurn.setAttribute('style', 'z-index:2; position:absolute; right:3px; bottom:2px;');
        bTurn.innerHTML = 'Turn';
        // auto button
        var bAuto = document.createElement('button');
        bAuto.className = 'btn';
        bAuto.setAttribute('style', 'z-index:2; position:absolute; right:3px; bottom:40px;');
        bAuto.innerHTML = 'Auto';

        // the engine
        var engine = new Difference(lines); // get engine from input lines
        var n = engine.getSize(), m = engine.getPrecision(); // n = 4, m = 6
        engine.reset();

        // difference table
        var cell; // make in doReset

        // make the wheels
        // radius = 25 for width = 100, 4 columns
        // anchor at [0, 110] for width/height = 100/100,
        // radius r = 25 for 100/4, 4 columns,
        // wheel width 50 = 2 * r, wheel height 20 = 120/6, 6 digits.
        // 110 = 120 - height/2, overlapping allows 120 instead of 100.
        var wheel = engine.makeWheels([0, 110], 50, 20);

        // make the pieces
        var pieces = new Pieces();
        for (var j = 0; j < n; j++) {
            for (var k = 0; k < m; k++) {
                pieces.put(wheel[j][k]);
            }
        }
        // clear extra
        function clearExtra() {
            if (bTurn.parentNode) bTurn.parentNode.removeChild(bTurn);
            if (bAuto.parentNode) bAuto.parentNode.removeChild(bAuto);
        }
        // refresh the pieces
        function refresh() {
            pieces.clear(); // remove all pieces
            // append the wheels
            for (var j = 0; j < n; j++) {
                for (var k = 0; k < m; k++) {
                    board.appendChild(wheel[j][k]);
                }
            }
        }

        // set up animation for Difference
        var timer;
        doReset();

        // flag an error message
        function flagError() {
            clearInterval(timer);
            clearExtra();
            args.info4.style['display'] = 'block'; // blink Error!
            args.button.innerHTML = 'Reset';
            args.button.onclick = doReset;
        }
        // start animation
        function doRun () { // Difference
            // alert('doRun!');
            // update info from difference list
            args.info3.innerHTML = engine.updateInfo();
            // reset button
            args.button.innerHTML = 'Reset';
            args.button.onclick = doReset;
            // move reset button above
            args.button.setAttribute('style', 'z-index:2; position:absolute; right:3px; bottom:80px;');
            // turn and auto buttons
            bTurn.onclick = doTurn;
            bAuto.onclick = doAuto;
            args.panel.appendChild(bTurn);
            args.panel.appendChild(bAuto);
            args.info2.innerHTML = 'Turn/Auto to do the yellow sums.';
            engine.updateTable(cell, true);
        }
        // turn of engine
        function doTurn () { // Difference
            // alert('doTurn!');
            // update info from difference list
            args.info3.innerHTML = engine.updateInfo();
            engine.turn();
            engine.updateTable(cell, true);
            engine.updateWheels(wheel);
            if (engine.getStatus()) flagError();
            else refresh();
        }
        // auto turn of engine
        function doAuto () { // Difference
            clearExtra();
            args.button.innerHTML = 'Reset';
            args.button.onclick = doReset;
            timer = setInterval(doTurn, 500); // 500 milliseconds = 0.5 second
        }
        // reset animation
        function doReset () { // Difference
            // alert('doReset!');
            clearInterval(timer);
            clearExtra();
            args.info4.style['display'] = 'none'; // hide Error
            args.info2.innerHTML = 'Add right to left, except rightmost.';
            // set initial values
            engine.reset();
            engine.initWheels(wheel);
            if (engine.getStatus()) flagError();
            // difference table
            args.table.innerHTML = engine.makeTable(8); // for 8 columns
            // *** makeTable(8) now correct!
            // *** makeTable(7) now correct!
            // end show for 10 columns
            //  970299  1000000*
            //   29107  (29701)
            //     588  (594)
            //       6  .... 6 (extruding) and wheels are: 970299, 29701, 600, 6.
            // end show for 9 columns
            //          1000000*
            //            29701   30301*
            //              594     600  606*
            //                6       6    6   6* , wheels: 970299, 29701, 600, 6.
            var row = args.table.getElementsByTagName('tr'); // the rows
            cell = []; // the table cells
            for (var j = 0; j < n; j++) {
                cell[j] = row[j].getElementsByTagName('td');
            }
            engine.updateTable(cell, false);
            // have add/sub buttons, total 8
            args.info3.innerHTML = ''; // remove all elements
            var button = engine.makeButtons(args.info3, 50, 30);
            engine.updateButtons(button, wheel, cell, flagError);
            if (engine.getStatus()) flagError();
            // set up screen
            refresh(false);
            args.button.innerHTML = 'Run';
            args.button.onclick = doRun;
            // move run button down
            args.button.setAttribute('style', 'z-index:2; position:absolute; right:3px; bottom:2px;');
        }

        // debug Reset afterwards:
        function doMyReset () { // Difference
            // alert('doMyReset!');
            clearInterval(timer);
            clearExtra();
            args.info4.style['display'] = 'none'; // hide Error
            args.info2.innerHTML = 'Add right to left, except rightmost.';
            // set initial values
            engine.reset();
            engine.initWheels(wheel);
            if (engine.getStatus()) flagError();
            // difference table
            args.table.innerHTML = engine.makeTable(8); // for 8 columns
            var row = args.table.getElementsByTagName('tr'); // the rows
            cell = []; // the table cells
            for (var j = 0; j < n; j++) {
                cell[j] = row[j].getElementsByTagName('td');
            }
            engine.updateTable(cell, false);
            // have add/sub buttons, total 8
            args.info3.innerHTML = ''; // remove all elements
            var button = engine.mymakeButtons(args.info3);
            /*
            engine.updateButtons(button, wheel, cell, flagError);
            if (engine.getStatus()) flagError();
            // set up screen
            refresh(false);
            args.button.innerHTML = 'Run';
            args.button.onclick = doRun;
            */
        }

    } // doDifference

    // for mode: analytical
    function doAnalytical(lines, auto) {
        auto = auto || false; // default false
        args.title.innerHTML = 'Analytical Engine';
        // args.title.setAttribute('style', 'position:absolute; left:100px; top:10px;');
        // make panel wider
        args.panel.style['width'] = '900px'; // original 660px
        //args.panel.style['height'] = '420px'; // original 280px
        // alter board
        board.style['left'] = '300px';
        board.style['top'] = '60px';
        board.style['width'] = '590px'; // original 200px
        board.style['height'] = '180px'; // original 200px
        // info4 = mill
        args.info4.setAttribute('style', 'position:absolute; left:310px; top:80px;' +
        ' width:150px; height:150px; text-align:center; font-family:Courier; font-size:16pt; font-weight:bold;' +
        ' z-index:-1; border-radius:50%; border:1px solid purple; background:yellow; color:#c34a2c'); // #c34a2c = chestnut
        args.info1.innerHTML = 'Punch Cards';
        args.info3.innerHTML = 'Mill';
        args.info3.setAttribute('style', 'position:absolute; left:380px; top:32px;');
        args.info2.innerHTML = 'Store';
        args.info2.setAttribute('style', 'position:absolute; left:480px; top:32px;');

        // have an engine
        var engine = new Analytical(lines);
        // engine.reset(); // now in doReset

        // make cards on left
        args.table.innerHTML = engine.makeCards();
        var card = args.table.getElementsByTagName('tr');

        // make 3 registers for mill
        args.info4.innerHTML = engine.makeMill('position:relative; left:0px; top:15px; text-align:right;');
        var row = args.info4.getElementsByTagName('tr');
        var mill = [];
        for (var j = 0; j < row.length; j++) {
            mill[j] = row[j].getElementsByTagName('td');
        }
        // mill[0][0] is blank, mill[1][0] is operator, mill[2][0] can be equal sign

        // make 6 storage cells cells
        board.innerHTML = engine.makeStore('position:relative; left:160px; top:50px; text-align:right; font-size:12pt;');
        var store = board.getElementsByTagName('td');

        // make the flying birds
        var bird = engine.makeBirds(board);

        var timer;
        doReset();

        // start animation
        function doRun () { // Analytical
            // alert('doRun!');
            engine.hideBirds(bird);
            var done = engine.next(card, mill, store, bird, args.button);
            if (done) {
                args.button.innerHTML = 'Reset';
                args.button.onclick = doReset;
            }
        }
        // reset animation
        function doReset () { // Analytical
            // alert('doReset!');
            engine.reset(); // will clearFly
            // reset
            engine.clearCards(card);
            engine.updateMill(mill);
            engine.updateStore(store);
            engine.hideBirds(bird);
            args.button.innerHTML = 'Run';
            args.button.onclick = doRun;
            //args.button.click(); // click the Next button!
        }

    } // end doAnalytical

    // for mode: bernoulli
    function doBernoulli(lines, auto) {
        auto = auto || false; // default false
        args.title.innerHTML = 'Computation of Bernoulli Numbers';
        args.title.setAttribute('style', 'position:absolute; left:180px; top:10px;');
        args.info1.innerHTML = 'pattern';
        args.info2.innerHTML = 'equation';
        args.info1.setAttribute('style', 'position:absolute; left:136px; top:32px;');
        args.info2.setAttribute('style', 'position:absolute; left:580px; top:32px;');
        // reset info3 style, move up as 'solve'
        args.info3.setAttribute('style', 'position:absolute; left:900px; top:32px;');
        args.info3.innerHTML = 'solve'; //
        // either hide, or move it to show pattern change
        args.info4.setAttribute('style', 'position:absolute; left:300px; top:240px;' +
        ' width:420px; height:150px; text-align:center; font-family:Courier; font-size:20pt; font-weight:bold;' +
        ' border-radius:25%; background:yellow; color:#c34a2c'); // #c34a2c = chestnut
        //args.info4.style['display'] = 'none'; // hide, for now
        args.info4.classList.add('instep');
        // make board just a vertical column of B_n
        board.setAttribute('style', 'position:absolute; right:54px; top:70px;');
        board.style['width'] = '130px'; // original 200px
        board.style['height'] = '345px'; // original 200px
        board.style['font-size'] = '25px';
        board.style['background-color'] = 'lavender'; // works
        // make table wider
        args.table.style['width'] = '900px'; // original 280px
        // make panel wider and longer
        args.panel.style['width'] = '1080px'; // original 660px
        args.panel.style['height'] = '420px'; // original 280px
        // these values just fits!

        // the generator
        var generator = new Bernoulli(lines); // get generator from input lines
        var max = generator.getMax(); // 8 rows
        var count; // count number of rows

        // pattern table and values
        var row, cell; // make in doReset

        // set up animation for Bernoulli
        var timer;
        doReset();

        // start animation
        function doRun () { // Bernoulli
            // alert('doRun!');
            // show the table row and info
            generator.update(row, cell);
            // each row triggers one animationend
            // args.info4.addEventListener("animationend", doAfter, false);
            args.info4.style['display'] = 'none'; // hide it
            timer = setTimeout(doAfter, 500); // 0.5 second
        }
        // after animation
        function doAfter () { // Bernoulli
            clearTimeout(timer);
            count++; // increment count
            if (count < max) {
                if (count > max/2) {
                    args.info4.style['top'] = '100px';
                    args.info4.style['height'] = '120px';
                    args.info4.style['font-size'] = '12pt';
                }
                generator.explain(args.info4);
                args.info4.style['display'] = 'block'; // show it
                generator.next(); // next pattern
            }
            else {
                args.info4.style['top'] = '200px';
                args.info4.style['height'] = '150px';
                args.info4.style['font-size'] = '20pt';
                args.button.innerHTML = 'Reset';
                args.button.onclick = doReset;
            }
        }
        // reset animation
        function doReset () { // Bernoulli
            // alert('doReset!');
            generator.reset();
            count = 0; // reset count
            args.table.innerHTML = generator.makeTable(max);
            row = args.table.getElementsByTagName('tr'); // the rows, fixed
            // after the table, which solves for value
            board.innerHTML = generator.makeValue(max);
            cell = board.getElementsByTagName('tr'); // the cells, fixed
            // this gives the correct cell height, why?
            for (var j = 0; j < cell.length; j++) cell[j].innerHTML = 'B<sub>' + j + '</sub> =';
            args.info4.style['display'] = 'none'; // hide info
            // next button
            args.button.innerHTML = 'Next';
            args.button.onclick = doRun;
            args.button.click(); // click the Next button!
        }

    } // doBernoulli

    // for mode: sudoku (todo)
    function doSudoku(lines, auto) {
        auto = auto || false; // default false
        args.title.innerHTML = 'Sudoku Puzzle';
        args.info1.innerHTML = 'Logical Rules';
        args.info4.style['display'] = 'none'; // hide this

        // for up: 80 = top, 50 = middle, 20 = low
        // for width: 100 is max, so 80 gives offset = (100-80)/2 = 10
        var up = 70;
        var n = 4, ww = 80/n, off = 10;
        var sq = new Array(n), bar = new Array(n), lower = new Array(n);
        for (var j = 0; j < n; j++) sq[j] = []; // for 2D array

        // make the pieces
        var pieces = new Pieces();
        for (var j = 0; j < n; j++) {
            for (var k = 0; k < n; k++) {
                sq[j][k] = pieces.put(makeSquare([k * ww + off, up - j * ww], ww)); // grid square
            }
            bar[j] = pieces.put(makeBar([j * ww + off, up - 3 * ww], n * ww, 1)); // grid line vertical
            lower[j] = pieces.put(makeLine([off, up - j * ww], n * ww, -1)); // grid line horizontal
        }
        var upper = pieces.put(makeLine([off, up + ww], n * ww, 1));
        var right = pieces.put(makeBar([n * ww + off, up - (n-1) * ww], n * ww, -1));

        // set up animation for Sudoku
        var timer;
        doReset();

        // start animation
        function doRun () { // Sudoku
            // alert('doRun!');
            var halt = turing.action(sq, hd, rows, args.info4);
            if (halt || turing.getState() == 0) {
                args.info2.innerHTML = 'Machine stops.';
                args.button.innerHTML = 'Reset';
                args.button.onclick = doReset;
            } else {
                args.info2.innerHTML = 'Tape changes, Head moves.';
                if (auto) {
                    timer = setTimeout(doRun, 500); // 500 milliseconds = 0.5 second
                    args.button.innerHTML = 'Reset';
                    args.button.onclick = doReset;
                }
                else {
                    args.button.innerHTML = 'Step';
                    args.button.onclick = doRun;
                }
            }
        }
        // reset animation
        function doReset () { // Sudoku
            // alert('doReset!');
            clearTimeout(timer);
            pieces.clear(); // remove all pieces
            // add pieces
            for (var j = 0; j < n; j++) {
                for (var k = 0; k < n; k++) board.appendChild(sq[j][k]);
            }
            // all squares first, then lines
            for (var j = 0; j < n; j++) {
                board.appendChild(bar[j]);
                board.appendChild(lower[j]);
            }
            board.appendChild(upper);
            board.appendChild(right);
            args.button.innerHTML = 'Start';
            //args.button.onclick = doRun;
        }
    } // doSudoku

    // final return
    return board; // the board
}

/* Note: color names:
https://www.w3schools.com/colors/colors_names.asp
*/

// transform the node
function transform(node) {
    var source = node.innerHTML;
    // remove comments and blank lines
    var lines = [];
    source.split('\n').forEach(function(line) {
        if (line && line.indexOf(';') !== -1) line = line.slice(0,line.indexOf(';'));
        if (line && line.length !== 0) lines.push(line);
    });
    // alert('filtered lines: ' + lines + ', count: ' + lines.length);
    var first = lines.shift().split(' '); // first line is mode, with optional auto
    var args = {mode: first[0],
                auto: first[1] && first[1] == 'auto',
                lines: lines }
    return new Panel(args);
}

// process all <simulate> nodes
function processSimulate() {
   // Collect all nodes that will receive <simulate> processing
   var simulateNodeArray = toArray(document.getElementsByClassName('simulate'))
                   .concat(toArray(document.getElementsByTagName('simulate')));

   // Process all nodes, replacing them as we progress
   simulateNodeArray.forEach(function (node) {
       node.parentNode.replaceChild(transform(node), node);
   });
}

// Start from here.
loadSimulateStyle(); // load the style CSS for panel, board, title, info, button
processSimulate();   // find and process all <simulate> nodes

})(); // invoke single function

/** End of Simulation Script processing.
 ** Author: Joseph Chan
 ** Date: 22 June, 2021.
 */
