/* ------------------------------------- *
 * Javascript for the site.              *
 *                                       *
 * Joseph Chan, November 2021.           *
 * ------------------------------------- */

/* ------------------------------------- *
 * Language setting and switching.       *
 * ------------------------------------- */

/**
<body class="note" onload="check()">
**/
// set defaults
function check() {
    // set_lang('en')  // default language
    // document.getElementById("english").checked = true // and mark en
    document.getElementById("english").click() // set default language and click it
}
/**
<input type="radio" name="lang" onclick="set_lang('en')" id="english" value="English" checked /> English |
<input type="radio" name="lang" onclick="set_lang('ch')" id="chinese" value="Chinese" /> 中文
**/
// get language
function getLang() {
    return document.getElementsByName('lang')[0].checked ? 'en' : 'ch'
}
// check if page is English
function isEnglish() {
    return getLang() == 'en'
}
// check if page is Chinese
function isChinese() {
    return getLang() == 'ch'
}
/**
<h2><span class="en">Notes and Links</span><span class="ch">說明及連結</span></h2>
**/
var langCallbacks = [] // callbacks for language change
// set language
function set_lang(lang) {
    var enArray = document.getElementsByClassName('en')
    var chArray = document.getElementsByClassName('ch')
    if (lang == 'en') {
       for (var i = 0; i < enArray.length; i++) enArray[i].style.display = 'inline'
       for (var i = 0; i < chArray.length; i++) chArray[i].style.display = 'none'
    }
    if (lang == 'ch') {
       for (var i = 0; i < enArray.length; i++) enArray[i].style.display = 'none'
       for (var i = 0; i < chArray.length; i++) chArray[i].style.display = 'inline'
    }
    langCallbacks.forEach(fun => fun(isEnglish()))
}

/* ------------------------------------- *
 * Modal Pop-up                          *
 * ------------------------------------- */

/**
<!-- Modal Pop-up: https://www.w3schools.com/howto/howto_css_modal_images.asp -->
<!-- The Modal -->
<div id="modal" class="modal">
  <!-- Modal Content (The Image) -->
  <img id="modal-content" />
  <!-- Modal Caption (Image Text) -->
  <div id="modal-caption"></div>
</div>
**/
// Show pop-up of image element
function showPopup(element, color) {
   color = color || 'black' // default black for background
   // Get the modal
   var modal = document.getElementById("modal")
   // Get the image and insert it inside the modal
   var image = document.getElementById("modal-content")
   image.src = element.src
   image.style.backgroundColor = color
   // Get the caption and replace it by image alt
   var caption = document.getElementById("modal-caption")
   caption.innerHTML = element.alt
   // When the user clicks on image, close the modal
   image.onclick = function() {
      modal.style.display = "none"
   }
   // Show the modal
   modal.style.display = "block"
}

// Show pop-up of image element with cycling
function showImages(element, color) {
   color = color || 'black' // default black for background
   // Get the list of pics by image alt, separated by comma
   var pics = element.alt.split(',')
   var max = pics.length, index = 0
   // Get the modal
   var modal = document.getElementById("modal")
   // Get the image and insert it inside the modal
   var image = document.getElementById("modal-content")
   image.src = pics[index]
   image.style.backgroundColor = color
   // When the user clicks on image, close the modal
   image.onclick = function() {
     index++
     if (index == max) {
        modal.style.display = "none"
     }
     else {
        image.src = pics[index]
     }
   }
   // Show the modal
   modal.style.display = "block"
}

/* ------------------------------------- *
 * Section show/hide                     *
 * ------------------------------------- */

/**
Sample <a href="javascript:showInfo('infox', true);" target="_self">show</a>.
<p>
<section id="infox" style="display:none">
Include showInfo in javascript, or use site javascript.
<p>
<a href="javascript:showInfo('infox', false);" target="_self">hide</a>
</section>
**/
// show info by flag, false = hide
function showInfo(name, flag) {
    var info = document.getElementById(name);
    info.style.display = flag ? 'block' : 'none';
}

// convert list to an array
function toArray(list) {
    return Array.prototype.slice.call(list);
}

/* ------------------------------------- *
 * Video Processing                      *
 * ------------------------------------- */

// process all <video> nodes
function processVideo(flag) {
   // default embed by URL protocol, possible values:
   // file:  ftp:  http:  https:  mailto:
   var embed = window.location.protocol == 'https:';
   // some music videos cannot embed for local URL.
   if (typeof flag === 'boolean') embed = flag; // override embed by flag = true or false
   // Collect all nodes that will receive <video> processing
   var nodeArray = toArray(document.getElementsByClassName('video'))
           .concat(toArray(document.getElementsByTagName('video')));
   // Process all nodes, replacing them as we progress
   nodeArray.forEach(function (node) {
       node.parentNode.replaceChild(transform(node), node);
   });
   // transform the node
   function transform(node) {
       var id = node.innerHTML; // video identifier
       var element = document.createElement('div');
       element.innerHTML = embed ?
           // from server, use <iframe> embedding
           // <iframe src="https://www.youtube.com/embed/abc" allowfullscreen></iframe>
           '<iframe src="https://www.youtube.com/embed/' + id + '"' +
           ' allowfullscreen></iframe>' :
           // otherwise, <iframe> embedding gives "Video unavailable", need to view on YouTube.
           // so use an image with a hyperlink to YouTube video.
           // <a href="https://www.youtube.com/watch?v=abc">
           // <img src="http://img.youtube.com/vi/abc/0.jpg" width="15%"/></a>
           '<a href="https://www.youtube.com/watch?v=' + id + '">' +
           '<img src="http://img.youtube.com/vi/' + id + '/0.jpg"' +               ' width="15%"/></a>';
       return element; // return DOM element
   }
}

/* ------------------------------------- *
 * Picture Processing                    *
 * ------------------------------------- */

 // process all <pic> nodes
 function processPic() {
    // Collect all <pic> nodes and process with replacement
    toArray(document.getElementsByClassName('pic'))
    .concat(toArray(document.getElementsByTagName('pic')))
    .forEach(function (node) {
        node.parentNode.replaceChild(transform(node), node);
    });
    // transform the node
    function transform(node) {
        var image = node.innerHTML; // picture image
        var element = document.createElement('div');
        element.innerHTML =
        '<img src="' + image + '" style="width:20%" alt="' + image + '" onclick="showPopup(this)" />';
        return element; // return DOM element
    }
 }
