
/*

SVG utilities for Javascript

*/

// scaling factor to match the grid, equivalent to 10 pixels per unit.
const scale = 10

/*
<!-- Use javascript to generate grid SVG:

<div style="width:0px;height:0px">
  <svg width="100%" height="100%" xmlns="http://www.w3.org/2000/svg">
    <defs>
      <pattern id="smallGrid" width="10" height="10" patternUnits="userSpaceOnUse">
        <path d="M 10 0 L 0 0 0 10" fill="none" stroke="gray" stroke-width="0.5"/>
      </pattern>
      <pattern id="grid" width="100" height="100" patternUnits="userSpaceOnUse">
        <rect width="100" height="100" fill="url(#smallGrid)"/>
        <path d="M 100 0 L 0 0 0 100" fill="none" stroke="gray" stroke-width="1"/>
      </pattern>
    </defs>
  </svg>  
</div>
-->
*/

// define a grid with p pixels per unit
function defineGrid(p) {
    var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg")
    svg.setAttribute('width', '100%')
    svg.setAttribute('height', '100%')
    // first definition: smallGrid
    var def1 = document.createElementNS("http://www.w3.org/2000/svg", "defs")
    var pat1 = document.createElementNS("http://www.w3.org/2000/svg", "pattern")
    var path1 = document.createElementNS("http://www.w3.org/2000/svg", "path")
    pat1.setAttribute('id', 'smallGrid')
    pat1.setAttribute('width', p)
    pat1.setAttribute('height', p)
    pat1.setAttribute('patternUnits', 'userSpaceOnUse')
    path1.setAttribute('d', 'M ' + p + ' 0 L 0 0 0 ' + p)
    path1.setAttribute('fill', 'none')
    path1.setAttribute('stroke', 'gray')
    path1.setAttribute('stroke-width', 0.5)
    pat1.appendChild(path1)
    def1.appendChild(pat1)
    svg.appendChild(def1)
    // second definition: grid
    var def2 = document.createElementNS("http://www.w3.org/2000/svg", "defs")
    var pat2 = document.createElementNS("http://www.w3.org/2000/svg", "pattern")
    var rect2 = document.createElementNS("http://www.w3.org/2000/svg", "rect")
    var path2 = document.createElementNS("http://www.w3.org/2000/svg", "path")
    pat2.setAttribute('id', 'grid')
    pat2.setAttribute('width', p * p)
    pat2.setAttribute('height', p * p)
    pat2.setAttribute('patternUnits', 'userSpaceOnUse')
    rect2.setAttribute('width', p * p)
    rect2.setAttribute('height', p * p)
    rect2.setAttribute('fill', 'url(#smallGrid)')
    path2.setAttribute('d', 'M ' + p * p + ' 0 L 0 0 0 ' + p * p)
    path2.setAttribute('fill', 'none')
    path2.setAttribute('stroke', 'gray')
    path2.setAttribute('stroke-width', 1)
    pat2.appendChild(rect2)
    pat2.appendChild(path2)
    def2.appendChild(pat2)
    svg.appendChild(def2)
    return svg
}

// fill the background with a grid
function makeBackground() {
    var rect = document.createElementNS("http://www.w3.org/2000/svg", "rect")
    rect.setAttribute('width', '100%')
    rect.setAttribute('height', '100%')
    rect.setAttribute('fill', 'url(#grid)')
    return rect
}

// make a blank canvas with background only
function makeCanvas() {
    var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg")
    svg.setAttribute('width', '100%')
    svg.setAttribute('height', '100%')
    svg.appendChild(makeBackground())
    return svg
}

// make an svg with viewbox width and height
function makeSVG(width, height) {
    var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg")
    svg.setAttribute('viewBox', width + ' ' + height)
    return svg
}

// make a windmill central square of size x with reference at p = (px, py), optional color
function makeSquare(x, px, py, color) {
    color = color || 'yellow'
    var square = document.createElementNS("http://www.w3.org/2000/svg", "rect")
    square.setAttribute('style', 'fill:' + color + ';stroke:pink;stroke-width:2;fill-opacity:0.3;stroke-opacity:0.9')
    square.setAttribute('width', x * scale)
    square.setAttribute('height', x * scale)
    square.setAttribute('x', px * scale)
    square.setAttribute('y', py * scale)
    return square
}

// make a point (x, y), optional color and radius in pixel
function makePoint(x, y, color, radius) {
    color = color || 'black'
    radius = radius || 4
    var point = document.createElementNS("http://www.w3.org/2000/svg", "circle")
    point.setAttribute('cx', x * scale)
    point.setAttribute('cy', y * scale)
    point.setAttribute('r', radius)
    point.setAttribute('fill', color)
    point.setAttribute('stroke', 'pink')
    point.setAttribute('stroke-width', 1)
    return point
}

// make a circle at (x, y), optional color and radius
function makeCircle(x, y, color, radius) {
    radius = radius || 1
    return makePoint(x, y, color, radius * scale)
}

// make a text element for SVG at p = (x,y) with line text, optional font-size and style
// <text x="50" y="50" style="font-family:Verdana;font-size:32">It's SVG!</text>
// <text x="5" y="30" fill="pink" stroke="blue" font-size="35">I love SVG!</text>
// verdana, georgia, tahoma, arial, helvetica, Brush Script MT
function makeText(x, y, line, size, color, style) {
   size = size || 22
   color = color || 'blue'
   style = style || 'font-family:Georgia;zfill:pink;zstroke:blue'
   var text = document.createElementNS("http://www.w3.org/2000/svg", "text")
   text.setAttribute('x', x * scale)
   text.setAttribute('y', y * scale)
   text.setAttribute('fill', 'pink')
   text.setAttribute('stroke', color)
   text.setAttribute('font-size', size)
   text.setAttribute('style', style)
   text.textContent = line
   return text
}

// make info at p = (x,y) adjusted, optional style and font-size
function makeInfo(x, info, style, size) {
     return makeText(info.length < 2 * x ? x - info.length/2 : 10, 56, info, 36)
}

/* SVG arrow:
https://stackoverflow.com/questions/60713744/

<svg>
  <defs>
    <marker 
      id='head' 
      orient="auto" 
      markerWidth='3' 
      markerHeight='4' 
      refX='0.1' 
      refY='2'
    >
      <path d='M0,0 V4 L2,2 Z' fill="black" />
    </marker>
  </defs>

  <path
    id='arrow-line'
    marker-end='url(#head)'
    stroke-width='4'
    fill='none' stroke='black'  
    d='M0,0, 80 100,120'
  />
        
</svg>

*/

// define arrow head marker with width w and height h, and reflects
function defineArrowHead(w, h, rx, ry) {
    w = w || 3
    h = h || 4
    rx = rx || 0.1
    ry = ry || 2
    var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg")
    // definition for marker
    var def = document.createElementNS("http://www.w3.org/2000/svg", "defs")
    var mark = document.createElementNS("http://www.w3.org/2000/svg", "marker")
    var path = document.createElementNS("http://www.w3.org/2000/svg", "path")
    mark.setAttribute('id', 'head')
    mark.setAttribute('orient', 'auto')
    mark.setAttribute('markerWidth', w)
    mark.setAttribute('markerHeight', h)
    mark.setAttribute('refX', rx)
    mark.setAttribute('refY', ry)
    path.setAttribute('d', 'M0,0 V4 L' + ry + ',' + ry + ' Z')
    path.setAttribute('fill', 'teal')
    path.setAttribute('stroke', 'context-stroke')
    // path.setAttribute('fill', 'context-fill')
    mark.appendChild(path)
    def.appendChild(mark)
    svg.appendChild(def)
    return svg
}

// make an arrow with from p = (px, py) to q = (qx, qy), optional color, stroke-width, or path
function makeArrow(px, py, qx, qy, color, width, path) {
    color = color || 'teal'
    width = width || 4
    var arrow = document.createElementNS("http://www.w3.org/2000/svg", "path")
    arrow.setAttribute('d', path || 'M' + px * scale + ',' + py * scale + ' L' + qx * scale + ',' + qy * scale)
    arrow.setAttribute('marker-end', 'url(#head)')
    arrow.setAttribute('stroke', color)
    arrow.setAttribute('stroke-width', width)
    arrow.setAttribute('fill', 'none')
    return arrow
}

// make a line from p = (px, py) to q = (qx, qy), optional color, stroke-width, or path
function makeLine(px, py, qx, qy, color, width, path) {
    color = color || 'blue'
    width = width || 1
    var line = document.createElementNS("http://www.w3.org/2000/svg", "path")
    line.setAttribute('d', path || 'M' + px * scale + ',' + py * scale + ' L' + qx * scale + ',' + qy * scale)
    line.setAttribute('stroke', color)
    line.setAttribute('stroke-width', width)
    line.setAttribute('fill', 'none')
    return line
}

// make a box of width w and height h at p = (px, py), top = true means p at top-left
function makeBox(w, h, px, py, top, color, stroke, style) {
    color = color || 'aliceblue'
    stroke = stroke || 'blue'
    var box = document.createElementNS("http://www.w3.org/2000/svg", "rect")
    box.setAttribute('style', style || 'fill:' + color + ';stroke:' + stroke + ';stroke-width:1;fill-opacity:0.8;stroke-opacity:0.9')
    box.setAttribute('width', w * scale)
    box.setAttribute('height', h * scale)
    box.setAttribute('x', px * scale)
    box.setAttribute('y', (py - (top ? 0 : h)) * scale)
    return box
}

/*

See path in:
https://svg-path-visualizer.netlify.app/

Bubble with up-tail: w = 10, h = 6
M 270,188l -20,12c -40,0 -50,5.999999999999998 -50,30c 0,24 9.999999999999998,30 50,30c 40,0 50,-5.999999999999998 50,-30c 0,-24 -9.999999999999998,-30 -40,-30z

Bubble with down-tail: w = 10, h = 6
M 250,200c -40,0 -50,5.999999999999998 -50,30c 0,24 9.999999999999998,30 30,30l -20,12l 40,-12c 40,0 50,-5.999999999999998 50,-30c 0,-24 -9.999999999999998,-30 -50,-30z

*/

// make a speech bubble at p = (px, py), size w by h, optional color and stroke-width, f for control, g for tip, flag: true = up, false = down
function makeBubble(w, h, px, py, flag, color, width, f, g) {
    color = color || 'violet' // or 'lavender'
    width = width || 2
    f = f || 0.8
    g = g || 0.2
    // scale up before compute
    w *= scale, h *= scale, px *= scale, py *= scale
    var path = flag ? // for a path with tip at top:
               'M ' + (px + w/2 + g * w)  + ',' + (py - g * h) +                                      // start at a bit off (px + w/2, py) and higher
               'l -' + g * w + ',' + g * h +                                                          // line to (px + w/2, py)
               'c -' + f * w/2 + ',0 -' + w/2 + ',' + (1 - f) * h/2 + ' -' + w/2 + ',' + h/2 +        // curve to (px, py + h/2) with controls
               'c 0,' + f * h/2 + ' ' + (1 - f) * w/2 + ',' + h/2 + ' ' + w/2 + ',' + h/2 +           // curve to (px + w/2, py + h) with controls
               'c ' + f * w/2 + ',0 ' + w/2 + ',-' + (1 - f) * h/2 + ' ' + w/2 + ',-' + h/2 +         // curve to (px + w, py + h/2) with controls
               'c 0,-' + f * h/2 + ' -' + (1 - f) * w/2 + ',-' + h/2 + ' -' + f * w/2 + ',-' + h/2 +  // curve to (px, py) but a bit off, with controls
               'z'                                                                                    // end
               : // for a path with tip at bottom: 
               'M ' + (px + w/2)  + ',' + py +                                                        // start at (px + w/2, py)
               'c -' + f * w/2 + ',0 -' + w/2 + ',' + (1 - f) * h/2 + ' -' + w/2 + ',' + h/2 +        // curve to (px, py + h/2) with controls
               'c 0,' + f * h/2 + ' ' + (1 - f) * w/2 + ',' + h/2 + ' ' + (w/2 - g * w) + ',' + h/2 + // curve to (px + w/2, py + h) off, with controls
               'l -' + g * w + ',' + g * h +                                                          // line to (px + w, py + h/2) but lower
               'l ' + 2 * g * w + ',-' + g * h +                                                      // line to (px + w, py + h/2) go back
               'c ' + f * w/2 + ',0 ' + w/2 + ',-' + (1 - f) * h/2 + ' ' + w/2 + ',-' + h/2 +         // curve to (px + w, py + h/2) with controls
               'c 0,-' + f * h/2 + ' -' + (1 - f) * w/2 + ',-' + h/2 + ' -' + w/2 + ',-' + h/2 +      // curve to (px, py) with controls
               'z'                                                                                    // end
    // console.log('JC: path: ', path)
    var bubble = document.createElementNS("http://www.w3.org/2000/svg", "path")
    bubble.setAttribute('d', path)
    bubble.setAttribute('stroke', color)
    bubble.setAttribute('stroke-width', width)
    bubble.setAttribute('fill', 'cornsilk')
    return bubble
}

// <image x="0" y="60" width="300" height="100" href="pulpitrock.jpg" />
// make an image from pic at p = (px, py)
function makeImage(pic, px, py, factor) {
    factor = factor || '40%'
    var image = document.createElementNS("http://www.w3.org/2000/svg", "image")
    image.setAttribute('href', pic)
    image.setAttribute('x', px * scale)
    image.setAttribute('y', py * scale)
    image.setAttribute('width', factor)
    image.setAttribute('height', factor)
    return image
}
