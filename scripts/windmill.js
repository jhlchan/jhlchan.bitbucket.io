
/*

Windmills and Fermat's Two Squares Theorem, by Joseph Chan.

Javascript SVG animation of the Two Squares Algorithms.

*/

/* Part 1: SVG formulation of windmills -- now in svg.js */

// scaling factor to match the grid, equivalent to 10 pixels per unit.
// const scale = 10 // defined in svg.js

// colors for the arms, to be rotated from the fixed one
var colors, armColors = ['red', 'orange', 'green', 'blue']

// reset the colors
function resetColors() {
    colors = armColors.slice(0) // copy from fixed list
}

// rotate the colors, turn value -1: last to top, 0: no rotate, 1: top to last
function rotateColors(turn) {
    if (turn < 0) colors.unshift(colors.pop())    
    if (turn > 0) colors.push(colors.shift())
}

// rotate the colors for hopping, m times
function rotateColorsHop(m) {
    if (m > 0) Array(m).fill().map(() => rotateColors(1)) // javascript repeat m times, can use m%4|0
    else if (m < 0) Array(-m).fill().map(() => rotateColors(-1)) // javascript repeat
}

// make a windmill arm of dimension y * z, with reference at p = (px, py) matching square x, indexed by j = 0,1,2,3.
function makeArm(x, y, z, px, py, j) {
    var arm = document.createElementNS("http://www.w3.org/2000/svg", "rect")
    arm.setAttribute('style', 'fill:' + colors[j] + ';stroke:pink;stroke-width:2;fill-opacity:0.3;stroke-opacity:0.9')
    switch (j) {
    case 0: // normal
      arm.setAttribute('width', y * scale)
      arm.setAttribute('height', z * scale)
      arm.setAttribute('x', px * scale)
      arm.setAttribute('y', (py - z) * scale)
      break
    case 1: // rotate 90 deg
      arm.setAttribute('width', z * scale)
      arm.setAttribute('height', y * scale)
      arm.setAttribute('x', (px + x) * scale)
      arm.setAttribute('y', py * scale)
      break
    case 2: // rotate 180 deg
      arm.setAttribute('width', y * scale)
      arm.setAttribute('height', z * scale)
      arm.setAttribute('x', (px + x - y) * scale)
      arm.setAttribute('y', (py + x) * scale)
      break
    case 3: // rotate 270 deg
      arm.setAttribute('width', z * scale)
      arm.setAttribute('height', y * scale)
      arm.setAttribute('x', (px - z) * scale)
      arm.setAttribute('y', (py + x - y) * scale)
      break
    }
    return arm
}

// make a shifted windmill arm of dimension y * z, with reference at p = (px, py) matching square x, indexed by j = 0,1,2,3.
function makeArmShift(x, y, z, px, py, j) {
    var arm = document.createElementNS("http://www.w3.org/2000/svg", "rect")
    arm.setAttribute('style', 'fill:' + colors[j] + ';stroke:pink;stroke-width:1;fill-opacity:0.3;stroke-opacity:0.9')
    switch (j) {
    case 0: // normal
      arm.setAttribute('width', y * scale)
      arm.setAttribute('height', z * scale)
      arm.setAttribute('x', (px + x - y) * scale)
      arm.setAttribute('y', (py - z) * scale)
      break
    case 1: // rotate 90 deg
      arm.setAttribute('width', z * scale)
      arm.setAttribute('height', y * scale)
      arm.setAttribute('x', (px + x) * scale)
      arm.setAttribute('y', (py + x - y) * scale)
      break
    case 2: // rotate 180 deg
      arm.setAttribute('width', y * scale)
      arm.setAttribute('height', z * scale)
      arm.setAttribute('x', px * scale)
      arm.setAttribute('y', (py + x) * scale)
      break
    case 3: // rotate 270 deg
      arm.setAttribute('width', z * scale)
      arm.setAttribute('height', y * scale)
      arm.setAttribute('x', (px - z) * scale)
      arm.setAttribute('y', py * scale)
      break
    }
    return arm
}

// mark out the mind of windmill (x,y,z) with reference at p = (px, py)
// mind (x,y,z) = if x < y - z then x + 2 * z else if x < y then 2 * y - x else x
function markMind(x, y, z, px, py) {
    var k = x < y - z ? 1 : x < y ? 2 : 3 // check cases 1, 2, 3
    var m = k == 1 ? x + 2 * z : k == 2 ? 2 * y - x : x // size of square mind for case 1, 2, 3
    var a = k == 1 ? z : k == 2 ? y - x : 0 // adjustment for px and py for case 1, 2, 3
    // console.log('JC: markMind, [' + [x, y, z] + '] case:', k, ' a =', a)
    var mind = document.createElementNS("http://www.w3.org/2000/svg", "rect")
    mind.setAttribute('style', 'fill:none;stroke:red;stroke-width:2;fill-opacity:0.1;stroke-opacity:0.9;stroke-dasharray:10,5')
    mind.setAttribute('width', m * scale)
    mind.setAttribute('height', m * scale)
    mind.setAttribute('x', (px - a) * scale)
    mind.setAttribute('y', (py - a) * scale)
    return mind
}

// append to svg components of windmill t = (x,y,z) at reference p = (px, py)
function windmill(svg, x, y, z, px, py, mind) {
    [0,1,2,3].forEach(j => svg.appendChild(makeArm(x, y, z, px, py, j)))
    svg.appendChild(makeSquare(x, px, py))
    if (mind) svg.appendChild(markMind(x, y, z, px, py))
}

// append to svg components of windmill t = (x,y,z) at reference p = (px, py) with shift arms
function windmillShift(svg, x, y, z, px, py, mind) {
    // [0,1,2,3].forEach(j => svg.appendChild(makeArmShift(x, y, z, px, py, j)))
    svg.appendChild(makeArmShift(x, y, z, px, py, 0))
    svg.appendChild(makeArmShift(x, y, z, px, py, 1))
    svg.appendChild(makeArmShift(x, y, z, px, py, 2))
    svg.appendChild(makeArmShift(x, y, z, px, py, 3))
    svg.appendChild(makeSquare(x, px, py))
    if (mind) svg.appendChild(markMind(x, y, z, px, py))
}

// make a windmill t = (x,y,z) with mind with reference at p = (px, py)
function makeWindmill(t, p, mind) {
    var svg = makeCanvas()
    windmill(svg, t[0], t[1], t[2], p[0], p[1], mind)
    return svg
}

/* Part 2: SVG animation of windmills */

// types of windmill triple, and name of types
const ping = 0, pong = 1, pung = 2, types = ['ping', 'pong', 'pung']

// return type of triple t = (x,y,z)
// is_ping (x,y,z) <=> x < z - y
// is_pong (x,y,z) <=> ~(x < z - y) /\ x < 2 * z
// is_pung (x,y,z) <=> ~(x < z - y) /\ ~(x < 2 * z)
function type(t) {
    return t[0] < t[2] - t[1] ? ping : t[0] < 2 * t[2] ? pong : pung
}

// animate the windmill central square, changing from size x to size a, at reference p = (px, py)
function animateSquare(x, a, px, py, start, duration) {
    start = start || '5s'
    duration = duration || '1s'
    // console.log('JC: animateSquare ', x, a, px, py)
    var sq = makeSquare(x, px, py)
    // example of sq: <rect width="70" height="70" x="100" y="100" style="fill:red;stroke:pink;stroke-width:3;fill-opacity:0.1;stroke-opacity:0.9" />
    // animate with changes to width, height, and center (x,y)
    // shifting new square center (x,y) or corner
    var d = (x - a)/2 // amount of shift
    var animate1 = document.createElementNS("http://www.w3.org/2000/svg", "animate")
    var animate2 = document.createElementNS("http://www.w3.org/2000/svg", "animate")
    var animate3 = document.createElementNS("http://www.w3.org/2000/svg", "animateMotion")
    // animate square to stretch or shrink
    animate1.setAttribute('begin', start)
    animate1.setAttribute('dur', duration)
    animate1.setAttribute('fill', 'freeze')
    animate1.setAttribute('attributeName', 'width')
    animate1.setAttribute('from', x * scale)
    animate1.setAttribute('to', a * scale)

    animate2.setAttribute('begin', start)
    animate2.setAttribute('dur', duration)
    animate2.setAttribute('fill', 'freeze')
    animate2.setAttribute('attributeName', 'height')
    animate2.setAttribute('from', x * scale)
    animate2.setAttribute('to', a * scale)

    // animate center movement
    animate3.setAttribute('begin', start)
    animate3.setAttribute('dur', duration)
    animate3.setAttribute('fill', 'freeze')
    animate3.setAttribute('path', 'M0,0 ' + d * scale + ',' + d * scale)

    sq.appendChild(animate1)
    sq.appendChild(animate2)
    sq.appendChild(animate3)
    return sq
}

// animate the arms of windmill changing from (x,y,z) to (a,b,c), at reference p = (px, py) for index j
function animateArm(x, y, z, a, b, c, px, py, j, start, duration) {
    start = start || '5s'
    duration = duration || '1s'
    // console.log('JC: animateArm (', x, y, z, b, px, py, j, ')')
    var arm = makeArm(x, y, z, px, py, j)
    var animate1 = document.createElementNS("http://www.w3.org/2000/svg", "animateTransform")
    var animate2 = document.createElementNS("http://www.w3.org/2000/svg", "animate")
    var animate3 = document.createElementNS("http://www.w3.org/2000/svg", "animate")
    var animate4 = document.createElementNS("http://www.w3.org/2000/svg", "animate")
    // animate1 is arm rotation
    animate1.setAttribute('attributeName', 'transform')
    animate1.setAttribute('type', 'rotate')
    animate1.setAttribute('begin', '2s')
    animate1.setAttribute('dur', '1s')
    animate1.setAttribute('fill', 'freeze')
    // animate2 is arm stretch or shrink
    animate2.setAttribute('begin', start)
    animate2.setAttribute('dur', duration)
    animate2.setAttribute('fill', 'freeze')
    // animate3 is arm shift due to stretch/shrink
    animate3.setAttribute('begin', start)
    animate3.setAttribute('dur', duration)
    animate3.setAttribute('fill', 'freeze')
    // animate4 is for pong with arms slide over, or ping with arms shift due to width/height change (same code!)
    animate4.setAttribute('begin', start)
    animate4.setAttribute('dur', duration)
    animate4.setAttribute('fill', 'freeze')

    // adjustment depends on type of t = [x,y,z]
    var u = type([x,y,z])
    var s = u == ping ? a - b : u == pong ? (x - a)/2 : a + c // shift amounts

    // arm indices: 0 = top (north), 1 = right (east), 2 = bottom (south), 3 = left (west)
    switch (j) {
    case 0: // normal
      var cx = (px + z/2) * scale, cy = (py - z/2) * scale, cz = (z - y) * scale  // center of rotation, shift
      // console.log('JC: animateArm 0, cx = ', cx, 'cy = ', cy, 'cz = ', cz)
      animate1.setAttribute('from', '0 ' + cx + ' ' + cy)
      animate1.setAttribute('to', '-90 ' + cx + ' ' + cy)
      animate2.setAttribute('attributeName', 'width')
      animate2.setAttribute('from', y * scale)
      animate2.setAttribute('to', b * scale)
      if (u == ping) { // change height and shift
          animate3.setAttribute('attributeName', 'height')
          animate3.setAttribute('from', z * scale)
          animate3.setAttribute('to', c * scale)
          arm.appendChild(animate3)
          // shift code is the same as slide code!
          // animate4.setAttribute('attributeName', 'y')
          // animate4.setAttribute('from', (py - z) * scale)
          // animate4.setAttribute('to', (py - z + s) * scale)
          // arm.appendChild(animate4)
      }
      else { // others shift after width change
          animate3.setAttribute('attributeName', 'x')
          animate3.setAttribute('from', px * scale)
          animate3.setAttribute('to', (px - s) * scale)        
          arm.appendChild(animate3)
      }
      // only pong needs arms to slide over, and ping needs shift
      if (u !== pung) { // u == pong || u == ping
          animate4.setAttribute('attributeName', 'y')
          animate4.setAttribute('from', (py - z) * scale)
          animate4.setAttribute('to', (py - z + s) * scale)
          arm.appendChild(animate4)
      }
      break
    case 1: // rotate 90 deg
      var cx = (px + x + z/2) * scale, cy = (py + z/2) * scale, cz = (y - z) * scale  // center of rotation, shift
      // console.log('JC: animateArm 1, cx = ', cx, 'cy = ', cy, 'cz = ', cz)
      animate1.setAttribute('from', '0 ' + cx + ' ' + cy)
      animate1.setAttribute('to', '-90 ' + cx + ' ' + cy)
      animate2.setAttribute('attributeName', 'height')
      animate2.setAttribute('from', y * scale)
      animate2.setAttribute('to', b * scale)
      if (u == ping) { // change width, no shift
          animate3.setAttribute('attributeName', 'width')
          animate3.setAttribute('from', z * scale)
          animate3.setAttribute('to', c * scale)
          arm.appendChild(animate3)
      }
      else { // others shift after height change
          animate3.setAttribute('attributeName', 'y')
          animate3.setAttribute('from', py * scale)
          animate3.setAttribute('to', (py - s) * scale)        
          arm.appendChild(animate3)
      }
      // only pong needs arms to slide over
      if (u == pong) {
          animate4.setAttribute('attributeName', 'x')
          animate4.setAttribute('from', (px + x) * scale)
          animate4.setAttribute('to', (px + x - s) * scale)
          arm.appendChild(animate4)
      }
      break
    case 2: // rotate 180 deg
      var cx = (px + x - z/2) * scale, cy = (py + x + z/2) * scale, cz = (y - z) * scale  // center of rotation, shift
      // console.log('JC: animateArm 2, cx = ', cx, 'cy = ', cy, 'cz = ', cz)
      animate1.setAttribute('from', '0 ' + cx + ' ' + cy)
      animate1.setAttribute('to', '-90 ' + cx + ' ' + cy)
      animate2.setAttribute('attributeName', 'width')
      animate2.setAttribute('from', y * scale)
      animate2.setAttribute('to', b * scale)
      if (u == ping) { // change height, no shift
          animate3.setAttribute('attributeName', 'height')
          animate3.setAttribute('from', z * scale)
          animate3.setAttribute('to', c * scale)
          arm.appendChild(animate3)
      }
      // only pong needs arms to slide over
      if (u == pong) {
          animate4.setAttribute('attributeName', 'y')
          animate4.setAttribute('from', (py + x) * scale)
          animate4.setAttribute('to', (py + x - s) * scale)
          arm.appendChild(animate4)
      }
      break
    case 3: // rotate 270 deg
      var cx = (px - z/2) * scale, cy = (py + x - z/2) * scale, cz = (y - z) * scale  // center of rotation, shift
      // console.log('JC: animateArm 3, cx = ', cx, 'cy = ', cy, 'cz = ', cz)
      animate1.setAttribute('from', '0 ' + cx + ' ' + cy)
      animate1.setAttribute('to', '-90 ' + cx + ' ' + cy)
      animate2.setAttribute('attributeName', 'height')
      animate2.setAttribute('from', y * scale)
      animate2.setAttribute('to', b * scale)
      if (u == ping) { // change width and shift
          animate3.setAttribute('attributeName', 'width')
          animate3.setAttribute('from', z * scale)
          animate3.setAttribute('to', c * scale)
          arm.appendChild(animate3)
          // shift code is the same as slide code!
          // animate4.setAttribute('attributeName', 'x')
          // animate4.setAttribute('from', (px - z) * scale)
          // animate4.setAttribute('to', (px - z + s) * scale)
          // arm.appendChild(animate4)
      }
      // only pong needs arms to slide over, and ping needs shift
      if (u !== pung) { // u == pong || u == ping
          animate4.setAttribute('attributeName', 'x')
          animate4.setAttribute('from', (px - z) * scale)
          animate4.setAttribute('to', (px - z + s) * scale)
          arm.appendChild(animate4)
      }
      break
    }
    arm.appendChild(animate1)
    arm.appendChild(animate2)
    return arm  
}

// animate mind of windmill (x,y,z), flag = true for before, flag = false for after
function animateMind(x, y, z, px, py, flag, start, duration) {
    start = start || '4s'
    duration = duration || '1s'
    var mind = markMind(x, y, z, px, py)
    var animate = document.createElementNS("http://www.w3.org/2000/svg", "animate")
    animate.setAttribute('attributeName', 'visibility')
    animate.setAttribute('from', flag ? 'visible' : 'hidden')
    animate.setAttribute('to', flag ? 'hidden' : 'visible')
    animate.setAttribute('begin', flag ? '0s' : start)
    animate.setAttribute('dur', duration)
    animate.setAttribute('fill', 'freeze')
    mind.setAttribute('visibility', flag ? 'visible' : 'hidden')
    mind.appendChild(animate)
    return mind
}

// animate windmill changing from t = (x,y,z) to w = (a,b,c) at reference p = (px, py) for svg
function animateWindmill(svg, t, w, p, line) {
    var x = t[0], y = t[1], z = t[2], a = w[0], b = w[1], c = w[2], px = p[0], py = p[1]
    var e = type(t) == pong ? c : b  // stretch for next windmill
    // ! [0,1,2,3].forEach(j => svg.appendChild(animateArm(x, y, z, a, e, c, px, py, j)))
    svg.appendChild(animateArm(x, y, z, a, e, c, px, py, 0))
    svg.appendChild(animateArm(x, y, z, a, e, c, px, py, 1))
    svg.appendChild(animateArm(x, y, z, a, e, c, px, py, 2))
    svg.appendChild(animateArm(x, y, z, a, e, c, px, py, 3))
    svg.appendChild(animateSquare(x, a, px, py))
    var s = type(t) == pong ? (x - a)/2 : b > c ? z : -y // new mind shift amount
    svg.appendChild(animateMind(x, y, z, px, py, true))
    svg.appendChild(animateMind(a, b, c, px + s, py + s, false))
    if (line) svg.appendChild(makeInfo(px, line))
}

// make an animation of windmill t = (x,y,z) to windmill w = (a,b,c) at reference p = (px, py)
function makeAnimate(t, w, p, line) {
    var svg = makeCanvas()
    animateWindmill(svg, t, w, p, line)
    return svg
}

// final arm motion with path
function finalArm(x, y, z, px, py, j, path, start, duration) {
    start = start || '1s'
    duration = duration || '1s'
    // <animateMotion begin="1s" dur="1s" fill="freeze" path="M0,0 0,-10" />
    // animation of arm1: move along a path
    var arm = makeArm(x, y, z, px, py, j)
    var animate = document.createElementNS("http://www.w3.org/2000/svg", "animateMotion")
    animate.setAttribute('begin', start)
    animate.setAttribute('dur', duration)
    animate.setAttribute('fill', 'freeze')
    animate.setAttribute('path', path)
    arm.appendChild(animate)
    return arm
}

// final animation of windmill t = (x,y,z) with y = z, at reference p = (px, py)
function finalAnimate(t, p, line, start, duration) {
    start = start || '1s'
    duration = duration || '1s'
    var x = t[0], y = t[1], z = t[2], px = p[0], py = p[1]
    var svg = makeCanvas()
    svg.appendChild(finalArm(x, y, z, px, py, 0, 'M0,0 h' + (x + y) * scale + ' v' + (x - y) * scale, start, duration))
    svg.appendChild(finalArm(x, y, z, px, py, 1, 'M0,0 v' + (x - y - z) * scale, start, duration))
    svg.appendChild(finalArm(x, y, z, px, py, 2, 'M0,0 h' + y * scale + ' v' + (-z) * scale, start, duration))
    svg.appendChild(finalArm(x, y, z, px, py, 3, 'M0,0 v' + y * scale + ' h' + (x + y + z) * scale + ' v' + (-z) * scale, start, duration))
    svg.appendChild(makeSquare(x, px, py))
    svg.appendChild(animateMind(x, y, z, px, py, true))
    if (line) svg.appendChild(makeInfo(px, line))
    return svg
}

// flip arms for t = [x,y,z], return [x,z,y]
// flip_def  |- !x y z. flip (x,y,z) = (x,z,y)
function flip(t) {
    return [t[0], t[2], t[1]]
}

// zagier map to keep mind for t = [x,y,z]
// zagier_def  |- !x y z. zagier (x,y,z) =
//      if x < y - z then (x + 2 * z,z,y - z - x)
// else if x < 2 * y then (2 * y - x,y,x + z - y)
// else (x - 2 * y,x + z - y,y)
function zagier(t) {
    var x = t[0], y = t[1], z = t[2]
    // zagier (x,y,z) = if x < y - z then (x + 2 * z,z,y - z - x) else if x < 2 * y then (2 * y - x,y,x + z - y) else (x - 2 * y,x + z - y,y)
    return x < y - z ? [x + 2 * z, z, y - z - x] :
           x < 2 * y ? [2 * y - x, y, x + z - y] :
                       [x - 2 * y, x + z - y, y]
}

// zagier o flip transform for t = [x,y,z]
// start with u = (1,1,k), the zagier fix.
// while (u is not a flip fix) :
//    u ← (zagier ◦ flip) u
// end while.
function iterate(t) {
    return zagier(flip(t))
}

// inverse of (zagier o flip) is (flip o zagier)
function inverse(t) {
    return flip(zagier(t))
}

// 5 types of windmill
function wtype(t) {
    // x < y, subcases: x < y - z, y - z < x, x = y, y < x, subcases: x < 2y, 2y < x
    var x = t[0], y = t[1], z = t[2]
    return x < y ? (x < y - z ? 1 : 2) : x == y ? 3 : (x < 2 * y ? 4 : 5)
}

// info string for animation from t = (x,y,z) to w = (a,b,c) 
function info(t, w) {
    return '(' + t + ') → (' + w + ')'
}

// result string for tik number n with final triple t = (x,y,z)
function result(n, t) {
    var x = t[0], y = t[1], z = t[2]
    return (y == z && n == x * x + 4 * y * z) ? // verification of triple (x,y,z)
           ' ' + n + ' = ' + x + '² + ' + (y + z) + '² = ' + (x * x) + ' + ' + (4 * y * z) :
           '!!! something is wrong: n = ' + n + ', triple = (' + t + ')'
}

// compute top-left corner reference point for center p = (px, py) and t = (x,y,z)
function corner(p, t) {
   var d = t[0]/2|0 // integer half of x = t[0]
   return [p[0] - d, p[1] - d]
}

/* Part 3: Hopping over Iteration */

// two_sq_def      |- !n. two_sq n = WHILE ($~ o found) (zagier o flip) (1,1,n DIV 4)
// two_sq_pop_def  |- !n. two_sq_pop n = WHILE ($~ o found) (popping (SQRT n)) (1,n DIV 4,1)
// popping_def     |- !c t. popping c t = pop (step c t) t
// pop_def         |- !m x y z. pop m (x,y,z) = (2 * m * z - x,z,y + m * x - m ** 2 * z)
// step_def        |- !c x y z. step c (x,y,z) = (x + c) DIV (2 * z)

var c = 0 // to be computed for hopping

// hopping algorithm
// (x,y,z) ← (1,k,1) // the flip of zagier fix
// c ← sqrt n // compute max parameter, integer square-root
// while (y ̸= z) :
//     m ← (x + c) div 2z // compute step to hop
//     (x, y, z) ← (2mz − x, z, y + mx − m² z) // compute pop node
// end while.
function hopping(t) {
    var x = t[0], y = t[1], z = t[2], m = (x + c)/(2 * z)|0, m0 = x/(2 * z)|0
    // console.log('JC: hopping, c', c, 'm', m, 'm0', m0)
    return [2 * m * z - x, z, y + m * x - m * m * z, m] // extra return extract by pop()
}

// animate hopping of an arm of windmill from t = (x,y,z) to w = (a,b,c) at p = (px, py) for index j
function hopArm(x, y, z, a ,b , c, px, py, j, start, duration) {
    start = start || '1s'
    duration = duration || '1s'
    var arm = makeArm(x, y, z, px, py, j)
    var animate1 = document.createElementNS("http://www.w3.org/2000/svg", "animate")
    var animate2 = document.createElementNS("http://www.w3.org/2000/svg", "animate")
    var animate3 = document.createElementNS("http://www.w3.org/2000/svg", "animateMotion")
    // change arm width
    animate1.setAttribute('begin', start)
    animate1.setAttribute('dur', duration)
    animate1.setAttribute('fill', 'freeze')
    // change arm height
    animate2.setAttribute('begin', start)
    animate2.setAttribute('dur', duration)
    animate2.setAttribute('fill', 'freeze')
    // move arm
    animate3.setAttribute('begin', start)
    animate3.setAttribute('dur', duration)
    animate3.setAttribute('fill', 'freeze')

    switch (j) {
    case 0:
        animate1.setAttribute('attributeName', 'width')
        animate1.setAttribute('from', y * scale)
        animate1.setAttribute('to', b * scale)
        animate2.setAttribute('attributeName', 'height')
        animate2.setAttribute('from', z * scale)
        animate2.setAttribute('to', c * scale)
        animate3.setAttribute('path', 'M0,0 ' + (x - a)/2 * scale + ',' + ((x - a)/2 + b - c) * scale)
        break
    case 1:
        animate1.setAttribute('attributeName', 'width')
        animate1.setAttribute('from', z * scale)
        animate1.setAttribute('to', c * scale)
        animate2.setAttribute('attributeName', 'height')
        animate2.setAttribute('from', y * scale)
        animate2.setAttribute('to', b * scale)
        animate3.setAttribute('path', 'M0,0 ' + (a - x)/2 * scale + ',' + (x - a)/2 * scale)
        break
    case 2:
        animate1.setAttribute('attributeName', 'width')
        animate1.setAttribute('from', y * scale)
        animate1.setAttribute('to', b * scale)
        animate2.setAttribute('attributeName', 'height')
        animate2.setAttribute('from', z * scale)
        animate2.setAttribute('to', c * scale)
        animate3.setAttribute('path', 'M0,0 ' + ((a - x)/2 + y - z) * scale + ',' + (a - x)/2 * scale)
        break
    case 3:
        animate1.setAttribute('attributeName', 'width')
        animate1.setAttribute('from', z * scale)
        animate1.setAttribute('to', c * scale)
        animate2.setAttribute('attributeName', 'height')
        animate2.setAttribute('from', y * scale)
        animate2.setAttribute('to', b * scale)
        animate3.setAttribute('path', 'M0,0 ' + ((x - a)/2 + b - c) * scale + ',' + ((a - x)/2 + y - z) * scale)
        break
    }
    arm.appendChild(animate1)
    arm.appendChild(animate2)
    arm.appendChild(animate3)
    return arm
}

// animate hopping from windmill t = (x,y,z) to windmill w = (a,b,c) at p = (px, py) for svg
function animateHopping(svg, t, w, p, line) {
    var x = t[0], y = t[1], z = t[2], a = w[0], b = w[1], c = w[2], px = p[0], py = p[1]
    svg.appendChild(animateSquare(x, a, px, py, '1s')) // begin = 1s, default for hopArm
    svg.appendChild(hopArm(x, y, z, a, b, c, px, py, 0))
    svg.appendChild(hopArm(x, y, z, a, b, c, px, py, 1))
    svg.appendChild(hopArm(x, y, z, a, b, c, px, py, 2))
    svg.appendChild(hopArm(x, y, z, a, b, c, px, py, 3))
    svg.appendChild(animateMind(x, y, z, px, py, true))
    if (line) svg.appendChild(makeInfo(px, line))
}

// make an animation of windmill t = (x,y,z) to windmill w = (a,b,c) at reference p = (px, py) for hopping
function hopAnimate(t, w, p, line) {
    var svg = makeCanvas()
    animateHopping(svg, t, w, p, line)
    return svg
}

/* Part 4: drawings for slides -- mostly in svg.js */

// make a sum of two squares for windmill (x,y,y), side by side, for svg
function sumOfSquares(svg, x, y, px, py) {
    svg.appendChild(makeSquare(x, px,py))
    svg.appendChild(makeArm(y,y,y, px + x + y,     py + x - y,    0))
    svg.appendChild(makeArm(y,y,y, px + x - y,     py + x - 2 * y,1))
    svg.appendChild(makeArm(y,y,y, px + x,         py + x - 2 * y,2))
    svg.appendChild(makeArm(y,y,y, px + x + 2 * y, py + x - y,    3))
}