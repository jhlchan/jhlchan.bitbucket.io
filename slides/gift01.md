---
title: Xmas 2020 Highlights
author: <span class="en">fate of Hong Kong</span><span class="ch">香港的命運</span>
date: <span class="en">Xmas 2023</span><span class="ch">2023年聖誕節</span>
theme: solarized
header-includes: |
    <meta property="og:type" content="website" />
    <meta property="og:title" content="2020年聖誕禮物" />
    <meta property="og:image" content="../images/highlight-01.jpg" />
    <base target="_blank"/>
    <link rel="stylesheet" type="text/css" href="../css/slides.css"></link>
    <script type="text/javascript" src="../scripts/site.js"></script>
include-before: |
    <!-- Any raw HTML code right after body tag, before anything else. -->
    <div id="modal" class="modal">
      <img id="modal-content" />
      <div id="modal-caption"></div>
    </div>
    <div align="right" class="banner">
    <input type="radio" name="lang" onclick="set_lang('en')" id="english" value="English" checked /> English |
    <input type="radio" name="lang" onclick="set_lang('ch')" id="chinese" value="Chinese" /> 中文
    </div>
include-after: |
    <!-- Any raw HTML code right before body end tag, after anything else. -->
    <script>window.onload = check</script>
    <!-- file:///Users/josephchan/jc/q/site/slides/gift01.html -->
    <!-- https://jhlchan.bitbucket.io/slides/gift01.html -->
...

#

<!-- ![](../images/gift01.jpg) next is slightly bigger -->

:::{.en}
<img src="../images/gift01_en.jpg" width="100%" />

- After years in Australia, I was to spend Xmas 2019 in Hong Kong, but COVID-19 runined my plans.
- Travels were canceled due to the epidemic. Everyone stayed at home, totally depressed for a year.
- Looking for fun 🙂️, I shared holiday "gifts" with friends online. The first episode is [Christmas 2020](../2020/xmas.html).
- As for the theme🤔️, I followed my earlier holiday email greetings, talking about interesting topics.
- Since the second half of 2019, Hong Kong had been torn apart, starting a new wave of emigration.
- Hong Kong was at crossroad again, and reminds me of my own emigration to Australia, back in 1989 ...
:::

:::{.ch}
<img src="../images/gift01_ch.jpg" width="100%" />

- 移民到澳洲多年，原定2019年聖誕節返回香港，與朋友聚舊。可惜因為 COVID-19，打亂了計劃。
- 疫情下航班取消，遊輪閑置。人人困在家中，幾乎足不出戶。大家悶悶不樂，鬱鬱不爽，整整一年。
- 節日尋開心🙂️，心生一計，想出與朋友分享節日「禮物」：是網上的禮物。首集是[2020年聖誕節](../2020/xmas.html)。
- 題材是什麼🤔️？其實早前與幾位朋友，每逢節日互通電郵，趁機祝賀，並談談我們有興趣的話題。
- 回顧香港，自2019下半年，新聞不斷，每每牽動港人的心。社會撕裂，意見分歧，揭發新移民潮。
- 縱然身在異鄉，但仍心繫香讧。東方之珠，前路茫茫。此情此景，讓我回憶自己1989年移民澳洲 ...
:::

<!--
** 2023/xmas
https://jhlchan.bitbucket.io/2020/xmas.html
我的節日「禮物」，首集是2020年聖誕節。自2019下半年，香港社會撕裂，揭起新移民潮，讓我回憶自己1989年移民 ...
-->

#

:::{.small}
<span class="en">Chinese song "Dawn, Please Don't Come"</span><span class="ch">歌曲：《黎明不要來》</span>【2:58】<br>
<a href="https://youtu.be/9l0q7DTD32E">
<img src="http://img.youtube.com/vi/9l0q7DTD32E/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- I started talking about this song ["Dawn, Please Don't Come"](../2020/lyrics.html#morning), written and composed by James Wong.
- The lyrics avoids reality with a free spirit: "Now that I feel romantic, let me leave this fleeting world."
- Later it became a movie song in "A Chinese Ghost Story", at that time the first to show special effects.
- James Wong boldly used electronic music for two movie songs, but one more was request at the end.
- As time was tight, they agreed on this never-released "Dawn, Please Don't Come", which became a hit.
:::

:::{.ch}
- 原本只想介紹一曲[《黎明不要来》](../2020/lyrics.html#morning)，由黃霑作曲兼填詞。緬懷香江歲月，流露不想回歸之情。
- 歌曲旋律優美，娓娓動聽。歌詞有點逃避現實，但寫得𤄙灑：「現在浪漫感覺，放我浮世外。」
- 此曲成為電影《倩女幽魂》的插曲，是當年出色的特效電影，由徐克監製，黃霑與戴樂民配樂。
- 黃霑大膽嘗試，以電子音樂，為電影先後創作兩首歌。拍攝近尾聲時，徐克要求再多一首插曲。
- 因時間緊迫，黃霑遲遲未有新作，提議起用未發行的《黎明不要来》。徐克同意，不料成為金曲。
:::

<!--
歌曲：《黎明不要來》第7屆香港電影金像獎，最佳電影歌曲 (1988年4月10日)【2:58】
https://www.youtube.com/watch?v=9l0q7DTD32E
https://youtu.be/9l0q7DTD32E
原本只想介紹一曲《黎明不要来》，其後變成講述此插曲的電影《倩女幽魂》，當年是出色特效電影。更發掘不少資料，舆黃霑有關，加入他的作品。本集視頻連結多多，有些已經消失，尚存的不乏精采。

![](https://youtu.be/9l0q7DTD32E)  -- not in pandoc, as no .mov or .mp4
[![](https://markdown-videos-api.jorgenkh.no/youtube/9l0q7DTD32E)](https://youtu.be/9l0q7DTD32E) -- works, but no width control, and a big red player button.
<iframe src="https://www.youtube.com/embed/9l0q7DTD32E" allowfullscreen></iframe> -- works only if embed in allowed by owner.
-->

#

:::{.small}
<span class="en">Theme song in movie "A Chinese Ghost Story"</span><span class="ch">電影《倩女幽魂》主題曲</span>【3:36】<br>
<a href="https://youtu.be/F6glgQZmVes">
<img src="http://img.youtube.com/vi/F6glgQZmVes/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- During filming of "A Chinese Ghost Story", both the script and character looks were often revised.
- The script writer said: "Our common point is that just like people, there are bad and good ghosts."
- "Both leading characters are persecuted by humans and ghosts respectively, with a unique story."
- James Wong had the main melody of the [theme song](../2020/lyrics.html#theme1) in one night, then wrote the lyrics immediately.
- New Visual Special Effects Studio used stop-motion animation for haunting resurrected mummies.
- What a remarkable scene when the main characters met and took turns writing a poem on a painting.
:::

:::{.ch}
- 電影《倩女幽魂》故事基於《聊齋志異》，拍攝期間角色造型經常變動，劇本更是一邊拍、一邊改。
- 劇本由阮繼志編寫：「我們均有相同想法，就是鬼與人一樣，有好也有壞，正如人也有忠奸之分。」
- 「寧采臣（張國榮）和聶小倩（王祖賢）分別受到人、鬼的壓迫，兩人同一陣線，故事別具意義。」
- 主題曲[《倩女幽魂》](../2020/lyrics.html#theme1)由張國榮演唱，黃霑指主旋律是參觀法國康城影展時，一夜間寫成，隨即填詞。
- 特效由「新視覺特技工作室」製作。片中乾屍復活、姥姥吸乾人類精元等，均以定格動畫製作而成。
- 寧采臣與聶小倩相識時，輪流於畫上題詩互接的一幕，十分形象化，成功聚焦一對璧人的情意相通。
:::

#

:::{.en}
<div class="imagecap" style="width:98%">
<img src="../2020/Evening-Breeze.jpg" style="width:57%"
     alt="The Evening Breeze, theme song of movie Shanghai Blues." onclick="showPopup(this)" />
<img src="../2020/Blue-Sea-Laughter-painting.jpg" style="width:42%"
     alt="Blue Sea Laughter, theme song of the movie Swordsman." onclick="showPopup(this)" />
</br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:98%">
<img src="../2020/Evening-Breeze.jpg" style="width:57%"
     alt="《晚風》是電影《上海之夜》主題曲。" onclick="showPopup(this)" />
<img src="../2020/Blue-Sea-Laughter-painting.jpg" style="width:42%"
     alt="《滄海一聲笑》是電影《笑傲江湖》主題曲。" onclick="showPopup(this)" />
</br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- Released in 1987, "A Chinese Ghost Story" was well received, with music in sequels by James Wong.
- Tsui Hark collaborated with James on "Shanghai Blues" in 1984, staying up all night to match up songs.
- At dawn they completed the theme song ["Evening Breeze"](../2020/lyrics.html#evening) with a mellow melody and nostalgic lyrics.
- In 1990 Tsui Hark produced the "Swordsman" series, but dissatisfied with songs composed by James.
- Finally, James skillfully turned the simple Chinese pentatonic scale over and over, filling in the lyrics.
- ["Blue Sea Laughter"](../2020/lyrics.html#laugh) was Jame's masterpiece, depicting the mutual sympathy of mountains and rivers.
:::

:::{.ch}
- 《倩女幽魂》1987年上映，頗受好評。隨後製作《倩女幽魂II》、《倩女幽魂III》，均由黃霑配樂。
- 徐克與黃霑合作，始於1984年製作《上海之夜》。𠉴人徹夜選曲，找葉蒨文試唱，邀戴樂民伴奏。
- 最後灌錄成[《晚風》](../2020/lyrics.html#evening)，旋律圓潤流暢，歌詞以「借來時間，借來晚風，把我的愛送到你心中」作結。
- 1990年徐克監製《笑傲江湖》系列，黃霑再次創作主題曲。但徐克總是不滿意作品，曾多次退稿。
- 最終，黃霑巧妙的把簡單的中國五聲音階，來回上下行幾次後，揉合一點自己的風格，就填上歌詞。
- [《滄海一聲笑》](../2020/lyrics.html#laugh)成為黃霑的代表作，樂曲旋律簡練，歌詞生動，描繪出惺惺相惜、高山流水的境界。
:::

#

:::{.small}
<span class="en">Under the Cultural Tree: James Wong・Good Hong Kong</span><span class="ch">文化樹下：黃霑・好香港</span>【52:11】<br>
<a href="https://youtu.be/NCUzVZCIC0M">
<img src="http://img.youtube.com/vi/NCUzVZCIC0M/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- James Wong came to Hong Kong in 1949, soon fell in love with Chinese literature due to his teacher.
- He learned the harmonica from a master at age eleven, starting his path towards the world of music.
- He entered the Chinese Department of University of Hong Kong in 1960, got a master in philosophy.
- When James started writing lyrics in late 1960s, he preferred modern styles relevant to everyday life.
- His lyrics depicted life, philosophy and feelings in natural and fluent languages, a change in pop music.
- His remark: "The best popular lyrics are simple but profound, otherwise just aim for simple and easy."
- His 2003 PHD thesis "Development, Rise and Fall of Cantopop: A Study of HK Pop Music 1949-1997".
:::

:::{.ch}
- 黃霑本名黃湛森，1949年來到香港。在學期間，深受中文老師影響，喜歡中國文學，向報刊投稿。
- 十一歲拜師梁日昭學習口琴，為電影和電台節目吹奏配樂，涉及古今中外各種樂曲，踏進音樂世界。
- 1960年入讀港大中文系，畢業論文是《姜白石詞研究》，其後哲學碩士論文是《粵劇問題探討》。
- 黃霑1960年代末初涉詞壇，一改傳統，強調歌詞內容和表達方式，必須富現代感、貼近大眾生活。
- 他以自然淺白的語言風格，描寫人生、哲理及感情，為早期香港流行樂界，提供嶄新的表現方式。
- 黃霑曾夫子自道：「寫流行歌詞貴『淺』，要雅俗共賞；深入淺出最佳，退而求淺入淺出也可以。」
- 最後，他在2003年，完成博士論文《粵語流行曲的發展與興衰：香港流行音樂研究(1949-1997)》。
:::

#

:::{.small}
<span class="en">Disney song in Cantonese "It's a small world" </span><span class="ch">世界真細小| 廣東話經典兒歌</span>【2:30】<br>
<a href="https://youtu.be/9OXOH7zibtE">
<img src="http://img.youtube.com/vi/9OXOH7zibtE/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- James Wong, nicknamed "talented wizard", translated this marvelous chorus (right, Chinese):
-
+-----------------------------------+--------------------------------------------------------+
| It's a small world after all</br> | 世界真細小小小 [It's a really small, small world]</br>   |
| It's a small world after all</br> | 小得真奇妙妙妙 [Fill with wonder after all]</br>         |
| It's a small world after all</br> | 實在真係細世界 [Within this really tiny world]</br>      |
| It's a small, small world</br>    | 嬌小而妙俏    [Petite and pretty after all]</br>        |
+-----------------------------------+--------------------------------------------------------+

- This episode has many story and video links. Some have vanished, those remaining are still wonderful.
- Among works of James Wong in the links, there are masterpieces, as well as funny Christmas carols.
:::

:::{.ch}
- 黃霑綽號「鬼才」，從這首英文童謠翻譯之生花妙筆，可見一斑。重唱部分 (左邊原文）：
-
+-----------------------------------+-------------------------+
| It's a small world after all</br> | 世界真細小小小</br>       |
| It's a small world after all</br> | 小得真奇妙妙妙</br>       |
| It's a small world after all</br> | 實在真係細世界</br>       |
| It's a small, small world</br>    | 嬌小而妙俏</br>          |
+-----------------------------------+-------------------------+

- 本集連結多多，有文章、有故事、有影音視頻，其中有些已經消失，尚存的不乏精采。
- 視頻連結的黃霑作品中，有原創佳作，亦有填詞不錯的英國民謠、中國民謠、聖誕頌曲。
:::

<!--
世界真細小| 廣東話經典兒歌 2023 | 超暖心繪本風 | 帶你進入夢想國度 | 迪士尼經典歌曲 @ 奇音樂奇世界【2:30】
https://www.youtube.com/watch?v=9OXOH7zibtE
https://youtu.be/9OXOH7zibtE
黃霑綽號「鬼才」，從這首英文童謠翻譯之生花妙筆，可見一斑。視頻連結的黃霑作品中，有原創佳作，亦有填詞不錯的英國民謠、中國民謠、聖誕頌曲。香港的美麗聖誕，或許只能緬懷過去。
-->

#

:::{.en}
<div class="imagecap" style="width:64%">
<img src="../2020/Australia.Apostle.Islands.jpg"
     alt="Apostle Islands in Port Campbell National Park, Victoria." onclick="showPopup(this)" /></br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:64%">
<img src="../2020/Australia.Apostle.Islands.jpg"
     alt="十二門徒石 (Apostle Islands)，在維多利亞州 Port Campbell 國家公園。" onclick="showPopup(this)" /></br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
+ I cherish dearly those beautiful Christmas days of Hong Kong in the past, nice Christmas in winter!
+ Australia lies in the southern hemisphere, so Christmas season is summer, with picturesque scenes.
+ The gift ends with [Travel in Australia](https://www.australia.com/en/travel-inspiration/experience-australia-in-8d-audio.html), take a virtual tour and enjoy 8D video by wearing headphones.
+ Please investigate this site on your own: how to enter full screen mode, and look for more 8D videos.
+ As always, merry Christmas and happy New Year! There are more gifts forthcoming, so ... stay tuned!
:::

:::{.ch}
+ 香港的美麗聖誕，或許只能緬懷美好的回憶。珍惜過去，憧景未來，冬天的聖誕節，簡直係奇妙！
+ 澳洲位於南半球，聖誕節目在夏季，風光如畫。英國移民懷念白色聖誕，搞「七月聖誕」哈哈😄！
+ 禮物結尾，加插[遨遊澳洲](https://www.australia.com/zh-hk/travel-inspiration/experience-australia-in-8d-audio.html)，戴上耳機，享受8D視頻，即 8 Dimension 環迴立體聲，飛入虛擬旅程。
+ 在網頁中，自行嘗試：如何進入全螢幕模式。並欣賞更多8D視頻。關於8D，我不求箇中道理 😊！
+ 一如既往，祝各位聖誕快樂，新年大吉！還有更多節日、更多「禮物」，即將陸續推出，敬請期待！
:::

<!--
透過8D環迴立體聲體驗澳洲
https://www.australia.com/zh-hk/travel-inspiration/experience-australia-in-8d-audio.html
澳洲在南半球，聖誕節目在夏季，風光如畫。禮物結尾加插遨遊澳洲，享受8D視頻（戴耳筒）。請自行研究：如何進入全螢幕模式。並欣賞更多8D視頻。
(22/12/23)
-->


<!--
pandoc -t revealjs -s --mathjax gift01.md -o gift01.html
-->
