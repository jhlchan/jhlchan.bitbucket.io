---
title: Lunar New Year 2021 Highlights
author: <span class="en">the world of Physics</span><span class="ch">物理世界</span>
date: <span class="en">Lunar New Year 2024</span><span class="ch">2024年農曆新春</span>
theme: solarized
header-includes: |
    <meta property="og:type" content="website" />
    <meta property="og:title" content="2021年新春禮物" />
    <meta property="og:image" content="../images/highlight-02.jpg" />
    <base target="_blank"/>
    <link rel="stylesheet" type="text/css" href="../css/slides.css"></link>
    <script type="text/javascript" src="../scripts/site.js"></script>
include-before: |
    <!-- Any raw HTML code right after body tag, before anything else. -->
    <div id="modal" class="modal">
      <img id="modal-content" />
      <div id="modal-caption"></div>
    </div>
    <div align="right" class="banner">
    <input type="radio" name="lang" onclick="set_lang('en')" id="english" value="English" checked /> English |
    <input type="radio" name="lang" onclick="set_lang('ch')" id="chinese" value="Chinese" /> 中文
    </div>
include-after: |
    <!-- Any raw HTML code right before body end tag, after anything else. -->
    <script>window.onload = check</script>
    <!-- file:///Users/josephchan/jc/q/site/slides/gift02.html -->
    <!-- https://jhlchan.bitbucket.io/slides/gift02.html -->
...

#

:::{.en}
<img src="../images/gift02_en.jpg" width="100%" />

- The [2021 Lunar New Year](../2021/lny.html) "gift" is mainly an attempt to explore the world of physics.
- Physics = the principle of all things, from big things to small things, encompassing many. 
- The physics of big things (solar system, galaxies, universe) is given by Relativity Theory.
- The physics of small things (atoms, nuclei, quarks) is described by Quantum Theory.
- Both are modern theories full of radical ideas. Let's begin by looking at some history ...
:::

:::{.ch}
<img src="../images/gift02_ch.jpg" width="100%" />

- [2021年農曆新春](../2021/lny.html)「禮物」，主要題材，是嘗試探討物理世界。
- 物理 = 萬物的道理，「萬物」包羅萬有，從大事物到小事物。
- 大事物（太陽系、星系、宇宙）的物理學，由「相對論」說明。
- 小事物（原子、核子、夸克子）的物理學，由「量子論」描述。
- 這些是現代理論，充滿革新思想。首先，讓我們回顧一些歷史 ...
:::

<!--
** 2024/lny
https://jhlchan.bitbucket.io/2021/lny.html
2021年農曆新春「禮物」，嘗試探討物理。現代物理建基於「相對論」和「量子論」，兩者皆高深，三言兩語無法解釋清楚 ...
-->

#

:::{.en}
<div class="imagecap" style="width:98%">
<img src="../2021/Albert-Einstein-1921.jpg" style="width:32%"
     alt="Albert Einstein in 1921. He developed a new theory about space, time and gravity." onclick="showPopup(this)" />
<img src="../2021/Max-Planck-1879.jpg" style="width:27.6%"
     alt="Max Planck around 1879. He started a new theory about the nature of light." onclick="showPopup(this)" />
</br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:98%">
<img src="../2021/Albert-Einstein-1921.jpg" style="width:32%"
     alt="1921年的愛因斯坦。他發展關於空間、時間和引力的新理論。" onclick="showPopup(this)" />
<img src="../2021/Max-Planck-1879.jpg" style="width:27.6%"
     alt="1879年的普朗克。他提出新理論，有關光的本質。" onclick="showPopup(this)" />
</br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- The theory of relativity was the brainchild of Albert Einstein, about space and time.
- Its main feature is that light speed is a constant _c_, making a 4D spacetime universe!
- Mass or energy will cause spacetime to curve, giving gravitation in Einstein's theory. 
- Quantum theory started from the pioneering work of Max Planck, on light emission.
- He envisioned that light is emitted in quanta, with energy proportional to frequency.
- This proportional factor is a fundamental constant _h_, now called Planck's constant.
- Many people contributed ideas for quantum theory, the basis of modern technology.
:::

:::{.ch}
- 相對論是愛因斯坦（Albert Einstein）的原創𠎀作，有關空間和時間的性質。
- 其中最重要的特徵，是光速為常數 _c_。由此形成一個四維時空：宇宙是4D！
- 質量或能量會改變時空，導致彎曲，從而產生愛因斯坦理論中的萬有引力。
- 量子論始於普朗克（Max Planck）的開創性想法，關於光是如何發射出來。
- 他設想光是以量子（quanta）形式發射，其中光的能量與頻率，互成正比。
- 並且能量與頻率的比例因子，是一個基本常數 _h_，現在稱為「普朗克常數」。
- 有許多科學家，對量子論思想作出貢獻。量子理論，成為現代科技的基礎。
:::

#

:::{.small}
<span class="en">Hong Kong astronomer taking photo of a black hole</span><span class="ch">港產天文學家有份影黑洞</span>【4:50】<br>
<a href="https://youtu.be/hIk-SCyUfYQ">
<img src="http://img.youtube.com/vi/hIk-SCyUfYQ/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- These modern theories of physics are profound, they cannot be explained clearly in a few words.
- As we shall only appreciate without seeking to understand, for "big things" I mention the black holes.
- Black holes are chosen: they are mysterious (a hole in space-time) and basic (we live in space-time).
- They are romantic (from Einstein’s theory of relativity), but realistic (proven by composite photos).
- The video introduces Dr. Chi-Kwan Chan from Hong Kong, who worked in "Black Hole Photography".
:::

:::{.ch}
- 相對論和量子論，是現代物理學基礎，兩者皆高深。只憑三言兩語，無法解釋清楚。
- 既然不求理解、只求欣賞，因此關於「大事物」的物理，提及黑洞（black hole）。
- 黑洞：夠神秘（時空穿咗個窿）、夠基本（我們生活在時空）。時空 = 時間 + 空間。
- 黑洞亦夠浪漫（出自 Einstein 的相對論）、夠逼真（有合成照片為證）。誰不嚮往？
- 視頻介紹陳志均，來自香港，曾參與「黑洞攝影」；並侃侃而談，同學生交流心得。
:::

<!--
科研人物—會考唔掂TOEFL肥佬 港產天文學家有份影黑洞【4:50】cc embedded
https://www.youtube.com/watch?v=hIk-SCyUfYQ
https://youtu.be/hIk-SCyUfYQ
既然不求理解、只求欣賞，因此提及黑洞（black hole）：夠神秘（時空穿咗個窿）、夠基本（我們生活在時空）、夠浪漫（出自 Einstein 相對論）、但逼真（有合成照片為證）。視頻介紹來自香港的陳志均，曾參與「黑洞攝影」。黑洞太深奧，點到即止，話題轉往 ANU 的物理教學。
-->

#

:::{.small}
<span class="en">Dr John Debs – Think like a physicist</span><span class="ch">Dr John Debs – 思考如物理學家</span>【2:53】cc<br>
<a href="https://youtu.be/i6zor_FEwWk">
<img src="http://img.youtube.com/vi/i6zor_FEwWk/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- Black holes are too much to fathom, so a quick turn to another topic: ANU’s physics teaching.
- Rather than teaching from textbooks, in ANU physics students learn by making toys and models.
- A researcher in ANU physics department, Dr John Debs is a dynamic and inspiring educator.
- His teaching is a blended approach of inquiry-based learning, hands-on design, building and making.
- He created Makerspace, a place for students to go and build things, bringing creative ideas to life.
:::

:::{.ch}
- 黑洞太難理解，點到即止。話題一轉，談談澳洲國立大學（ANU）的物理教學。
- 在 ANU，物理學系學生不是依靠教科書，而是製作玩具模型來學習，生動有趣。
- John Debs 博士身為 ANU 物理系研究員，是一位充滿活力、鼓舞人心的教育家。
- 他採用的教授方法，結合探究式學習、實踐型設計，著重建造和製作實物模型。
- 他成立 Makerspace，提供一片地方，讓學生自行砌東砌西，把創意轉化為成品。
:::

#

:::{.small}
<span class="en">Hidden Science Superstars: Chien-Shiung Wu</span><span class="ch">隱藏的科學巨星：吳健雄</span>【2:21】cc<br>
<a href="https://youtu.be/7PWUsiqn0bI">
<img src="http://img.youtube.com/vi/7PWUsiqn0bI/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- Relativity guides the macroscopic world, and quantum theory guides the microscopic world.
- It is difficult to have photos of the quantum world, even good pictures to illustrate are hard to find.
- Fortunately, I found a US stamp commemorating the Chinese female physicist Chien-Shiung Wu.
- Her experiments confirmed that in the quantum world of particles, left and right are not symmetric.
- Video features both Chinese and non-Chinese students telling her remarkable and inspiring story.
:::

:::{.ch}
- 相對論（relativity）主導宏觀世界，量子論（quantum theory）主導微觀世界。
- 量子世界，虛無縹緲，難得有照片佐證。想要好的圖片作說明，也不容易找到。
- 幸好尋得一枚珍貴郵票，由美國郵政總局發行，紀念華裔女物理學家：吳健雄。
- 她的實驗，證實在高能粒子的量子世界中，左右不對稱（violation of parity）。
- 視頻製作由華籍與非華籍學生現身，講述她的故事。成就非凡，值得後輩敬仰。
:::

<!--
Hidden Science Superstars: Chien-Shiung Wu【2:21】cc
https://www.youtube.com/watch?v=7PWUsiqn0bI
https://youtu.be/7PWUsiqn0bI
相對論（relativity）描述宏觀世界，量子論（quantum theory）描述微觀世界。量子論難有照片，幸好找到一枚郵票，紀念華裔女物理學家吳健雄。她的實驗，證實在粒子的量子世界中，左右不對稱。視頻由外國學生現身，講述吳健雄的故事。

Discovering Dr. Wu
by Jada Yuan, 13 December 2021.
https://www.washingtonpost.com/lifestyle/2021/12/13/chien-shiung-wu-biography-physics-grandmother/
The world reveres Chien-Shiung Wu as a groundbreaking nuclear physicist who made a startling find 65 years ago. But to me, she was Grandma — and I long to know more about her private universe.
(gather all cartoons for a little booklet?) first is a slow video!

https://jobsacademy.futurewomen.com

-->

#

:::{.small}
<span class="en">"Legend of the girl Chien-Shiung Wu" in ink cartoon</span><span class="ch">《少女傳奇吳健雄》水墨動畫片</span>【1:32:24】cc<br>
<a href="https://youtu.be/zwpPDhRxyjo">
<img src="http://img.youtube.com/vi/zwpPDhRxyjo/0.jpg" width="44%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- Chien-Shiung Wu's childhood anecdotes constitute this brilliant animation.
- In the final scene, she left her hometown alone and embarked on her life journey.
- The ending song is the touching "I Want to Fly", with the last verse in Chinese:
-
| _我要飛翔，去最遠的地方，心中充滿希望的曙光。_
| _[I want to fly, with rays of hope like dawn filling my heart, to the farthest place.]_
| _我的夢想，在遥遠的地方，那有美麗無瑕的幻想。_
| _[I have a dream, full of beautiful and flawless imagination, in a distant place.]_

- New wishes in New Year, do you have a dream to fly? Drifting to a faraway place?
:::

:::{.ch}
- 吳健雄的童年軼事，拍成動畫，細膩入微，精彩萬分。
- 最後一幕，她告別親人，隻身離鄉別井，踏上人生路。
- 片尾曲是《我要飛》，相當感人。歌詞最後一段：
-
| _我要飛翔，去最遠的地方，心中充滿希望的曙光。_
| _我的夢想，在遥遠的地方，那有美麗無瑕的幻想。_

- 新年新願望，你可有飛翔的夢想？輕飄去遠遠的地方？
:::

<!--
《少女傳奇吳健雄》水墨動畫片。2020最新電影【1:32:24】cc
https://www.youtube.com/watch?v=zwpPDhRxyjo
https://youtu.be/zwpPDhRxyjo
吳健雄的童年軼事，拍成動畫，最後隻身離鄉別井，踏上人生路。片尾曲是感人的《我要飛》，歌詞最後一段：「我要飛翔，去最遠的地方，心中充滿希望的曙光。我的夢想，在遥遠的地方，那有美麗無瑕的幻想。」新年新願望，你可有飛翔的夢想？
-->


<!--
pandoc -t revealjs -s --mathjax gift02.md -o gift02.html
-->
