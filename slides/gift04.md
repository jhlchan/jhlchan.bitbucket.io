---
title: Dragon Boat 2021 Highlights
author: <span class="en">folding shapes</span><span class="ch">摺出形狀</span>
date: <span class="en">Dragon Boat 2024</span><span class="ch">2024年端午節</span>
theme: solarized
header-includes: |
    <meta property="og:type" content="website" />
    <meta property="og:title" content="2021年端午禮物" />
    <meta property="og:image" content="../images/highlight-04.jpg" />
    <base target="_blank"/>
    <link rel="stylesheet" type="text/css" href="../css/slides.css"></link>
    <script type="text/javascript" src="../scripts/site.js"></script>
include-before: |
    <!-- Any raw HTML code right after body tag, before anything else. -->
    <div id="modal" class="modal">
      <img id="modal-content" />
      <div id="modal-caption"></div>
    </div>
    <div align="right" class="banner">
    <input type="radio" name="lang" onclick="set_lang('en')" id="english" value="English" checked /> English |
    <input type="radio" name="lang" onclick="set_lang('ch')" id="chinese" value="Chinese" /> 中文
    </div>
include-after: |
    <!-- Any raw HTML code right before body end tag, after anything else. -->
    <script>window.onload = check</script>
    <!-- file:///Users/josephchan/jc/q/site/slides/gift04.html -->
    <!-- https://jhlchan.bitbucket.io/slides/gift04.html -->
...

#

:::{.en}
<img src="../images/gift04_en.jpg" width="100%" />

- For the [2021 Dragon Boat](../2021/dragon.html) "gift", the theme is geometry = math of shapes（in Chinese: 幾何）.
- Two types of things of concern in math: numbers ($0, 1, 2, 3, 4, \dots$) and shapes (🔺, 🔷, ⭓, 🛑, 🟣).
- Calculation with numbers becomes arithmetic, the study with symbols it turns into algebra.
- The reasoning of shapes involves measurements, with length and angle it evolves into geometry.
- Without drawing tools like the compass or ruler, how can anyone learn about shapes in geometry?
:::

:::{.ch}
<img src="../images/gift04_ch.jpg" width="100%" />

- [2021年端午節](../2021/dragon.html)「禮物」，主題是幾何 = 形之數學（中文「幾何」= 問有多少，推敲作答）。
- 數學考慮的對象，不外乎是兩類：數字（$0, 1, 2, 3, 4, \dots$）與圖形（🔺, 🔷, ⭓, 🛑, 🟣）。
- 數字運算，成為算術，代入符號作研究，演變成代數（algebra）。
- 圖形審視，涉及測量，推敲長度和角度，演變成幾何（geometry）。
- 學習幾何圖形，如果冇「規」（compass）冇「矩」（ruler），如何進行？
:::

<!--
** 2024/dragon
https://jhlchan.bitbucket.io/2021/dragon.html
2021年端午節「禮物」，主題是幾何 = 形之數學。數學的元素，是數字與圖形。但冇「規」（compass）冇「矩」（ruler），如何學習幾何圖形？
-->

#

:::{.en}
<div class="imagecap" style="width:70%">
<img src="../2021/origami-color-cranes.jpg" style="width:61%"
     alt="A pair of colorful origami cranes." onclick="showPopup(this)" />
<img src="../2021/origami-folds-crane.jpg" style="width:38%"
     alt="How to fold a paper cranes in 16 steps." onclick="showPopup(this)" />
</br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:70%">
<img src="../2021/origami-color-cranes.jpg" style="width:61%"
     alt="一對彩色繽紛的摺紙鶴。" onclick="showPopup(this)" />
<img src="../2021/origami-folds-crane.jpg" style="width:38%"
     alt="如何以16步摺成栩栩如生的紙鶴。" onclick="showPopup(this)" />
</br>
<span>點擊放大(再擊關閉)</span>
</div>
:::


:::{.en}
- Geometry of shapes can be studied through paper folding, or __origami__.
- The word _origami_ is derived from Japanese "oru" (to fold) and "kami" (paper).
- Folding can construct lines, taking paper corners as initial points.
- Folding lines can make intersections, thereby constructing more points.
- A "circle" can appear if you can fold carefully all the tangent lines.
- Folding allows adjustment of line placement so that points can be coincident.
- This makes folding geometry much more interesting than classical geometry! 
:::

:::{.ch}
- 幾何形狀的研究，可以利用 __摺紙__，英文是 __origami__。似是小玩意，內藏大學問。
- _origami_ 一詞，源自日本文「折り紙 」，取自日文「oru」（折摺）和「kami」（平紙）。
- 紙角是點。從紙角點開始，摺紙依次摺出線條。
- 摺紙摺線，互相縱橫交錯，繼而建立兩線交點。
- 圓形有切線。如果細心摺出所有切線，就會出現「圓形」。
- 摺紙容許調整摺線位置，以便兩點重合。
- 因此，摺紙幾何比經典幾何，更為有趣！
:::

#

:::{.en}
<div class="imagecap" style="width:72%;">
<img src="../2021/angle-trisection.jpg"
     alt="Folding paper to trisect an angle (adapted from a method by the Japanese folder and mathematician Hisashi Abe)." onclick="showPopup(this)" /></br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:72%;">
<img src="../2021/angle-trisection.jpg"
     alt="摺紙完成三等分角（根據日本摺紙家和數學家：阿部恒的方法改編而成）。" onclick="showPopup(this)" /></br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- Using origami to bisect an angle is easy: just fold the angle till the angle arms meet!
- Using origami to trisect an angle is not that hard: just fold as shown in diagram above.
- In my webpage, watch the origami animations demonstrating the folding sequences, step by step.
- Interactive geometric figures help to explain theorems of bisectors and trisectors in triangles.
- Do you know that any triangle has its angle trisectors marking out a nice equilateral triangle?
:::

:::{.ch}
- 角度作二等分，利用摺紙很簡單：只需把角臂互對摺即可！
- 角度作三等分，利用摺紙並不難：只需摺如上圖所示即可。
- 網頁中，配有摺紙動畫，觀看逐步示範，摺折次序一一呈現。
- 加插幾何互動，說明三角形中，有二分線定理、三分線定理。
- 可知道，任何三角形的內角三分線， 齊集帶動著一個漂亮的等邊三角形嗎？
:::

<!--
(images/angle-trisection.jpg)
屏棄傳統圓規和直尺，改用摺紙探究幾何。配有摺紙動畫，顯示如何二分角、三分角。加插幾何互動，說明二分線定理、三分線定理。
-->

#

:::{.small}
「摺紙達人」陳柏熹摺出《異形》2019-03-31【4:38】cc<br>
<a href="https://youtu.be/efcuP2VdYGs">
<img src="http://img.youtube.com/vi/efcuP2VdYGs/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- Origami is fun to play with, amazing to touch and quite enjoyable, a rare combination of art and math.
- Kade Chan, the "Prince of Origami", is one of the ten outstanding young people in Hong Kong in 2020.
- Very often he remarked that "Origami allows me to create unlimited possibilities from limited space."
- Graduated from School of Design in HK Polytechnic University, his products aim at life improvement.
- Recently, his work merges origami structures with industrial product design, e.g. foldable furniture.
:::

:::{.ch}
- 摺紙既帶有樂趣，亦給予驚奇的觸覺，是藝術和數學的罕有結合。
- 陳柏熹（Kade Chan）又稱「摺紙王子」，是2020年香港十大傑出青年之一。
- Kade 常常這樣勉勵自己：「摺紙讓我可從有限空間，創造無限可能。」
- 畢業於香港理工大學設計學院，Kade 致力研發改善人類生活的產品。
- 近年，他著手於融合摺紙結構和工業產品設計，例如：可摺疊的傢具。
:::

<!--
「摺紙達人」陳柏熹摺出《異形》2019-03-31【4:38】cc
https://www.youtube.com/watch?v=efcuP2VdYGs
https://youtu.be/efcuP2VdYGs
摺紙既是樂趣，亦是藝術。其中蘊藏數學，也應用於科學，例如發射太空望遠鏡 JWST（連結有更新）。陳柏熹又稱「摺紙王子」，是2020年香港十大傑出青年之一。
-->

#

:::{.small}
James Webb Space Telescope Deployment Sequence【1:47】<br>
<a href="https://youtu.be/RzGLKQ7_KZQ">
<img src="http://img.youtube.com/vi/RzGLKQ7_KZQ/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- To launch the James Webb Space Telescope (JWST) from a rocket capsule, its mirrors must be folded.
- How to fold up mirrors and then unfold in space? This is an application of origami in science.
- After the successful launch of JWST on Christmas Day 2021, links in my webpage are updated.
- The above video gives an account of the steps in unfolding the whole JWST structure, day by day.
- There are more things to unfold besides mirrors, so the JWST team had to consult an origami expert!
:::

:::{.ch}
- James Webb 太空望遠鏡 (JWST)，從火箭發射。其中主鏡必須摺疊，藏在火箭艙內。
- 如何把鏡子摺疊起來，然後在太空中展開？解決此問題，正是摺紙在科學上的應用。
- 太空望遠鏡 JWST，於2021年聖誕日成功發射。之後，網頁中連結有更新。
- 以上視頻，介紹太空望遠鏡 JWST 整個結構的展開步驟，逐天逐天講解。
- 不僅是鏡子，還有更多的硬件需要展開，因此，JWST 團隊諮詢摺紙專家！
:::

#

:::{.small}
If You Must Blink Do It Now | Kubo And The Two Strings【4:16】cc<br>
<a href="https://youtu.be/6SjLM4tSGPs">
<img src="http://img.youtube.com/vi/6SjLM4tSGPs/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- "Kubo and The Two Strings" is a 3D stop-motion animation (links on webpage) with origami features.
- The movie tells a moving story of a young Japanese boy’s adventures to understand his own tale.
- He carries a shamisen, a traditional Japanese guitar with three strings, two are from his parents. 
- The paper-folding art form is the centerpiece of a storytelling show that Kubo performs for his village.
- "If you must blink, do it now" -- so Kubo begins the legend after spreading out his origami characters.
:::

:::{.ch}
- 《酷寶：魔弦傳說》（Kubo and The Two Strings）是立體定格動畫（連結見網頁），具摺紙特色。
- 這齣電影，講述一名日本小男孩 Kubo，為了解自己身世，經歷一場冒險的故事。
- 他隨身攜帶一把三弦琴，傳統的日本樂器結他，有三根弦，其中兩根與父母相連。
- Kubo 來到他的村莊廣場，講故事並作表演。摺紙藝術形式，是表演故事的核心。
- 「若想眨眼，就趁此刻。」 -- Kubo 放置好他的摺紙角色後，開始細訴傳奇故事。
:::

<!--
Kubo And The Two Strings HD if you must blink do it now origami art【4:20】cc
https://www.youtube.com/watch?v=yWDUl_mIdXg
-->

#

:::{.en}
<div class="imagecap" style="width:50%">
<img src="../kubo/The.Art.of.Kubo.and.the.Two.Strings.jpg"
     alt="Cover of a picture book by Emily Haynes." onclick="showPopup(this)" /></br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:50%">
<img src="../kubo/The.Art.of.Kubo.and.the.Two.Strings.jpg"
     alt="Emily Haynes 繪畫本的封面。" onclick="showPopup(this)" /></br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- As characters and scenes are static, making a 3D stop-motion animation has lots of constraints.
- Creating jaw-dropping 3D shapes within origami needs innovative ideas under constraints.
- Both classical geometry and folding geometry are playing by the rules, but under constraints.
- For details in making the movie "Kubo and The Two Strings", refer to my elaborate page of [animation](../kubo/index.html).
- If you are mesmerised by the movie, you will find this [art book](../kubo/The.Art.of.Kubo.and.the.Two.Strings.pdf) (165 pages) of the movie fascinating.
:::

:::{.ch}
- 製作立體定格動畫，限制多多。因為無論角色人物，或場景變化，都是靜態。
- 在摺紙藝術中，創作令人瞠目結舌的立體形狀，需要在限制下，有創新想法。
- 經典幾何和摺紙幾何，都是要求在不同限制下，仍遵守規則的邏輯推理遊戲。
- 有關電影《酷寶：魔弦傳說》的製作詳情，請參閱我的[動畫](../kubo/index.html)網頁，精心策劃。
- 如果這部電影令你著迷，這本描述電影的[藝術圖書](../kubo/The.Art.of.Kubo.and.the.Two.Strings.pdf)（165頁），更引人入勝。
:::

#

:::{.small}
<span class="en">Kubo end credits song</span><span class="ch">《酷寶：魔弦傳說》片尾曲</span>【5:11】<br>
<a href="https://youtu.be/0xxAAXZvCO8">
<img src="http://img.youtube.com/vi/0xxAAXZvCO8/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- Ending song of the movie is a [Beatles classic](../songs/WhileMyGuitarGentlyWeeps.html), played with Japanese musical instruments.
- George Harrison wrote this song when he was experimenting with oriental philosophy.
- The song is about love in the widest sense, this version is fitting sung by a female voice.
- Such a combination of features to interpret a Beatles' song is amazing beyond words.
- In the lyrics, "how to unfold your love" even relates to origami, somewhat vaguely!
:::

:::{.ch}
- 電影的片尾曲，是[披頭四經典之作](../songs/WhileMyGuitarGentlyWeeps.html)，以傳統日本樂器演奏。
- George Harrison 創作這首歌曲時，正嘗試探討東方哲學。
- 整首歌曲，演譯最廣義的「愛」，以女聲演唱，尤其適合。
- 如此組合，詮釋披頭四的作品，確實十分精彩，難以言喻。
- 英文歌詞中有「how to unfold your love」，隱隱與摺紙相關！
:::

<!--
Kubo end credits song【5:11】
https://www.youtube.com/watch?v=0xxAAXZvCO8
https://youtu.be/0xxAAXZvCO8
更有摺紙電影，是定格動畫《酷寶：魔弦傳說》（網頁有連結）。片尾曲正是披頭四經典之作，以東方樂器演奏，由女聲演唱，恰到好處。歌詞中有「how to unfold your love」，隱隱與摺紙有關！
-->

#

:::{.small}
While My Guitar Gently Weeps (George Harrison and Ringo Starr)【5:45】<br>
<a href="https://youtu.be/XzSrShbqlz8">
<img src="http://img.youtube.com/vi/XzSrShbqlz8/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- Beatles' fans will be delighted with this live performance of [While My Guitar Gently Weeps](../songs/WhileMyGuitarGentlyWeeps.html).
- An introduction by Elton John, with rare appearances of George Harrison and Ringo Starr together.
- Words in the lyrics are well chosen:
-
| _I look at the world and I notice it's turning,_
| _While my guitar gently weeps._
| _With every mistake we must surely be learning,_
| _Still my guitar gently weeps._

- The live show ends in a climax: a guitar duet by George Harrison and Eric Clapton.
:::

:::{.ch}
- 披頭四的粉絲，不容錯過 [While My Guitar Gently Weeps《當我的結他輕輕啜泣》](../songs/WhileMyGuitarGentlyWeeps.html) 的現場表演。
- 有 Elton John 的開場白，及 George Harrison 和 Ringo Starr 罕有地齊齊亮相，一演唱、一打鼓。
- 英文歌詞中，選詞用字頗有心思，別具一格：
-
| _[環顧四周，發現世界正在轉，] I look at the world and I notice it's turning,_
| _[我的結他，儘管輕輕的哭訴。] While my guitar gently weeps._
| _[從錯誤中，汲取教訓並研鑽，] With every mistake we must surely be learning,_
| _[我的結他，仍然輕輕地哀訴。] Still my guitar gently weeps._

- 表演結束前，George Harrison 與 Eric Clapton 結他二重奏，相得益彰，現場即時推向高潮。
:::

<!--
pandoc -t revealjs -s --mathjax gift04.md -o gift04.html
-->
