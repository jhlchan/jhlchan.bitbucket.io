---
title: Mid-Autumn 2021 Highlights
author: <span class="en">computing machines</span><span class="ch">計算機器</span>
date: <span class="en">Mid-Autumn 2024</span><span class="ch">2024年中秋節</span>
theme: solarized
header-includes: |
    <meta property="og:type" content="website" />
    <meta property="og:title" content="2021年中秋禮物" />
    <meta property="og:image" content="../images/highlight-05.jpg" />
    <base target="_blank"/>
    <link rel="stylesheet" type="text/css" href="../css/slides.css"></link>
    <script type="text/javascript" src="../scripts/site.js"></script>
include-before: |
    <!-- Any raw HTML code right after body tag, before anything else. -->
    <div id="modal" class="modal">
      <img id="modal-content" />
      <div id="modal-caption"></div>
    </div>
    <div align="right" class="banner">
    <input type="radio" name="lang" onclick="set_lang('en')" id="english" value="English" checked /> English |
    <input type="radio" name="lang" onclick="set_lang('ch')" id="chinese" value="Chinese" /> 中文
    </div>
include-after: |
    <!-- Any raw HTML code right before body end tag, after anything else. -->
    <script>window.onload = check</script>
    <!-- file:///Users/josephchan/jc/q/site/slides/gift05.html -->
    <!-- https://jhlchan.bitbucket.io/slides/gift05.html -->
...

#

:::{.en}
<img src="../images/gift05_en.jpg" width="100%" />

- The [2021 Mid-Autumn](../2021/autumn.html) "gift" is a grand production on this: smartphone = computer = Turing machine.
- Although equal, conceptually there are three jumps over levels, starting from your hand-held phone.
- A smartphone works like a computer, they look different but they both come from the same concept.
- A computer is a real machine built from an abstract machine in math logic, both having the same idea.
- The last jump is to realize that the simple math machine, as devised by [Alan Turing](../turing/index.html), is indeed powerful.
:::

:::{.ch}
<img src="../images/gift05_ch.jpg" width="100%" />

- [2021年中秋節](../2021/autumn.html)「禮物」是一項大製作，探討：手機 = 電腦 = 圖靈機（Turing machine）。
- 雖然是一串等式，但概念上進行了三級跳。簡而言之，從日常輕巧方便的手提電話開始。
- 第一級跳：也許你略知道，智能手機的工作原理，與電腦相似。其實，兩者概念上相同。
- 第二級跳：電腦即計算機，源自數學中的抽象計算機器，演變成實物，採用相同的思維。
- 最後一跳，是漸漸意識到：[Alan Turing](../turing/index.html) 設計的數學機器，雖然十分簡單，但確實頂呱呱。
:::

<!--
2024/autumn
https://jhlchan.bitbucket.io/2021/autumn.html
2021年中秋節「禮物」進行大製作，探討：手機 = 電腦 = 圖靈機（Turing machine）。雖然是等式，但概念上三級跳，更帶出今期主題人物 Alan Turing ...
-->

#

:::{.en}
<div class="imagecap" style="width:60%">
<img src="../turing/macbook_vs_iphone.jpg"
     alt="Both laptops and smartphones are computing machines." onclick="showPopup(this)" /></br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:60%">
<img src="../turing/macbook_vs_iphone.jpg"
     alt="筆記本電腦和智能手機，都是計算機器。" onclick="showPopup(this)" /></br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- Smartphones and computers may differ in size, but are identical in specification: hardware + software.
- Hardware is the internal stuff, what we don't usually see or touch, well hidden inside the metallic case.
- Software is the external stuff, what we commonly called applications, or "apps" with fashionable icons.
- Both the Chinese terms「電腦」and 「計算機」are "computers" in English, i.e. calculating machines.
- Thus, software + hardware = computer. Only "soft" and "hard" parts are relevant, nothing else matter.
- Hence smartphones are not just like computers, they __are__ computers. This is the first conceptual jump.
:::

:::{.ch}
- 手機與電腦，大小明顯不同，但規格卻一樣：硬件 + 軟件。兩者可謂「軟、硬」兼備，樣樣齊全。
- 硬件（hardware）是泛指內部，即金屬外殼內藏的電子零件。在一般情況下，看不見、摸不到。
- 軟件（software）是泛指外部，即一般常開啟的應用程式，或稱「apps」，以圖標（icon）顯示。
- 「電腦」又稱「計算機」，英文字同樣是 computer，代表「計算機器」（calculating machine）。
- 因此，軟件 + 硬件 = 電腦，只分「軟、硬」，與「電」無關。「軟、硬」相互補，其他都不重要。
- 如是，從「軟、硬」角度看來，智慧型手機不僅 _似是_ 電腦，簡直 __就是__ 電腦。這是概念上的第一跳。
:::

<!--
(turing/macbook_vs_iphone.jpg)
手機與電腦，大小不同，但規格一樣：硬件（手機內部）+ 軟件（手機 app）。電腦又稱計算機，英文同是 computer。因此，軟件 + 硬件 = 電腦，只分「軟、硬」，與「電」無關（第一跳）。
-->

#

:::{.en}
<div class="imagecap" style="width:60%">
<img src="../turing/turing_machine_concept.jpg"
     alt="A Turing machine shown conceptually." onclick="showPopup(this)" /></br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:60%">
<img src="../turing/turing_machine_concept.jpg"
     alt="概念性顯示「圖靈機」。" onclick="showPopup(this)" /></br>
<span>點擊放大(再擊關閉)</span>
</div>
:::


:::{.en}
- Computers are called "electronic brains", but human and animal brains don't run on electricity👌.
- Alan Turing conceived the simplest "computer", now called Turing machine, as shown above.
- For a Turing machine: hardware = read/write head, software = long tape with marked squares.
- By using rules to operate this machine, Alan pointed out that every computation can be done.
- Therefore, Turing machines __are__ computers, as least conceptually due to same functionality.
- The webpage has interactive animations for you to operate a simple Turing machine by rules.
:::

:::{.ch}
- 「電腦」與「電」無關，應該見怪不怪：人類、動物皆有腦袋，都不用充電👌。
- Alan Turing 構思最簡單的「電腦 = 計算機器」，現稱為「圖靈機」，如上圖。
- 圖靈機（Turing machine）：硬件 = 讀寫頭，軟件 = 長格帶，方格記上符號。
- Alan 指出：利用規則（rules）來操作此機器，任何計算理論上都難不倒它。
- 因此，圖靈機 __就是__ 計算機，也是「電腦」，因為在概念上，兩者功能相同。
- 網頁有互動元素，讓你試玩簡單的「圖靈機」，一步又一步，依循規則操作。
:::

<!--
(turing/turing_machine_concept.jpg)
Alan Turing 構思最簡單的「電腦」，現稱為「圖靈機」👆：硬件 = 讀寫頭，軟件 = 長格帶。網頁有互動元素，讓你試玩「圖靈機」。他以數學邏輯分析，指出圖靈機 = 計算機，因為功能相同。
-->

#

:::{.en}
<div class="imagecap" style="width:60%">
<img src="../turing/algorithm.jpg"
     alt="Algorithm is a recipe for computation." onclick="showPopup(this)" /></br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:60%">
<img src="../turing/algorithm.jpg"
     alt="演算法，是計算的逐步方法。" onclick="showPopup(this)" /></br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- Both smartphones and computers are devices for processing information: text, sound, and images.
- Information employs symbols, and symbols can be encoded as numbers, or codes for computation.
- Computers use algorithms to operate with codes, representing pictures, texts, sounds and colors.
- So, symbol + algorithm = computer, in contrast to the previous: computer = software + hardware.
- The rules to operate a Turing machine form an algorithm, a step-by-step method for computation.
- Based on whatever viewpoint, Turing machines __are__ computers. This is the second conceptual jump.
:::

:::{.ch}
- 手機、電腦的功能，林林總總，但都離不開是處理各種不同的資訊：文字、聲音、圖象。
- 資訊利用符號，符號可以編碼，成為數字，進行計算。可以見得，處理資訊，全靠計算。
- 計算機以算法（algorithm）轉化編碼，更新資訊：圖文並茂，聲色俱全。不斷計、不斷變。
- 因此，符號 + 算法 = 計算機，是從另一角度，分析計算機器。先前是：計算機 = 軟件 + 硬件。
- 算法 = 演算的法則，表明逐步計算的方法。「圖靈機」的操作，依據規則，形成一套算法。
- 觀點不同，但「圖靈機」 __是__ 計算機：有軟件、硬件，有符號、算法。這是概念上的第二跳。
:::

<!--
(turing/algorithm.jpg)
手機、電腦的功能，是處理資訊：文字、聲音、圖象。資訊用符號，符號可編碼。計算機以算法（algorithm）轉化編碼，更新資訊：圖文並茂，聲色俱全。因此，符號 + 算法 = 計算機，各款快慢不同（第二跳）。
-->

#

:::{.en}
<div class="imagecap" style="width:98%">
<img src="../turing/Alan_Turing_at_1950.jpg" style="width:36%"
     alt="Alan Turing is known as the father of computing." onclick="showPopup(this)" />
<img src="../turing/turing_machine_universal.jpg" style="width:48%"
     alt="Alan Turing proved that one machine can simulate all computing machines." onclick="showPopup(this)" />
</br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:98%">
<img src="../turing/Alan_Turing_at_1950.jpg" style="width:36%"
     alt="Alan Turing 現被稱為「計算機之父」。" onclick="showPopup(this)" />
<img src="../turing/turing_machine_universal.jpg" style="width:48%"
     alt="Alan Turing 證明，有一台機器，可以模擬所有計算機器。" onclick="showPopup(this)" />
</br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- Alan Turing made a major discovery: he could design one machine to simulate any Turing machine!
- Such a Turing machine is called a "universal machine". For all the details, read my [Turing main page](../turing/index.html).
- This is significant, as it means that there is one general algorithm that can simulate any computation.
- The core of a smartphone is a universal machine: a microprocessor, called a "chip", to run all apps.
- To process information is to compute by codes. To calculate anything, all you need is one machine.
- Therefore, just one __single__ machine can process any information. This is the third conceptual jump.
:::

:::{.ch}
- Alan Turing 的研究，有重大發現：可以設計一台機器，模擬（simulate）任何「圖靈機」！
- 這樣的「圖靈機」，稱為「萬能機」（universal machine）。詳情請參閱我的 [Turing 主頁](../turing/index.html)。
- 「萬能機」不容小觑，因為這意味著：世間上有一套通用的算法，可以模擬任何計算方法。
- 手機的蕊片，就是萬能機：微型處理器（microprocessor），稱為「晶片」，運行所有 app。
- 處理資訊，就是透過程式碼來作計算。要進行任何計算，理論上只需要一台機器：萬能機。
- 因此，的而且確：只要 __單單__ 有一台萬能機器，就足以處理任何資訊。這是概念上的第三跳。
:::

<!--
Alan Turing: math and computation
https://jhlchan.bitbucket.io/turing/
Alan Turing 的研究，有重大發現：可以設計一套算法，模擬（simulate）任何計算！這樣的圖靈機，稱為「萬能機」（universal machine）。手機的蕊片，就是萬能機：同一晶片，運行所有 app。因此，計算萬能，只需一機（第三跳）。
-->

#

:::{.en}
<div class="imagecap" style="width:64%">
<img src="../turing/movie-streaming-with-phone.jpg"
     alt="A smartphone playing a streaming video." onclick="showPopup(this)" /></br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:64%">
<img src="../turing/movie-streaming-with-phone.jpg"
     alt="智能手機，正在播放串流影片。" onclick="showPopup(this)" /></br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- Watching movies on smartphone: video is played frame by frame, images are assembled pixel by pixel.
- Every pixel is color-coded, every frame is computed via encoding and decoding, by number crunching.
- When you enjoy a streaming movie, the chip in your phone keeps computing, all them time non-stop!
- Latest smartphones have chips specialized for graphics and AI, each one is a universal Turing machine.
- Thus smartphones (P) __are__ computers (C), which __are__ Turing machines (TM), i.e. P = C = TM, wonderful.
- In other words, the phone in your hands is a product of math: a device doing all computations by logic!
:::

:::{.ch}
- 在手機睇片，影片逐格播，畫面逐點砌。每一格、每一點，都要經過千千萬萬計算，才能得出成果。
- 每點 = 像素（pixel），顏色要編碼。每格 = 片幀（frame），畫面要計算。編碼解碼，皆用上數字。
- 當你享受串流電影，手機蕊片就不停計算！簡直是「數如輪轉、有數得計」，周而復始，忙個不休。
- 最新款的手機，配置專用的圖像晶片和人工智能晶片。每塊都是「萬能」圖靈機，分工合作來運算。
- 毫無疑問，手機 __是__ 計算機，而且 __的確是__ 圖靈機。亦即：手機 = 電腦或計算機 = 圖靈機，冇花冇假。
- 換句話說，每日必看的手機，其實係數學產品：一具現代化的精密電子設備，利用邏輯，進行計算！
:::

<!--
(turing/movie-streaming-with-phone.jpg)
在手機睇片，影片逐格播，畫面逐點砌。每一點、每一格都是「有數得計」。當你享受串流電影，手機蕊片就不停計算！可見，手機 = 圖靈機，冇花冇假。
-->

#

:::{.en}
<div class="imagecap" style="width:32%">
<img src="../turing/imitation_game_ver5_xlg.jpg"
     alt="Movie poster of 'The Imitation Game' in Los Angeles Times." onclick="showPopup(this)" /></br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:32%">
<img src="../turing/imitation_game_ver5_xlg.jpg"
     alt="洛杉磯時報《模仿遊戲》電影海報。" onclick="showPopup(this)" /></br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- From smartphone to computer to Turing machine, ultimately a universal machine, that's a lot to grasp!
- Hopping up through abstract concepts is hard. Better to relax and appreciate Turing and his machines.
- Alan Turing applied the principles of logical computation to crack Enigma, the formidable Nazi codes.
- Indeed, today Alan Turing was better known for his contribution in codebreaking during World War II.
- His life and story were the theme of a biographical drama "The Imitation Game" (links in my webpage).
:::

:::{.ch}
- 從手機開始，到電腦 = 計算機，再到「圖靈機」，最終達至「萬能機」，花多眼亂！
- 概念抽象，又跳幾級，自然難理解。不如輕鬆一下，欣賞「圖靈」和他的「機器」。
- Alan Turing 通曉計算原理，應用計算邏輯，破解艱巨的納粹密碼 Enigma，令人敬佩。
- 事實上，如今 Alan Turing 流芳後世，皆因他在第二次大戰時期，破解密碼的出色功績。
- Alan Turing 的故事，拍成傳奇戲劇電影《模仿遊戲》（我的網頁有連結），節奏緊湊。
:::

#

:::{.en}
<div class="imagecap" style="width:32%">
<img src="../turing/The.Imitation.Game.GN.jpg"
     alt="A comic book giving a biography of Alan Turing." onclick="showPopup(this)" /></br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:32%">
<img src="../turing/The.Imitation.Game.GN.jpg"
     alt="一本漫畫書，講述 Alan Turing 的傳奇。" onclick="showPopup(this)" /></br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- I present the main ideas of Alan Turing in [mid-autumn page](../2021/autumn.html), the machines and logic for computation.
- I present a separate page about [Alan Turing](../turing/index.html), introducing his life and achievements in various periods.
- My webpages are so packed with info that some contents are hidden, please click "Show" to explore.
- You can also read a [graphic novel](../turing/The.Imitation.Game.GN.pdf) (207 pages) about Alan Turing, a marvelously illustrated biography.
- There are documentaries, novels and books about Alan Turing, even children's books: a feast for you!
:::

:::{.ch}
- 我在[中秋網頁](../2021/autumn.html)，講解 Alan Turing 的主要想法，即計算邏輯和計算機器，及兩者關係。
- 亦提供另一版 [Alan Turing 主頁](../turing/index.html)，介紹他的生平，以及各段時期的成就，模仿是主線。
- 題材太多，範圍太廣，滔滔不絕。部分內容要隱藏，請按「顯示」發掘，自得其樂。
- 又可閱讀關於 Alan Turing 的[漫畫小說](../turing/The.Imitation.Game.GN.pdf)（207頁），一本精彩的插圖傳記，難得一見。
- 網頁內，除了關於他的視頻、紀錄片、小說和書籍，甚至還有兒童讀物，目不暇給！
:::

<!--
(turing/The.Imitation.Game.GN.jpg)
題材太多，部分要隱藏，請按「顯示」發掘。更有 Alan Turing 主頁（見第三跳），介紹生平及成就。網頁內除了關於他的電影，還有關於他的漫畫小說、兒童圖書，目不暇給。
-->

#

:::{.small}
<span class="en">Alan Turing's proposal to Joan Clarke | The Imitation Game </span><span class="ch">電影《模仿遊戲》中 Alan Turing 向 Joan Clarke 求婚</span>【1:32】<br>
<a href="https://youtu.be/_tAYVGNZwGQ">
<img src="http://img.youtube.com/vi/_tAYVGNZwGQ/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- Alan Turing is homosexual, but he tries to imitate normal people and proposes to his "girlfriend".
- The movie scirpt invents a story of their first meeting, and how she helps Alan to be sociable.
- The scriptwriters even come up with the following heart-warming and inspirational quote:
-
| _Sometimes it's the very people_
| _who no one imagines anything of_
| _who do the things no one can imagine._

- A self-breakthrough by a nobody. An arrangement by fate, or one is in control of one's destiny?
:::

:::{.ch}
- Alan Turing 自幼養成同性癖，卻模仿正常人。有一次，他竟然向「女朋友」求婚。
- 電影劇本中，巧妙的安排兩人第一次見面，以及她如何勸誡 Alan，變得善於交際。
- 以下一段精彩對白，溫馨勵志，但這句並非歷史有記載，只是編劇組的得意創作：
-
| _很多時候，[Sometimes it's the very people]_
| _正是大家不抱期望之人，[who no one imagines anything of]_
| _作出大家不敢想像之事。[who do the things no one can imagine.]_

- 無名小卒，自我突破，成就非凡。這是命運中冥冥安排，還是自己牢牢掌握命運？
:::

<!--
Alan Turing's break down scene | The Imitation Game【4:04】
https://www.youtube.com/watch?v=bhyi154pgJE
https://youtu.be/bhyi154pgJE
概念抽象，又跳幾級，自然難理解，不如輕鬆一下，欣賞「圖靈」和他的「機器」。Alan Turing 把邏輯計算原理，應用於破解納粹密碼。故事拍成電影《模仿遊戲》（網頁有連結）。他有同性癖，但模仿正常人，向「女友」求婚。視頻是影片尾聲，她回來探訪一幕。
-->

#

:::{.small}
<span class="en">Anita Mui sings "Like an Old Friend Comes" </span><span class="ch">梅艷芳《似是故人來》</span>【4:05】<br>
<a href="https://youtu.be/x0XzBnYw4n0">
<img src="http://img.youtube.com/vi/x0XzBnYw4n0/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::
<!-- x0XzBnYw4n0 【4:05】 or A6teCykKAGc 【3:41】 -->

:::{.en}
- In the ending scenes of the movie, Alan Turing's lady friend comes back to pay a visit.
- The song ["Like an Old Friend Comes"](../songs/LikeAnOldFriendComes.html) is selected to match such a situtation of reunion.
- Anita Mui's deep and emotional interpretation is one of a kind, singing the Chinese lyrics:
-
| _在年月深淵，[In the endless abyss of time gone by,]_
| _望明月遠遠，[Looking at the bright moon from afar,]_
| _想像你幽怨。[Imagine the resentfulness you feel inside.]_

- This brings out the lingering feeling of Mid-Autumn, with a dramatic and sentimental style.
:::

:::{.ch}
- 電影臨近結尾時，Alan Turing 的女友回來探訪。二人重逢，恍如隔世。
- 選曲[《似是故人來》](../songs/LikeAnOldFriendComes.html)，回應以上一幕。面對命運安排，不禁令人唏嘘。
- 梅艷芳亮麗的風采，堪稱一絕。唱功感情投入，扣人心弦。其中歌詞：
- 
| _在年月深淵，_
| _望明月遠遠，_
| _想像你幽怨。_

- 以感性的文字，勾畫飛越時空，帶出中秋的纏綿情懷，讓人感慨萬千。
:::

<!--
梅艷芳 似是故人來【4:23】Live Performance
https://www.youtube.com/watch?v=vPj_humiQnQ
https://youtu.be/vPj_humiQnQ
選曲《似是故人來》，回應以上一幕。梅艷芳的風采，堪稱一絕。歌詞「在年月深淵，望明月遠遠，想像你幽怨。」，帶出中秋感概。

(a bit later)
The Imitation Game Full HD in English【01:54:14】no cc
https://www.bilibili.tv/en/video/4788888992486912
Alan Turing 一生是《模仿遊戲》。如果未看過電影，先閱讀我的 Alan Turing 網頁，然後欣賞，有另一番體會。

(total 9 + 1 = 10 parts, really too much! one more if adding 'human computer' later.)
xxx

也許認為，計算是數學的小兒科。但可知道，計算其實是數學的全部 ...
因此，電腦不單只是計算機，更是「萬能計算機」=「圖靈機」。原本是數學邏輯的玩意，後來 Alan Turing 用它來破解德國納粹密碼。故事拍成電影，相當精采。
選曲涉及同性戀愛，配合故事主角，帶出劇終前一幕。
Alan Turing 被稱為「計算機之父」。追溯淵源，手機 <- 電腦 <- 計算機
這是 Alan Turing 研究數學邏輯的成果。
雖然歌詞隱喻同性戀，但用字一流。加上梅艷芳的風采，堪稱一絕。一流 = 出神入化
(diagram of: smartphone/PC/Turing machine)
第一，手機是電腦 -- 大小各不同，但規格一樣：硬件（手機內部）+ 軟件（手機 app）。兩者功能，都是處理資訊：文字、聲音、圖象。
第二，電腦要計算 -- 資訊用符號，符號可編碼。計算機利用算法，轉化編碼，更新資訊，顯示手機上：圖文並茂、聲色俱全。
新型手機輕巧，早期電腦笨重。計算機可以幾原始？

第一級：返璞歸真。
第一級：先進到原始，「電腦」的本質：硬體（手機內部）+ 軟體（手機 app）
第二級：實用到抽象，「計算機」的本質：資料（data）+ 演算法（algorithm），讀/寫+磁帶=圖靈機
第三級：計算範圍，「萬能機」的本質：演算法模擬計算機，現在是微處理器。

第一級：手機是電腦 = 硬件 + 軟件。手機內部是硬件，每一個 app 都是軟件。早期電腦又硬又大，軟件用磁盤𥔵碟。
第二級：電腦是計算機 = 符號 + 算法。手機、電腦的用途，是處理資訊：文字、聲音、圖象。把資訊符號編碼，便是「數碼資訊」（digital information）。計算機利用計算方法，轉化編碼，成為資訊，即手機顯示：圖文並茂、聲色俱全。
第三級：計算機是萬能。這是 Alan Turing 的發現：可以設計一套算法，模擬任可計算機！這套萬能算法，安裝在手機蕊片內，稱為「微處理器」（microprossor），變得「智能手機」！
本頁+圖靈專頁

Hidden Figures - Attribution【5:36】no cc
https://www.youtube.com/watch?v=bxG9qyZs73k
(just ciips from Hidden Figures)

A Black Woman Comes From Humble Beginnings But Rewrites The American Aerospace History【12:29】cc
https://www.youtube.com/watch?v=GrnRgLRUeDU
(a narrative of human computing using scenes from Hidden Figures)
movie name《Hidden Figures》
早期 computer = 計算的人，通常是女性，工資低微。NASA 未安裝電腦前，就聘用女士，以手動計數機計算，故事拍成電影《NASA 無名英雌》（Hidden Figures）。視取自電影，頻集中講述其中一名主角（有三位）。她表現出色，更勝第一代電子計算機。

計算是數學，因此「計算機」=「數學機器」，即「智能手機」=「數學機器」，信不信由你。
有數得計 數學離不開數字 數字組成的資料，就是數據（data），處理數據，使用電腦。在手機睇片播片，片由格組胡，格由點組成。每一點、每一格都是計算岀來。當你不斷睇片，手機就不斷計算！可見，手機=計算機，冇花冇假。

Alan Turing: math and computation
https://jhlchan.bitbucket.io/turing/
我們學習計算，從數手指開始，最簡單的方法。計算方法依步驟，步驟跟據邏輯。Alan Turing 研究數學邏輯，領悟出計算的基本原理。他的「圖靈機」(Turing machine)，是「萬能計算機」。現代科技靠電腦，原因是「萬能」。現代人人玩手機，全因是「智能」。每一功能就是一個 app，你手機上有多少個 app？

Turing Machines - How Computer Science Was Created By Accident【17:05】cc
https://www.youtube.com/watch?v=PLVCscCY4xI
https://youtu.be/
(a bit long, but close to my account, can skip example if it is too hard for you)
xxx

-->


<!--
pandoc -t revealjs -s --mathjax gift05.md -o gift05.html
-->
