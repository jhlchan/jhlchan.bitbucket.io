---
title: Xmas 2021 Highlights
author: <span class="en">mechanical computing</span><span class="ch">機械計算</span>
date: <span class="en">Xmas 2024</span><span class="ch">2024年聖誕節</span>
theme: solarized
header-includes: |
    <meta property="og:type" content="website" />
    <meta property="og:title" content="2021年聖誕禮物" />
    <meta property="og:image" content="../images/highlight-06.jpg" />
    <base target="_blank"/>
    <link rel="stylesheet" type="text/css" href="../css/slides.css"></link>
    <script type="text/javascript" src="../scripts/site.js"></script>
include-before: |
    <!-- Any raw HTML code right after body tag, before anything else. -->
    <div id="modal" class="modal">
      <img id="modal-content" />
      <div id="modal-caption"></div>
    </div>
    <div align="right" class="banner">
    <input type="radio" name="lang" onclick="set_lang('en')" id="english" value="English" checked /> English |
    <input type="radio" name="lang" onclick="set_lang('ch')" id="chinese" value="Chinese" /> 中文
    </div>
include-after: |
    <!-- Any raw HTML code right before body end tag, after anything else. -->
    <script>window.onload = check</script>
    <!-- file:///Users/josephchan/jc/q/site/slides/gift06.html -->
    <!-- https://jhlchan.bitbucket.io/slides/gift06.html -->
...

#

:::{.en}
<img src="../images/gift06_en.jpg" width="100%" />

- For the [2021 Xmas](../2021/xmas.html) "gift" the theme is computers, past and present, spanning over centuries.
- Computers are computing machines, the principles are rooted in math, logic and algorithms.
- The previous episode talked about "Smartphone = Turing Machine", and introduced [Alan Turing](../turing/index.html).
- It turns out that similar ideas were already in place a century earlier, during the Victorian era.
- Although the development is different, the process of innovation also takes 3 conceptual jumps.
:::

:::{.ch}
<img src="../images/gift06_ch.jpg" width="100%" />

- [2021年聖誕節](../2021/xmas.html)「禮物」，主題是：計算機的前世今生，時空橫跨幾個世紀。
- 計算機 = 計算的機器，其操作原理，源於數學、邏輯和算法（algorithm）。
- 上一集講「智能手機 = 圖靈機」，還有介紹 [Alan Turing](../turing/index.html)，即「計算機之父 」。
- 但原來早一個世紀，在維多利亞時代（Victorian era），已經出現同樣的想法。
- 雖然發展不盡相同，創新過程亦是三級跳。從加數機到算術機，再到計算機器。
:::

<!--
** 2024/dragon
2024/xmas
https://jhlchan.bitbucket.io/2021/xmas.html
(video?: Christmas Dinner)
2021年聖誕節「禮物」，主題是計算機的前世今生。上一集講「手機 = 圖靈機」，但原來早一世紀，在維多利亞時代，已經有同樣的想法，亦是三級跳。
-->

#

:::{.en}
<div class="imagecap" style="width:98%">
<img src="../lovelace/charles-babbage-sitting.jpg" style="width:36%"
     alt="Charles Babbage had plans to build mechanical machines for computation." onclick="showPopup(this)" />
<img src="../lovelace/difference-engine-model.jpg" style="width:37.2%"
     alt="Only a desktop model was built for the Difference Engine envisioned by Babbage." onclick="showPopup(this)" />
</br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:98%">
<img src="../lovelace/charles-babbage-sitting.jpg" style="width:36%"
     alt="Charles Babbage 滿腦計劃，建造機械計算機器。" onclick="showPopup(this)" />
<img src="../lovelace/difference-engine-model.jpg" style="width:37.2%"
     alt="Babbage 構想的「差分法」，只建成桌面模型。" onclick="showPopup(this)" />
</br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- In 1823, Charles Babbage began designing machines to calculate and print math table automatically.
- Using columns of wheels and gears, his conceived a "Difference Engine" that can add and subtract.
- The machine is so named because it can compute squares and cubes, by the method of difference.
- It can compute polynomials by suitable settings and hand cranking. This is the first jump in innovation.
- He tried to acquire funds to build one, but only got enough to build a model 👆 for display on a table.
:::

:::{.ch}
- 1823年，Charles Babbage 開始設計機器，自動計算並列印數表（math table）。
- 利用整齊排列的轉輪和齒輪，他構思「差分機」，能夠進行一連串的加、減運算。
- 這台機器的名稱，來自數學中的「差分法」。透過差分法，可以計算平方和立方。
- 適當的起初設定，隨後手搖機器，便計算出多項式。此乃計算機器創新的第一跳。
- 他不斷籌備經費，想建造一台。苦無支持，只夠錢建造一具模型👆，放在桌面上。
:::

<!--
(lovelace/difference-engine-model.jpg)
1823年，Charles Babbage 開始設計機器作數表（math table）計算，只有加減，稱為「差分機」（Difference Engine）（第一跳）。他一直籌備經費，但只夠建造模型👆，放在桌面。
-->

#

:::{.en}
<div class="imagecap" style="width:98%">
<img src="../lovelace/Augustus.Ada.Lovelace.jpg" style="width:39.5%"
     alt="Ada Lovelace was the daughter of the English poet Lord Byron." onclick="showPopup(this)" />
<img src="../lovelace/ada_byron_met_babbage.jpg" style="width:60%"
     alt="The young Ada was fascinated by the working of Babbage's model." onclick="showPopup(this)" />
</br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:98%">
<img src="../lovelace/Augustus.Ada.Lovelace.jpg" style="width:39.5%"
     alt="Ada Lovelace 是英國詩人拜倫勳爵的女兒。" onclick="showPopup(this)" />
<img src="../lovelace/ada_byron_met_babbage.jpg" style="width:60%"
     alt="年輕的 Ada，對 Babbage 計算模型的運作，極感興趣。" onclick="showPopup(this)" />
</br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- Babbage had been hosting social gatherings demonstrating his Difference Engine model to guests.
- Ten years later (1833), one of the guests invited was young Ada Byron, daughter of poet Lord Byron.
- Ada was unique in that era to have science education, as her mother did not wish her to be a poet.
- The two met on 5 June in that year. Ada was intrigued by the machine, and examined it thoroughly.
- She was quick to understand its operation, and her imagination about computing machines fired up.
- They became lifelong friends, as Ada yearned to know how complex math can be done by machines.
:::

:::{.ch}
- Babbage 常常舉辦社交聚會，展示「差分機」模型。機器計算平方、立方快而準，眾人嘖嘖稱奇。
- 十年後（1833年），年輕少女 Ada Byron 也是嘉賓之一，她就是詩人拜倫（Byron）勳爵的女兒。
- Ada 自幼接受科普教育，在那個年代鮮有。原因是她的母親，並不希望 Ada 習染文學，成為詩人。
- 當年6月5日，兩人相遇，Ada 對這台機器十分感興趣。她仔細檢查機件齒輪，即時分析進行研究。
- 她很快就懂得「差分機」的操作，明白其中數學原理。津津言談間，激發她對計算機器的想像力。
- 兩人興趣相投，成為忘年之交。Ada 渴望知道：以機器完成數學運算，可以有幾複雜，去到幾盡。
:::

<!--
(June 5 1833 page 18 of comic book, or there about)
(lovelace/ada_byron_met_babbage.jpg)
十年後（1833年），他舉辦社交聚會，示範差分機模型，少女 Ada Byron 也是嘉賓之一。兩人相遇，成為忘年之交。
-->

#

:::{.en}
<div class="imagecap" style="width:64%">
<img src="../lovelace/babbage_ada_analytical_engine.jpg"
     alt="An artistic depiction of the dream mechanical computer, the Analytical Engine." onclick="showPopup(this)" /></br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:64%">
<img src="../lovelace/babbage_ada_analytical_engine.jpg"
     alt="藝術家描繪的夢想機械計算機，稱為「分析引擎」。" onclick="showPopup(this)" /></br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- Babbage regularly held dinners to talk about his plans for computing machines, and Ada was present.
- He aimed at general calculations (add, subtract, multiply and divide), so he designed a dream machine.
- Inspired by punch cards in textile, he used cards with holes to sequence instructions for computation.
- He had columns of gears to store intermediate calculation results, just like modern computer memory.
- Such a versatile computing machine is called "Analytical Engine". This is the second jump in innovation.
:::

:::{.ch}
- Babbage 博學多才，時常安排晚宴，高談「計算機器」大計。Ada 當然是座上客，悉心聆聽。
- 他的目標，是全部四則計算（加、減、乘、除）。反覆思考設計，追求他的 dream machine。
- 紡織打孔卡讓他靈機一觸，利用卡孔作指令，計算流程便可遵循步驟，大大增加計算的靈活性。
- 安裝一排排齒輪柱列，用於暫存計算步驟的中期結果。柱列數量很多，像現代電腦記憶體一樣。
- 這台夢想全能計算機器，稱為「分析機」（Analytical Engine），此乃計算機器創新的第二跳。
:::

<!--
(lovelace/babbage_ada_analytical_engine.jpg)
Babbage 常設晚宴，高談「計算機器」大計，Ada 均是座上客。他追求全面計算（加減乘除），開始設計 dream machine，稱為「分析機」（Analytical Engine）（第二跳）。並配合計算指令，靈感來自紡織打孔卡，再加一排排設置，暫存計算結果。
-->

#

:::{.en}
<div class="imagecap" style="width:98%">
<img src="../lovelace/ada_lovelace_work_on_notes.jpg" style="width:56%"
     alt="In fact, Babbage suggested Ada to add her notes in the translation of his lectures." onclick="showPopup(this)" />
<img src="../lovelace/techlab_babbage_04.jpg" style="width:42%"
     alt="Ada foresees more applications for the Analytical Engine than Babbage." onclick="showPopup(this)" />
</br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:98%">
<img src="../lovelace/ada_lovelace_work_on_notes.jpg" style="width:56%"
     alt="事實上，Babbage 建議 Ada 在翻譯他的講座時，加上她的附錄筆記。" onclick="showPopup(this)" />
<img src="../lovelace/techlab_babbage_04.jpg" style="width:42%"
     alt="分析機的應用範圍，Ada 比 Babbage 預見得更多更遠。" onclick="showPopup(this)" />
</br>
<span>點擊放大(再擊關閉)</span>
</div>
:::


:::{.en}
- After ten years (1843), Babbage was still designing the Analytical Engine, kept revising his blueprints.
- Once he was invited to make a speech about his machines. His lecture notes was published in French. 
- Ada translated and edited the lecture notes in English, adding several explanatory notes as appendix.
- Babbage imagined his machine to work on numbers, but Ada knew that numbers can encode symbols.
- She understood the principles of Analytical Engine, imagined encoding music 🎵 to create new tunes.
- She foresaw that the Analytical Engine is a "general-purpose computer", the third jump in innovation.
- To show how this dream machine works, she used an example and wrote the first computer program.
:::

:::{.ch}
- 又十年後（1843年），Babbage 仍在設計「分析機」。藍圖不斷修改，因為軟件指令可代替硬件。
- 一次他接受邀請前往義大利，就「計算機器」發表演講。在座一名軍官筆錄，其後講稿以法文出版。
- Ada 把講稿翻譯成英文，稍作編輯。Babbage 甚欣賞，建議她添上自己的註釋加以說明，作為附錄。
- Babbage 想像他的計算機器，完全處理數字。但 Ada 知道，符號可以編碼，變為數字，進行計算。
- 她明白箇中原理，想像音符🎵編碼，「分析機」便可創作新曲譜。想像力非凡，不愧為詩人後裔！
- 她預見「分析機」是「萬能計算機」（general-purpose computer），此乃計算機器創新的第三跳。
- 對於空談的夢想機器，她示範如何運作。Ada 孜孜不倦，埋首鑽研演算步驟，編寫成首個計算程式。
:::

<!--
(lovelace/ada_lovelace_work_on_notes.jpg)
又十年後（1843年），Babbage 仍在設計分析機，藍圖不斷修改。曾作一次演講，講稿由 Ada 翻譯編輯，加入她的說明。她明白箇中原理，想像音符🎵編碼，分析機便可製作新曲譜。她預見分析機是「萬能計算機」（general computer）（第三跳）。對於空談的夢想機器，她示範如何運作，編寫首個計算程式。
-->


#

:::{.en}
<div class="imagecap" style="width:98%">
<img src="../lovelace/ada_lovelace_notes_03.jpg" style="width:45%"
     alt="First and last pages of the published talk of Babbage on his Analytical Engine." onclick="showPopup(this)" />
<img src="../lovelace/Lovelace-1843-9-program.jpg" style="width:54.6%"
     alt="Ada Lovelace detailed a work example for the Analytical Engine, considered the first computer program." onclick="showPopup(this)" />
</br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:98%">
<img src="../lovelace/ada_lovelace_notes_03.jpg" style="width:45%"
     alt="Babbage 發表關於「分析機」的演講，首頁和末頁。" onclick="showPopup(this)" />
<img src="../lovelace/Lovelace-1843-9-program.jpg" style="width:54.6%"
     alt="Ada Lovelace 詳細介紹「分析機」的操作範例，被認為是首個電腦程式。" onclick="showPopup(this)" />
</br>
<span>點擊放大(再擊關閉)</span>
</div>
:::


:::{.en}
- Charles Babbage and Michael Faraday, inventor of electric motors and generators, were close friends.
- Ada Lovelace attended the popular [Faraday lectures](../slides/rixmas2023.html), even tried unsuccessfully to get him as her tutor.
- Babbage was pleased when the lecture and notes were finally published 👆, so he sent Faraday a copy.
- Faraday didn't understand the technical details, but he praised Babbage for the insight in the notes.
- Babbage replied that he should not get the credit as the notes were Ada's, describing her as:
- 
| _that Enchantress who has thrown her magical spell around the most abstract of Sciences and has grasped it_
| _with a force which few masculine intellects (in our own country at least) could have exerted over it._
| _I remember well your first interview with the youthfull fairy which she herself has not forgotten ..._
:::

:::{.ch}
- Charles Babbage 和 Michael Faraday 屬同齡好友。前者醉心計算機器，後者發明電動機和發電機。
- Ada Lovelace 出席當代風靡一時的 [Faraday 講座](../slides/rixmas2023.html)，甚至試圖說服他成為她的導師，可惜未能如願。
- 多年後，「分析機」演講文本和附錄最終發表 👆。Babbage 非常滿意，隨即發送副本給 Faraday。
- Faraday 讀不懂其中技術性細節，但看得懂生花妙筆的附錄。他稱讚 Babbage 在附錄中的洞察力。
- Babbage 回覆說，自己不應該得到讚賞，因為附錄是 Ada 所撰寫，並將她描述為：
-
| _那才貌超群女巫，把她美妙的魔法，施展在最抽象的科學上。_
| _她的智慧程度與理解能力，男性中也很少見（至少在我們國家內）。_
| _你和這位年輕仙女的首次會面，我清楚記得，她自己也沒有忘記 ..._
:::

<!--
Ada Lovelace and the IET
Posted on September 25, 2012 by Suw
https://findingada.com/blog/2012/09/25/ada-lovelace-and-the-iet/
>>>
We are privileged to be holding Ada Lovelace Day Live! at the Institution of Engineering and Technology (IET) this year, because of our partnership with the Women’s Engineering Society (WES). As you’ve probably noticed, WES will be presenting the prestigious Karen Burt Memorial Award to a newly chartered woman engineer during ALD Live!.

But our presence at the IET is also appropriate for another reason: held in the IET archives are letters from Ada Lovelace to Michael Faraday, a letter from Lovelace’s close friend Charles Babbage to Faraday, and portraits of both Lovelace and Babbage. When I visited, IET archivist Sarah Hale was kind enough to arrange for me to see the letters and Lovelace’s portrait, although sadly the room where Babbage’s hangs was in use at the time.

(with photos and transcript of letters)
>>>
(longer version)

Ada Lovelace: Victorian computing visionary
by Suw Charman-Anderson
https://findingada.com/shop/a-passion-for-science-stories-of-discovery-and-invention/ada-lovelace-victorian-computing-visionary/
This updated chapter is from the second edition of our women in STEM anthology, A Passion For Science: Tales of Discovery and Invention, available as an ebook for £1.99 from Amazon.


Faraday to Charles Babbage   1 September 1843
https://epsilon.ac.uk/view/faraday/letters/Faraday1516
>>>
My dear Babbage

I think I may thank you for the translation1 & though I cannot understand your great work yet I can well comprehend by its effect upon those who do understand it how great a work it is[.] Wishing you life & health to see it in full action

I am | Yours Most Truly | M. Faraday

C. Babbage Esq | &c &c
>>>

Charles Babbage to Faraday   9 September 1843
https://epsilon.ac.uk/view/faraday/letters/Faraday1520
>>>
My dear Faraday,

I am not quite sure whether I thanked you for a kind note[1] imputing to me unmeritedly the merit of a present you received I conjecture from Lady Lovelace.

I now send you what ought to have accompanied that Translation[2].

So you will now have to write another note so that Enchantress who has thrown her magical spell around the most abstract of Sciences and has grasped it with a force which few masculine intellects (in our own country at least) could have exerted over it. I remember well your first interview with the youthfull fairy which she herself has not forgotten and I am gratefull to you both for making my drawings rooms the Chateau D'Eu[3] of Science[.]

I am going for a short time to Lord Lovelaces[4] place in Somersetshire. It is a romantic spot on the rocky coast called Ashley about 2 miles from the Post town Porlock.

I am | My dear Faraday | Ever truly Yours | C. Babbage

Dorset St | Manch Sq | 9 Sep 1843
(see also the photos: ~/lovelace/Babbage_Faraday_Letter_1.jpg, Babbage_Faraday_Letter_2.jpg)
>>>

A century of women at the Royal Institution
Posted on September 28, 2023 by Suw
https://findingada.com/blog/2023/09/28/a-century-of-women-at-the-royal-institution/
Ada Lovelace herself attended lectures at the Ri, in the very theatre where ALD 2023 will be taking place this year and the same theatre where Michael Faraday first demonstrated many of his discoveries. The Ri is still home to his original laboratory and his collection of notes, which are preserved as part of their internationally significant collection, on display in the Ri’s free museum.

The Ri is not just a home for science where everyone is welcome, it continues to champion the known and unknown contributions of women to science. It has hosted many amazing female speakers, including:

Tara Shears – Antimatter: Why the anti-world matters
Kate Lancaster – The extreme world of ultra intense lasers
Eleanor Maguire – The neuroscience of memory
Hannah Fry – Should computers run the world?
Aoife McLysaght  – Copy number variation and the secret of life

-->

#

:::{.small}
<span class="en">Ada Byron Lovelace and the Thinking Machine </span><span class="ch">Ada Byron Lovelace 與「思想機器」</span>【17:34】<br>
<a href="https://youtu.be/-7guY-QkDRk">
<img src="http://img.youtube.com/vi/-7guY-QkDRk/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- This episode's main character is Ada Lovelace, her life is briefly told in this picture book (read aloud).
- The [2021 Christmas page](../2021/xmas.html) gives an overall view of the development of Victorian computing ideas.
- There is [Ada Lovlace main page](../lovelace/index.html), telling her legendary story from a sweet baby to a bed-striken lady.
- There is [Ada Lovlace extra page](../lovelace/extra.html), showcasing her many literary works and other documents about her.
- Many biographies, kid's storybooks (in extra page), comic books, and documentaries (in main page).
- This episode includes interactive elements (click "Show"). A dedicated production, not to be missed!
:::

:::{.ch}
- 聰穎可愛的 Ada Lovelace ，正是本期的主題人物，圖書繪本簡述她的生平事跡（視頻朗讀）。
- 在 [2021年聖誕節網頁](../2021/xmas.html) ，背景是維多利亞時代。我對當時機械計算的思想發展，作全面介紹。
- 備有 [Ada Lovlace 主頁](../lovelace/index.html)，講述她的精彩傳奇故事，從甜蜜可愛的小女嬰，到臥床不起的女士。
- 兼有 [Ada Lovlace 附頁](../lovelace/extra.html)，展示她的優秀文學作品，充滿詩意，字字鏗鏘，及與她有關的文獻。
- 亦有多本 Ada 的傳記，兒童故事書（在附頁找），奇情漫畫圖書，及記錄專輯（在主頁找）。
- 這一集包含有幾個互動元素（請按「顯示」），認真大堆頭、大製作，精心炮製，不容錯過！
:::

<!--
Ada Byron Lovelace and the Thinking Machine【17:34】cc
https://www.youtube.com/watch?v=-7guY-QkDRk
https://youtu.be/-7guY-QkDRk
本期的主題人物是 Ada Lovelace，視頻簡述她的故事。有專頁 https://jhlchan.bitbucket.io/lovelace/，有附頁 https://jhlchan.bitbucket.io/lovelace/extra.html，有多本傳記，兒童故事書（在附頁找），奇情漫畫圖書，及記錄片（在專頁找），這一輯有互動元素，認真大製作，不容錯過！
-->

#

:::{.small}
<span class="en">Calculating Ada: the Countess of Computers</span><span class="ch">計算機的伯爵夫人 Ada Lovelace</span>【58:41】cc<br>
<a href="https://youtu.be/QgUVrzkQgds">
<img src="../lovelace/Calculating.Ada.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- Ada Lovelace was a most unlikely computer pioneer, as explained in this documentary by Hannah Fry.
- The presenter is a mathematician, and she talked to biographers of Ada to tell Ada's remarkable story.
- She also consulted with experts in computing history for technical details about Babbage's machines.
- The story begins at Ada's home, then her encounter with Babbage. A Jacquard Loom demo is included.
- An actual Difference Engine was built according to blueprints in Babbage's archives, see it in action.
- The Analytical Engine = cogwheel computer was never built, but what if Queen Victoria did have one?
:::

:::{.ch}
- 正如紀錄片中 Hannah Fry 所說，Ada Lovelace 成為計算機科學先驅，雖匪夷所思，但非意料之外。
- 主講是數學家，有興趣探討這段不尋常的歷史。她訪問多位 Ada 傳記作者，講述其中非凡的情節。
- 她也諮詢計算機科學歷史專家，以了解有關 Babbage 計算機器的設計，以及其中機械技術的細節。
- 故事從 Ada 的故居開始，其後是她與 Babbage 相遇。亦有參觀紡織間，示範「提花織機」的操作。
- 電影展示真實的「差分機」，根據 Babbage 檔案中的藍圖構建完成，請看它的實際機械操作情況。
- 「分析機」= 齒輪「電腦」，從未製成。忽發奇想，若維多利亞女王擁有一台，是否會天翻地覆？
:::

#

:::{.en}
<div class="imagecap" style="width:32%">
<img src="../lovelace/The.Thrilling.Adventures.of.Lovelace.and.Babbage.jpg"
     alt="The comic book takes on a fantasy that Ada and Babbage operating the Analytical Engine." onclick="showPopup(this)" /></br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:32%">
<img src="../lovelace/The.Thrilling.Adventures.of.Lovelace.and.Babbage.jpg"
     alt="漫畫書天馬行空，幻想 Ada 和 Babbage 建成並操作「分析機」。" onclick="showPopup(this)" /></br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- A graphic novel by Sydney Padua imagined that Babbage and Ada finally built the dream machine.
- No electricity during Victorian Era, so Babbage had the gigantic Analytic Engine driven by steam.
- The machine will be so huge that Ada would get inside and repair it, of course not in a pretty dress!
- Their "computer" was presented to Queen Victoria. Can you imagine how the hilarious demo ends?
- The stories are based on available documents, and the cartoons depict a style called "steampunk".
- Enjoy this [comic book](../lovelace/The.Thrilling.Adventures.of.Lovelace.and.Babbage.pdf) (311 pages) in your own leisure time, be amused by the thrilling adventures!
:::

:::{.ch}
- Sydney Padua 的圖畫小說，想像 Babbage 和 Ada 最終成功建造「分析機」，一台夢幻機械計算機。
- 維多利亞時代未有電力，動能靠蒸汽。所以 Babbage 設計的「分析機」由蒸汽驅動，是龐然大物。
- 這台計算機十分巨型，Ada 會進入機內，進行修理。擔當如此超級任務，當然不可穿著漂亮的衫裙！
- 二人的「夢幻電腦」，送交維多利亞女王。試想想，在女王面前示範計算機，錯漏百出，如何收科？
- 作者撰寫故事情節，參考現存文檔，加入自己想像。繪圖風格，稱為「蒸氣朋克」（steampunk）。
- 閒暇休息時，請享受這本[漫畫書](../lovelace/The.Thrilling.Adventures.of.Lovelace.and.Babbage.pdf)（311頁）。親身感受一下，Ada 與 Babbage 驚心動魄的冒險樂趣！
:::

#

:::{.small}
Christmas Dinner, Noel Paul Stookey and Doug Cameron【4:09】<br>
<a href="https://youtu.be/lLrKJH-ACas">
<img src="http://img.youtube.com/vi/lLrKJH-ACas/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- During Ada's days, poor children did not have enough food to eat or clothes to wear.
- In this video is a slow but gripping [Christmas song](../songs/ChristmasDinner.html) that touches the heart of everyone.
- Listen to the lyrics:
-
| _The gray-haired lady brought forth to the table_
| _Glasses two, and her last drops of wine_
| _Said she, "Here's a toast to everyone's Christmas_
| _And especially, yours and mine!"_
- The skillful guitarist is Paul, a member of "Peter, Paul and Mary", with a voice like silk.
:::

:::{.ch}
- 在 Ada 的年代，貧童得不到溫飽。每逢節日，眾人皆慶祝，他們卻是斯人獨憔悴。
- 視頻雙人演奏，唱出一首此情此景的[聖誕歌曲](../songs/ChristmasDinner.html)，娓娓動聽，扣人心弦，感人肺腑。
- 聆聽其中一段英文歌詞：
-
| _[白髮女士，在餐桌放上兩杯，] The gray-haired lady brought forth to the table_
| _[徐徐添酒，只有最後幾點滴。] Glasses two, and her last drops of wine_
| _[她說：為大家的聖誕節乾杯，] Said she, "Here's a toast to everyone's Christmas_
| _[尤其可喜，這晚餐是你我的！] And especially, yours and mine!_
- 技藝精湛的結他手，是「Peter, Paul and Mary」的成員 Paul，唱腔柔滑細緻如絲。
:::

<!--
Christmas Dinner, Noel Paul Stookey and Doug Cameron【4:09】
https://www.youtube.com/watch?v=lLrKJH-ACas
https://youtu.be/lLrKJH-ACas
在 Ada 的年代，貧童得不到溫飽。這是一首扣人心弦的聖誕歌曲，感人肺腑。

(now 7 parts, too little?)
(also 9 parts, too much?)


xxx
Ada Lovelace: Enchantress of Number
https://jhlchan.bitbucket.io/lovelace/
本期的主題人物是 Ada Lovelace，有專頁👆，有附頁👇，有多本傳記，兒童故事書，奇情漫畫圖書，及記錄片（網頁有連結），認真大製作，不容錯過！

Augusta Ada Lovelace: extra collection
https://jhlchan.bitbucket.io/lovelace/extra.html
Ada Lovelace 附加頁，可查看她編寫程式的真本。足以證明，她是最早的寫 app 專家！

Ada Byron Lovelace and the Thinking Machine【17:34】cc
https://www.youtube.com/watch?v=-7guY-QkDRk
https://youtu.be/-7guY-QkDRk
朗讀的兒童讀物，講述 Ada Lovelace 的故事。如需更多故事書，請在此網頁中找到連結。
https://jhlchan.bitbucket.io/lovelace/extra.html
(this style is better)

Calculating Ada: the Countess of Computers【58:43】cc
https://vimeo.com/188265119
《走近 Ada，計算學的伯爵夫人》Calculating Ada - The Countess Of Computing【58:23】no cc
https://www.bilibili.com/video/BV16L4y1J7b2/ (試看30秒) click can keep watching
今集算是大製作，這一輯亦有互動元素，內容更伸延至 Ada Lovelace 主頁，介紹她的生平及成就，再加附錄文章。除了關於她的影片，還有關於她的漫畫小說、兒童圖書，目不暇給。妙不可言。
or
(Ada comic book cover)
(lovelace/The.Thrilling.Adventures.of.Lovelace.and.Babbage.jpg)
奇情漫畫圖書，

2021年聖誕節「禮物」，主題是：計算機的前生。200年前首架機械計算機模型面世，由 Charles Babbage 設計，直接影響一名少女的一生。她就是 Ada Lovelace，也是首位軟件編程師，即是創作 app。未有電腦，如何寫 app？故事曲折離奇但千真萬確。請欣賞紀錄片，或一首扣人心弦的聖誕歌曲。

可查看她編寫程式的真本。足以證明，她是最早的寫 app 專家！上☝️👆下👇
，音樂。這個萬能，是第三跳。，富想像力，並爲空談的分析機作運作示範例子，編寫程式首個。
汽車里程表是機械加法器 odometer 想出例子，在一次示範中認識他，兩人相遇了
xxx

-->

<!--
pandoc -t revealjs -s --mathjax gift06.md -o gift06.html
-->
