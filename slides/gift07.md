---
title: Lunar New Year 2022 Highlights
author: <span class="en">artificial intelligence</span><span class="ch">人工智能</span>
date: <span class="en">Lunar New Year 2025</span><span class="ch">2025年農曆新春</span>
theme: solarized
header-includes: |
    <meta property="og:type" content="website" />
    <meta property="og:title" content="2022年新春禮物" />
    <meta property="og:image" content="../images/highlight-07.jpg" />
    <base target="_blank"/>
    <link rel="stylesheet" type="text/css" href="../css/slides.css"></link>
    <script type="text/javascript" src="../scripts/site.js"></script>
include-before: |
    <!-- Any raw HTML code right after body tag, before anything else. -->
    <div id="modal" class="modal">
      <img id="modal-content" />
      <div id="modal-caption"></div>
    </div>
    <div align="right" class="banner">
    <input type="radio" name="lang" onclick="set_lang('en')" id="english" value="English" checked /> English |
    <input type="radio" name="lang" onclick="set_lang('ch')" id="chinese" value="Chinese" /> 中文
    </div>
include-after: |
    <!-- Any raw HTML code right before body end tag, after anything else. -->
    <script>window.onload = check</script>
    <!-- file:///Users/josephchan/jc/q/site/slides/gift07.html -->
    <!-- https://jhlchan.bitbucket.io/slides/gift07.html -->
...

#

:::{.en}
<img src="../images/gift07_en.jpg" width="100%" />

- For the [2022 Lunar New Year](../2022/lny.html) "gift" the theme is AI = Artificial Intelligence, a hot topic of today.
- Can machine learn? Can machine think? Can machine be intelligent? These are the studies of AI.
- ChatGPT was launched in November 2022, so the page has been updated with new development.
- Before outlining the scope of AI and latest development, we look at the origin of AI, a bit of history.
- For that, the story goes back to two visionary people we met before: Ada Lovelace and Alan Turing.
:::

:::{.ch}
<img src="../images/gift07_ch.jpg" width="100%" />

- [2022年農曆新春](../2022/lny.html)「禮物」，主題是 AI = 人工智能（Artificial Intelligence），當今最熱門的話題。
- 機器可不可以學習？ 機器能不能思考？ 機器會不會變得智慧？全都是「人工智能」研究的範疇。
- 聊天機器人 ChatGPT 於 2022年11月推出，因此頁面已有更新，添加一些連結，針對最新發展。
- 在概述人工智能的範圍，以及最新發展之前，先探討人工智能的起源，回顧一段蠻有趣的歷史。
- 為此，故事追溯到之前談及的人物：Ada Lovelace 和 Alan Turing。兩位計算界前輩，甚有遠見。
:::

#

:::{.en}
<div class="imagecap" style="width:98%">
<img src="../turing/alan_turing_logo.jpg" style="width:46%"
     alt="Alan Turing worked out the theory of computing, touching on AI." onclick="showPopup(this)" />
<img src="../lovelace/ada_lovelace_logo.jpg" style="width:53%"
     alt="Ada Lovelace worked out the principles of computing, thinking about AI." onclick="showPopup(this)" />
</br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:98%">
<img src="../turing/alan_turing_logo.jpg" style="width:46%"
     alt="Alan Turing 提出計算理論，涉及人工智能。" onclick="showPopup(this)" />
<img src="../lovelace/ada_lovelace_logo.jpg" style="width:53%"
     alt="Ada Lovelace 研究計算原理，思考人工智能。" onclick="showPopup(this)" />
</br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- Both [Alan Turing](../turing/index.html) and [Ada Lovelace](../lovelace/index.html) have colorful stories, see [2021 Mid-Autumn](../2021/autumn.html) and [2021 Xmas](../2021/xmas.html).
- They are both pioneers in computing science, long before any computing machines become available.
- They differ in ideas about AI, whether computing machines can be intelligent, due to opposite views.
- Ada looks at hardware, maintains that machines only follow instructions, so there is no intelligence.
- Alan looks at software, argues that machines can learn to improve, so getting smart with intelligence.
- Ada saw the beauty of software, noted that computers can weave math patterns like Jacquard loom.
- Alan saw the power of software, noted that software can simulate hardware, making improvements. 
:::

:::{.ch}
- [Alan Turing](../turing/index.html) 和 [Ada Lovelace](../lovelace/index.html) 的故事，多姿多彩，請參閱「禮物」[2021年中秋節](../2021/autumn .html) 和 [2021年聖誕節](../2021/xmas.html)。
- 二人都是計算科學的先驅者，眼光獨到。早在任何計算機出現之前，已經清楚計算機背後的原理。
- 兩者對人工智能（AI）、亦即計算機器會否擁有智慧，持不同想法。觀點有異，因為著眼點不同。
- Ada 著眼於硬件。她認為，計算機器只是遵循指令，操作由軟件控制，因此機器沒有真正的智能。
- Alan 著眼於軟件。他認為計算程式，可以透過學習來提升其性能，越更新越聰明，從而取得智能。
- Ala 知道軟件之美妙，她指出：計算程式可以編織數學圖案，像提花織機（Jacquard loom）一樣。
- Alan 明白軟件的威力，他指出：軟件原則上可以模擬（simulate）任何硬件，改良模擬計算機器。
:::

#

:::{.en}
<div class="imagecap" style="width:98%">
<img src="../turing/turing_test_diagram.jpg" style="width:33%"
     alt="Arrangement for a Turing Test, to decide which respondent is intelligent." onclick="showPopup(this)" />
<img src="../turing/imitation_game_police.jpg" style="width:66%"
     alt="Alan Turing is interrogated by a detective in the movie 'The Imitation Game'." onclick="showPopup(this)" />
</br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:98%">
<img src="../turing/turing_test_diagram.jpg" style="width:33%"
     alt="圖靈測試（Turing Test）的安排，決定哪一位回答者有智能。" onclick="showPopup(this)" />
<img src="../turing/imitation_game_police.jpg" style="width:66%"
     alt="在電影《模仿遊戲》中，Alan Turing 被一名偵探審問。" onclick="showPopup(this)" />
</br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- Alan devised the Turing Test: if machine and human outputs are indistinguishable, both are intelligent.
- This cleverly avoids the definition of intelligence, and puts the focus solely on machine performance.
- In his seminal paper published in 1950, Alan Turing proposed this test and named it "Imitation Game".
- At the start of the movie "The Imitation Game", Alan played this game with the investigating detective.
- Detective: Can machines think? Alan offered himself for a test whether he is human or just a machine.
- Alan: All you have to do is ask me one question. Detective: What did you really do during the war?
:::

:::{.ch}
- Alan 設計「圖靈測試」（Turing Test）：若從輸出無法分辨是來自機器或人類，那麼兩者都有智能。
- 這個方法，巧妙地迴避了「智能」的定義，而將重點放在機器性能上。有沒有智能，只是判斷問題。
- Alan Turing 在 1950 年發表的開創性論文，提出這項測試，稱為「模仿遊戲」（Imitation Game）。
- 電影《模仿遊戲》甫開始，Alan 被揭發為同性戀者，遭調查偵探盤問。劇本安排他們玩這個遊戲。
- 偵探：機器能思考嗎？ Alan：機器思考方式與人不同。他主動提出，測試自己是正常人、機器人？
- Alan：你需要做的，是問我一個問題。偵探：戰爭期間，你到底在做什麼？故事情節，從此展開。
:::

#

:::{.en}
<div class="imagecap" style="width:98%">
<img src="../ai/ELIZA_conversation.png" style="width:49%"
     alt="A sample conversation with the program Eliza." onclick="showPopup(this)" />
<img src="../ai/chatgpt-features.jpg" style="width:50%"
     alt="ChatGPT original frontpage, explaining how it can be used." onclick="showPopup(this)" />
</br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:98%">
<img src="../ai/ELIZA_conversation.png" style="width:49%"
     alt="與 Eliza 程式對話的示例。" onclick="showPopup(this)" />
<img src="../ai/chatgpt-features.jpg" style="width:50%"
     alt="聊天機器人 ChatGPT 最初首頁，說明如何使用。" onclick="showPopup(this)" />
</br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- Chatbots are implementations of the Turing Test, although you are definitely talking to a machine.
- ELIZA is an early chatbot mimicking a psychotherapist, using pattern matching and substitutions.
- ChatGPT is a modern chatbot making use of transformer architecture and training on large datasets.
- Transformer architecture is based on deep learning models with selective focus on input elements.
- GPT = Generative Pre-trained Transformer, generating the most appropriate response from a prompt.
:::

:::{.ch}
- 聊天機器人（chatbot）是「圖靈測試」（Turing Test）的具體落實，儘管你肯定不是與人交談。
- ELIZA 是早期的聊天機器人，模仿心理治療師，使用模式匹配（pattern matching）和字句替換。
- ChatGPT 是現代聊天機器人，架構使用「變換模型」（Transformer），並利用大數據進行訓練。
- 「變換模型」架構，是一門「深度學習」（deep learning）模型，可選擇性地專注於輸入的字句。
- GPT = Generative Pre-trained Transformer，根據字句提示（prompt），分析後生成最貼切回應。
:::

#

:::{.en}
<div class="imagecap" style="width:98%">
<img src="../ai/John-Conway-Game-of-Life.jpg" style="width:54%"
     alt="'Game of Life' (on the left) is a math simulation puzzle invented by John Horton Conway (on the right)." onclick="showPopup(this)" />
<img src="../ai/Gospers_glider_gun.gif" style="width:42%"
     alt="A glider gun in John Conway's Game of Life." onclick="showPopup(this)" />
</br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:98%">
<img src="../ai/John-Conway-Game-of-Life.jpg" style="width:54%"
     alt="「生命遊戲」（左）是數學模擬謎題，由 John Horton Conway（右）發明。" onclick="showPopup(this)" />
<img src="../ai/Gospers_glider_gun.gif" style="width:42%"
     alt="John Conway 生命遊戲中的滑翔機槍（glider gun）。" onclick="showPopup(this)" />
</br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- Can simple rules lead to complex behaviour? John Conway's Game of Life showed this is possible.
- Only 3 simple rules (left) for the Game of Life, played on a 2D grid of cells either white or black.
- The complex pattern (right) on the 2D grid is due to applying the simple rules, over and over again.
- By varying the rules, Game of Life motivated studies in cellular automata and simulation modeling.
- Neural network in the human brain fires by simple rules, somehow giving rise to our intelligence!
:::

:::{.ch}
- 簡單的規則，能導致複雜的表現嗎？John Conway 的「生命遊戲」（Game of Life）表明有可能。
- 「生命遊戲」只有3個簡單規則（左），在二維方格陣上進行。方格非黑即白，代表生與死。
- 二維方格陣上，產生複雜的圖案（右），只是由於應用簡單規則，一遍又一遍，不停地重複。
- 透過改變其中規則，「生命遊戲」促進元胞自動機（cellular automata）和模擬模型的研究。
- 人腦中的神經網路（neural network），依照簡單的規則激發。不知何故，形成我們的智力！
:::

#

:::{.en}
<div class="imagecap" style="width:98%">
<img src="../ai/chess-and-go.jpg" style="width:43%"
     alt="Chess and Go are favourite board games to check progress in AI." onclick="showPopup(this)" />
<img src="../ai/AI_vs_ML_vs_DL.jpg" style="width:53.8%"
     alt="Modern AI systems incorporate Machine Learning (ML), the latest ones take Deep Learning (DL)." onclick="showPopup(this)" />
</br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:98%">
<img src="../ai/chess-and-go.jpg" style="width:43%"
     alt="測試人工智慧進展的棋盤遊戲，西洋棋和圍棋是最常應用例子。" onclick="showPopup(this)" />
<img src="../ai/AI_vs_ML_vs_DL.jpg" style="width:53.8%"
     alt="現代人工智慧系統，融入機器學習（ML）。最新的系統，採用深度學習（DL）。" onclick="showPopup(this)" />
</br>
<span>點擊放大(再擊關閉)</span>
</div>
:::


:::{.en}
- Early AI research focused on playing games against humans and machines, especially board games.
- The idea was: if a machine can learn to improve strategy and win over human, the machine was smart.
- ML = Machine Learning uses models with parameters and learn by examples to adjust parameters.
- DL = Deep Learning uses artificial neural networks to self-adjust parameters, fine tuning the model.
- Mathematical logic, in addition to analyzing game rules, can also analyze steps in long math proofs.
:::

:::{.ch}
- 早期的人工智能研究，專注於人機對壘，互相玩遊戲。尤其是棋盤遊戲，落子位置不可能一一嘗試。
- 想法是：如果機器能夠通過學習，改良對策，並最後戰勝人類，那麼機器就是十分聰明，具備智能。
- ML = 機器學習（Machine Learning），建立模型，功能取決於參數，並透過範例學習，調整參數。
- DL = 深度學習（Deep Learning），利用「人工神經網絡」，自行調節參數，模型不斷微調、改進。
- 數學邏輯，除用作分析遊戲規則外，亦可分析數學證明。檢查冗長推理步驟，是否全遵守邏輯法規。
:::

#

:::{.small}
AlphaGo - The Movie | Full Documentary【1:30:28】cc<br>
<a href="https://youtu.be/WXuK6gekU1Y">
<img src="http://img.youtube.com/vi/WXuK6gekU1Y/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- The documentary is about a match between the Go champion Lee Sedol and the computer AlphaGo.
- Lee Sedol（李世乭）from Korea is a Go grandmaster of 9 dan, the highest professional rank of Go.
- AlphaGo is a computer program trained by AI deep learning, developed by DeepMind from Google.
- After losing 3 games in a row, Lee came back to defeat AlphaGo with a "god move" (Game 4, Move 78).
- In Game Two, the Google machine made a move that no human ever would try. And it was beautiful.
- In Game Four, the human made a move that no machine would ever expect. And it was beautiful too.
:::

:::{.ch}
- 這齣紀錄片，講述 2016年三月一場公開比賽，由圍棋冠軍李世乭，對陣電腦程式 AlphaGo。 
- 來自韓國的李世乭（Lee Sedol），是一位九段圍棋特級大師，「九段」是圍棋最高職業級別。
- AlphaGo 是由 Google 的 DeepMind 開發，是人工智能電腦程式，曾以「深度學習」方式訓練。
- 在連輸3局後，李世乭以「神棋」（第4局78手）逆轉，擊敗 AlphaGo，追回一局，大為振奮。
- 第二場比賽中，Google 機器做出了人類從未試過的一步，落子很美麗。李世乭登時目瞪口呆。
- 第四場比賽中，人類作出了任何機器都意想不到的一步，這子也很漂亮。AlphaGo 一頭霧水。
:::

<!--
AlphaGo - The Movie | Full Documentary【1:30:28】cc
https://www.youtube.com/watch?v=WXuK6gekU1Y
Documentary comes from the AlphaGo site (https://www.alphagomovie.com/).
The movie shows how researchers apply deep learning, a iterative form of machine learning, to develop a go-playing program.
-->

#

:::{.small}
How AI Solved Protein Folding and Won a Nobel Prize【22:20】cc<br>
<a href="https://youtu.be/cx7l9ZGFZkw">
<img src="http://img.youtube.com/vi/cx7l9ZGFZkw/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- After AlphaGo, DeepMind developed AlphaFold to take on a challenge in biochemistry.
- Proteins are biomolecules with long chains of amino acids, with shapes for their functions.
- AlphaFold tackles the protein folding problem: how its long chain folds into a 3D shape.
- AlphaFold has partial success, but AlphaFold2 with transformer delivers a breakthrough.
- This video by [Quanta Magazine](https://www.quantamagazine.org/how-ai-revolutionized-protein-science-but-didnt-end-it-20240626/) tells the inside story of how AI cracked this hard problem.
- This spectacular achievement won the AI researchers the 2024 Nobel Prize in Chemistry.
:::

:::{.ch}
- 繼 AlphaGo 之後，DeepMind 開發 AlphaFold，應對來自生物化學領域的挑戰。
- 蛋白質是生物分子，由長鏈胺基酸單元組成。其相關功能，取決於蛋白質形狀。
- AlphaFold 的任務，是解決「蛋白質摺疊問題」：其長鏈如何摺疊成 3D 形狀。
- AlphaFold 取得部分成果，但建基於變換模型的 AlphaFold2 ，成功實現突破。
- 視頻由 [Quanta Magazine](https://www.quantamagazine.org/how-ai-revolutionized-protein-science-but-didnt-end-it-20240626/) 製作，講述其中內幕故事： AI 如何破解這一道難題。
- 這項成就，令人鼓舞，為 DeepMind 的研究人員，贏得 2024年諾貝爾化學獎。
:::

<!--
[1] How AI Solved Protein Folding and Won a Nobel Prize【22:20】cc
https://youtu.be/cx7l9ZGFZkw
The inside story of how AI cracked the protein folding code, produced by Quanta Magazine (see article below).

[2] How AI Revolutionized Protein Science, but Didn’t End It
by Yasemin Saplakoglu, 26 June 2024.
https://www.quantamagazine.org/how-ai-revolutionized-protein-science-but-didnt-end-it-20240626/
This article was published before the Nobel prize announcement, with infographs explaining some fine points.
-->

#

:::{.en}
<div class="imagecap" style="width:100%">
<img src="../ai/the-truth-about-ai.jpg"
     alt="A poster for the 2023 Christmas Lectures." onclick="showPopup(this)" /></br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:100%">
<img src="../ai/the-truth-about-ai.jpg"
     alt="2023年聖誕節講座的海報。" onclick="showPopup(this)" /></br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- The Royal Institution (RI) in London, following a long tradition, delivers Christmas lectures every year.
- For Christmas 2023, there are three lectures, all talk about the same theme "The Truth about AI".
- Each lecture is about an hour, and the audience, mostly school kids, is excited, never snoozing off💤!
- The lectures are well-prepared, with various speakers and demos, to keep everyone in rapt attention.
- Flip through my slides in [Christmas Lectures 2023](../slides/rixmas.html) for the background and links to the lectures.
:::

:::{.ch}
- 倫敦皇家學院 (RI = Royal Institution) 沿循悠久的傳統，每年都會舉辦「聖誕講座」。
- 2023年的聖誕節，RI 準備了共三場講座，全部環繞同一主題：「人工智能的真相」。
- 每場講座，大約一小時，聽眾大多是中、小學生，他們都很興奮，一刻也不打瞌睡💤！
- 每場講座，準備充足，有不同講者和各種示範，提供觀眾參與，讓每個人都全神貫注。
- 請翻閱我在 [2023年聖誕節講座](../slides/rixmas.html) 中的投影片，了解聖誕講座背景，尋找各場講座連結。
:::


#

:::{.small}
Venus - The Windmills Of Your Mind【3:52】<br>
<a href="https://youtu.be/OCNIrP3UHz4">
<img src="http://img.youtube.com/vi/OCNIrP3UHz4/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- Perhaps the ultimate AI test is to play mind games, as in the movie "The Thomas Crown Affair".
- The lead characters knew each other's intention, still wanted to outwit and intrigue one another.
- The theme song from the movie is ["The Windmills Of Your Mind"](../songs/WindmillsOfYourMind.html), telling nothing about the storyline!
- The lyrics begins with:
-
| _Round, Like a circle in a spiral,_
| _Like a wheel within a wheel._
| _Never ending or beginning,_
| _On an ever spinning reel._

:::

:::{.ch}
- 也許，最終的人工智能測試，是玩心理遊戲，就像電影《龍鳳鬥智》中那樣。
- 電影中男女主角，明明知道對方的意圖，但仍然想智取對方，互相勾心鬥角。
- 電影主題曲，是[《The Windmills Of Your Mind》](../songs/WindmillsOfYourMind.html)，內容不涉及電影故事情節！
- 英文歌詞開首是：
-
| _[圓形完美，像螺旋在轉，] Round, Like a circle in a spiral,_
| _[似輪子內，再套上輪子。] Like a wheel within a wheel._
| _[在捲軸上，不斷的旋轉，] Never ending or beginning,_ 
| _[永無止境，也永無起始。] On an ever spinning reel._

:::

<!--
2025/lny
https://jhlchan.bitbucket.io/2022/lny.html
(video?: Windmills of Your Mind)
2022年農曆新春「禮物」，主題是 AI = 人工智能。前兩輯的主角 Ada Lovelace 與 Alan Turing，對 AI 有不同想法，亦代表現今我們對 AI 不同的取向。早期 AI 注重人機對壘玩遊戲，期望機器取勝，以證明機器聰明。數學邏輯，除分析遊戲規則外，亦可分析數學證明。紀錄片是圍棋冠軍人機對陣，附上一首電影《龍鳳鬥智》主題曲。

José Feliciano - The Windmills Of Your Mind【4:11】Live b/w
https://www.youtube.com/watch?v=0TENBIrwOHU
https://youtu.be/
or
Sinne Eeg - The Windmills Of Your Mind【6:06】Live color (long)
https://www.youtube.com/watch?v=67gHlsWiKp0
https://youtu.be/
or
Windmills of Your Mind - Alison Moyet【4:03】Live color (short)
https://www.youtube.com/watch?v=3SJpppdi_ik
https://youtu.be/
or
Venus - The Windmills Of Your Mind (cover)【3:52】piano
https://www.youtube.com/watch?v=OCNIrP3UHz4
https://youtu.be/
-->

#

:::{.small}
Windmills of the Minds: An Algorithm for Fermat's Two Squares Theorem【15:14】cc<br>
<a href="https://youtu.be/ETL3bE3kDfA">
<img src="http://img.youtube.com/vi/ETL3bE3kDfA/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- Around that time, I have a conference paper with main title "Windmills of the Minds".
- The paper is about a wonderful theorem for primes of the form $4k + 1$, e.g. $5, 13, 17, \dots$
- I explained an appealing proof: using windmill diagrams to represent such numbers.
- The proof is checked by an interactive theorem prover, an application of AI to math.
- I guide a computer program to follow the chain of logical deductions in the entire proof.
- See my [slideshow](windmill.html) for a simple and entertaining introduction, describing the algorithms.
:::

:::{.ch}
- 當時，正值我有一篇數理文章，在研討會上發表，主標題是「Windmills of the Minds」。
- 這篇文章，說明一個精采定理，是關於形如 $4k + 1$ 的素數，例如 $5, 13, 17, 29, 37, 41, \dots$
- 我採用「風車」 (windmill) 圖形，勾畫出這些數字，從以解釋定理證明的內容，引人入勝。
- 證明由「互動式定理證明器」（interactive theorem prover）檢查，這是在數學上應用 AI。
- 證明中的邏輯推論，串串連連。我引導此電腦程式，進行全面追蹤，過程有點似偵探故事。
- 請參閱我的[投影片](windmill.html)，以簡單而有趣的手法，介紹其中演算法。其後，更發表演算法改良版。
:::

<!--
Windmills of the Minds: An Algorithm for Fermat's Two Squares Theorem【15:14】cc
https://www.youtube.com/watch?v=ETL3bE3kDfA
Talk at CPP2022, based on these slides: books/windmills_slides.pdf
-->

<!--
pandoc -t revealjs -s --mathjax gift07.md -o gift07.html
-->
