---
title: Easter 2022 Highlights
author: <span class="en">math truth</span><span class="ch">數學真理</span>
date: <span class="en">Easter 2025</span><span class="ch">2025年復活節</span>
theme: solarized
header-includes: |
    <meta property="og:type" content="website" />
    <meta property="og:title" content="2022年復活禮物" />
    <meta property="og:image" content="../images/highlight-08.jpg" />
    <base target="_blank"/>
    <link rel="stylesheet" type="text/css" href="../css/slides.css"></link>
    <script type="text/javascript" src="../scripts/site.js"></script>
include-before: |
    <!-- Any raw HTML code right after body tag, before anything else. -->
    <div id="modal" class="modal">
      <img id="modal-content" />
      <div id="modal-caption"></div>
    </div>
    <div align="right" class="banner">
    <input type="radio" name="lang" onclick="set_lang('en')" id="english" value="English" checked /> English |
    <input type="radio" name="lang" onclick="set_lang('ch')" id="chinese" value="Chinese" /> 中文
    </div>
include-after: |
    <!-- Any raw HTML code right before body end tag, after anything else. -->
    <script>window.onload = check</script>
    <!-- file:///Users/josephchan/jc/q/site/slides/gift08.html -->
    <!-- https://jhlchan.bitbucket.io/slides/gift08.html -->
...

#

:::{.en}
<img src="../images/gift08_en.jpg" width="100%" />

- For the [2022 Easter](../2022/easter.html) "gift" the theme is mathematical logic, to seek the truth in math.
- To appreciate this, we need to know what is math, and how math truth are derived.
- Mathematics studies include numbers in arithmetic and algebra, and shapes in geometry.
- Mathematics consists of formula and theorems, some are obvious but some are unexpected.
- For numbers in math, refer to [2021 Easter](../2021/easter.html) for the story of the Man who knew Infinity.
- For shapes in math, refer to [2021 Dragon Boat](../2021/dragon.html) for looking at geometry through origami.
:::

:::{.ch}
<img src="../images/gift08_ch.jpg" width="100%" />

- [2022年復活節](../2022/easter.html)「禮物」，主題是數學邏輯，追求數學真理。
- 看透其中道理，要知道數學是什麼，以及如何導出數學真理。
- 數學研究，包括算術（arithmetic）與代數（algebra）中的數字，和幾何（geometry）中的形狀。
- 數學真理，由公式和定理組成，有些是顯而易見，但有些是意想不到。
- 數學中的數字，請參閱[2021年復活節](../2021/easter.html) ，了解「天才無限家」的故事。
- 數學中的形狀，請參閱[2021年端午節](../2021/dragon.html)，透過「摺紙」（origami）觀察幾何特性。
:::

#

:::{.en}
<div class="imagecap" style="width:48%">
<img src="../images/math-landscape.jpg"
     alt="A bird's eye view of the landscape of mathematics." onclick="showPopup(this)" /></br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:48%">
<img src="../images/math-landscape.jpg"
     alt="形象化的數學江山鳥瞰圖。" onclick="showPopup(this)" /></br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- Arithmetic is the study of numbers, extending to algebra and stretching to number theory.
- Geometry is the study of shapes, rigid for working with coordinates but deformable in topology.
- The landscape of math is vast, with arithmetic and geometry marking out two main divisions.
- Math is about truths, expressed by formula and theorems, established by providing proofs.
- Steps in a proof are chains of deduction by simple logic, the source in the math landscape. 
:::

:::{.ch}
- 算術，是對數字的研究。代入符號，延伸到代數，並演變到數論（number theory）。
- 幾何，是對形狀的研究。處理形狀，放入座標成為剛性，但放入拓樸思維，形狀可變。
- 數學領域廣闊，影響深遠。算術和幾何，劃分為兩個主要部分。
- 數學強調真理，透過公式和定理來表達，通過提供證明來建立。
- 證明中的步驟，是簡單邏輯的連串推論，這是數學領域的泉源。
:::

#

:::{.en}
<div class="imagecap" style="width:98%">
<img src="../logic/asking-questions.jpg" style="width:44%"
     alt="A math proof is to pursue the root of the truth." onclick="showPopup(this)" />
<img src="../logic/Pythagoras-theorem.gif" style="width:54%"
     alt="An animation proof of the Pythagoras theorem." onclick="showPopup(this)" />
</br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:98%">
<img src="../logic/asking-questions.jpg" style="width:44%"
     alt="數學證明，是追尋真理的根源。" onclick="showPopup(this)" />
<img src="../logic/Pythagoras-theorem.gif" style="width:54%"
     alt="Pythagoras 定理的動畫證明。" onclick="showPopup(this)" />
</br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- Mathematical truth is black and white: either right or wrong. To know which one, a proof is required.
- How to prove? Result is true because $\dots$ due to $\dots$ &nbsp;One must insist on getting to the bottom of math.
- The bottom truths of math are called __axioms__, self-evident truths to be accepted without question.
- A right-angled triangle with sides $a, b, c$ where $c$ the longest will satisfy this formula: $a^{2} + b^{2} = c^{2}$.
- Why is this true? See visual proof 👆 if you take as axiom that shapes are rigid when moving around.
- Slide $4$ right-angled triangles $a$-$b$-$c$ around and equate exposed areas: left $a^{2} + b^{2} = c^{2}$ on the right.
:::

:::{.ch}
- 數學真理黑白分明：非對即錯。要知對錯，全靠證明。進行證明，以前靠人腦，現引入電腦幫忙。
- 如何證明？結果成立，是因為「乜乜乜」、由於「物物物」$\dots$ &nbsp;即是：打爛沙盆璺到㞘﹙問到篤﹚。
- 數學的基本真理，稱為 __公理__ = 公認的道理，冇得再追問「點解」。一切數學證明，皆從公理開始。
- 任何直角三角形，設各邊長為 $a、b、c$，其中 $c$ 最長，一定滿足此公式：$a^{2} + b^{2} = c^{2}$。
- 為什麼這是真確？請參閱動畫證明👆，其中應用公理：物件在移動時，保持剛性，形狀不變。
- 平滑移動 $4$ 個直角三角形 $a$-$b$-$c$ ，並考慮露出的正方形，總面積應該相等：左邊 $a^{2} + b^{2} = c^{2}$ 右邊。
:::

<!--

#

![](../logic/Platonic-Solids.jpg){ width=55% }
![](../logic/asking-questions.jpg){ width=44% }

:::{.en}
- Mathematical truth is black and white: either right or wrong. To know which one, a proof is required.
- How to prove? The result is because $\dots$, due to $\dots$ &nbsp;One must insist on getting to the bottom of math.
- The bottom truths of math are called axioms = self-evident truths accepted without question.
- For example, there are only five regular polyhedra, convex solids with all corners and edges equal.
- Why only five such solids? Around 300 BC Euclid proved this result, starting from a few axioms.
- (This one needs another slideshow talking about Platonic solids!)
:::

:::{.ch}
- 數學真理黑白分明：非對即錯。要知對錯，全靠證明。
- 如何證明？打爛沙盆璺到㞘﹙問到篤﹚。
- 數學的「篤」，稱為公理 = 公認的道理，冇得再追問。
- 例如，只有五個正多面體、即所有角和邊都相等的凸立體。
- 為什麼這樣的立體，只有五個？ 西元前300年左右，Euclid 給出證明，從幾條公理出發。
:::

-->

<!--
The four regular non-convex polyhedra
https://cage.ugent.be/~hs/polyhedra/keplerpoinsot.html
Small Stellated Dodecahedron    
Great Stellated Dodecahedron    
Great Dodecahedron  
Great Icosahedron
It is known that the five Platonic polyhedra are the only regular convex polyhedra. A polyhedron, considered as a solid is convex if and only if the line segment between any two points of the polyhedron belongs entirely to the solid. However, if we admit a polyhedron to be non-convex, there exist four more types of regular polyhedra!
The four regular non-convex polyhedra are known as the Kepler-Poinsot Polyhedra.
In 1810 the French mathematician Augustin-Louis Cauchy proved that the five Platonic and the four Kepler-Poinsot polyhedra are the only possible regular polyhedra.

Platonic Solids - Why Five?
https://www.mathsisfun.com/geometry/platonic-solids-why-five.html
Simplest Reason: Angles at a Vertex
Another Reason (using Topology) Euler's Formula
-->

#

:::{.en}
<div class="imagecap" style="width:64%">
<img src="../logic/Platonic-Solids.jpg"
     alt="The only five regular polyhedra, called Platonic solids." onclick="showPopup(this)" /></br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:64%">
<img src="../logic/Platonic-Solids.jpg"
     alt="正多面體只有五個，稱為「柏拉圖立體」。" onclick="showPopup(this)" /></br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- In math, a collection of things putting together is called a __set__. For example: $\mathbb{N} = \{0, 1, 2, 3, \dots\}$.
- Things in a set are called __elements__, or __members__. For example: $1\in\mathbb{N}$, where $\in$ means _membership_.
- The set of regular convex polyhedra, 3D symmetric beauties 👆 (Platonic solids), has only $5$ elements.
- The set of counting numbers $\mathbb{N}$ has two subsets, the odd numbers and the even numbers.
- In symbols, the subsets are: $odd = \{1, 3, 5, 7, 9, 11, 13, \dots \}, even = \{0, 2, 4, 6, 8, 10, 12, \dots \}$.
- The _empty set_, a set without elements, has a special symbol $\phi = \{\}$, e.g. set of odd and even $= \phi$.
:::

:::{.ch}
- 數學中，一堆東西構成的整體，稱為 __集__ 或 __集合__（set）。例如：$\mathbb{N} = \{0, 1, 2, 3, \dots\}$。
- 集合中的東西，稱為 __元素__ 或 __成員__。 例如：$1\in\mathbb{N}$，其中$\in$ 表示 _是否成員_ 。
- 凸面的正多面體，三維兼對稱又漂亮 👆（稱為「柏拉圖立體」），組成的集合只有 $5$ 個元素。
- 集合 $\mathbb{N}$ 包含所有計數的數字，有兩個子集（subset）：奇數和偶數。
- 使用符號，兩個子集為：$奇數 = \{1, 3, 5, 7, 9, 11, 13, \dots \}，偶數 = \{0, 2, 4, 6, 8, 10, 12 , \dots \}$。
- 沒有元素的集合是 _空集_（empty set），有特殊符號 $\phi = \{\}$，例如：數字亦奇亦偶的集合 $= \phi$。
:::

<!--
- Using (x,y) as coordinates, plane geometry is reduced to algebra (good?)
- Decartes watched a fly buzzing in his room, and imagined (x,y,z) coordinates?
- By reducing geometry to algebra (!), and algebra to numbers (!), how to build math?
- Using sets, the set of Platonic solids is finite.
-->

#

:::{.en}
<div class="imagecap" style="width:98%">
<img src="../logic/Georg-Cantor-1870.jpg" style="width:22%"
     alt="Georg Cantor founded set theory, capable to describe all math." onclick="showPopup(this)" />
<img src="../logic/Russell-paradox.jpg" style="width:72%"
     alt="Bertrand Russell found a paradox in set theory, and worked hard to fix this." onclick="showPopup(this)" />
</br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:98%">
<img src="../logic/Georg-Cantor-1870.jpg" style="width:22%"
     alt="Georg Cantor 創立「集合論」，能夠描述所有數學。" onclick="showPopup(this)" />
<img src="../logic/Russell-paradox.jpg" style="width:72%"
     alt="Bertrand Russell 發現「集合論」中的悖論，並努力解決此問題。" onclick="showPopup(this)" />
</br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- To find the ultimate math axioms, all math have to be built from the same thing.
- Mathematicians tried to build up all math from sets, using a theory of sets by Cantor.
- Russell uncovered a paradox in set theory, leading to the first crisis in foundation.
- With help from Whitehead, Russell fixed axioms in set theory and build up elementary math.
- After a lot of pages of deduction from set theory axioms, they proved: $1 + 1 = 2$! 
:::

:::{.ch}
- 數學的終極公理，如何追尋？首先，所有數學，必須建基於同一類東西。
- 數學家們，試圖從集合開始，建立全部數學，早前 Cantor 創立「集合論」（set theory）。
- Russell 發現，「集合論」蘊藏著一個悖論（paradox），這導致數學基礎的第一次危機。
- 憑藉 Whitehead 的幫助，Russell 修正集合論的公理，加以改良，並逐步建立初等數學。
- 他們兩人著書，利用集合論公理作大量推算。經過許多頁後，終於證明了：$1 + 1 = 2$！
:::

#

:::{.en}
<div class="imagecap" style="width:98%">
<img src="../logic/David-Hilbert-1912.jpg" style="width:31.5%"
     alt="David Hilbert believed that everything in math can be proved." onclick="showPopup(this)" />
<img src="../logic/Godel-incompleteness-theorem.jpg" style="width:64%"
     alt="Kurt Gödel showed that something true in math but cannot be proved." onclick="showPopup(this)" />
</br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:98%">
<img src="../logic/David-Hilbert-1912.jpg" style="width:31.5%"
     alt="David Hilbert 相信，數學中的一切，都會被證明。" onclick="showPopup(this)" />
<img src="../logic/Godel-incompleteness-theorem.jpg" style="width:64%"
     alt="Kurt Gödel 證明，數學中存在某些真確的句子，但無法證明。" onclick="showPopup(this)" />
</br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- David Hilbert was the foremost mathematician of the modern era, active around 1900.
- He praised the efforts of Russel and Whitehead, building math from logic.
- He dreamed that their method will show math is complete and consistent.
- Math is complete means all statements will be proved true or false.
- Math is consistent means no statement will be proved true and false.
- Using logic, Kurt Gödel dashed this dream, leading to the second crisis in foundation. 
:::

:::{.ch}
- David Hilbert 是近代第一流的數學家，活躍於 1900年前後。數學界對他推崇備至，敬佩非常。
- 他的想法，主張從邏輯構建數學。因此高度讚揚，Russel 和 Whitehead 在方面所作的努力。
- 他的夢想，是藉著邏輯推理方法，證明數學本身，是完整的、並且是一致的。
- 數學是完整的（complete），意味著所有數學陳述，最終會有證明，是對或是錯。
- 數學是一致的（consistent），意味著任何數學陳述，不會出現證明，是對亦是錯。
- 同樣利用邏輯推理，Kurt Gödel 證明只能二選其一，令夢想幻滅，這導致數學基礎的第二次危機。
:::

#

:::{.en}
<div class="imagecap" style="width:32%">
<img src="../logic/Logicomix.jpg"
     alt="Logicomix is a comic book about the ultimate truth in math." onclick="showPopup(this)" /></br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:32%">
<img src="../logic/Logicomix.jpg"
     alt="Logicomix 是一本關於數學終極真理的漫畫書。" onclick="showPopup(this)" /></br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- Mathematicians search for the ultimate truth, but in vain! The amazing story is full of twists and turns.
- This pursue of math truths is depicted in a well-crafted colorful graphic novel [Logicomix](../logic/Logicomix.pdf) (332 pages).
- It starts by telling its own story, why such a novel is conceived, and how the math story is developed.
- In the story, Russell recounts his own story, from childhood to meeting math people and falling in love.
- On the positive side, new proof ideas need computers + AI to check logic, leading to theorem proving.
- Believe it or not, this comic book is on the recommended list in a Theorem Proving course at ANU!
:::

:::{.ch}
- 數學家尋找終極真理，但徒勞無功，到頭來得個吉！其中故事，曲折離奇，峰迴路轉，出人意表。
- [Logicomix《邏輯漫畫》](../logic/Logicomix.pdf)（332頁）是一本彩色圖畫小說，精心製作，描述對數學真理的不斷追求。
- 在小說中，首先講述自身的故事，為什麼要構思這本圖書，以及圖書中的數學故事，是如何發展。
- 在故事中，Russell 講述自己的故事，從童年到成年到老年，交遊廣闊，結識數學家，並墜入愛河。
- 積極來看，數學證明，日新月異。證明中複雜的邏輯，利用電腦+AI幫手，演成「定理證明」學科。
- 信不信由你，在澳洲國立大學（ANU）「定理證明」課程的閱讀推薦清單上，出現這本漫畫圖書！
:::

#

:::{.small}
Making of LOGICOMIX (Greek Subtitles)【21:07】cc (English)<br>
<a href="https://youtu.be/gLFeKTUebgs">
<img src="http://img.youtube.com/vi/gLFeKTUebgs/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- This documentary was filmed over five years during the making of the graphic novel LOGICOMIX.
- You can see various stages of the production, from conception, research, script, to story-telling.
- There is also scriptwriter-artist collaboration, various stages of sketching , the ink, the coloring.
- The creators had advice and comments from a mathematician of Harvard University, Barry Mazur.
- Such efforts were devoted to the creation of such a fantastic graphic novel about math and logic.
:::

:::{.ch}
- 紀錄片講述漫畫小說《LOGICOMIX》 的製作過程，歷時五年拍攝。
- 可以看到製作過程的各個階段，從構思、研究、劇本到故事劇情。
- 還有編劇和藝術家之間的合作，包括素描、著墨、著色各個階段。
- 哈佛大學的數學家 Barry Mazur，為小說創作者們，給予建議和評論。
- 各人齊心合力，費盡心思，致力製作一本精彩的圖畫小說，關於數學與邏輯！
:::

<!--
【M1】Making of LOGICOMIX (Greek Subtitles)【21:07】cc (English)
https://www.youtube.com/watch?v=gLFeKTUebgs
-->

#

:::{.small}
【LIVE】關正傑 東方之珠 1986【3:34】<br>
<a href="https://youtu.be/SEERkCdRIes">
<img src="http://img.youtube.com/vi/SEERkCdRIes/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- During Easter 2022, Hong Kong people was expecting a change by electing a new chief executive.
- Easter brings expectations, so I offer a Chinese song looking back on the better times of Hong Kong.
- [Pearl of the Orient《東方之珠》](../songs/PearlOfTheOrient.html) (with many versions), full of spirit by Michael Kwan, has these lyrics:
-
| _東方之珠，誰也讚羨，[Pearl of the Orient, praised and admired by everyone,]_
| _猶似加上美麗璀璨的冠冕。[Like wearing a beautiful and dazzling crown under the sun.]_

- Hong Kong is still glittering with gold, full of opportunities, nostalgic in our hearts. Stay calm!
:::

:::{.ch}
- 2022年復活節期間，正值香港籌備新一屆行政長官選舉。香港人萬眾期待，誰將會登上寶座。
- 復活節帶來期待，行政首長更替，正好緬懷香港歷盡滄桑。送上一首中文歌，回顧香江盛世。
- [《東方之珠》](../songs/PearlOfTheOrient.html)（有多個版本）。關正𠍇主唱的一首，沉實穩重，氣概十足，發揮正能量。歌詞有：
-
| _東方之珠，誰也讚羨，_
| _猶似加上美麗璀璨的冠冕。_

- 香江香港，仍是金光燦爛，充滿機遇，令人懷念。希望香港人能沉得住氣，挑戰未來！
:::

#

:::{.small}
Bette Midler - From a Distance (Official Video) (1990)【4:35】<br>
<a href="https://youtu.be/BBLo3vO0XuU">
<img src="http://img.youtube.com/vi/BBLo3vO0XuU/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- When writing this, the Russian invasion of Ukraine had barely begun. Now conflict in Gaza continues. 
- Easter brings hope, everyone hopes for a quick end to the war. I offer a song that looks at world peace.
- Believers pray that God will save the world, but where is God? Listen to the lyrics in [From a Distance](../songs/FromADistance.html):
-
| _From a Distance, You look like my friend,_
| _Even though we are at war._
| _From a Distance, I just cannot comprehend._
| _What all this fighting's for._

- The songwriter wrote score and lyrics, but insisted she didn't write it: God puts the song in her head.
:::

:::{.ch}
- 當年執筆時，俄烏戰事已經持續兩個月，世人皆心痛。今時今日，加沙衝突持續，世人更心痛。
- 復活節帶出希望，全世界都盼望戰爭早日完結，送上一首英文歌，放眼天際，但願世界和平。
- 宗教信徒，祈求天主拯救世人，但天主在何方？耐心聽聽 [《From a Distance》](../songs/FromADistance.html)，英文歌詞有：
-
| _[遙遙遠望，你我似友善和諧，] From a Distance, You look like my friend,_
| _[實情卻是，我活你死得更多。] Even though we are at war._
| _[遙遙遠看，我百思不得其解，] From a Distance, I just cannot comprehend._
| _[互相惡鬥，到底是爭奪什麼。] What all this fighting's for._

- 歌曲作者編譜兼填詞，但她堅稱，作品不是她寫的：是上帝把這首歌，植入她的腦海裡。 
:::


<!--
2025/easter
https://jhlchan.bitbucket.io/2022/easter.html
(video?: 東方之珠)
2022年復活節「禮物」，主題是數學邏輯，追求數學真理。數學真理黑白分明：非對即錯。要知對錯，全靠證明。如何證明？打爛沙盆璺到㞘﹙問到篤﹚。數學的「篤」，稱為公理 = 公認的道理，冇得再追問。數學家尋找終極公理，到頭來得個吉！其中故事，峰迴路轉。送上追求數學真理的漫畫小說，有小說製作視頻。歌曲有兩首：中文歌回顧香江盛世，英文歌放眼世界和平。

Russian invasion of Ukraine  24 February 2022
HONG KONG, February 2022 (Reuters): Hong Kong will roll out compulsory testing for COVID-19 starting in mid-March for its 7.4 million residents.
18 Feb 2022 — The election of Hong Kong's chief executive, scheduled to be held in March, will be postponed to May ...
Easter Sunday, 17 April 2022.

【LIVE】關正傑 東方之珠 1986【3:34】
https://www.youtube.com/watch?v=SEERkCdRIes
and?
Bette Midler – From a Distance (1991) HQ Audio【4:52】
https://www.youtube.com/watch?v=tlWrcejjd3g
Bette Midler - From a Distance (Official Video) (1990)【4:35】
https://www.youtube.com/watch?v=BBLo3vO0XuU
-->


<!--
pandoc -t revealjs -s --mathjax gift08.md -o gift08.html
-->
