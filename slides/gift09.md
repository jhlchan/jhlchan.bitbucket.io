---
title: Dragon Boat 2022 Highlights
author: <span class="en">animation</span><span class="ch">動畫</span>
date: <span class="en">Dragon Boat 2025</span><span class="ch">2025年端午節</span>
theme: solarized
header-includes: |
    <meta property="og:type" content="website" />
    <meta property="og:title" content="2022年端午禮物" />
    <meta property="og:image" content="../images/highlight-09.jpg" />
    <base target="_blank"/>
    <link rel="stylesheet" type="text/css" href="../css/slides.css"></link>
    <script type="text/javascript" src="../scripts/site.js"></script>
include-before: |
    <!-- Any raw HTML code right after body tag, before anything else. -->
    <div id="modal" class="modal">
      <img id="modal-content" />
      <div id="modal-caption"></div>
    </div>
    <div align="right" class="banner">
    <input type="radio" name="lang" onclick="set_lang('en')" id="english" value="English" checked /> English |
    <input type="radio" name="lang" onclick="set_lang('ch')" id="chinese" value="Chinese" /> 中文
    </div>
include-after: |
    <!-- Any raw HTML code right before body end tag, after anything else. -->
    <script>window.onload = check</script>
    <!-- file:///Users/josephchan/jc/q/site/slides/gift09.html -->
    <!-- https://jhlchan.bitbucket.io/slides/gift09.html -->
...

#

:::{.en}
<img src="../images/gift09_en.jpg" width="100%" />

- For the [2022 Dragon Boat Festival](../2022/dragon.html) "gift" the theme is animation, making picture alive.
- Animation is to create a movie by drawing each scene, frame by frame (break up).
- The frames are organised into a coherent whole, playing back as movie (put together).
- A flipbook produces a cartoon from pictures, a nice illustration of the principles behind animation.
:::

:::{.ch}
<img src="../images/gift09_ch.jpg" width="100%" />

- [2022年端午節](../2022/dragon.html)「禮物」，主題是動畫製作，令圖畫變得生動。
- 動畫是把每個場景，逐幀繪製，以製作電影（分解，化整為零）。
- 畫面成為一格，連貫串入整體，播放出電影（整合，化零為整）。
- 翻頁書讓靜態圖畫、變為動態卡通，很好地說明了動畫背後的原理。
:::

#

:::{.en}
<div class="imagecap" style="width:64%">
<img src="../animation/Newton-vs-Leibniz.jpg"
     alt="Both Newton and Leibniz invented calculus, from different approaches." onclick="showPopup(this)" /></br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:64%">
<img src="../animation/Newton-vs-Leibniz.jpg"
     alt="Newton 和 Leibniz 分別發明「微積分」，透過不同方法。" onclick="showPopup(this)" /></br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- These ideas of breaking into bits and putting bits together are basic in calulus.
- Indeed, breaking up is differentiation $\displaystyle\partial$, and putting together is integration $\displaystyle\int$.
- Isaac Newton invented calculus for physics, Gottfried Leibniz invented calculs for math.
- With different approaches, they used different notations. Nowadays most follow Leibniz.
- Of course, we shall skip the details of calculus, just take the ideas to understand animation.
:::

:::{.ch}
- 分解成元件、元件作整合。這些想法，是高級數學中「微積分」（calculus）的基礎。
- 的而且確，分解是微分（differentiation） $\displaystyle\partial$，整合是積分（integration） $\displaystyle\int$。
- Isaac Newton 發明物理的「微積分」，Gottfried Leibniz 發明數學的「微積分」。
- 透過不同的觀點，他們使用不同的符號。如今，大多數人跟隨 Leibniz 的符號寫法。
- 當然，本文將略過「微積分」的細節，只要求利用「微積分」的想法，以理解動畫。
:::

#

:::{.en}
<div class="imagecap" style="width:98%">
<img src="../animation/walt-disney-at-storyboard-1.jpg" style="width:64%"
     alt="Walt Disney commenting in front of a storyboard." onclick="showPopup(this)" />
<img src="../animation/Walt-Disney-Snow-White-poster.jpg" style="width:27.5%"
     alt="Original poster for 'Snow White and the Seven Dwarfs'." onclick="showPopup(this)" />
</br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:98%">
<img src="../animation/walt-disney-at-storyboard-1.jpg" style="width:64%"
     alt="Walt Disney 在故事板前，發表意見。" onclick="showPopup(this)" />
<img src="../animation/Walt-Disney-Snow-White-poster.jpg" style="width:27.5%"
     alt="《白雪公主與七個小矮人》的原創海報。" onclick="showPopup(this)" />
</br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- Over the years, Walt Disney perfected the technique of storyboarding for animation.
- A storyboard is series of sketches that maps out a story for the movie.
- The sketches allow filmmakers to visualize the sequence of the plot.
- The team rearranges the storyboard to improve the lucidity of a story.
- In fact, this is applying the concepts of differentiation and integration (i.e. calculus) in animation.
- In 1938 he released the first full-featured color animation ["Snow White and the Seven Dwarfs"](https://www.disneyplus.com/en-au/movies/snow-white-and-the-seven-dwarfs/7X592hsrOB4X).
:::

:::{.ch}
- 多年來，Walt Disney 完善了動畫分鏡技術，又稱「故事板」（storyboard）。
- 分鏡是作一系列草圖，分別勾劃出電影故事情節。
- 草圖方便電影製作者，能夠直接了解情節的排序。
- 團隊重新排列故事板，以提高故事發展的清晰度。
- 事實上，這就是動畫製作過程中，分解和整合（即「微積分」）的實際應用。
- 1938年，他發行第一部長篇彩色卡通片[《白雪公主與七個小矮人》](https://www.disneyplus.com/en-au/movies/snow-white-and-the-seven-dwarfs/7X592hsrOB4X)，創新電影史。
:::

<!--
Open Studio: Storyboards
by Alyssa Carnahan, Open Studio Coordinator, at The Walt Disney Family Museum
https://www.waltdisney.org/blog/open-studio-storyboards
Walt Disney believed that story development was one of the most important parts of producing an animated film. In the 1930s, the Walt Disney Studios developed storyboards as a tool for creating engaging and coherent stories. A storyboard is series of sketches that maps out a story and allows filmmakers to visualize the sequence of the plot. Story sketches are created to depict the key storytelling moments of a film. Storyboards help filmmakers determine the lucidity of a story, and whether there are any significant “plotholes,” or inconsistencies.
-->

#

:::{.en}
<div class="imagecap" style="width:98%">
<img src="../animation/Jobs-Catmull-Lasseter.jpg" style="width:60%"
     alt="Steve Jobs, Ed Catmull and John Lasseter honored with an award from Producers Guild of Americas (PGA) in 2002." onclick="showPopup(this)" />
<img src="../animation/Pixar-Toy-Story-Vintage-1995.jpg" style="width:33%"
     alt="Poster for 'Toy Story' in 1995, a Pixar production but released under Disney." onclick="showPopup(this)" />
</br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:98%">
<img src="../animation/Jobs-Catmull-Lasseter.jpg" style="width:60%"
     alt="2002年，Steve Jobs、Ed Catmull 和 John Lasseter 榮獲美國製片人協會 (PGA) 頒發的獎項。" onclick="showPopup(this)" />
<img src="../animation/Pixar-Toy-Story-Vintage-1995.jpg" style="width:33%"
     alt="1995年《玩具總動員》的海報，動畫由 Pixar 製作，但經 Disney 出品發行。" onclick="showPopup(this)" />
</br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- The first full-featured animation by computer, "Toy Story", had to wait years before it can be realized.
- This is for the computer technology to catch up, keeping the work of Ed Catmull at the leading edge.
- With the help of John Lasseter, an ex-Disney animator, they pushed the limit of available technology.
- An inspired comment by John, "The art challenges the technology and the technology inspires the art."
- "Toy Story" was released in December 1995 by Pixar, funded by Steve Jobs but he didn't get involved.
:::

:::{.ch}
- 第一部長篇彩色電腦動畫《玩具總動員》（港譯《反斗奇兵》），需要等待數年，才能實現。
- 電腦技術得以迎頭趕上，配合 Ed Catmull 的開創性工作，保持領先地位。
- 在前迪士尼動畫師 John Lasseter 的幫助下，他們突破現有科技的種種極限。
- 正如 John Lasseter 作出的評論，富有啟發性：「藝術挑戰科技，科技啟發藝術。」
- 《玩具總動員》由 Pixar 於1995年12月發行，由 Steve Jobs 資助，但他並沒有參與電影製作。
:::

<!--
Pixar To Receive PGA Vanguard Award
By Leigh Godfrey | Friday, February 8, 2002 at 12:00am
https://www.awn.com/news/pixar-receive-pga-vanguard-award
Pixar Animation Studios will be honored with the Producers Guild of Americas (PGA) inaugural Vanguard Award.

PGA gives Vanguard honour to Pixar Animation
by Mike Goodridge, 23 January 2002.
https://www.screendaily.com/pga-gives-vanguard-honour-to-pixar-animation/408073.article
The Producers Guild Of America is to give its inaugural Vanguard Award to Pixar Animation Studios at the annual PGA Awards on March 3 in Los Angeles.
The award was established to honour the "brilliant and creative contributions in entertainment production in the fields of new media and technology." It will be accepted by Ed Catmull (president of Pixar), John Lasseter and Steve Jobs at the ceremony.
-->
#

<!--
![](../animation/rendering-techniques.jpg){ width=48% }
![](../animation/render-farm.jpg){ width=50% }
-->

:::{.en}
<div class="imagecap" style="width:98%">
<img src="../animation/rendering-techniques.jpg" style="width:48%"
     alt="A picture of a room with windows rendered by two methods." onclick="showPopup(this)" />
<img src="../animation/render-farm.jpg" style="width:50%"
     alt="An array of computer workstations in a render farm." onclick="showPopup(this)" />
</br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:98%">
<img src="../animation/rendering-techniques.jpg" style="width:48%"
     alt="有窗的房間場景圖片，由兩種「渲染」方法顯示。" onclick="showPopup(this)" />
<img src="../animation/render-farm.jpg" style="width:50%"
     alt="「渲染農場」內，有一排陣列的電腦工作台。" onclick="showPopup(this)" />
</br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- Putting colors and shades to make pictures realistic, like taking a photo, is called "rendering".
- Rendering is achieved by computing the effects of light and shade on various surfaces.
- That is, compute each ray of light that comes into view, although math provides some shortcuts.
- Ed Catmull, with background in physics, was a pioneer in rendering hardware and software.
- The computation is performed by many computers working in coordination, forming a Render Farm.
- Under Ed Catmull, Pixar was among the first to build large scale render farms for computer animation.
- Still, rendering first-rate movies is time-consuming: it took two years to render _Monsters University_.
- Explore this informative and eye-opening exhibit of [The Science Behind Pixar](https://sciencebehindpixar.org), especially educators.
:::

:::{.ch}
- 添加顏色和塗上陰影，使圖片呈現逼真，媲美拍攝，稱為「渲染」（rendering）。
- 渲染技術，是考慮光射在各種表面上的投影，效果如何，透過計算來實現。
- 也就是說，要計算進入視野的每一條光線，儘管有些數學捷徑，可供參考。
- 渲染需要硬體和軟體配合，擁有物理學背景的 Ed Catmull，是兩個領域的開拓者。
- 渲染需要大量的數據計算，由多台電腦執行，協調各項工作，形成「渲染農場」（Render Farm）。
- 在 Ed Catmull 的領導下，Pixar 是其中一間電腦動畫製作公司，最早建立大規模的「渲染農場」。
- 儘管如此，渲染一流的動畫，仍然很費時：渲染 _Monsters University_《怪獸大學》，花上了兩年。
- 請發掘這個展覽：[Pixar 背後的科學](https://sciencebehindpixar.org)，內容豐富，令人大開眼界。尤其是對教育工作者，特別推薦。
:::

<!--

The Science Behind Pixar
https://sciencebehindpixar.org
The Science Behind Pixar is a 13,000 square foot exhibition touring two copies — one nationally, and one internationally. It was created by the Museum of Science, Boston, in collaboration with Pixar Animation Studios. This website features some of the activities, videos, and images from the exhibition that describe the math, computer science, and science that go into making computer animated films.

Rendering | The Science Behind Pixar
Rendering turns a virtual 3D scene into a 2D image
https://sciencebehindpixar.org/pipeline/rendering
Story and Act (starting to Next)
https://sciencebehindpixar.org/pipeline/story-and-art

-->

#

:::{.small}
Toy Story: "Buzz Lightyear Revealed" Storyboard Comparison【5:06】<br>
<a href="https://youtu.be/xvLgKXIYi2A">
<img src="http://img.youtube.com/vi/xvLgKXIYi2A/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- A storyboard comparison with Buzz Lightyear's first scene in "Toy Story", released in 1995.
- Audio is from final film, storyboard is black when there isn't an equivalent film section.
- Note the rendering, especially the optical effects of Buzz Lightyear's spacesuit helmet.
- There is a reason for picking "Toy Story" as the first full-featured computer animation.
- This is because toys, alive or not, are easy to render compared to human face and skin.
- The rendering system [RenderMan](https://renderman.pixar.com/product) developed by Pixar is used for special effects in other movies.
:::

:::{.ch}
- 《玩具總動員》於1995 年上映，視頻顯示其中一段：Buzz Lightyear 出場與故事板草圖的比較。
- 視頻聲音，來自電影最終版本。故事板只標記一個場景。當沒有對應畫面時，故事板呈現黑色。
- 注意「渲染」效果，尤其是 Buzz Lightyear 的太空衣上頭盔，透明的光學效應，十分精確玲瓏。
- 選擇《玩具總動員》Toy Story，作為第一部長篇電腦動畫，是有其箇中原因，可說是權宜之計。
- 主要的原因是：渲染玩具，無論是活生生還是死板板，與渲染人臉和皮膚相比，都更簡單容易。
- Pixar 開發的渲染系統 [RenderMan](https://renderman.pixar.com/product)，也用於其他特技電影中，如《侏羅紀世界》Jurassic World。
:::

<!--
From Storyboard to Animation: See how Pixar Created Toy Story 2
17 October 2016.
https://www.motionpictures.org/2016/10/storyboard-animation-see-how-pixar-created-toy-story-2/
Storyboards are an essential part of the filmmaking process, and perhaps no studio on the planet relies on them quite the way that Pixar does. A Pixar storyboard is essentially a hand-drawn version of the movie, and helps the artists making diagram the action and the dialogue. The storyboard is basically the blueprint for the film. "Each storyboard artist receives script pages or a 'beat outline', a map of the characters' emotional changes that need to be seen through actions," the studio explains. "Using these as guidelines, the artists envision their assigned sequences, draw them out and then 'pitch' their work to the director. Over four-thousand storyboard drawings are created as the blueprint for the action and dialog of a feature-length Pixar animated film. They are revised many times during the creative development process."

Toy Story 2 Opening Side by Side Part One | Disney•Pixar【1:53】no cc
https://youtu.be/Q8-0iUbgiJ0

Toy Story 2 Opening Side by Side Part Two | Disney•Pixar【1:24】no cc
https://youtu.be/6VvsCB_hVlU

Toy Story: "Buzz Lightyear Revealed" Storyboard Comparison【5:06】no cc
https://www.youtube.com/watch?v=xvLgKXIYi2A
Storyboard comparison with Buzz Lightyear's first scene! Audio is from final film, storyboards will cut to black when there isn't an equivalent section, as the animatic does not match the film exactly.

The Toy Story Story
by Burr Snider, 1 December 1995.
https://www.wired.com/1995/12/toy-story/
How John Lasseter came to make the first 100-percent computer-generated theatrical motion picture.
For Jobs, who negotiated Pixar's deal with Disney and played a hands-on role as Toy Story's executive producer, the new tools are revolutionary. "I had the same experience when we shipped the first laser printer at Apple. You looked at it and thought, There's an awesome amount of technology in this box, but you don't need to know that to enjoy its output. There are more PhDs working on this film than any other in movie history, and yet you don't need to know a thing about technology to love it."

With all the elements assembled, the film was ready to go to the "render farm," where Pixar's bank of 300 Sun processors would render the movie into its final form. After the processor was fed massive amounts of digital information to determine the animation, shading, and lighting, RenderMan software stirred the mix slowly (taking anywhere from 2 to 15 hours per frame) in a huge, simmering computational soup. The finished images were then transferred to the Avid editing system for Lasseter and his team to digitally cut and paste into the final version of Toy Story, which was then used to create the final 35-mm cut.
>>>

Toy Story’s 20th Anniversary: A Look Back at 1995
https://pixarpost.com/2015/11/toy-storys-20th-anniversary-look-back.html
by Pixar Post - T.J., 23 November 2015.
Take a look back at how the world was in 1995 and celebrate the 25th anniversary of Toy Story.

-->

#

:::{.small}
Creative Spark: Jennifer Yuh Nelson【5:33】cc<br>
<a href="https://youtu.be/YQVKyMWhM0A">
<img src="http://img.youtube.com/vi/YQVKyMWhM0A/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- Jennifer is a storyboard artist, joining Dreamworks as head of story department of "Kung Fu Panda".
- She is also an illustrator, a character designer, now best known for directing "Kung Fu Panda" 2 and 3.
- She talks about her creative process, where her ideas come from, how to layout for the virtual camera.
- She explained, "I've always been a very visual person, so it's always a picture first or a moment first."
- "This moment becomes the hook for the story: everything goes off from it, and everything goes to it."
- For "Kung Fu Panda 2", that was a front image of a female panda, the last moment Po saw his mother.
:::

:::{.ch}
- Jennifer 是故事板藝術家，加入 Dreamworks 後，在《功夫熊貓》故事情節部門，擔任主管。
- 她也是插畫家、角色設計師，現時因曾執導《功夫熊貓2》和《功夫熊貓3》，打響名堂。
- 在視頻中，Jennifer 侃侃而談，她的創作過程、她的想法從何而來、如何佈置虛擬相機。
- 她解釋說，「一直以來，我非常注重視覺，所以總是從一張照片或某一瞬間，開始想像。」
- 「就是這一刻，成為故事的鈎點，勾畫出整個故事：一切都從它開始，一切都追溯回它。」
- 在《功夫熊貓2》中，鈎點是一隻雌性熊貓的正面圖像，也是「阿寶」看見媽媽的最後一刻。
:::

<!--

Creative Spark: Jennifer Yuh Nelson【5:33】cc
https://www.youtube.com/watch?v=YQVKyMWhM0A
Jennifer Yuh Nelson ("Kung Fu Panda 2 & 3") takes viewers inside her creative process in an exploration of where ideas come from.
(showing storyboard)

I've always been a very visual person, so it's always a picture first or a moment first.
When you're constructing a narrative for the film, this moment becomes kind of the hook.
Everything goes off from it, and everything goes to this moment.

Directing an animated feature film is like directing a 90-minute effects movie.
Everything is still the same.
We have to come up with the script, come up with the story, come up with the characters, come up with all the development.
But the difference is execution because we don't get coverage.
We actually have to build every single frame from the very beginning.
It's a bit of a backwards process.
I personally am a storyboard artist, so a lot of it is started at a visual level.
In "Kung Fu Panda 2," in the very beginning, trying to come up with what the movie was about, it started off with a picture.
It was just an image of a female looking right at camera.
It's the last moment Po saw his mom.
>>>

The Tech of Shrek 2【6:33】no cc
https://www.youtube.com/watch?v=QEh-kSpt7jQ
not HD

Making-of Kung-Fu Panda【5:46】cc
https://www.youtube.com/watch?v=LVsP4Bm4rSM
good, but not HD

Behind the Scenes (The making of and original scores) - How to Train Your Dragon【5:39】cc
https://www.youtube.com/watch?v=HdX7S2AYisc
HD, but not much.

Dreamworks Didn't Make Shrek (The Making of Shrek)【2:49】cc
https://www.youtube.com/watch?v=mm2zM5-DyVo
After purchasing the rights to Steig's book in 1991, Steven Spielberg planned to produce a traditionally-animated film based on the book, but John H. Williams convinced him to bring the project to the newly founded DreamWorks in 1994.
The film was initially intended to be created using motion capture, but after poor test results, the studio hired Pacific Data Images to complete the final computer animation.

Jennifer Yuh Nelson on the Passion and Creative Collaboration of Kung Fu Panda 2
by Mary Ann Skweres and Bob Bayless, 27 May 2011.
https://www.btlnews.com/awards/kung-fu-panda-2/

呂寅榮
https://zh.wikipedia.org/zh-hk/吕寅荣
呂寅榮（Jennifer Yuh Nelson，1972年6月13日—）是一名美國女導演、喜劇演員和情節串連圖板藝術家。她導演的首部作品為2011年動畫電影《功夫熊貓2》，是荷里活首位單獨執導動畫電影的女性導演[2]。憑藉《功夫熊貓》獲得了安妮獎。

MoonRay
https://openmoonray.org
MoonRay is DreamWorks’ open-source, multi-award-winning, state-of-the-art production MCRT renderer, which has been used on feature films such as How to Train Your Dragon: The Hidden World, The Bad Guys, Puss In Boots: The Last Wish, the newest release, Kung Fu Panda 4, as well as future titles. MoonRay was developed at DreamWorks and is in continuous active development and includes an extensive library of production-tested, physically based materials, a USD Hydra render delegate, multi-machine and cloud rendering via the Arras distributed computation framework.

-->

#

:::{.small}
Frozen 2! How Disney Animation made it look even better than Frozen【14:13】cc<br>
<a href="https://youtu.be/k37DcLpoAac">
<img src="http://img.youtube.com/vi/k37DcLpoAac/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- Disney animation artists put a lot of effort to create an astounding sequel to the original Frozen.
- A showcase of [Hyperion](https://disneyanimation.com/technology/hyperion/), the in-house renderer of Disney animation (see website for details).
- Their team work make use of many tools for simulation of the elements: earth, wind, water and fire.
- The team pitch scenes on storyboards, work on character design, with attention to hairstyles.
- Many fun facts about Snow Queen Elsa, and how to animate the invisible Gale in Enchanted Forest.
- Be amazed by the water effects engine called Splash for the epic towering waves scenes with Elsa.
- The coordination of body movements and simulation of waves rushing to shore are based on physics!
:::

:::{.ch}
- Disney 動畫工作室，集一眾藝術家，投入大量精力創作，製成《冰雪奇緣》續集，莫不嘖嘖稱奇。
- 視頻重點展示，Disney 動畫室自主研發的「渲染機器」 [Hyperion](https://disneyanimation.com/technology/hyperion/) ，功能多樣化（詳情參閱網頁）。
- 團隊的工作精細，應用不同電腦工具，模擬各種自然現象：土、風、水和火。選取樹木，砌成森林。
- 團隊在故事板上，展示場景段落，提出想法，互相討論。並進行角色設計，特別專注於髮型、衣飾。
- 附有許多冷知識，關於冰雪女王 Elsa 的點滴。及如何動畫化，在魔法森林中的隱形「大風」Gale 。
- 令人驚嘆的，是運用「水性效果機」Splash，呈現史詩般的場景：Elsa 奮不顧身，撲向高聳的巨浪。
- 為求逼真，模擬身體動作要講求協調，模擬海浪沖上岸要明白是如何衝擊，統統都是基於物理計算！
:::

<!--

Frozen 2! How Disney Animation made it look even better than Frozen【14:13】cc
https://www.youtube.com/watch?v=k37DcLpoAac
I went behind the scenes with Disney Animation's Frozen 2 to find out all the things you didn't know about it. There's a lot of cool fun facts in here and how Disney Animation used new technology to make Frozen 2 look so darn good.

Disney's Hyperion Renderer
https://disneyanimation.com/technology/hyperion/

The Design and Evolution of Disney’s Hyperion Renderer
ACM Transactions on Graphics (TOG) 2018
https://www.disneyanimation.com/publications/the-design-and-evolution-of-disneys-hyperion-renderer/
The Design and Evolution of Disney’s Hyperion Renderer
Walt Disney Animation Studios
https://media.disneyanimation.com/uploads/production/publication_asset/177/asset/a.pdf
(22 pages) a technical paper

FROZEN II (2019) | Behind the Scenes of Disney Animation Movie【10:54】cc
https://www.youtube.com/watch?v=P6ipgCDJgpI
showing the storyboard, missing most audio.

Frozen: Behind the Scenes of the Animation | ScreenSlam【2:47】cc
https://www.youtube.com/watch?v=z-4c3JSLVww
(similar to above, but for Frozen original)

FROZEN 3 (2024) Everything We Know【8:38】cc
https://www.youtube.com/watch?v=WUGqc06KASM

FROZEN | Making of 'Let It Go'【4:29】no cc
https://www.youtube.com/watch?v=0eEHYbH2lQg
(good, but no cc)

Disney's Frozen - Behind the Scenes - The Making of Frozen (Part 1 of 2)【12:06】cc
https://www.youtube.com/watch?v=3kZ9B8DDQzk
British TV Presenter Nigel Clarke shows us around Disney animation studios. Finding out just how the amazing characters, Anna, Elsa, Olaf and Kristoff are brought to life.

Disney's Frozen Behind the Scenes - The Making of Frozen (Part 2 of 2)【9:44】no cc
https://www.youtube.com/watch?v=gO1XKGCZ3H8
Part 2 of our look behind the scenes of Disney's Frozen! More facts about how this record breaking animation was made. British TV Presenter Nigel Clarke shows us around Disney animation studios.
(good, but no cc)
-->

#

:::{.en}
<div class="imagecap" style="width:98%">
<img src="../animation/Abominable-2019-scene-01.jpg" style="width:49.5%"
     alt="Yi plays her violin under the Giant Buddha of Leshan in Sichuan." onclick="showPopup(this)" />
<img src="../animation/Abominable-2019-scene-02.jpg" style="width:49.5%"
     alt="Fly over the Lake of Thousand Islands, a man-made fresh water lake in Zhejiang." onclick="showPopup(this)" />
</br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:98%">
<img src="../animation/Abominable-2019-scene-01.jpg" style="width:49.5%"
     alt="「阿怡」在四川樂山大佛下。演奏小提琴。" onclick="showPopup(this)" />
<img src="../animation/Abominable-2019-scene-02.jpg" style="width:49.5%"
     alt="飛越鳥瞰千島湖，是浙江的一個人工淡水湖。" onclick="showPopup(this)" />
</br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- Abominable is an animated adventure movie produced by DreamWorks Animation and Pearl Studio.
- In the story, a Chinese teenage girl Yi managed to return a runaway Yeti, with help from her friends.
- The trio of friends stayed ahead of a party led by a zoologist intended to capture the escaped Yeti.
- Yeti is mythical, so the creature is given a power to control nature, blow up plants, shaking mountains.
- Yi realized their journey echoes her father's dream trip, now possible her encounter with magical Yeti.
- An arduous and epic travel, from bustling streets of Shanghai to breathtaking Himalayan snowscapes.
- Combining technology and art into fantasy worlds, stories become movies and dreams become life.
:::

:::{.ch}
- 《雪人奇緣》是一部動畫冒險電影，由 DreamWorks 動畫和 Pearl Studio﹙東方夢工廠﹚合力製作。
- 故事中，中國少女「阿怡」遇上迷失的「雪怪」Yeti，在朋友的幫助下，成功把雪人帶返雪山故鄉。
- 三位小朋友往西走，沿途保持節節領先，因為一位動物學家率領她的隊伍，正準備抓捕逃跑的雪人。
- 雪人 Yeti，是神話中的生物，劇中賦予雪人控制自然的能力：植物變得巨大，排山倒「油菜花」海。
- 漸漸「阿怡」意識到，她父親夢想之旅，與自己艱巨的旅程相呼應。今次遇上神奇雪人，得以實現。
- 如史詩般的傳奇旅程，從熙來攘往的繁華上海，到令人神往的喜馬拉雅山雪景，遊遍中華名山大川。
- 「DreamWorks」直譯「夢工廠」。科技結合藝術，創造幻想世界。故事變成動畫，夢想化成人生。
:::

#

:::{.small}
<span class="en">Abominable 2019 (FULL HD)</span><span class="ch">2019年《雪人奇緣》</span>【1:37:12】<br>
<a href="https://www.bilibili.tv/en/video/2041335728">
<img src="http://img.youtube.com/vi/Ap0NRJD-2ts/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- This "Abominable" is an outstanding example of 3D computer animation, with heart-warming stories.
- If you would like subtitles, look for [download](../2022/dragon.html#movie) in my webpage. The Web may have a Chinese version.
- The movie production team was predominantly female, with the story around a strong female lead.
- Jill Culton was the first female director of an original animated film, she wrote and revised the story.
- She worked closely with DreamWorks president [`MC`] and chief creative officer [`PC`] of Pearl Studio.
- [`MC`] Margie Cohn, "Our works celebrate artistry, memorable characters and compelling storytelling."
- [`PC`] Peilin Chou, "We went to great lengths to ensure the film is as culturally authentic as possible."
- Chloe Bennet is the voice of Yi, pre-recorded, "You have to imagine everything, as there's nothing."
:::

:::{.ch}
- 《雪人奇緣》是 3D電腦動畫中，傑出的典範。故事感人至深：溫馨家庭、少女成長、風光如畫。
- 視頻沒有字幕。如果想睇字幕，請在我的網頁中，尋找電影 [下載](../2022/dragon.html#movie)。在網上可能有電影中文版本。
- 動畫製作團隊，主要由女性組成，完全由女性主導，並且以女性角色為中心，算得上是嶄新嘗試。
- Jill Culton 是第一位原創動畫電影的女導演，也負責編劇，撰寫故事。改完又改，前後花上七年。
- 與她緊密合作的，有 DreamWorks 動畫總裁 [`MC`]，和 Pearl Studio 首席創意師 [`PC`]，皆是女強人。
- [`MC`] Margie Cohn：「我們引以為榮的，是作品的藝術性、令人難忘的角色、和引人入勝的故事。」
- [`PC`] Peilin Chou：「我們竭盡全力，在盡可能範圍內，確保這部電影，具有文化真實性、精準性。」
- Chloe Bennet 為「阿怡」前期配音，未有畫面：「劇本只有文字，感情空空如也，一切憑想像。」
:::

#

:::{.small}
Beautiful Life by Bebe Rexha【3:24】cc<br>
<a href="https://youtu.be/teJObISySfs">
<img src="http://img.youtube.com/vi/teJObISySfs/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- A female singer songwriter composed and performed the theme song [Beautiful Life](../songs/BeautifulLife.html).
- Bebe Rexha wrote the score with a brisk rhythm, and began the lyrics with:
-
| _I was lost, but you found me._
| _Hiding in the shadows from my destiny._
| _And you saw things, that no one else could see._
| _You saved me when I was my worst enemy._

- She even included a fine-tuned but thought-provoking rap section in the song! 
:::

:::{.ch}
- 講開女性，順便一提。《雪人奇緣》電影主題曲[Beautiful Life](../songs/BeautifulLife.html)，由女詞曲歌手創作並演唱。
- Bebe Rexha 編寫樂譜，節奏輕快，英文歌詞開頭是：
-
| _I was lost, but you found me.[我已迷失，但你找到了我。]_
| _Hiding in the shadows from my destiny.[我躲避隱藏，在命運的陰影中。]_
| _And you saw things, that no one else could see.[你看見的東西，其他人看不到。]_
| _You saved me when I was my worst enemy.[我是自己最大的敵人，你卻拯救了我。]_

- 她甚至在歌曲中，加入了說唱部分，清晰押韻，並且發人深省！
:::

#

:::{.small}
張韶涵 《奇妙相遇》官方版 MV【2:47】cc<br>
<a href="https://youtu.be/xjg6W4QT1es">
<img src="http://img.youtube.com/vi/xjg6W4QT1es/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- Another female singer, Angela Zhang (張韶涵), sings the theme song in Chinese, [《奇妙相遇》](../songs/BeautifulLife.html).
- The Chinese lyrics cannot be an identical translation of the original, but close:
-
| _當迷失在，黑暗裡，[When I am lost in darkness,]_
| _就算在遙遠天際，你是閃亮的星。[Even in the sky afar, you are a shining star.]_
| _我們相遇，穿越空間距離，[We connect and meet, crossing the distance of space,]_
| _你點燃了我的勇氣，面對最大的強敵。[You ignited my courage to face the greatest enemy.]_

- Translation of the rap section into Chinese is even harder, but still witty!
:::

:::{.ch}
- 另一位女歌手張韶涵（Angela Zhang），演唱電影主題曲中文版本[《奇妙相遇》](../songs/BeautifulLife.html)。
- 中文歌詞翻譯，不可能與英文歌詞完全一致，但相當接近（方括號[]內是原文）：
-
| _當迷失在，黑暗裡，[I was lost, but you found me.]_
| _就算在遙遠天際，你是閃亮的星。[Hiding in the shadows from my destiny.]_
| _我們相遇，穿越空間距離，[And you saw things, that no one else could see.]_
| _你點燃了我的勇氣，面對最大的強敵。[You saved me when I was my worst enemy.]_

- 歌曲中說唱部分，翻譯成中文更加困難，但仍然不錯，十分風趣！
:::

#

:::{.small}
Abominable | “Beautiful Life” (Violin Cover)【3:41】<br>
<a href="https://youtu.be/ylw2YQCkYj4">
<img src="http://img.youtube.com/vi/ylw2YQCkYj4/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- In the movie storyline, the teenage girl Yi always carries a violin, a gift from her father, on her back.
- She was playing the instrument on the roof when she first met Yeti hiding and cowering in a shed.
- When her violin was broken during the adventure, Yeti mended it with magic fur strands as strings.
- Later Yi would use her magical violin to wake up Yeti, breaking out to escape from the capture team.
- A lively instrumental version of the theme song by a group of young violinists, mostly teenage girls!
- They even make [another video](https://youtu.be/nSIUpwySd3k) introducing themselves and showing the production with interest. 
:::

:::{.ch}
- 電影故事中，少女「阿怡」很多時候，總是背著一把小提琴，這是父親留下的禮物。
- 她登上屋頂，練習小提琴時，正正第一次遇到，瑟縮躲在棚子裡的雪人 Yeti。
- 在冒險旅程中，她的小提琴被摔碎，雪人用魔法修復了它，並拉上毛髮弦線。
- 後來，「阿怡」會利用她的神奇小提琴，遙遙喚醒雪人，脫身逃離追捕隊伍。
- 電影主題曲的純樂器版本，由一群小小提琴家演奏，青春活潑，其中多是可愛女孩子！
- 她們甚至製作[另一個視頻](https://youtu.be/nSIUpwySd3k) ，自我介紹。並展示拍攝過程，剪接專業，奏樂認真又有趣。
:::

<!--
2025/dragon
https://jhlchan.bitbucket.io/2022/dragon.html
(video?: Beautiful Life)
2022年端午節「禮物」，主題是動畫製作。利用微積分，概述首部手繪電影動畫，以及如何製作電腦電影動畫。當然啦，微積分只提理念，不求甚解。從幻想世界，到科技結合藝術，故事變成電影，夢想化成人生。電影是2019年的《雪人奇緣》，其中主題曲節奏輕快，有中英歌詞。

Abominable 2019 (FULL HD)【1:37:12】no cc
https://www.bilibili.tv/en/video/2041335728
https://youtu.be/
化整為零  化零為整

Beautiful Life by Bebe Rexha【3:24】cc
https://www.youtube.com/watch?v=teJObISySfs
https://youtu.be/
English lyrics and Chinese translation
or
Abominable | “Beautiful Life” (Violin Cover)【3:41】
https://www.youtube.com/watch?v=ylw2YQCkYj4
Performed by the Peacock Kids

張韶涵 《奇妙相遇》官方版MV【2:47】cc
https://www.youtube.com/watch?v=xjg6W4QT1es
-->


<!--
pandoc -t revealjs -s --mathjax gift09.md -o gift09.html
-->
