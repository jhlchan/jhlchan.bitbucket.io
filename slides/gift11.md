---
title: Xmas 2022 Highlights
author: <span class="en">WiFi</span><span class="ch">無線上網</span>
date: <span class="en">Xmas 2025</span><span class="ch">2025年聖誕節</span>
theme: solarized
header-includes: |
    <meta property="og:type" content="website" />
    <meta property="og:title" content="2022年聖誕禮物" />
    <meta property="og:image" content="../images/highlight-11.jpg" />
    <base target="_blank"/>
    <link rel="stylesheet" type="text/css" href="../css/slides.css"></link>
    <script type="text/javascript" src="../scripts/site.js"></script>
include-before: |
    <!-- Any raw HTML code right after body tag, before anything else. -->
    <div id="modal" class="modal">
      <img id="modal-content" />
      <div id="modal-caption"></div>
    </div>
    <div align="right" class="banner">
    <input type="radio" name="lang" onclick="set_lang('en')" id="english" value="English" checked /> English |
    <input type="radio" name="lang" onclick="set_lang('ch')" id="chinese" value="Chinese" /> 中文
    </div>
include-after: |
    <!-- Any raw HTML code right before body end tag, after anything else. -->
    <script>window.onload = check</script>
    <!-- file:///Users/josephchan/jc/q/site/slides/gift11.html -->
    <!-- https://jhlchan.bitbucket.io/slides/gift11.html -->
...

#

:::{.en}
<img src="../images/gift11_en.jpg" width="90%" />

- For the [Xmas 2022](../2022/xmas.html) "gift" the theme is WiFi = Wireless Internet access 🛜, symbol usually blinks.
- We all enjoy WiFi for our laptops and smartphones. It is also forms the backbone of a smart city.
- Not too long ago, phones have no internet, laptops need a cable to connect to internet. Remember?
- How did WiFi come about? Did someone invented it? Did some technology company promoted it?
- WiFi history involves many unexpected twists and turns, an interesting story that needs to be told.
:::

:::{.ch}
<img src="../images/gift11_ch.jpg" width="90%" />

- [2022年聖誕節](../2022/xmas.html)「禮物」，主題是 WiFi = 無線上網 🛜，常見符號閃下閃下。
- 無線上網是我們的至愛，支援筆記本電腦和智能手機，也構成智慧城市的支柱。
- 不久以前，手機不能上網，筆記本電腦連接到互聯網，需要網絡電線。可記得？
- WiFi 是如何從無到有？有聰明伙子發明嗎？有科技公司推廣嗎？
- WiFi 的歷史，錯綜複雜，出人意表，曲折有趣，故事值得講述。
:::

#

:::{.en}
<div class="imagecap" style="width:98%">
<img src="../wifi/WiFi-technology-info-01.jpg" style="width:44%"
     alt="CSIRO poster for the history of WiFi, part 1." onclick="showPopup(this)" />
<img src="../wifi/WiFi-technology-info-02.jpg" style="width:55%"
     alt="CSIRO poster for the history of WiFi, part 2." onclick="showPopup(this)" />
</br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:98%">
<img src="../wifi/WiFi-technology-info-01.jpg" style="width:44%"
     alt="CSIRO 製作的 WiFi 歷史海報，第1部分。" onclick="showPopup(this)" />
<img src="../wifi/WiFi-technology-info-02.jpg" style="width:55%"
     alt="CSIRO 製作的 WiFi 歷史海報，第2部分。" onclick="showPopup(this)" />
</br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- This cartoon shows a version of the history of WiFi as told by CSIRO, a research agency in Australia.
- The Commonwealth Scientific and Industrial Research Organisation (CSIRO) leads innovative studies.
- In 1990s, internet access to research centers was provided by a Local Area Networks (LAN).
- Computers are connected to LAN by high speed data cables, moving them around was inconvenient.
- CSIRO took up the challenge to eliminate wires: how to devise a fast, reliable wireless LAN (WLAN)?
:::

:::{.ch}
- 有趣的漫畫，講述 WiFi 發展歷史其中一個版本，由 CSIRO 提供，是澳洲科研機構。
- 聯邦科學與工業研究組織 (CSIRO) 接受政府資助，領導澳洲創新科技研究，人才輩出。
- 1990年代，研究中心的網絡連接，由區域網絡 (LAN = Local Area Network) 供應。
- 中心每台電腦，若要連接到區域網絡，需要透過高速數據電線，不方便搬來搬去。
- CSIRO 接受挑戰：如何消除電線，設計快速、可靠的無線 LAN (WLAN = wireless LAN)？
:::

#

:::{.en}
<div class="imagecap" style="width:98%">
<img src="../wifi/John-OSullivan-research-engineer.jpg" style="width:56.5%"
     alt="John O'Sullivan is an engineer at CSIRO." onclick="showPopup(this)" />
<img src="../images/faraday-disk-1831.gif" style="width:42%"
     alt="Animation of a Faraday disk in action." onclick="showPopup(this)" />
</br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:98%">
<img src="../wifi/John-OSullivan-research-engineer.jpg" style="width:56.5%"
     alt="John O'Sullivan 是 CSIRO 的工程師。" onclick="showPopup(this)" />
<img src="../images/faraday-disk-1831.gif" style="width:42%"
     alt="動畫展示 Faraday disk 的操作情況。" onclick="showPopup(this)" />
</br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- John O'Sullivan was intrigued by the Faraday disk during a high school visit to Sydney University.
- Later he entered Sydney University, and in his final year spent time at CSIRO (more about [Faraday](rixmas.html)).
- With a PhD in Electrical Engineering, he got a job in Netherlands workinbg with a radio telescope.
- At that time, Stephen Hawking predicted that [black holes](gift02.html) has a temperature, emitting radiation.
- No one believed this, yet he led a team to detect such extremely weak radio signals from far far away.
- Nothing was detected, but he worked out how to clean up faint radio signals in a noisy background.
- When he returned to Australia in 1983 and joined CSIRO, he put this knowledge into use in WLAN.
:::

:::{.ch}
- John O'Sullivan 在高中時，曾訪問雪梨大學，對「圓盤發電機」（Faraday disk）很感興趣。
- 後來他進入雪梨大學，修讀工程與物理，並在最後一年，前往 CSIRO 實習（更多關於 [Faraday](rixmas.html)）。
- 畢業後，擁有電機工程博士學位，在荷蘭找到一份工作，專門使用電波望遠鏡。
- 當年，物理學家 Stephen Hawking 從理論上，預言[黑洞](gift02.html)帶有溫度，因此有輻射發射。
- 無人相信這一點，但 John 帶領團隊，嘗試探測如此微弱的無線電訊號，因為黑洞很遠很遠。
- 最後，團隊偵測徒勞無功。但 John 對於在偵測微弱無線電訊號中，如何清除嘈雜背景，想出辦法。
- 1983年他返回澳洲，並加入 CSIRO。當時正值發展 WLAN，遇上難題。他的經驗，正好派上用場。

:::

<!--

Homopolar generator
https://en.wikipedia.org/wiki/Homopolar_generator
It is also known as a unipolar generator, acyclic generator, disk dynamo, or Faraday disc.
The first homopolar generator was developed by Michael Faraday during his experiments in 1831. It is frequently called the Faraday disc or Faraday wheel in his honor. It was the beginning of modern dynamos — that is, electrical generators which operate using a magnetic field.

-->

#

:::{.en}
<div class="imagecap" style="width:98%">
<img src="../nasa/Voyager-spacecrafts-grand-tour.png" style="width:48%"
     alt="Grand Tour of Solar System by Voyager spacecrafts." onclick="showPopup(this)" />
<img src="../nasa/Voyager-flight-path-animation.gif" style="width:48%"
     alt="Animation of spacecrafts fly-by over planets." onclick="showPopup(this)" />
</br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:98%">
<img src="../nasa/Voyager-spacecrafts-grand-tour.png" style="width:48%"
     alt="航行者號宇宙飛船的太陽系「偉大巡遊」。" onclick="showPopup(this)" />
<img src="../nasa/Voyager-flight-path-animation.gif" style="width:48%"
     alt="動畫展示宇宙飛船如何飛越各行星。" onclick="showPopup(this)" />
</br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- Meanwhile, during 1980s there was a real need to detect extremely weak radio signals from far away.
- NASA had started the Grand Tour of the Solar system, launching two spacecrafts to fly-by the planets.
- The purpose of two identical spacecrafts is redundancy: if one fails, the other one acts as backup.
- This is also the key to maintain reliable transmission with unreliable signals: add redundanct bits.
- Reed-Solomon codes are designed with redundancy to detect and correct errors in data transmission.
- This code was used in the transmission of images from the spacecrafts, also in CDs, QR codes, etc.
:::

:::{.ch}
- 其實，在20世紀80年代，確實有此需要：檢測極遙遠、極微弱的無線電訊號，來自宇宙飛船。
- NASA 正展開太陽系「偉大巡遊」（Grand Tour），發射兩艘宇宙飛船，近距離飛越各行星。
- 兩艘宇宙飛船一模一樣，究其原因是冗餘：如果其中一艘發生故障，另一艘將充當後補備用。
- 假如訊號傳送不可靠，仍要保持可靠傳輸，這就是關鍵：增加冗餘位元（redundanct bits）。
- RS 碼（Reed-Solomon codes）採用冗餘設計，以便能夠偵測資料傳送錯誤，並修正錯誤。
- 宇宙飛船拍攝影像，清晰傳送回地球，正是應用 RS 碼，現今也用於 CD 儲存、QR 圖碼等。
:::

#

:::{.en}
<div class="imagecap" style="width:98%">
<img src="../wifi/NCR-Vic-Hayes-wifi.jpg" style="width:53%"
     alt="Vic Hayes established and chaired the WiFi committee." onclick="showPopup(this)" />
<img src="../wifi/NCR-255-cash-register.jpg" style="width:46%"
     alt="NCR cash register as electronic checkout in 1980s." onclick="showPopup(this)" />
</br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:98%">
<img src="../wifi/NCR-Vic-Hayes-wifi.jpg" style="width:53%"
     alt="Vic Hayes 成立 WiFi 委員會，並擔任主席。" onclick="showPopup(this)" />
<img src="../wifi/NCR-255-cash-register.jpg" style="width:46%"
     alt="NCR 收銀機，是20世紀80年代的電子結帳機。" onclick="showPopup(this)" />
</br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- While universities or offices with computers can tolerate wired connections to LAN, others don't.
- In 90s, Victor Hayes, an engineer at the National Cash Registers Corporation (NCR), was given a task.
- NCR was keen to use electronic cash registers at checkouts, which talk to backend computers.
- A wired checkout register means hiding wires by digging up shop floors, so wireless is to go. 
- A WLAN system was developed, but for widespread adoption they needed an industry standard.
- Victor Hayes chaired a WiFi committe in IEEE (Institute of Electrical and Electronics Engineers).
- In 1999, the committe published a WLAN standard called "IEEE 802.11b" for wireless manufacturers.
- This name "IEEE 802.11b" was too technical, so another name was suggested for marketing: WiFi.
:::

:::{.ch}
- 雖然擁有電腦的研究所、大學或辦公室，可以容忍周圍有電線連接到 LAN，但其他地方則辦不到。
- 1990年代，國家收銀機公司 (NCR = National Cash Registers) 工程師 Victor Hayes，承接一項任務。
- 銷售電子化，NCR 熱衷於在付款台使用電子收銀機，方便與後端主電腦通訊，進行電子結賬。
- 有線收銀機，若安置在高級百貨公司，意味要鑿開地板藏電線，因此無線收銀機有迫切需要。
- NCR 團隊費盡心思，成功開發 WLAN 系統，他們希望會被廣泛採用，但需要一個業界標準。
- Victor Hayes 游說 IEEE（電氣和電子工程師協會），成立 WiFi 委員會，並擔任委員會主席。
- 1999年，該委員會發佈名為「IEEE 802.11b」的 WLAN 標準，為無線製造商訂立產品規格。
- 「IEEE 802.11b」這個名稱，太技術性，為求推廣，接受營銷建議，使用另一名稱：WiFi。
:::

#

:::{.en}
<div class="imagecap" style="width:98%">
<img src="../wifi/wifi-bouncing-echoes.jpg" style="width:48%"
     alt="Indoor signals bouncing around cause difficulties in wireless reception." onclick="showPopup(this)" />
<img src="../wifi/wifi-US-patent-1996.jpg" style="width:48%"
     alt="The US patent for wireless LAN technology applied by CSIRO." onclick="showPopup(this)" />
</br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:98%">
<img src="../wifi/wifi-bouncing-echoes.jpg" style="width:48%"
     alt="室內訊號來回跳動，導致無線接收困難。" onclick="showPopup(this)" />
<img src="../wifi/wifi-US-patent-1996.jpg" style="width:48%"
     alt="無線區域網絡技術的美國專利，由 CSIRO 申請。" onclick="showPopup(this)" />
</br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- The WiFi standard was a specification to achieve high performance in wireless connection.
- Only the performance result is specified, how to achieve the result is up to manufacturers.
- For wireless LAN (WLAN), the main difficulty is to clean up bouncing radio signals in a room.
- This was first solved by the CSIRO team led by John O'Sullivan, with a US patent in 1996.
- As a radio astronomer, John could pick up signals from noise using Fast Fourier Transform (FFT).
- John's team developed a WiFi chip, an efficient [Turing machine](gift05.html) to process all FFT computations.
- As the CSIRO solution is part of the WiFi standard, all WiFi manufacturers wanted the WiFi chip.
:::

:::{.ch}
- WiFi 標準，是一系列規範，實現高效能的無線網絡連線。上至數據傳輸方法，下至無電訊號頻率。
- 標準僅規定效能指標，例如數據傳輸速度多少，至於如何實現指標，由製造商想辦法，自行解決。
- 無線區域網路（WLAN）的問題，來自室內訊號電波反彈，造成干擾。如何清除，是主要困難之處。
- 這個難題，由 John O'Sullivan 領導的 CSIRO 團隊，首先完滿解決，並於1996年獲得美國專利。
- 身為電波天文學家，他採用 FFT (Fast Fourier Transform，快速 Fourier 變換) ，從雜訊中認出訊號。
- John 與團隊合作，開發一款 WiFi 晶片，相當於一種高效的[圖靈機](gift05.html)，用於處理所有繁複的 FFT 計算。
- 由於 CSIRO 的解決方案，是 WiFi 標準的一部分，因此所有 WiFi 製造商，都期待取得 WiFi 晶片。
:::

#

:::{.en}
<div class="imagecap" style="width:98%">
<img src="../wifi/wifi-CSIRO-chip.jpg" style="width:44%"
     alt="WiFi chip prototype, developed by CSIRO." onclick="showPopup(this)" />
<img src="../wifi/wifi-CSIRO-team-celebration.jpg" style="width:55.5%"
     alt="The CSIRO team celebrated their victory of the WiFi patent war." onclick="showPopup(this)" />
</br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:98%">
<img src="../wifi/wifi-CSIRO-chip.jpg" style="width:44%"
     alt="WiFi 晶片原型，由 CSIRO 開發。" onclick="showPopup(this)" />
<img src="../wifi/wifi-CSIRO-team-celebration.jpg" style="width:55.5%"
     alt="CSIRO 團隊，慶祝 WiFi 專利戰勝利。" onclick="showPopup(this)" />
</br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- CSIRO had a prototype of the WiFi chip from 1991, undergoing several improvements over the years.
- In 1997, Radiata Communications was founded to streamline the WiFi chip production, reducing cost.
- By 2000, Radiata demonstrated the first WiFi chip for sale at major international trade shows in USA.
- Later, a major US network manufacturer acquired Radiata, at first payment CSIRO for the patent.
- When a team of US lawyers tried picking loopholes to invalidate the patent, payment stopped.
- This started a long patent war between CSIRO and the US manufacturers, but CSIRO won in 2009.
:::

:::{.ch}
- 自從1991年，CSIRO 就生產 WiFi 晶片的原型 (prototype) ，多年來不斷改良，精益求精。
- 1997年，成立 Radiata Communications，旨在簡化 WiFi 晶片生產工序，以降低生產成本。
- 2000年，Radiata 在美國主要國際貿易展上，展示第一款商業 WiFi 晶片，被受製造商青睞。
- 後來，美國一家主要網路製造商，收購 Radiata。起初，根據協議向 CSIRO 支付專利費用。
- 漸漸地，付款停止。當時一群美國律師，試圖鑽專利細節漏洞，挑戰優先權，令專利無效。
- 這一下子，引發 CSIRO 和美國製造商之間，漫長的專利戰，最終 CSIRO 在2009年獲勝訴。
:::

#

:::{.en}
<div class="imagecap" style="width:98%">
<img src="../apple/Apple-iBook-AirPort-1999.jpg" style="width:48%"
     alt="Apple iBook with AirPort had WiFi connection." onclick="showPopup(this)" />
<img src="../apple/Apple-iBook-with-Steve-Jobs.jpg" style="width:50.5%"
     alt="Steve Jobs demonstrated iBook in MacWorld." onclick="showPopup(this)" />
</br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:98%">
<img src="../apple/Apple-iBook-AirPort-1999.jpg" style="width:48%"
     alt="Apple iBook 附有 AirPort，具備 WiFi 連線。" onclick="showPopup(this)" />
<img src="../apple/Apple-iBook-with-Steve-Jobs.jpg" style="width:50.5%"
     alt="Steve Jobs 在 MacWorld 發佈會上，展示 iBook。" onclick="showPopup(this)" />
</br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- At late 1990s WiFi technology was available, but few computers had built-in WiFi due to high cost.
- In July 1999 at MacWorld, Apple released their iBook series of laptops, featuring the AirPort.
- This was the first mass consumer product to offer fast and affordable WiFi network connectivity.
- Steve Jobs demonstrated wireless Internet by walking about on stage with the laptop in his hand.
- [Like a magician](https://www.youtube.com/watch?v=3iTNWZF2m3o), he passed the iBook through a hula hoop while the audience cheered with awe.
:::

:::{.ch}
- 1990年代後期，WiFi 技術日趨成熟，但成本很高，產品售價高昂，電腦內置 WiFi，十分少見。
- 1999年7月，蘋果公司在 MacWorld 上，發佈 iBook 系列筆記本電腦，配備 AirPort 無線上網。
- 這是首款大眾消費產品，提供 WiFi 網絡連線，快速可靠，經濟實惠，讓人喜出望外，耳目一新。
- 發佈會上，Steve Jobs 手持筆記本電腦，在台上走來走去，示範無線連接互聯網，瀏覽蘋果網頁。
- [像魔術師一樣](https://www.youtube.com/watch?v=3iTNWZF2m3o)，他托起 iBook ，穿過呼啦圈，一眾嘩然。此後，WiFi 聲名大噪，人人爭先想要。
:::

#

:::{.small}
The story of Wi Fi【12:18】cc <br>
<a href="https://youtu.be/esA9YhdgvIg">
<img src="http://img.youtube.com/vi/esA9YhdgvIg/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- This is the Australian version, from Australian Broadcasting Corporation (ABC) first aired in 2009.
- Also, Dr John O'Sullivan of CSIRO was awarded Australia's Prime Minister's Science Prize for 2009.
- Members of John's team related the problems and solutions in making wireless connection possible.
- The CSIRO solution delivered high-speed WiFi, became a key part of the industry standard for WiFi.
- See how CSIRO fought the David vs Goliath battle to get patent royalties from the technology giants.
:::

:::{.ch}
- 澳洲版本的 WiFi 起源故事，由澳洲廣播公司 (ABC) 於2009年首次播出，內容強調 CSIRO 的貢獻。
- 此外，CSIRO 的 John O'Sullivan 博士，榮獲2009年澳洲總理科學獎，讚揚他的 WiFi 研究及功績。
- John 是電波天文學家，他的團隊各成員，講述如何實現無線連接，分析遇到的問題，及解決方案。
- CSIRO 的研究成果，提供高速而可靠的 WiFi，成為 WiFi 標準的關鍵部分，亦設計生產 WiFi 晶片。
- 瞭解 CSIRO 如何從國際科技巨頭，取得專利使用費，過程是一場以卵擊石、以弱制強的專利戰爭。
:::

#

:::{.small}
2013 Lifetime Achievement - Vic Hayes【12:31】cc <br>
<a href="https://youtu.be/Kl4dtLfnCBM">
<img src="http://img.youtube.com/vi/Kl4dtLfnCBM/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- Vic Hayes, the Father of WiFi, accepted his Lifetime Achievement at the 3rd Annual Lovie Award.
- In his acceptance speech, Vic thanked the people helping him to establish a standard for WiFi.
- He explained how WiFi technology enabled broadband internet access for remote communities.
- The Lovie Award is the leading European award recognizing excellence in Internet technology.
- It is named in honour of [Ada Lovelace](gift06.html), credited as being the worldʼs first computer programmer.
:::

:::{.ch}
- 2013年，享有「WiFi 之父」之稱的 Vic Hayes，在第三屆年度 Lovie 頒獎台上，接受終身成就獎。
- 在獲獎感言中，Vic 提到 NCR 的工作，並對幫助他最終制定 WiFi 標準的各界人士，致以衷心感謝。
- 他解釋 WiFi 技術的廣泛應用，如何為偏遠社區的普羅大眾，提供寬頻網絡連線，人人受惠。
- Lovie 獎項是歐洲領先的科技大獎，旨在表彰網絡技術領域的卓越成就，於2011年首次頒發。
- 獎項的命名，是紀念 [Ada Lovelace](gift06.html)。她眼光獨到，遠見卓識，被認為是世上第一位電腦程式設計師。
:::

#

:::{.small}
Bose | The Gift【22:37】cc <br>
<a href="https://youtu.be/9kIPvDYPJ1Y">
<img src="http://img.youtube.com/vi/9kIPvDYPJ1Y/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- One component of WiFi technology is the use of FFT (Fast Fourier Transform) for signal analysis.
- The central idea behind FFT is how to break down a complex waves according to its frequencies.
- My webpage has interactive elements to visualise how waves are related to circular motions.
- Combining different waves can be computed by adding circular motions with different periods. 
- This is applied to noise-cancellation: wave + wave = no wave, because the waves cancel out.
- Amar Bose, an acoustic pioneer, had the idea of a noise-cancellation headphone on a plane trip.
:::

:::{.ch}
- WiFi 技術，由許多部分組成。其中之一是使用 FFT（快速 Fourier 變換），進行訊號分析。
- FFT 背後的中心思想，是如何根據頻率（frequency），分解複雜的波形（waveform）。
- 我的網頁，內有互動元素，以動畫顯示：波形與圓週運動（circular motion）的關係。
- 透過疊加不同週期的圓週運動，可以計算組合不同的波形。反之亦然，任何波形可作分解。
- 這適用於噪音消除（noise-cancellation）：波浪 + 波浪 = 平靜無波浪，因為兩個波形互相抵消。
- Amar Bose 是一位聲學工程先驅，在一次飛行中，機上噪音難以忍受，萌生「降噪耳機」的想法。
:::

#

:::{.small}
Voyager: The Grand Tour - Dot of Documentary【29:48】cc <br>
<a href="https://youtu.be/39U508PeItM">
<img src="http://img.youtube.com/vi/39U508PeItM/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- Another component of WiFi technology is the use of error-correcting codes for reliable transmission.
- Such codes enabled us to receive clear fly-by images of planets during the Grand Tour of Solar System.
- Two spacecrafts passed over Jupiter, Saturn, Uranus and Neptune, they aligned once every 174 years.
- Picking up faint signals from faraway spacecrafts would be either missed or altered, these are errors.
- Error-correctig codes include extra specific signals to locate and fix such errors, i.e. using redundancy.
- This is similar to talking in a noisy room when your friend is at the other end, you will add gestures.
:::

:::{.ch}
- WiFi 技術，另一個組成部分，是使用「錯誤糾正碼」（error-correcting code），實現可靠的傳輸。
- 這些糾錯碼，使我們能夠在太陽系「偉大巡遊」（Grand Tour）期間，接收到清晰的飛越行星影像。
- 兩艘宇宙飛船，經過木星、土星、天王星和海王星。這四大外行星，每隔 174 年，才貼近一次。
- 從遙遠的宇宙飛船，接收到的微弱訊號要麼會被違失，要麼會被更改，這些都是訊號「錯誤」。
- 糾錯碼添加額外的特定訊號，來指出和修復此類錯誤，即使用冗餘（redundancy），增強可靠性。
- 這個方法，有點似在嘈吵的房間裡交談。當你的朋友在房間另一端時，你會添加手勢，表達意思。
:::

#

:::{.small}
Joan Baez : Farewell Angelina, Bratislava 1989【3:59】<br>
<a href="https://youtu.be/GoIbsHw8-54">
<img src="http://img.youtube.com/vi/GoIbsHw8-54/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- Xmas marks the end of the year, so I choose this song [Farewell Angelina](../songs/FarewellAngelina.html) by Bob Dylan.
- He offered this song to his folk partner Joan Baez, perhaps saying goodbye to his folk style.
- The lyrics was a mix of fantasy with a world-ending scenario, ending with:
-
| _Call me any name you like I will never deny it_
| _But Farewell Angelina_
| _The sky is erupting_
| _And I must go where it’s quiet_

- To everyone's surprise, Bob Dylan won the 2016 Nobel Prize in literature, even shocking himself.
:::

:::{.ch}
- 聖誕節，標誌著一年的結束，所以選擇了 Bob Dylan 這首歌[《告別安祖蓮娜》Farewell Angelina](../songs/FarewellAngelina.html)。
- Bob Dylan 把這首歌，送給他的民謠伴侶 Joan Baez。或許，他是為了自己的民謠風格，說聲再見。
- 歌詞充滿幻想，混合世界末日的場景，最尾段英文是：
-
| _[你如何呼名辱罵，我一律不否認。] Call me any name you like I will never deny it_
| _[安祖蓮娜，要告別講再見，] But Farewell Angelina_
| _[漫天怒吼，似將爆噴火焰，] The sky is erupting_
| _[必須趕往清靜地，讓我享受安寧。] And I must go where it’s quiet_

- 幾乎沒有人預料到，Bob Dylan 獲得2016年諾貝爾文學獎。他自己得知消息，也感到愕然。
:::

<!--

:::{.en}
- For the [Xmas 2022](../2022/xmas.html) "gift" the theme is WiFi, which is the symbol 🛜, or wireless Internet access.
- Australians invented WiFi? Aussie cheered, of course, but the others booed.
- The truth is an interesting story that needs to be told.
- Failed to study black holes, but lead in wireless Internet access, life is a blessing and a curse.
- But when WiFi came out, no one paid much attention to it. Until the godfather of Apple showed up to demonstrate, it was like waking up from a dream.
- It is true that no one is plowing the thin land, and everyone is fighting for the plowing.
- The videos have two stories of invention. The song composer is a Nobel Prize winner in literature. There is no mistake! .
:::

:::{.ch}
- [2022年聖誕節](../2022/xmas.html)「禮物」，主題是WiFi，即符號 🛜，或無線上網。
- 澳洲人發明 WiFi？Aussie 當然喝彩，其他人卻喝倒彩。
- 實情如何，堪稱回味。
- 研究黑洞失敗，但領先無線上網，人生就是禍褔相依。
- 但WiFi面世，卻無人問津，直至蘋果教父現身示範，果迷如夢初醒。
- 真是瘦田無人耕，耕開人人爭。
- 視頻是兩段發明故事，選歌作曲者是諾貝爾文學得獎人，冇攪錯！。
:::

2025/xmas
https://jhlchan.bitbucket.io/2022/xmas.html
(video?: Bob Dylan/Joan Baez)
2022年聖誕節「禮物」，主題是WiFi，即符號 🛜，或無線上網。澳洲人發明WiFi？Aussie當然喝彩，其他人卻喝倒彩。實情如何，堪稱回味。研究黑洞失敗，但領先無線上網，人生就是禍褔相依。但WiFi面世，卻無人問津，直至蘋果教父現身示範，果迷如夢初醒。真是瘦田無人耕，耕開人人爭。視頻是兩段發明故事，選歌作曲者是諾貝爾文學得獎人，冇攪錯！。

Joan Baez - Farewell Angelina (France, 1966)【3:15】Live b/w
https://www.youtube.com/watch?v=eWecjQ_-DU0
or
Joan Baez : Farewell Angelina, Bratislava 1989【3:59】Live color
https://www.youtube.com/watch?v=GoIbsHw8-54

-->


<!--
pandoc -t revealjs -s --mathjax gift11.md -o gift11.html
-->
