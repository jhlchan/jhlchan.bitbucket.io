---
title: Lunar New Year 2023 Highlights
author: <span class="en">Bluetooth</span><span class="ch">藍牙</span>
date: <span class="en">Lunar New Year 2026</span><span class="ch">2026年農曆新春</span>
theme: solarized
header-includes: |
    <meta property="og:type" content="website" />
    <meta property="og:title" content="2023年新春禮物" />
    <meta property="og:image" content="../images/highlight-12.jpg" />
    <base target="_blank"/>
    <link rel="stylesheet" type="text/css" href="../css/slides.css"></link>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script type="text/javascript" src="../scripts/site.js"></script>
include-before: |
    <!-- Any raw HTML code right after body tag, before anything else. -->
    <div id="modal" class="modal">
      <img id="modal-content" />
      <div id="modal-caption"></div>
    </div>
    <div align="right" class="banner">
    <input type="radio" name="lang" onclick="set_lang('en')" id="english" value="English" checked /> English |
    <input type="radio" name="lang" onclick="set_lang('ch')" id="chinese" value="Chinese" /> 中文
    </div>
include-after: |
    <!-- Any raw HTML code right before body end tag, after anything else. -->
    <script>window.onload = check</script>
    <!-- file:///Users/josephchan/jc/q/site/slides/gift12.html -->
    <!-- https://jhlchan.bitbucket.io/slides/gift12.html -->
...

#

<!--
fa fa-bluetooth
https://www.w3schools.com/icons/tryit.asp?filename=tryicons_fa-bluetooth
<title>Font Awesome Icons</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
the script cannot be local, e.g. as ../font-awesome.min.css
just read: ../others/font-awesome.css

<p>Unicode:</p>
<i style="font-size:24px" class="fa">&#xf293;</i>

-->

:::{.en}
<img src="../images/gift12_en.jpg" width="90%" />

- For the [2023 Lunar New Year](../2023/lny.html) "gift" the theme is Bluetooth <i style="font-size:24px" class="fa">&#xf293;</i> for wireless connection.
- Last time WiFi is about wireless connections among computers over a local network.
- This time Bluetooth is about wireless connections among accessories to a computer.
- Although wireless broadcast is public, these connections keep data transmission private!
:::

:::{.ch}
<img src="../images/gift12_ch.jpg" width="90%" />

- [2023年農曆新春](../2023/lny.html)「禮物」主題是藍牙 = Bluetooth <i style="font-size:24px" class="fa">&#xf293;</i>，即無線連接。
- 上一次 WiFi，是關於本地網絡與電腦之間，進行無線連接。
- 這一次 藍牙，是關於一台電腦與配件之間，進行無線連接。
- 雖然無線廣播是公開，但這些無線連接，卻可以保障傳輸資料的私隱！
:::


#

:::{.en}
<div class="imagecap" style="width:98%">
<img src="../images/old-radio-tuning.jpg" style="width:47%"
     alt="Tuning a radio set for the correct frequency to receive broadcast." onclick="showPopup(this)" />
<img src="../images/electronic-interference-and-antennas.jpg" style="width:51%"
     alt="Television signals can be jammed by a strong pulse of electromagnetic radiation." onclick="showPopup(this)" />
</br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:98%">
<img src="../images/old-radio-tuning.jpg" style="width:47%"
     alt="把收音機調諧到正確的頻率，以接收廣播。" onclick="showPopup(this)" />
<img src="../images/electronic-interference-and-antennas.jpg" style="width:51%"
     alt="電視訊號，會受到強烈電磁輻射脈衝的干擾。" onclick="showPopup(this)" />
</br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- For radio communication, it is fundamental that both sender and receiver tune to the same frequency.
- If you keep using the same frequencey, sooner or later an eavesdropper will discover the frequency.
- Moreover, someone can jam the communication by sending another signal to confuse the receiver.
- To avoid these, one way is to keep changing the frequencey, the problem is how to sync both parties.
- Another way is to send using a lot of frequencies, the problem is how to encrypt with a secure key.
- Such methods of secret communication will lead us to two people: Hedy Lamarr and Claude Shannon.
:::

:::{.ch}
- 無線電通訊的基本原理，是發送器和接收器，都必須頻率相同。收音機需「調諧」至廣播電台。
- 如果長期繼續，使用單一頻率，竊聽者遲早會發現該頻率，訊息毫無秘密可言。
- 此外，其他壞人，可以透過發送另一相同頻率訊號，來混淆接收者，干擾通訊。
- 為避免竊聽和干擾，其中一種方法，是不斷改變頻率。當然，問題是如何雙方同步，保持變頻。
- 另一種方法，是使用多個頻率，發送訊息，無從干擾。當然，問題是如何安全加密，不被破解。
- 因此，這些通訊交流方法，可作保守秘密。將引入兩名人士：Hedy Lamarr 和 Claude Shannon。
:::

#

:::{.en}
<div class="imagecap" style="width:98%">
<img src="../lamarr/Hedy-Kiesler-at-Salzburg-1935.jpg" style="width:31%"
     alt="Hedy Kiesler at Salzburg of Austria in 1935." onclick="showPopup(this)" />
<img src="../lamarr/Vienna-1900-street-with-tram.jpg" style="width:67.5%"
     alt="The CSIRO team celebrated their victory of the WiFi patent war." onclick="showPopup(this)" />
</br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:98%">
<img src="../lamarr/Hedy-Kiesler-at-Salzburg-1935.jpg" style="width:31%"
     alt="1935年，Hedy Kiesler 在奧地利 Salzburg。" onclick="showPopup(this)" />
<img src="../lamarr/Vienna-1900-street-with-tram.jpg" style="width:67.5%"
     alt="1900年代維也納街景，公共交通工具包括有軌電車。" onclick="showPopup(this)" />
</br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- Young Hedy Kiesler was curious, she took a music box apart then assembled it back to working order.
- Her father took her for evening walks, talking to her about the electric trams and electric motors.
- Her mother took her to opera, so she learned about music and dance, playing piano, reciting dialogue.
- The 1900s was a period of new ideas and inventions: airships, airplanes, electric cars, neon lights, etc. 
- She was captivated by the new fashion of movie advancing science, and dreamed one day to be a star.
:::

:::{.ch}
- Hedy Kiesler 年輕時，十分好奇。她會拆開音樂盒，研究零件，然後砌回原狀，恢復奏音樂。
- 父親黃昏帶她散步，與她暢談科技，理解電車和電動馬達。雖然不求甚解，但她滿腦子想像。
- 媽媽帶她去看歌劇，所以她對音樂和舞蹈，甚感興趣。她曉得彈鋼琴，又會自言自語唸台詞。
- 1900年代初期，科技進步，發明各式各樣：飛艇、飛機、電動車、霓虹燈等，有如百花齊放。
- 電影在新時代成風，製作結合科學，日新月異。她對此深深吸引，夢想有朝一日，成為明星。
:::

#

:::{.en}
<div class="imagecap" style="width:98%">
<img src="../lamarr/hedy-lamarr-in-ecstasy.png" style="width:34%"
     alt="A scene in Ecstasy (1933) featuring Hedy Kiesler." onclick="showPopup(this)" />
<img src="../lamarr/Hedy-Lamarr-on-board-Normandie.jpg" style="width:65%"
     alt="Hedy on board the steam liner Normandie, on her way to New York." onclick="showPopup(this)" />
</br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:98%">
<img src="../lamarr/hedy-lamarr-in-ecstasy.png" style="width:34%"
     alt="Hedy Kiesler 主演的《神魂顛倒》（Ecstasy，1933年）其中一幕。" onclick="showPopup(this)" />
<img src="../lamarr/Hedy-Lamarr-on-board-Normandie.jpg" style="width:65%"
     alt="諾曼第號（Normandie）蒸汽客輪上的 Hedy，正前往紐約。" onclick="showPopup(this)" />
</br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- Hedy appeared in a Czech movie with nude scenes, ending her dream. She became an actress in opera.
- Soon she married a businessman in Austria, who was wealthy as he was a well-connected arms dealer.
- When she learned that her husband was dealing with the Nazies, she escaped to London for a new life.
- As Jew actors/actresses in Europe were outcasted by Nazies, the MGM boss was recruiting in London.
- On the return trip to America, Hedy impressed him so much that she was signed up, as Hedy Lamarr. 
:::

:::{.ch}
- Hedy 演出一齣捷克電影，在戲中露體，明星夢即時破滅。她搖身一變，成為一名歌劇女演員。
- 不久，她嫁給奧地利一位富豪。他是一名人脈廣泛的軍火商，她享受豪華生活，無後顧之憂。
- 不過，她逐漸得知丈夫，原來正與納粹打交道。她毅然離鄉別井，逃往倫敦，重拾自信生活。
- 由於納粹黨興起，歐洲的猶太裔演員，紛紛被排斥。當時，米高梅老闆正在倫敦，招募人才。
- 返回美國途中，Hedy 在他眼前出現，讓他留下深刻印象。扺步後，她獲簽約為 Hedy Lamarr。
:::

#

:::{.en}
<div class="imagecap" style="width:98%">
<img src="../lamarr/Hedy-Lamarr-Ziegfeld-Girl-color.jpg" style="width:33%"
     alt="Hedy Lamarr in the movie Ziegfeld Girl (1941)." onclick="showPopup(this)" />
<img src="../lamarr/hedy-lamarr-double-life-sample-02.jpg" style="width:65%"
     alt="Hedy Lamarr was an amateur inventor as well as a star." onclick="showPopup(this)" />
</br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:98%">
<img src="../lamarr/Hedy-Lamarr-Ziegfeld-Girl-color.jpg" style="width:33%"
     alt="Hedy Lamarr 主演電影《齊格菲女郎》（Ziegfeld Girl，1941年）。" onclick="showPopup(this)" />
<img src="../lamarr/hedy-lamarr-double-life-sample-02.jpg" style="width:65%"
     alt="Hedy Lamarr 既是一位業餘發明家，也是一位明星。" onclick="showPopup(this)" />
</br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- Hedy Lamarr made her name in Hollywood, but she was an inventor and worried about war in Europe.
- America was not involved, but her mother was in Europe and supplies across Atlantic were sabotaged.
- In her spare time Hedy worked on her idea of beating the Nazi: an unjammable wireless torpedo.
- She reasoned that if radio signals to torpedo keep changing frequency, that'd be hard to jam.
- By taking apart a radio remote-controller, she worked out a circuit able to change frequencies.
- But she could not work out how to keep the emitter and receiver changing frequencies _in sync_.
:::

:::{.ch}
- Hedy Lamarr 在荷里活，一舉成名。她亦是發明家，曾獻策改良軍機外型設計，並且擔心歐洲戰爭。
- 美國仍未參戰，但她的母親滯留在歐洲，時局形勢，每況愈下。跨大西洋的物料供應，屢次遭破壞。
- 在業餘時間，Hedy 研究方法，如何擊敗納粹德國。她新奇的想法：一種不可干擾的無線控制魚雷。
- 她推斷，如果遙控魚雷的無線電訊號，不斷改變頻率，敵人無法調頻干擾，魚雷就輕鬆地擊中目標。
- 當時有新產品，是座地收音機無線電遙控器。她拆解遙控器，設計無線控制電路，能夠改變頻率。
- 但她遇上難題，無法解決：發射器和接收器，頻率改變必須 _同步_，保持一致。如何能做到這一點？
:::

#

:::{.en}
<div class="imagecap" style="width:98%">
<img src="../lamarr/George-Antheil-1936.jpg" style="width:24.5%"
     alt="George Antheil in 1936, known as the 'Bad Boy of Music'." onclick="showPopup(this)" />
<img src="../lamarr/Hedy-Lamarr-and-her-patent.jpg" style="width:75%"
     alt="Hedy Lamarr with her 1941 patent in background." onclick="showPopup(this)" />
</br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:98%">
<img src="../lamarr/George-Antheil-1936.jpg" style="width:24.5%"
     alt="1936年的 George Antheil，被稱為「音樂界壞男孩」。" onclick="showPopup(this)" />
<img src="../lamarr/Hedy-Lamarr-and-her-patent.jpg" style="width:75%"
     alt="Hedy Lamarr，背景是她1941年的專利。" onclick="showPopup(this)" />
</br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- Hedy had a breakthrough when she met George Antheil, a musician who got pianos played _in sync_.
- Each was a self-playing piano, called pianola, with keyboard activated by a paper roll with marks.
- In one production, George arranged 4 pianola to start simultanously, each playing a different part.
- Hedy incorporated the marked paper rolls in her design, together they filed a patent for the invention.
- They urged the Navy to implement their invention, but the patent remained hidden in Navy's archives.
- Hedy Lamarr's novel and brilliant idea is now called FHSS (Frequency Hopping Spread Spectrum).
- Hedy knew that frequency-hopping is the key for a "Secret Communication System", her patent's title.
:::

:::{.ch}
- Hedy 的突破，是當她遇到音樂家 George Antheil。他安排一列鋼琴，同步演奏，她想知其中奧妙。
- 每台鋼琴，都是一架自動演奏鋼琴（pianola），內部安裝有標記的紙卷，鋼琴鍵由紙卷帶動敲打。
- 其中一部舞台作品，George 擺放 4 台演奏鋼琴，同時起動紙卷，每台鋼琴，演奏樂章不同的部分。
- Hedy 將標記紙卷，融入她的變頻設計中。兩人合作，構思同步變頻系統，並為這項發明申請專利。
- 兩人敦促海軍，把發明付諸實行，改變戰局。可惜無人理會，專利長期封麈，埋藏在海軍檔案中。
- Hedy Lamarr 新穎絕妙的想法，現稱為 FHSS（跳頻展頻，Frequency Hopping Spread Spectrum）。
- Hedy 把發明，稱爲「秘密通訊系統」。因為她明白，跳頻是關鍵：跟不上頻變，竊聽者無計可施！
:::

#

:::{.en}
<div class="imagecap" style="width:98%">
<img src="../lamarr/Washington-to-London-link.png" style="width:57%"
     alt="Hotline is a wireless telephone connection from US to UK." onclick="showPopup(this)" />
<img src="../lamarr/two-factor-authentication.jpg" style="width:42%"
     alt="One Time Passcode is used in two-factor authentication." onclick="showPopup(this)" />
</br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:98%">
<img src="../lamarr/Washington-to-London-link.png" style="width:57%"
     alt="「熱線」（Hotline），是從美國到英國的無線電話連接。" onclick="showPopup(this)" />
<img src="../lamarr/two-factor-authentication.jpg" style="width:42%"
     alt="一次性密碼（One Time Passcode），用於雙重認證。" onclick="showPopup(this)" />
</br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- Hedy's idea was ignored, but actually there was an urgent need for secret communication at wartime.
- The US president needed a phoneline to UK prime minister, especially after the Pearl Harbour attack.
- A radio signal team from Army was delegated the task to setup such a wireless phoneline, or Hotline.
- For Hotline not to be jammed, they used many frequencies together, but signals would be intercepted.
- For signals to be useless to the interceptor, they asked a cryptographer to find an unbreakable cipher.
- Claude Shannon suggested the _one time pad_: a cipher with a random key as long as the message itself.
- This is similar to _One Time Passcode_ (random digits generated on-the-fly) in two-factor authentication.
:::

:::{.ch}
- Hedy 的想法，被打入冷宮。但萬萬想不到，當時戰雲密布，實在有迫切需要，雙方進行秘密通訊。
- 歐洲戰火四起，美國總統與英國首相，缺乏直通電話。尤其是在偷襲珍珠港後，需求更迫在眉睫。
- 隸屬陸軍司令的無線電通訊組，臨危接受一項任務：建立一條安全保密的無線電話線，即「熱線」。
- 熱線訊號，為了不受外界干擾，設計同時使用多種頻率。干擾是片面，不能全面，但訊號容易攔截。
- 熱線訊號，為了讓攔截者得無所用，內容要加密。唯有徵求密碼學家意見，有冇牢不可破的密碼。
- Claude Shannon 建議使用 _一次性密碼本_（one time pad）：密碼的密鑰是隨機，長度與訊息一樣。
- 這類似於「雙重認證」中的 _一次性密碼_（one time passcode）：數字是即時隨機生成，次次不同。
:::

#

:::{.en}
<div class="imagecap" style="width:98%">
<img src="../lamarr/SIGSALY-exhibit-in-museum.jpg" style="width:45%"
     alt="Exhibit of a reconstructed model of the Hotline, named SIGSALY, in a museum. " onclick="showPopup(this)" />
<img src="../lamarr/SIGSALY-exhibit-with-vinyls.jpg" style="width:54%"
     alt="The vinyl record to store the long random key in SIGSALY." onclick="showPopup(this)" />
</br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:98%">
<img src="../lamarr/SIGSALY-exhibit-in-museum.jpg" style="width:45%"
     alt="博物館中展示「熱線」重建模型，名為 SIGSALY。" onclick="showPopup(this)" />
<img src="../lamarr/SIGSALY-exhibit-with-vinyls.jpg" style="width:54%"
     alt="在 SIGSALY 中的黑膠唱片，儲存長長的隨機密鑰。" onclick="showPopup(this)" />
</br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- Recall from [cryptography](gift10.html) that a cipher works with a key. The longer the key, the stronger the cipher.
- The random bits of the key were recorded as soundtrack in a gramophone record (no disk in 1940s).
- The Hotline was to mask the telephone speech by random noise, so interceptors got only gibberish.
- The original record was kept in US, and a copy was sent to UK by the ambassador escorted by Navy.
- The Hotline had two crude terminals, one under Pentagon, and one under a London department store.
- After the war the machines were dismantled, relevant documents were either destroyed or classified.
- The wonderful idea developed for the Hotline is now called DSSS (Direct Sequence Spread Spectrum).
:::

:::{.ch}
- 回想[密碼學](gift10.html)，密碼使用與密鑰匹配。密鑰越長，密碼越強。隨機性及一次性，保證密碼牢不可破。
- 密鑰的隨機位元（bits），轉爲音訊，記錄在黑膠唱片中的音軌（1940年代，沒有 USB 隨身碟）。
- 「熱線」的無線語音通訊，是把電話語音，混入隨機噪音。因此攔截一方，只能收聽到胡言亂語。
- 密鑰唱片只作一備份，真本保存在美國，副本由海軍護送的大使，交給英國。唱片用於加密與解密。
- 現在看來，「熱線」由兩台簡陋龐大的終端機組成，分別放在五角大樓地庫、倫敦大百貨商店地庫。
- 大戰結束後，「熱線」無用武之地。全部機器遭拆除，相關文件被銷毀或保密。儼然從歴史上消失。
- 設計「熱線」的想法，相當精彩，現稱為 DSSS（直序展頻，Direct Sequence Spread Spectrum）。
:::

#

:::{.en}
<div class="imagecap" style="width:98%">
<img src="../lamarr/Claude-Shannon-and-Alan-Turing.jpg" style="width:44.5%"
     alt="Both Claude Shannon and Alan Turing are pioneers of the information age." onclick="showPopup(this)" />
<img src="../lamarr/Claude-Shannon-with-communication-model.jpg" style="width:55%"
     alt="Claude Shannon and his model for a mathematical theory of communication." onclick="showPopup(this)" />
</br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:98%">
<img src="../lamarr/Claude-Shannon-and-Alan-Turing.jpg" style="width:44.5%"
     alt="Claude Shannon 和 Alan Turing，都是資訊時代的先驅。" onclick="showPopup(this)" />
<img src="../lamarr/Claude-Shannon-with-communication-model.jpg" style="width:55%"
     alt="Claude Shannon 和他的「通訊數學理論」模型。" onclick="showPopup(this)" />
</br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- So Hedy's idea was forgotten, and Hotline had vanished. Both FHSS and DSSS seemed never existed.
- Claude Shannon had met [Alan Turing](gift05.html) at Princeton when he briefed the US about cryptography in UK.
- Shannon admired Turing's simple and clear abstract model of computation, building up a rich theory.
- He took the same approach, devised a simple abstract model of communication, and build up a theory.
- Claude Shannon published his masterpiece "The Mathematical Theory of Communication" in 1948.
- He applied his theory to analyse cryptography, proving "One Time Pad" was unbreakable in theory.
- The theory revived the ideas FHSS and DSSS, applying to military, civilian and space communications.
:::

:::{.ch}
- 世事就是如此：Hedy 的想法被遺忘，「熱線」亦已消聲匿跡。至於 FHSS 和 DSSS，似乎從未面世。
- 在普林斯頓大學，Claude Shannon 認識 [Alan Turing](gift05.html)。當時，Alan 到美國，介紹英國密碼學的進展。
- Shannon 十分欣賞 Turing 抽的象計算模型。簡單清晰，一步一步建立完整理論，兼有豐富的應用。
- 他採取同樣方法，設計抽象「通訊模型」。雖然簡單，卻一步一步建立整套理論，亦有多方面應用。
- 1948年，Claude Shannon 發表《通訊的數學理論》。這是他精闢原創的代表作，奠定訊息論基礎。
- 他以通訊的數學原理，分析密碼學，證明「一次性密碼本」在理論上，牢不可破，因為密鑰隨機。
- 電子工程師鑽研理論，FHSS 與 DSSS 再重見天日。如今，展頻技術應用於軍事、民用及太空通訊。
:::

<!--

The Impact of Alan Turing: Formal Methods and Beyond
by Jonathan Peter Bowen, April 2018.
https://www.researchgate.net/figure/An-exhibit-on-Alan-Turing-and-Claude-Shannon-in-the-Information-Age-gallery-21-at-the_fig1_332451239
An exhibit on Alan Turing and Claude Shannon, in the Information Age gallery at the Science Museum, London. (Photograph by Jonathan Bowen.)
Claude-Shannon-and-Alan-Turing.jpg

-->

#

:::{.en}
<div class="imagecap" style="width:98%">
<img src="../lamarr/wireless-communication-technology.jpg" style="width:36.5%"
     alt="The diverse applications of wireless communication." onclick="showPopup(this)" />
<img src="../lamarr/Bluetooth-how-it-works.jpg" style="width:63%"
     alt="Bluetooth technology enables wireless connection between devices." onclick="showPopup(this)" />
</br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:98%">
<img src="../lamarr/wireless-communication-technology.jpg" style="width:36.5%"
     alt="無線通訊，應用多樣化。" onclick="showPopup(this)" />
<img src="../lamarr/Bluetooth-how-it-works.jpg" style="width:63%"
     alt="藍牙技術，實現不同裝置之間的無線連接。" onclick="showPopup(this)" />
</br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- Hedy Lamarr had the idea of frequency hopping, and Claude Shannon put cipher in multiple channels.
- Spread-spectrum technology, both FHSS and DSSS, is widely used to develop modern communication.
- Global Positioning System (GPS) with DSSS was developed by Navy to locate their vessels worldwide.
- Cellular network for mobile phones had standards for 2G, 3G, etc. first using DSSS, later using FHSS.
- WiFi standard for wireless connection in local area network allowed both, now most adopted DSSS.
- Bluetooth standard for devices to form a personal area network had FHSS during pairing of devices.
- All these technologies require secure communication: signals are delivered only to intended recipient.
:::

:::{.ch}
- 安全通訊，不受干擾，內容保密。Hedy Lamarr 提出跳頻方法，Claude Shannon 加密碼入多頻道。
- 無線電通訊的近代發展，廣泛應用展頻技術（FHSS 和 DSSS）。提供安全可靠、不被竊聽的通訊。
- 全球定位系統 (GPS) ，由海軍率先開發，爲其船艦在全球範圍內計算方位。其中技術，具備 DSSS。
- 手機流動蜂窩網絡 (cellular network) ，有 2G、3G 等標準。規格原先使用 DSSS，後來改用 FHSS。
- 區域網絡 (local area network) 無線連接的 WiFi 標準，規格中兩者皆可選用。現在普遍採用 DSSS。
- 個人區域網絡 (personal area network) 的裝置，連接採用藍牙標準。裝置配對期間，就用上 FHSS。
- 所有這些技術，都支援安全通訊：訊號傳遞，只送到指定接收者，外人無法得知，無從干擾或改動。
:::

#

:::{.en}
<div class="imagecap" style="width:98%">
<img src="../lamarr/Bluetooth-devices-pairing.jpg" style="width:42%"
     alt="Bluetooth devices establish frequency hopping pattern by pairing." onclick="showPopup(this)" />
<img src="../lamarr/Milstar-award-to-Hedy-Lamarr-2.jpg" style="width:57%"
     alt="Hedy Lamarr won the Milstar Award in 1997, pictured here is the award statement." onclick="showPopup(this)" />
</br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:98%">
<img src="../lamarr/Bluetooth-devices-pairing.jpg" style="width:42%"
     alt="藍牙設備，透過配對，建立跳頻模式。" onclick="showPopup(this)" />
<img src="../lamarr/Milstar-award-to-Hedy-Lamarr-2.jpg" style="width:57%"
     alt="Hedy Lamarr 於1997年榮獲 Milstar 獎，圖示獎項的聲明。" onclick="showPopup(this)" />
</br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- Bluetooth, nickname of Harald Blåtann who united Denmark and Norway, unites different devices.
- Pairing of <i style="font-size:24px" class="fa">&#xf293;</i> devices is to establish the pattern of frequency hopping, like a couple dancing _in sync_.
- Isn't it amazing that both WiFi and Bluetooth are derived from schemes for secret communication?
- Hedy Lamarr, a movie star as well as an inventor, had an ingenious solution via frequency hopping.
- When her patent was declassified in 1981 and recognized by experts, Hedy had retired in Florida.
- Experts interviewed Hedy, found that her idea was original, and lobbied hard for her recognition.
- As Hedy's story became known, she received many awards. Her son accepted them on her behalf.
- E.g., Milstar award in 1997, "for inspiring the modern era of secure military communications ..."
- Milstar, with a network of satellites, is the space communication command system for US president. 
:::

:::{.ch}
- 「藍牙」（Bluetooth）是維京戰士 Harald Blåtann 的暱稱，統一丹麥和挪威，而 <i style="font-size:24px" class="fa">&#xf293;</i> 團結不同裝置。
- <i style="font-size:24px" class="fa">&#xf293;</i> 裝置配對（pairing），是建立跳頻模式。兩者頻率齊齊改變，像一對情侶，同步翩翩起舞一樣。
- 誰會想到，展頻技術的應用：GPS、手機網絡、WiFi 和藍牙，都源於秘密通訊的解決方案？真棒！
- 電影明星兼發明家 Hedy Lamarr，提出巧妙的解決方案：跳頻。她想得出、做得到，但無人賞識。
- 1981年，Hedy 的專利終於解封，並獲通訊專家認可。她早已淡出娛樂圈，在佛羅裡達州享受退休。
- 專家們訪問 Hedy，與她詳談，認為想法具原創性。各方努力游說，希望 Hedy 的貢獻，得到認同。
- Hedy 的故事，大眾津津樂道，獎項接踵而來。歲月催人，她沒有出席頒獎禮，均由兒子上台代領。
- 例如，1997年的 Milstar 獎座有刻字：「表彰她以跳頻概念，啟發安全軍事通訊系統新一代，.....」
- Milstar 是美國總統的太空通訊指揮系統，擁有衛星網絡，配備先進跳頻科技，確保傳送安全可靠。
:::

#

:::{.en}
<div class="imagecap" style="width:98%">
<img src="../lamarr/Hedy-Lamarr-Algiers-1938-George-Hurrell.jpg" style="width:31.5%"
     alt="This portrait photo is highly praised by Hedy Lamarr herself." onclick="showPopup(this)" />
<img src="../lamarr/Hedy-Lamarr-quote-hope-and-curiosity.jpg" style="width:68%"
     alt="A quote from Hedy Lamarr about curiosity and hope." onclick="showPopup(this)" />
</br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:98%">
<img src="../lamarr/Hedy-Lamarr-Algiers-1938-George-Hurrell.jpg" style="width:31.5%"
     alt="Hedy Lamarr 本人對這幅肖像照片，給予高度評價。" onclick="showPopup(this)" />
<img src="../lamarr/Hedy-Lamarr-quote-hope-and-curiosity.jpg" style="width:68%"
     alt="Hedy Lamarr 的金句，關於好奇心和希望。" onclick="showPopup(this)" />
</br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- For this gift, the [main webpage](../2023/lny.html) covers Hedy Lamarr briefly, along with civil applications of her ideas.
- A [Hedy Lamarr page](../lamarr/index.html) covers her life story, with explanation of her ideas and also military applications.
- An [extra page](../lamarr/extra.html) for Hedy Lamarr covers details of her invention, her legacy and her viewpoints of life.
- Please click "show" to reveal hidden contents, and take time to play with the interactive elements.
- She was posthumously inducted in National Inventors Hall of Fame (2014) and IP Hall of Fame (2019). 
:::

:::{.ch}
- 這份禮物，[主題網頁](../2023/lny.html) 簡要介紹 Hedy Lamarr，說明展頻概念，詳細探討與她想法相關的民事應用。
- 另有 Hedy Lamarr [人物網頁](../lamarr/index.html) ，涵蓋她的生平，解釋通訊理論，詳細探討與她想法相關的軍事應用。
- 更有 Hedy Lamarr [附加網頁](../lamarr/extra.html) ，對她的發明、人生觀點、傳承故事及後世評論，提供更多詳細資訊。
- 本期大結局，內容豐富。請點擊「顯示」瀏覽隱藏內容，並花點時間細玩「互動元素」，十分有趣。
- 過身後，她獲追授入選2014年「國家發明家名人堂」。她還在2019年，入選「知識產權名人堂」。 
:::

#

:::{.small}
Bombshell The Hedy Lamarr Story (2017)【1:28:56】cc <br>
<a href="https://youtu.be/3QB_P8gvZ-c">
<img src="http://img.youtube.com/vi/tQhsZg54jFg/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::
<!-- using a different picture, not the video picture -->

:::{.en}
- Hedy Lamarr almost tells her own story, in her own voice, in this incredible 2017 documentary.
- Alexandra Dean, the film director, digged out the 'lost' interview tapes of Hedy by a reporter.
- Hedy's son and daughter talked about their mother, reflecting on their days through her stories.
- After working for six years, the documentary had its premiere at various world film festivals.
- Both the film and the director won awards, e.g. the 2017 NYFCO Award for Best Documentary.
- "Bombshell: The Hedy Lamarr Story is a film for lovers of history, Hollywood and science." Enjoy!
:::

:::{.ch}
- 這齣出色的 2017年紀錄片，幾乎由 Hedy Lamarr 自白，講述自己故事。歴歴在目，令人難以置信。
- 電影女導演 Alexandra Dean，刻意追尋 Hedy 的心聲。終於找回當年記者「遺失」的採訪錄音帶。
- Hedy 的兒子和女兒，談及自己的母親。從她以往的瑣事，反思共處的日子，如何面對她的知名度。
- 紀錄片從籌劃到完成，歷時六年。在世界各地電影節，首映亮相，好評如潮。讓觀眾留下深刻印象。
- 情節出人意表，電影和導演屢獲殊榮，如 2017年 NYFCO（紐約線上影評人協會）最佳紀錄片獎。
- 「《艷星發明家》這一部紀錄電影，絕對適合歷史、荷里活和科學的愛好者，萬勿錯過。」請欣賞！
:::

#

:::{.small}
It's Storytime! - Hedy Lamarr's Double Life【33:41】cc <br>
<a href="https://youtu.be/ii8mAZN4JeU">
<img src="http://img.youtube.com/vi/ii8mAZN4JeU/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- If the documentary is too long, read this book for kids about Hedy Lamarr.
- Hedy Lamarr indeed led a double life: movie star by day, inventor by night.
- Many technical details are grossed over or simplified for the sake of kids.
- The author is a former software engineer and computer science professor.
- Now her full-time job is writing for children, especially on women in STEM.
- The illustrator has been working at animation studios such as Dreamworks.
:::

:::{.ch}
- 如果嫌紀錄片太長，請細閱這一本兒童讀物，描述 Hedy Lamarr 的精釆故事，美麗人生。
- 說 Hedy Lamarr 擁有雙重身份，雖不中亦不遠矣：白天是電影明星，晚上是業餘發明家。
- 為了遷就兒童，許多技術細節被忽略或簡化。雖然如此，作者花上不少心思，勾劃主題。
- 作者以前是軟件工程師，亦曾經教過電腦科學。因此對 Hedy Lamarr 的經歷，或有同感。
- 現在她全職寫兒童讀物，尤其是寫關於 STEM （科學、技術、工程和數學）領域的女性。
- 插畫家曾在 Dreamworks 等動畫工作室繪畫，她善於創造風格化人物、外觀獨特的世界。
:::

#

<!-- 
![](../lamarr/Hedy.Lamarr.An.Incredible.Life.jpg){ width=33% }
![](../lamarr/Hedy-Lamarr-lite-7a.jpg){ width=32% }
A graphic novel based on the life stories of Hedy Lamarr. A sample page in the graphic novel. -->
<!-- image percentages do not add up to 99%, in order to accomodate text. -->

:::{.en}
<div class="imagecap" style="width:98%">
<img src="../lamarr/Hedy.Lamarr.An.Incredible.Life.jpg" style="width:33%"
     alt="A graphic novel based on the life stories of Hedy Lamarr." onclick="showPopup(this)" />
<img src="../lamarr/Hedy-Lamarr-lite-7a.jpg" style="width:32%"
     alt="A sample page in the graphic novel." onclick="showPopup(this)" />
</br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:98%">
<img src="../lamarr/Hedy.Lamarr.An.Incredible.Life.jpg" style="width:33%"
     alt="一部漫畫小說，根據 Hedy Lamarr 的生平故事改編。" onclick="showPopup(this)" />
<img src="../lamarr/Hedy-Lamarr-lite-7a.jpg" style="width:32%"
     alt="漫畫小說其中一頁。" onclick="showPopup(this)" />
</br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- A graphic novel depicting the awe-inspiring life story of Hedy Lamarr using amusing pictures.
- This [picture book](../lamarr/Hedy.Lamarr.An.Incredible.Life.pdf) has 177 pages, with snippets taken from other biographies of Hedy Lamarr.
- "Such a sophisticated, smartly-written graphic novel, with the storylines moving at a brisk pace.
- The novel not only chronicles Hedy Lamarr's independence, drive, creativity, and intelligence ...
- ... but also accentuates her many significant yet little-known achievements beyond tinseltown."
:::

:::{.ch}
- 說精釆，沒有比這本漫畫小說更精釆。用有趣的圖畫，描繪 Hedy Lamarr 的人生經歷，令人驚嘆。
- 精緻[繪畫本](../lamarr/Hedy.Lamarr.An.Incredible.Life.pdf) 共 177頁，其中段落，摘錄自其他 Hedy Lamarr 的傳記。內容略有誇大，但仍算合理。
- 「這是一本雅俗共賞、妙筆生花的漫畫小說，以輕盈爽快的節奏，展開一連串引人入勝的情節。
- 小說描述細膩，圖畫與故事融為一體。不僅記述 Hedy Lamarr 的獨立性、創造力、幹勁和智慧， ...
- ... 還強調她在荷李活以外的顯著成就，許多鮮為人知。絕對是一本流暢緊湊，妙趣横生的傳記。」
:::

#

:::{.small}
Carole King - Tapestry (Live)【3:22】  <br>
<a href="https://youtu.be/2k8P9tkQ8uU">
<img src="http://img.youtube.com/vi/2k8P9tkQ8uU/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- The incredible life of Hedy Lamarr, with ups and downs, is full of colored patches, like a [Tapestry](../songs/Tapestry.html).
- Composed by Carole King, this song became a concept for her album "Tapestry". The lyrics begins:
-
| _My life has been a tapestry_
| _of rich and royal hue,_
| _An ever-lasting vision_
| _of the ever-changing view._

- If you listen to the song carefully, you'll find that the entire lyrics reads like a familiar fairy tale. 
:::

:::{.ch}
- Hedy Lamarr 美妙非凡的一生，跌宕起伏，充滿一格一格的色彩，就像一張[《織錦花毯》Tapestry](../songs/Tapestry.html)。
- 歌曲作者，是美國詞曲作家兼歌手 Carole King，引發她推出專輯《Tapestry》。英文歌詞開頭是：
-
| _[人生多美好，像一幅掛毯，] My life has been a tapestry_
| _[濃鬱的色調，富麗又堂皇。] of rich and royal hue,_
| _[永恆的願景，多彩更多姿，] An ever-lasting vision_
| _[觀點常常變，似閃閃發光。] of the ever-changing view._

- 如果細心聆聽這首歌，會發現整篇歌詞，讀起來彷彿像童話故事，似曾相識。
:::

#

:::{.small}
Blue Danube Waltz (New Years in Vienna)【9:41】 <br>
<a href="https://youtu.be/6mAbVEc5-Pc">
<img src="http://img.youtube.com/vi/6mAbVEc5-Pc/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- Hedy Lamarr was born in Austria, and she loved this piece of music, the unofficial anthem of Austria.
- [The Blue Danube](../songs/BlueDanube.html) by Johann Strauss II is the most famous waltz ever written, with five waltz themes.
- Featured during the Paris World’s Fair in 1867, it created a sensation among classical music lovers.
- With a smooth melody, soft and elegant, free rhythm full of vitality, this waltz belongs to Austrians.
- Indeed, it is a truly iconic piece of music that has earned its place in the classical music hall of fame.
- The graceful and stylish melody is very suitable for ice-skating performances, either [a pair](https://youtu.be/tBcRvR3j7cU) or [single](https://youtu.be/PfFoIX79Y4E).
:::

:::{.ch}
- Hedy Lamarr 出生於奧地利，熱愛祖國。她十分欣賞這首樂章，普遍認為是非正式的奧地利國歌。
- Johann Strauss II（小約翰·史特勞斯）的佳作[《藍色多瑙河》](../songs/BlueDanube.html) ，堪稱是有史以來最著名的圓舞曲。
- 1867年，於巴黎世界博覽會中公開演奏。超可愛又動聽的管弦樂章，在古典音樂界中，引起轟動。
- 《藍色多瑙河》旋律流暢、柔和優雅、節奏自由，生機盎然，的確是一首屬於奧地利人的華麗舞曲。
- 無可否認，這是一首極其出色的音樂作品，真正具標誌性，在古典音樂名人堂中，贏得漂亮的一席。
- 《藍色多瑙河》格調優雅，氣派高尚，流暢自然，非常適合花式滑冰表演，有[雙人項目](https://youtu.be/tBcRvR3j7cU)或[單人項目](https://youtu.be/PfFoIX79Y4E)。
:::

#

:::{.small}
Donovan - Epistle To Derroll (Live in Copenhagen 1975)【6:54】cc <br>
<a href="https://youtu.be/hahpW_P67vQ">
<img src="http://img.youtube.com/vi/hahpW_P67vQ/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- Hedy Lamarr was a star of a different kind, so a song about stars in the deep blue sea is appropriate.
- In [Epistle to Derroll](../songs/EpistleToDerroll.html), Donovan Leitch tells a funny conversation with these stars. The lyrics ends with:
-
| _Contemplating every other_
| _word the starfish said._
| _Whistly winds they filled my dreams_
| _in my dreaming bed._

- This lovely lullaby is the final song from Donovan’s 1967 album "A Gift from a Flower to a Garden".
:::

:::{.ch}
- Hedy Lamarr 是一位明星，但與眾不同。送上一首民謠歌曲，關於蔚藍深海中的星星，尤其合適。
- 在 [《給 Derroll 的書信》Epistle to Derroll](../songs/EpistleToDerroll.html) 中，Donovan Leitch 與星星娓娓而談。英文歌詞結尾是：
-
| _[海星的對話，雋永又迷茫。] Contemplating every other_
| _[沉沉深思考，字字皆繞樑。] word the starfish said._
| _[躺在睡床上，夢寐意難忘。] Whistly winds they filled my dreams_
| _[呼嘯的風聲，帶我入夢鄉。] in my dreaming bed._

- 這首可愛的搖籃曲，是 1967年 Donovan 專輯《A Gift from a Flower to a Garden》中的壓軸歌曲。
:::

<!--

:::{.en}
- For the [2023 Lunar New Year](../2023/lny.html) "gift" the theme is Bluetooth = Bluetooth, that is, the symbol is used for 🎧, or wireless transmission.
- Wireless is convenient, but non-directional = broadcast. Just tune in and you can listen and intercept (think atomic particle radio).
- Bluetooth headsets, only listen to your own music, because everyone's frequency is different.
- But the spectrum is limited (telecommunications companies bid for spectrum from the government). How can everyone in the subway car enjoy Bluetooth?
- The only method is "frequency hopping": the communication frequency between the earphones and the mobile phone is constantly changing but synchronized, like tango!
:::

:::{.ch}
- [2023年農曆新春](../2023/lny.html)「禮物」主題是藍牙 = Bluetooth，即符號用於 🎧，或無線傳輸。
- 無線方便，但無方向性 = 廣播。只需調頻，便可收聽、截聽﹙想想原子粒收音機﹚。
- 藍牙耳機，只收聽自己的音樂，皆因人人頻率不同。
- 但頻譜有限﹙電訊商向政府競投頻譜﹚，如何造到地鐵車箱內人人享用藍牙？
- 唯一方法是「跳頻」：耳機和手機的通訊頻率，不停變、但同步，好似跳 tango！
:::

2026/lny
https://jhlchan.bitbucket.io/2023/lny.html
(video?: Epistle to Derroll)
2023年農曆新春「禮物」主題是藍牙 = Bluetooth，即符號用於 🎧，或無線傳輸。無線方便，但無方向性 = 廣播。只需調頻，便可收聽、截聽﹙想想原子粒收音機﹚。藍牙耳機，只收聽自己的音樂，皆因人人頻率不同。但頻譜有限﹙電訊商向政府競投頻譜﹚，如何造到地鐵車箱內人人享用藍牙？唯一方法是「跳頻」：耳機和手機的通訊頻率，不停變、但同步，好似跳 tango！

誰發明「跳頻」？竟然是 Hedy Lamarr。她是40年代荷里活天后，卻醉心發明。當然啦，女明星是發明家？無人相信！她的出道成名、發明遭冷落、聲譽大翻身，勝過任何荷里活劇本。生前未能贏得奧斯卡，死後卻躋身發明家名人堂。邊個夠佢威？

這一輯是特大製作，除了人物主頁，更附人物副頁。請欣賞多齣人物紀錄片，加上人物漫畫小說。選曲共三首：一首緬懷她的繽紛一生，另一首是她至愛的經典，最後一首幻想她星光熠熠。
-->


<!--
pandoc -t revealjs -s --mathjax gift12.md -o gift12.html
-->
