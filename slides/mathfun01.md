---
title: <span class="en">Math Fun (Episode 1)</span><span class="ch">趣味數學（第1集）</span>
author: <span class="en">Leaping Frogs Puzzle</span><span class="ch">青蛙謎題</span>
date: <span class="en">February 2025</span><span class="ch">2025年2月</span>
theme: solarized
header-includes: |
    <base target="_blank"/>
    <link rel="stylesheet" href="../css/slides.css"/>
    <script type="text/javascript" src="../scripts/site.js"></script>
    <script type="text/javascript" src="../scripts/svg.js"></script>
include-before: |
    <!-- Any raw HTML code right after body tag, before anything else. -->
    <div id="modal" class="modal">
      <img id="modal-content" />
      <div id="modal-caption"></div>
    </div>
    <div align="right" class="banner">
    <input type="radio" name="lang" onclick="set_lang('en')" id="english" value="English" checked /> English |
    <input type="radio" name="lang" onclick="set_lang('ch')" id="chinese" value="Chinese" /> 中文
    </div>
include-after: |
    <!-- Any raw HTML code right before body end tag, after anything else. -->
    <script>window.onload = check</script>
    <!-- file:///Users/josephchan/jc/q/site/slides/mathfun01.html -->
    <!-- https://jhlchan.bitbucket.io/slides/mathfun01.html -->
...

#

:::{.en}
<div class="imagecap" style="width:50%">
<img src="../games/leapfrog/frogs-puzzle.jpg" style="width:100%"
     alt="The frogs and toads puzzle." onclick="showPopup(this)" />
</br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:50%">
<img src="../games/leapfrog/frogs-puzzle.jpg" style="width:100%"
     alt="青蛙和蟾蜍謎題。" onclick="showPopup(this)" />
</br>
<span>點擊放大(再擊關閉)</span>
</div>
:::


:::{.en}
- In the middle of a lilypond there are 7 pieces of stones.
- The middle stone is empty, but left and right sides have 3 frogs and 3 toads.
- Each can either step on an empty stone, or jump over a frog or toad.
- But frogs can only go right, and toads and only go left.
- Can they pass each other so that 3 toads on right and 3 frogs on left?
:::

:::{.ch}
- 月色下的荷花池中央，有 7塊石頭。
- 中間的石頭是空的，但左右兩側有3隻青蛙和3隻蟾蜍。
- 每一隻都可以踩在空石頭上，或跳過一隻青蛙或蟾蜍。
- 但是青蛙只能向右走，蟾蜍只能向左走。
- 牠們能否互相穿過，使得 3隻蟾蜍在右邊，3隻青蛙在左邊？
:::

<!-- Calculator symbol from:
https://www.w3schools.com/icons/tryit.asp?filename=tryicons_fa-calculator
need:
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
use:
<i class="fa fa-calculator"></i>
<i class="fa fa-calculator" style="font-size:24px"></i>
<i class="fa fa-calculator" style="font-size:36px"></i>
<i class="fa fa-calculator" style="font-size:48px;color:red"></i>
-->

<!-- javascript setup for slides -->
<script type="text/javascript">
// locate DOM element
function $(id) {
    return document.getElementById(id)
}
// setup SVG for url(#grid)
var svg = defineGrid(scale)
// wrap inside a zero-dimension div and insert to DOM
var div = document.createElement("div")
div.style.width = '0px'
div.style.height = '0px'
div.appendChild(svg)
div.appendChild(defineArrowHead())
document.body.appendChild(div) // this avoid the problem of only 2 slides can use url(#grid)
// make a blank canvas with background only, customised for slides
function makeCanvasForSlides(view) {
    view = view || '0 0 800 500' // for 45x45 square
    var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg")
    svg.setAttribute('width', '1000px')
    svg.setAttribute('height', '500px')
    // svg.setAttribute('viewBox', '0 0 600 300') // change ratio = 500/300 = 5/3
    // 600 400 gives max 60x40, 700 400 gives max 70x40, 800 400 gives max 80x40
    // svg.setAttribute('viewBox', '0 0 800 500') // ratio = 500/500 = 1
    svg.setAttribute('viewBox', view)
    svg.appendChild(makeBackground())
    return svg
}
// override for slides
makeCanvas = makeCanvasForSlides
var debug = false // true when slides keep refreshing

/* additional drawing utilities */

// make a slide move, from (x,y) for distance d
function makeSlide(x, y, d) {
    return makeArrow(0,0, 0,0, null, 3, 'M' + x * scale + ',' + y * scale + ' h' + d * scale)
}

// make a jump move, from (x,y) for distance d
function makeJump(x, y, d) {
    return makeArrow(0,0, 0,0, null, 3, 'M' + x * scale + ',' + y * scale + ' S' + (x + d/2) * scale + ',' + (y - 8) * scale + ' ' + (x + d) * scale + ', ' + y * scale)
}

// make a dashed arrow, from (px,py) to (qx,qy)
function makeDashedArrow(px, py, qx, qy, color, width, path) {
    var arrow = makeArrow(px, py, qx, qy, color, width, path)
    arrow.setAttribute('stroke-dasharray', "5,5")
    return arrow
}

// local left arrow from (x,y) with extend e
function makeLeft(x, y, e) {
    e = e || 10
    return makeArrow(x - 1,y, x - e + 1.5,y)
}
// local right arrow from (x,y) with extend e
function makeRight(x, y, e) {
    e = e || 10
    return makeArrow(x + 1,y, x + e - 1.5,y)
}
// local left dashed arrow from (x,y) with extend e
function makeLeftDash(x, y, e) {
    e = e || 5
    return makeDashedArrow(x - 1, y  + 1, x - e + 1.5, y + e - 1.5)
}
// local right dashed arrow from (x,y) with extend e
function makeRightDash(x, y, e) {
    e = e || 5
    return makeDashedArrow(x + 1, y  + 1, x + e - 1.5, y + e - 1.5)
}
</script>

#

:::{#slideA}
:::

:::{.en}
- If there are only 1 frog and 1 toad, the solution is shown above 👆.
- Let R = step right, L = step left, J = jump, each move is unique.
- The whole sequence is: RJR, total 3 steps for 1 pair.
- Another sequence is a mirror image: LJL.
:::

:::{.ch}
- 如果只有 1 隻青蛙和 1 隻蟾蜍，則解決方案如上圖所示👆。
- 設 R = 右步，L = 左步，J = 跳躍，每個動作都是獨一無二的。
- 1對的整個步驟序列是：RJR，共3個步驟。
- 另一步驟是鏡像序列：LJL。
:::

<script type="text/javascript">
svg = makeCanvas()
var x = 30, y = 10
svg.appendChild(makeImage('../games/leapfrog/green_frog.png', x +  0 - 4,y - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/brown_frog.png', x + 20 - 4,y - 3, '10%'))
svg.appendChild(makeSlide(x + 0, y, 10))
y = 20
svg.appendChild(makeImage('../games/leapfrog/green_frog.png', x + 10 - 4,y - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/brown_frog.png', x + 20 - 4,y - 3, '10%'))
svg.appendChild(makeJump(x + 20, y, -20))
y = 30
svg.appendChild(makeImage('../games/leapfrog/green_frog.png', x + 10 - 4,y - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/brown_frog.png', x +  0 - 4,y - 3, '10%'))
svg.appendChild(makeSlide(x + 10, y, 10))
y = 40
svg.appendChild(makeImage('../games/leapfrog/green_frog.png', x + 20 - 4,y - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/brown_frog.png', x +  0 - 4,y - 3, '10%'))
// done
$('slideA').appendChild(svg)
</script>

#

:::{#slideB}
:::

:::{.en}
- If there are 2 frogs and 2 toads, the solution is shown above 👆.
- Let R = step right, L = step left, J = jump, each move is unique.
- The whole sequence is: RJLJJLJR, total 8 steps for 2 pairs.
- Another sequence is a mirror image: LJRJJRJL.
:::

:::{.ch}
- 如果有 2 隻青蛙和 2 隻蟾蜍，則解決方案如上圖所示👆。
- 設 R = 右步，L = 左步，J = 跳躍，每個動作都是獨一無二的。
- 2對的整個步驟整個序列是：RJLJJLJR，共8個步驟。
- 另一步驟是鏡像序列：LJRJJRJL。
:::

<script type="text/javascript">
svg = makeCanvas()
svg.appendChild(makeImage('../games/leapfrog/green_frog.png',  10 - 4,10 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/green_frog.png',  15 - 4,10 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/brown_frog.png',  25 - 4,10 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/brown_frog.png',  30 - 4,10 - 3, '10%'))
svg.appendChild(makeSlide(15, 10, 5))
svg.appendChild(makeImage('../games/leapfrog/green_frog.png',  10 - 4,20 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/green_frog.png',  20 - 4,20 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/brown_frog.png',  25 - 4,20 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/brown_frog.png',  30 - 4,20 - 3, '10%'))
svg.appendChild(makeJump(25, 20, -10))
svg.appendChild(makeImage('../games/leapfrog/green_frog.png',  10 - 4,30 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/green_frog.png',  20 - 4,30 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/brown_frog.png',  15 - 4,30 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/brown_frog.png',  30 - 4,30 - 3, '10%'))
svg.appendChild(makeSlide(30, 30, -5))
svg.appendChild(makeImage('../games/leapfrog/green_frog.png',  10 - 4,40 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/green_frog.png',  20 - 4,40 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/brown_frog.png',  15 - 4,40 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/brown_frog.png',  25 - 4,40 - 3, '10%'))
svg.appendChild(makeJump(20, 40, 10))
svg.appendChild(makeImage('../games/leapfrog/green_frog.png',  40 + 10 - 4,10 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/green_frog.png',  40 + 30 - 4,10 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/brown_frog.png',  40 + 15 - 4,10 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/brown_frog.png',  40 + 25 - 4,10 - 3, '10%'))
svg.appendChild(makeJump(40 + 10, 10, 10))
svg.appendChild(makeImage('../games/leapfrog/green_frog.png',  40 + 20 - 4,20 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/green_frog.png',  40 + 30 - 4,20 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/brown_frog.png',  40 + 15 - 4,20 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/brown_frog.png',  40 + 25 - 4,20 - 3, '10%'))
svg.appendChild(makeSlide(40 + 15, 20, -5))
svg.appendChild(makeImage('../games/leapfrog/green_frog.png',  40 + 20 - 4,30 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/green_frog.png',  40 + 30 - 4,30 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/brown_frog.png',  40 + 10 - 4,30 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/brown_frog.png',  40 + 25 - 4,30 - 3, '10%'))
svg.appendChild(makeJump(40 + 25, 30, -10))
svg.appendChild(makeImage('../games/leapfrog/green_frog.png',  40 + 20 - 4,40 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/green_frog.png',  40 + 30 - 4,40 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/brown_frog.png',  40 + 10 - 4,40 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/brown_frog.png',  40 + 15 - 4,40 - 3, '10%'))
svg.appendChild(makeSlide(40 + 20, 40, 5))
svg.appendChild(makeImage('../games/leapfrog/green_frog.png',  40 + 25 - 4,48 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/green_frog.png',  40 + 30 - 4,48 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/brown_frog.png',  40 + 10 - 4,48 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/brown_frog.png',  40 + 15 - 4,48 - 3, '10%'))
// done
$('slideB').appendChild(svg)
</script>

#

:::{#slideC}
:::

:::{.en}
- To analyze the solution shown above 👆, note the movement of vacant stone.
:::

:::{.ch}
- 為了分析上圖顯示的解決方案👆，請注意空缺石頭的移動。
:::

<script type="text/javascript">
svg = makeCanvas()
x = 5
// first row
svg.appendChild(makeImage('../games/leapfrog/green_frog.png', x +  0 - 4,10 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/brown_frog.png', x + 20 - 4,10 - 3, '10%'))
// second row
svg.appendChild(makeImage('../games/leapfrog/green_frog.png', x + 10 - 4,20 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/brown_frog.png', x + 20 - 4,20 - 3, '10%'))
// third row
svg.appendChild(makeImage('../games/leapfrog/green_frog.png', x + 10 - 4,30 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/brown_frog.png', x +  0 - 4,30 - 3, '10%'))
// fourth row
svg.appendChild(makeImage('../games/leapfrog/green_frog.png', x + 20 - 4,40 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/brown_frog.png', x +  0 - 4,40 - 3, '10%'))
// rock movement
svg.appendChild(makeImage('../games/leapfrog/rock.png', x + 10 - 3,10 - 3, '8%'))
svg.appendChild(makeImage('../games/leapfrog/rock.png', x +  0 - 3,20 - 3, '8%'))
svg.appendChild(makeImage('../games/leapfrog/rock.png', x + 20 - 3,30 - 3, '8%'))
svg.appendChild(makeImage('../games/leapfrog/rock.png', x + 10 - 3,40 - 3, '8%'))
// indicators
svg.appendChild(makeDashedArrow(x + 10 - 1,10 + 1, x + 0 + 2.5,20 - 2.5))
svg.appendChild(makeArrow(x + 0 + 1,20 + 1, x + 20 - 2.2,30 - 2.2))
svg.appendChild(makeDashedArrow(x + 20 - 1,30 + 1, x + 10 + 2.5,40 - 2.5))
// schematic diagram
x = 40
y = 10 // first line
svg.appendChild(makeCircle(x +  0, y, 'white'))
svg.appendChild(makeCircle(x +  5, y, 'black'))
svg.appendChild(makeCircle(x + 10, y, 'white'))
y = 15 // next line
svg.appendChild(makeCircle(x +  0, y, 'black'))
svg.appendChild(makeCircle(x +  5, y, 'white'))
svg.appendChild(makeCircle(x + 10, y, 'black'))
y = 20 // last line
svg.appendChild(makeCircle(x +  0, y, 'white'))
svg.appendChild(makeCircle(x +  5, y, 'black'))
svg.appendChild(makeCircle(x + 10, y, 'white'))
// arrows
svg.appendChild(makeLeftDash(x + 5,10))
svg.appendChild(makeRight(x + 0 ,15))
svg.appendChild(makeLeftDash(x + 10,15))
// mirror pattern
x = 60
y = 10 // first line
svg.appendChild(makeCircle(x +  0, y, 'white'))
svg.appendChild(makeCircle(x +  5, y, 'black'))
svg.appendChild(makeCircle(x + 10, y, 'white'))
y = 15 // next line
svg.appendChild(makeCircle(x +  0, y, 'black'))
svg.appendChild(makeCircle(x +  5, y, 'white'))
svg.appendChild(makeCircle(x + 10, y, 'black'))
y = 20 // last line
svg.appendChild(makeCircle(x +  0, y, 'white'))
svg.appendChild(makeCircle(x +  5, y, 'black'))
svg.appendChild(makeCircle(x + 10, y, 'white'))
// arrows
svg.appendChild(makeRightDash(x + 5,10))
svg.appendChild(makeLeft(x + 10,15))
svg.appendChild(makeRightDash(x + 0,15))
// done
$('slideC').appendChild(svg)
</script>

#

:::{#slideD}
:::

:::{.en}
- To analyze the solution shown above 👆, note the movement of vacant stone.
:::

:::{.ch}
- 為了分析上圖顯示的解決方案👆，請注意空缺石頭的移動。
:::

<script type="text/javascript">
svg = makeCanvas()
x = 2
svg.appendChild(makeImage('../games/leapfrog/green_frog.png', x +  0 - 4,10 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/green_frog.png', x +  5 - 4,10 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/brown_frog.png', x + 15 - 4,10 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/brown_frog.png', x + 20 - 4,10 - 3, '10%'))
// next row
svg.appendChild(makeImage('../games/leapfrog/green_frog.png', x +  0 - 4,20 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/green_frog.png', x + 10 - 4,20 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/brown_frog.png', x + 15 - 4,20 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/brown_frog.png', x + 20 - 4,20 - 3, '10%'))
// next row
svg.appendChild(makeImage('../games/leapfrog/green_frog.png', x +  0 - 4,30 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/green_frog.png', x + 10 - 4,30 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/brown_frog.png', x +  5 - 4,30 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/brown_frog.png', x + 20 - 4,30 - 3, '10%'))
// next row
svg.appendChild(makeImage('../games/leapfrog/green_frog.png', x +  0 - 4,40 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/green_frog.png', x + 10 - 4,40 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/brown_frog.png', x +  5 - 4,40 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/brown_frog.png', x + 15 - 4,40 - 3, '10%'))
// rock movement
svg.appendChild(makeImage('../games/leapfrog/rock.png', x + 10 - 3,10 - 3, '8%'))
svg.appendChild(makeImage('../games/leapfrog/rock.png', x +  5 - 3,20 - 3, '8%'))
svg.appendChild(makeImage('../games/leapfrog/rock.png', x + 15 - 3,30 - 3, '8%'))
svg.appendChild(makeImage('../games/leapfrog/rock.png', x + 20 - 3,40 - 3, '8%'))
// next column
x = 30
svg.appendChild(makeImage('../games/leapfrog/green_frog.png', x +  0 - 4,10 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/green_frog.png', x + 20 - 4,10 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/brown_frog.png', x +  5 - 4,10 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/brown_frog.png', x + 15 - 4,10 - 3, '10%'))
// next row
svg.appendChild(makeImage('../games/leapfrog/green_frog.png', x + 10 - 4,20 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/green_frog.png', x + 20 - 4,20 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/brown_frog.png', x +  5 - 4,20 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/brown_frog.png', x + 15 - 4,20 - 3, '10%'))
// next row
svg.appendChild(makeImage('../games/leapfrog/green_frog.png', x + 10 - 4,30 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/green_frog.png', x + 20 - 4,30 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/brown_frog.png', x +  0 - 4,30 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/brown_frog.png', x + 15 - 4,30 - 3, '10%'))
// next row
svg.appendChild(makeImage('../games/leapfrog/green_frog.png', x + 10 - 4,40 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/green_frog.png', x + 20 - 4,40 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/brown_frog.png', x +  0 - 4,40 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/brown_frog.png', x +  5 - 4,40 - 3, '10%'))
// next row
svg.appendChild(makeImage('../games/leapfrog/green_frog.png', x + 15 - 4,48 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/green_frog.png', x + 20 - 4,48 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/brown_frog.png', x +  0 - 4,48 - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/brown_frog.png', x +  5 - 4,48 - 3, '10%'))
// rock movement
svg.appendChild(makeImage('../games/leapfrog/rock.png', x + 10 - 3,10 - 3, '8%'))
svg.appendChild(makeImage('../games/leapfrog/rock.png', x +  0 - 3,20 - 3, '8%'))
svg.appendChild(makeImage('../games/leapfrog/rock.png', x +  5 - 3,30 - 3, '8%'))
svg.appendChild(makeImage('../games/leapfrog/rock.png', x + 15 - 3,40 - 3, '8%'))
svg.appendChild(makeImage('../games/leapfrog/rock.png', x + 10 - 3,48 - 3, '8%'))
// schematic diagram
x = 58
y = 10 // first line
svg.appendChild(makeCircle(x +  0, y, 'white'))
svg.appendChild(makeCircle(x +  5, y, 'white'))
svg.appendChild(makeCircle(x + 10, y, 'black'))
svg.appendChild(makeCircle(x + 15, y, 'white'))
svg.appendChild(makeCircle(x + 20, y, 'white'))
y = 15 // second line
svg.appendChild(makeCircle(x +  0, y, 'white'))
svg.appendChild(makeCircle(x +  5, y, 'black'))
svg.appendChild(makeCircle(x + 10, y, 'white'))
svg.appendChild(makeCircle(x + 15, y, 'black'))
svg.appendChild(makeCircle(x + 20, y, 'white'))
y = 20 // third line
svg.appendChild(makeCircle(x +  0, y, 'black'))
svg.appendChild(makeCircle(x +  5, y, 'white'))
svg.appendChild(makeCircle(x + 10, y, 'black'))
svg.appendChild(makeCircle(x + 15, y, 'white'))
svg.appendChild(makeCircle(x + 20, y, 'black'))
y = 25 // fourth line
svg.appendChild(makeCircle(x +  0, y, 'white'))
svg.appendChild(makeCircle(x +  5, y, 'black'))
svg.appendChild(makeCircle(x + 10, y, 'white'))
svg.appendChild(makeCircle(x + 15, y, 'black'))
svg.appendChild(makeCircle(x + 20, y, 'white'))
y = 30 // fifth line
svg.appendChild(makeCircle(x +  0, y, 'white'))
svg.appendChild(makeCircle(x +  5, y, 'white'))
svg.appendChild(makeCircle(x + 10, y, 'black'))
svg.appendChild(makeCircle(x + 15, y, 'white'))
svg.appendChild(makeCircle(x + 20, y, 'white'))
// arrows
y = 10
svg.appendChild(makeLeftDash(x + 10,y))
y = 15
svg.appendChild(makeRight(x + 5,y))
svg.appendChild(makeRightDash(x + 15,y))
y = 20
svg.appendChild(makeLeft(x + 20,y))
svg.appendChild(makeLeft(x + 10,y))
svg.appendChild(makeRightDash(x + 0,y))
y = 25
svg.appendChild(makeRight(x + 5,y))
svg.appendChild(makeLeftDash(x + 15,y))
// done
$('slideD').appendChild(svg)
</script>

#

:::{#slideE}
:::

:::{.en}
- From the movement pattern of vacant stone, the solution for 3 pairs of frogs is shown above 👆.
:::

:::{.ch}
- 從空缺石頭的移動模式來看，3對青蛙的解法如上圖👆所示。
:::

<script type="text/javascript">
svg = makeCanvas()
// schematic diagram
x = 20, y = 5
// first line
svg.appendChild(makeCircle(x +  0, y, 'white'))
svg.appendChild(makeCircle(x +  5, y, 'white'))
svg.appendChild(makeCircle(x + 10, y, 'white'))
svg.appendChild(makeCircle(x + 15, y, 'black'))
svg.appendChild(makeCircle(x + 20, y, 'white'))
svg.appendChild(makeCircle(x + 25, y, 'white'))
svg.appendChild(makeCircle(x + 30, y, 'white'))
// second line
y = 10
svg.appendChild(makeCircle(x +  0, y, 'white'))
svg.appendChild(makeCircle(x +  5, y, 'white'))
svg.appendChild(makeCircle(x + 10, y, 'black'))
svg.appendChild(makeCircle(x + 15, y, 'white'))
svg.appendChild(makeCircle(x + 20, y, 'black'))
svg.appendChild(makeCircle(x + 25, y, 'white'))
svg.appendChild(makeCircle(x + 30, y, 'white'))
// third line
y = 15
svg.appendChild(makeCircle(x +  0, y, 'white'))
svg.appendChild(makeCircle(x +  5, y, 'black'))
svg.appendChild(makeCircle(x + 10, y, 'white'))
svg.appendChild(makeCircle(x + 15, y, 'black'))
svg.appendChild(makeCircle(x + 20, y, 'white'))
svg.appendChild(makeCircle(x + 25, y, 'black'))
svg.appendChild(makeCircle(x + 30, y, 'white'))
// fourth line
y = 20
svg.appendChild(makeCircle(x +  0, y, 'black'))
svg.appendChild(makeCircle(x +  5, y, 'white'))
svg.appendChild(makeCircle(x + 10, y, 'black'))
svg.appendChild(makeCircle(x + 15, y, 'white'))
svg.appendChild(makeCircle(x + 20, y, 'black'))
svg.appendChild(makeCircle(x + 25, y, 'white'))
svg.appendChild(makeCircle(x + 30, y, 'black'))
// fifth line
y = 25
svg.appendChild(makeCircle(x +  0, y, 'white'))
svg.appendChild(makeCircle(x +  5, y, 'black'))
svg.appendChild(makeCircle(x + 10, y, 'white'))
svg.appendChild(makeCircle(x + 15, y, 'black'))
svg.appendChild(makeCircle(x + 20, y, 'white'))
svg.appendChild(makeCircle(x + 25, y, 'black'))
svg.appendChild(makeCircle(x + 30, y, 'white'))
// sixth line
y = 30
svg.appendChild(makeCircle(x +  0, y, 'white'))
svg.appendChild(makeCircle(x +  5, y, 'white'))
svg.appendChild(makeCircle(x + 10, y, 'black'))
svg.appendChild(makeCircle(x + 15, y, 'white'))
svg.appendChild(makeCircle(x + 20, y, 'black'))
svg.appendChild(makeCircle(x + 25, y, 'white'))
svg.appendChild(makeCircle(x + 30, y, 'white'))
// seventh line
y = 35
svg.appendChild(makeCircle(x +  0, y, 'white'))
svg.appendChild(makeCircle(x +  5, y, 'white'))
svg.appendChild(makeCircle(x + 10, y, 'white'))
svg.appendChild(makeCircle(x + 15, y, 'black'))
svg.appendChild(makeCircle(x + 20, y, 'white'))
svg.appendChild(makeCircle(x + 25, y, 'white'))
svg.appendChild(makeCircle(x + 30, y, 'white'))
// arrows
y = 5
svg.appendChild(makeLeftDash(x + 15,y))
y = 10
svg.appendChild(makeRight(x + 10,y))
svg.appendChild(makeRightDash(x + 20,y))
y = 15
svg.appendChild(makeLeft(x + 25,y))
svg.appendChild(makeLeft(x + 15,y))
svg.appendChild(makeLeftDash(x + 5,y))
y = 20
svg.appendChild(makeRight(x + 0,y))
svg.appendChild(makeRight(x + 10,y))
svg.appendChild(makeRight(x + 20,y))
svg.appendChild(makeLeftDash(x + 30,y))
y = 25
svg.appendChild(makeLeft(x + 25,y))
svg.appendChild(makeLeft(x + 15,y))
svg.appendChild(makeRightDash(x + 5,y))
y = 30
svg.appendChild(makeRight(x + 10,y))
svg.appendChild(makeLeftDash(x + 20,y))
// game
y = 45
svg.appendChild(makeImage('../games/leapfrog/green_frog.png', x +  0 - 4,y - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/green_frog.png', x +  5 - 4,y - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/green_frog.png', x + 10 - 4,y - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/rock.png',       x + 15 - 3,y,      '8%'))
svg.appendChild(makeImage('../games/leapfrog/brown_frog.png', x + 20 - 4,y - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/brown_frog.png', x + 25 - 4,y - 3, '10%'))
svg.appendChild(makeImage('../games/leapfrog/brown_frog.png', x + 30 - 4,y - 3, '10%'))

// done
$('slideE').appendChild(svg)
</script>

#

```{=html}
<svg viewBox="0 0 200 100" xmlns="http://www.w3.org/2000/svg">
  <path
    fill="none"
    stroke="lightgrey"
    d="M20,50 C20,-50 180,150 180,50 C180-50 20,150 20,50 z" />

  <circle r="5" fill="red">
    <animateMotion
      dur="10s"
      repeatCount="indefinite"
      path="M20,50 C20,-50 180,150 180,50 C180-50 20,150 20,50 z" />
  </circle>
</svg>
```

Animate motion along a path!

<!--
pandoc -t revealjs -s --mathjax mathfun01.md -o mathfun01.html
-->
