---
title: Mona Lisa and her Smile
author: <span class="en">art and science of a genius</span><span class="ch">天才的藝術與科學</span>
date: <span class="en">February 2025</span><span class="ch">2025年2月</span>
theme: solarized
header-includes: |
    <meta property="og:type" content="website" />
    <meta property="og:title" content="蒙娜麗莎之微笑" />
    <meta property="og:image" content="../images/highlight-lisa.jpg" />
    <base target="_blank"/>
    <link rel="stylesheet" type="text/css" href="../css/slides.css"></link>
    <script type="text/javascript" src="../scripts/site.js"></script>
include-before: |
    <!-- Any raw HTML code right after body tag, before anything else. -->
    <div id="modal" class="modal">
      <img id="modal-content" />
      <div id="modal-caption"></div>
    </div>
    <div align="right" class="banner">
    <input type="radio" name="lang" onclick="set_lang('en')" id="english" value="English" checked /> English |
    <input type="radio" name="lang" onclick="set_lang('ch')" id="chinese" value="Chinese" /> 中文
    </div>
include-after: |
    <!-- Any raw HTML code right before body end tag, after anything else. -->
    <script>window.onload = check</script>
    <!-- file:///Users/josephchan/jc/q/site/slides/monalisa.html -->
    <!-- https://jhlchan.bitbucket.io/slides/monalisa.html -->
...

#

:::{.en}
<div class="imagecap" style="width:70%">
<img src="../images/Mona-Lisa-retouched.jpg" style="width:48%"
     alt="Mona Lisa as shown in Louvre Museum in Paris." onclick="showPopup(this)" />
<img src="../images/Mona-Lisa-restored.jpg" style="width:47.2%"
     alt="Mona Lisa with color restored, pixel by pixel." onclick="showPopup(this)" />
</br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:70%">
<img src="../images/Mona-Lisa-retouched.jpg" style="width:48%"
     alt="《蒙娜麗莎》現藏於巴黎羅浮宮博物館。" onclick="showPopup(this)" />
<img src="../images/Mona-Lisa-restored.jpg" style="width:47.2%"
     alt="經逐點彩色像素修復的《蒙娜麗莎》。" onclick="showPopup(this)" />
</br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- This famous smile of Mona Lisa was created by Leonardo da Vinci, when he was over 50 years old.
- On the left is the painting on display today, fading and without any trace of eyebrows.
- On the right is the painting restored by modern technology, showing very faint eyebrows.
- The smile is elusive: it appears only when you focus on her eyes, not her mouth!
:::

:::{.ch}
- 著名《蒙娜麗莎》的微笑，是 Leonardo da Vinci（達文西）的傑作，當時他年屆50多歲。
- 左邊一幅，是今天看得到的那幅畫，已經褪色，沒有任何眉毛的痕跡。
- 右邊一幅，是現代技術修復的畫作，還原色彩，可見非常淡淡的眉毛。
- 微笑若隱若現：只有把注意力集中在她的眼睛，而不是她的嘴唇，才會出現！
:::

<!--

Mona Lisa
https://en.wikipedia.org/wiki/Mona_Lisa
https://upload.wikimedia.org/wikipedia/commons/e/ec/Mona_Lisa%2C_by_Leonardo_da_Vinci%2C_from_C2RMF_retouched.jpg (file too large)
https://upload.wikimedia.org/wikipedia/commons/thumb/e/ec/Mona_Lisa%2C_by_Leonardo_da_Vinci%2C_from_C2RMF_retouched.jpg/540px-Mona_Lisa%2C_by_Leonardo_da_Vinci%2C_from_C2RMF_retouched.jpg  as  ../images/Mona-Lisa-retouched.jpg
https://upload.wikimedia.org/wikipedia/commons/b/b0/Mona_Lisa_Restored_Colour._Based_on_newly_discovered_%22Prado_Copy%22_painted_by_pupil_alongside_Leonardo..jpg as ../images/Mona-Lisa-restored.jpg

13.06.16
The colours of the Mona Lisa
by Matías Ventura Bausero, 13 June 2016.
https://matiasventura.com/post/the-colours-of-the-mona-lisa/
The Mona Lisa, by Leonardo da Vinci.
https://i0.wp.com/matiasventura.com/wp-content/uploads/2014/03/Leonardo_da_Vinci_-_Mona_Lisa.jpg
https://i0.wp.com/matiasventura.com/wp-content/uploads/2014/03/Leonardo_da_Vinci_-_Mona_Lisa.jpg?w=669&ssl=1
Restored Mona Lisa copy at the Prado Museum, Madrid.
https://i0.wp.com/matiasventura.com/wp-content/uploads/2014/03/gioconda-restored-prado.jpg?w=900&ssl=1
Digital reconstruction of colours in Leonardo’s Mona Lisa.
https://i0.wp.com/matiasventura.com/wp-content/uploads/2016/06/matias-ventura-mona-lisa-recomposition.jpg?w=800&ssl=1
(these are no good compared to those in Wikipedia)

elusive  難以捉摸 若隱若現


The detail that unlocks the Mona Lisa
Kelly Grovier, 12 February 2021
https://www.bbc.com/culture/article/20210211-the-detail-that-unlocks-the-mona-lisa
Mona-Lisa-unlock-01.jpg
Mona-Lisa-unlock-02.jpg -> highlight-lisa.jpg (by https://compressjpeg.com, for thumbnail)
Many scholars have been fascinated by the mystery of Mona Lisa's smile (Credit: Alamy)
Mona-Lisa-unlock-03.jpg
Some viewers are as transfixed by Mona Lisa's hands as by her face (Credit: Alamy)
Mona-Lisa-unlock-04.jpg
By placing Mona Lisa on a 'little well', surrounded by water, Da Vinci could be drawing on earlier spiritual connections with springs (Credit: Alamy)

The 1503 painting by Leonardo da Vinci is the world's most famous piece of art. Kelly Grovier explores an overlooked object that offers a different perspective on the masterpiece.

For centuries, our attention has largely been focused elsewhere in the small (77 x 53cm/30 x 21in) oil-on-poplar panel, which Da Vinci never fully finished and is thought to have continued to tinker with obsessively until his death in 1519 – as if the painting's endless emergence were the work itself. A preoccupation principally with Mona Lisa's inscrutable smile is almost as old as the painting, and dates back at least to the reaction of the legendary Renaissance writer and historian Giorgio Vasari, who was born a few years after Da Vinci began work on the likeness. "The mouth with its opening and with its ends united by the red of the lips to the flesh-tints of the face," Vasari observed in his celebrated Lives of the Most Excellent Painters, Sculptors, and Architects, "seemed, in truth, to be not colours but flesh. In the pit of the throat, if one gazed upon it intently, could be seen the beating of the pulse." He concluded: "In this work of Leonardo, there was a smile so pleasing, that it was a thing more divine than human to behold, and it was held to be something marvellous, in that it was not other than alive."
>>>

-->

#

:::{.en}
<div class="imagecap" style="width:75%">
<img src="../images/Leonardo-da-Vinci-color.jpg" style="width:51.5%"
     alt="Self portrait of Leonardo da Vinci around 1505." onclick="showPopup(this)" />
<img src="../images/Leonardo-da-Vinci-botany.jpg" style="width:46%"
     alt="Notes and sketches on botany, in mirror writing as Leonardo was left-handed." onclick="showPopup(this)" />
</br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:75%">
<img src="../images/Leonardo-da-Vinci-color.jpg" style="width:51.5%"
     alt="Leonardo da Vinci 的自畫像，大約 1505年。" onclick="showPopup(this)" />
<img src="../images/Leonardo-da-Vinci-botany.jpg" style="width:46%"
     alt="植物學筆記和草圖，以鏡像書寫，因為 Leonardo 是左撇子。" onclick="showPopup(this)" />
</br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- Leonardo da Vinci (1452 - 1519) was an Italian in the age of Renaissance, learning art in studio.
- He wanted to know everything about nature, long before the arrival of modern science.
- Thus he studied nature by observation, using drawings and became the first scientific illustrator.
- His detailed drawings were recorded in his notebooks, giving us a glimpse of his genius.
:::

:::{.ch}
- Leonardo da Vinci（達文西，1452 - 1519）是文藝復興時期的義大利人，在畫室學習。
- 當時，未有現代科學方法，但他求知若渴，想了解自然界中的一切。
- 因此，他透過觀察和繪圖，進行深入研究，成為最早的科學插畫家。
- 他的詳細繪圖，記錄在一頁頁的筆記本中，讓我們瞧一瞧他的天才。
:::

<!--
Leonardo da Vinci
https://en.wikipedia.org/wiki/Leonardo_da_Vinci
(Presumed self-portrait of Leonardo (c. 1510) at the Royal Library of Turin, Italy)
https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Leonardo_da_Vinci_-_presumed_self-portrait_-_WGA12798.jpg/800px-Leonardo_da_Vinci_-_presumed_self-portrait_-_WGA12798.jpg

Is this Leonardo da Vinci's earliest surviving work? Self-portrait as Archangel Gabriel unveiled (although some experts say there is 'zero chance' it's real)
by Annie Palmer, 5 December 2018.
https://www.dailymail.co.uk/sciencetech/article-5875983/Is-Leonardo-da-Vincis-earliest-surviving-work-Self-portrait-Archangel-Gabriel-unveiled.html
https://i.dailymail.co.uk/i/newpix/2018/06/23/10/4D8BF0D400000578-5875983-image-m-7_1529746658320.jpg as ../images/Leonardo-da-Vinci-color.jpg

Historians Identify 14 Living Relatives of Leonardo da Vinci
by Nora McGreevy, 13 July 2021.
https://www.smithsonianmag.com/smart-news/da-vinci-relatives-dna-testing-genome-180978153/
An ongoing effort to trace the artist’s male lineage may help researchers sequence his genome
(Presumed self-portrait of Leonardo da Vinci, circa 1512, red chalk on paper)
https://tf-cmsv2-smithsonianmag-media.s3.amazonaws.com/filer/e1/16/e116f891-b0c0-431f-8bed-206b15c0e40f/leonardo_da_vinci_-_presumed_self-portrait_-_wga12798.jpg
as ../images/Leonardo-da-Vinci-self.jpg

Leonardo da Vinci
BY: HISTORY.COM EDITORS, updated 13 July 2022.
https://www.history.com/topics/renaissance/leonardo-da-vinci
(color portrait, long)
https://assets.editorial.aetnd.com/uploads/2012/05/self-portrait-by-leonardo-da-vinci.jpg
?width=3840&height=1920&crop=3840%3A1920%2Csmart&quality=75&auto=webp (crops)

Who posed for Leonardo da Vinci’s Mona Lisa?
BY: ELIZABETH NIX, updated 30 August 2018.
https://www.history.com/news/who-posed-for-leonardo-da-vincis-mona-lisa
(color photo, long)
https://assets.editorial.aetnd.com/uploads/2013/09/ask-mona_lisa-2.jpg
?width=3840&height=1920&crop=3840%3A1920%2Csmart&quality=75&auto=webp  (crops)


See Leonardo da Vinci’s Genius Yourself in These Newly Digitized Sketches
by Jason Daley, 31 August 2018.
https://www.smithsonianmag.com/smart-news/get-close-newly-digitized-da-vinci-notebooks-180970183/
The Victoria and Albert Museum in London has made ultra high-resolution scans of two codices available online


Leonardo da Vinci’s Notebooks Get Digitized: Where to Read the Renaissance Man’s Manuscripts Online
in Archives, Art, History, Science | 13 May 2021
https://www.openculture.com/2021/05/where-to-read-leonardo-da-vincis-notebooks-online.html

The Art of the Plant – Leonardo da Vinci
https://www.phaidon.com/agenda/art/articles/2016/december/07/the-art-of-the-plant-leonardo-da-vinci/
Artist or scientist? Discover why da Vinci's botanical drawings lie at the very heart of this Renaissance Man's work
Star of Bethlehem (Ornithogalum umbellatum), wood anemone (Anemone nemorosa) and sun spurge (Euphorbia helioscopia), c.1505–10 Pen and ink with red chalk on paper, 19.8 × 16 cm / 7¾ × 6¼ in Royal Collection Trust, London
https://www.phaidon.com/resource/davincistarofbethlehem.jpg

Leonardo Da Vinci, Artist/Scientist
by Matthew Wills, 15 April 2016.
https://daily.jstor.org/leonardo-da-vinci-artist-scientist/
Leonardo was the first scientific illustrator.
Leonardo da Vinci botanical study, circa 1490  By Leonardo da Vinci - Milena Magnano, Leonardo, collana I Geni dell'arte, Mondadori Arte, Milano 2007, Public Domain
https://daily.jstor.org/wp-content/uploads/2016/04/daVinciBotanicals_1050x700.jpg

Notes and sketches of botany and geometry: narcissus. Drawing on double page of manuscript by Leonardo da Vinci (Leonardo da Vinci) (1452-1519)
https://www.meisterdrucke.us/fine-art-prints/Leonardo-da-Vinci/946266/Notes-and-sketches-of-botany-and-geometry%3A-narcissus.-Drawing-on-double-page-of-manuscript-by-Leonardo-da-Vinci-%28Leonardo-da-Vinci%29-%281452-1519%29.html
https://www.meisterdrucke.us/kunstwerke/1200w/Leonardo_da_Vinci_-_Notes_and_sketches_of_botany_and_geometry_narcissus_Drawing_on_double_page_of_ma_-_%28MeisterDrucke-946266%29.jpg
as ../images/Leonardo-da-Vinci-botany.jpg
-->

#

:::{.en}
<div class="imagecap" style="width:86%">
<img src="../images/Leonardo-da-Vinci-human.jpg" style="width:36%"
     alt="This diagram shows proportions of the human body within a circle and a square." onclick="showPopup(this)" />
<img src="../images/Leonardo-da-Vinci-mouth.jpg" style="width:36.2%"
     alt="Drawings of the anatomy of mouth and lips, with a smile at the very top." onclick="showPopup(this)" />
</br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:86%">
<img src="../images/Leonardo-da-Vinci-human.jpg" style="width:36%"
     alt="此圖顯示人體的比例，放在圓形和正方形內。" onclick="showPopup(this)" />
<img src="../images/Leonardo-da-Vinci-mouth.jpg" style="width:36.2%"
     alt="嘴巴和嘴唇的解剖圖，最上方有微笑。" onclick="showPopup(this)" />
</br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- As an artist, he figured out the proportions for the human body, the math behind.
- As a scientist, he studied anatomy by dissecting the human body in the morgue.
- As a result, he understood every muscle that pulls a smile, an extraordinary feat.
- He also investigated optics, the interplay between light, colors and shades.
- Many believed that he applied all these techniques in his masterpiece: Mona Lisa.
:::

:::{.ch}
- 作為一名藝術家，他弄清楚人體的比例，以及背後的數學原理。
- 作為一名科學家，他透過解剖人體研究，把太平間化身解剖室。
- 結果，他明白了引發微笑的每一塊肌肉，真是一項非凡的壯舉。
- 他也研究光學，認識光線、顏色和陰影之間的相互作用和效果。
- 有理由相信，在傑作《蒙娜麗莎》中，他應用了所有這些技巧。
:::

<!--
Vitruvian Man
https://en.wikipedia.org/wiki/Vitruvian_Man
https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/Da_Vinci_Vitruve_Luc_Viatour.jpg/1280px-Da_Vinci_Vitruve_Luc_Viatour.jpg as ../images/Leonardo-da-Vinci-human.jpg
The Vitruvian Man (Italian: L'uomo vitruviano; [ˈlwɔːmo vitruˈvjaːno]) is a drawing by the Italian Renaissance artist and scientist Leonardo da Vinci, dated to c. 1490.

The Elegant Mathematics of Vitruvian Man, Leonardo da Vinci’s Most Famous Drawing: An Animated Introduction
in Art, History, Math | 6 November 2017
https://www.openculture.com/2017/11/the-elegant-mathematics-of-vitruvian-man.html
(comment)
Roman architect Marcus Vitruvius Pollio who suggested that somehow the perfect man could with arms and legs akimbo transect a perfect circle and a perfect square. In his work, Leonardo measure lengths, ratios and angles but could not find the perfect ratios suggested by Vitruvius 1500 years before. Instead, he obtained odd fractions 5/11’s, 7/17th’s none of which seemed to point to the perfect circle or the perfect man and Leonardo turned back from this dead-end.
https://www.sciencebase.com/science-blog/the-rights-and-lefts-of-leonardo-da-vinci-anatomist.html

Da Vinci's Vitruvian Man of math - James Earle【3:21】cc
https://youtu.be/aMsaFP3kgqQ
What's so special about Leonardo da Vinci's Vitruvian Man? With arms outstretched, the man fills the irreconcilable spaces of a circle and a square -- symbolizing the Renaissance-era belief in the mutable nature of humankind. James Earle explains the geometric, religious and philosophical significance of this deceptively simple drawing.
(an attempt to poke at the puzzle of squaring the circle)

Drawings by Leonardo da Vinci
https://www.leonardodavinci.net/drawings.jsp

10 Secrets of The Mona Lisa by Leonardo da Vinci
https://www.leonardodavinci.net/the-mona-lisa.jsp


The Secrets of Mona Lisa【3:48】cc
https://www.youtube.com/watch?v=oczyOd5YJ8Q
Widely considered as the most famous painting of all time, Mona Lisa is a portrait painting by Renaissance polymath Leonardo da Vinci. Leonardo worked on this picture as a scholar and thinker, not only as a painter and poet. In his Mona Lisa, the individual, a miraculous creation of nature, represents at the same time the species: the portrait goes beyond its social limitations and acquires a universal meaning. Before Leonardo da Vinci, portraits had lacked mystery; artists only represented outward appearances without any soul, or, if they showed the soul, they tried to express it through gestures, symbolic objects or inscriptions. The Mona Lisa alone is a living enigma: the soul is there, but inaccessible.
(video of the site)

The Mona Lisa Story For Kids【8:07】cc
https://www.youtube.com/watch?v=WQvxUyaruPU
Learn about the history of the Mona Lisa, from it's creation by Leonardo Da Vinci, to its path through history, including its theft and eventual home in the Louvre Museum.

Leonardo da Vinci: The Mechanics of Man
https://www.rct.uk/sites/default/files/file-downloads/9781909686834_High%20Res..pdf
(PDF, 162 pages) but still no facial expressions

ANATOMICAL STUDIES
https://nicofranz.art/en/leonardo-da-vinci/anatomical-studies
Leonardo dissected about 30 people in the course of his life and made drawings for them. However, he dissected not only people, but also animals. Based on notes in his surviving notebooks, dissections on the following animals can be traced: Horse, cattle, pig and bear. Leonardo also knew of the skeleton of an ape.
Lip muscles    Windsor, Royal Library (9) 19055 verso
https://nicofranz.art/fileadmin/_processed_/1/f/csm_19055v_4a66546523.webp
Lip-muscles.webp -> 
Leonardo da Vinci's drawings of human anatomy are now largely held by the Royal Library at Windsor Castle near London. Leonardo's drawings from their holdings have largely been digitized and can be viewed online. All the illustrations shown on this page can be viewed with explanatory texts on the website of the Royal Collection Trust (RCT). All you have to do is enter the catalog number shown below the images in the search mask. A leading 9 leads to exact search results.
https://www.rct.uk/

Recto: The uterus of a gravid cow. Verso: The anatomy of the mouth c.1508
https://www.rct.uk/collection/919055/recto-the-uterus-of-a-gravid-cow-verso-the-anatomy-of-the-mouth
https://www.rct.uk/sites/default/files/collection-online/5/0/262927-1333102622.jpg as ../images/Leonardo-da-Vinci-mouth.jpg
-->

#

:::{.en}
<div class="imagecap" style="width:32%">
<img src="../images/Mona-Lisa-viewport.jpg"
     alt="View Mona Lisa a bit left of center, in direct alignment with her eyes." onclick="showPopup(this)" /></br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:32%">
<img src="../images/Mona-Lisa-viewport.jpg"
     alt="欣賞《蒙娜麗莎》時，中心偏左一點，與她的眼睛對接。" onclick="showPopup(this)" /></br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- "Mona" in English is "Monna" in Italian, short for "Madonna", meaning "My Lady" or "Madam".
- "Lisa" was Lisa del Giocondo, the wife of Florentine silk merchant Francesco del Giocondo.
- That means Lisa was a noblewoman, in possesion of jewels, necklaces, and splendid dresses.
- These sparkling accessories were absent in the portrait of Mona Lisa, because only her face matters!
:::

:::{.ch}
- 英文 Mona 是義大利文 Monna，為 Madonna 的縮寫，意思是 My Lady 或 Madam，即「夫人」。
- Lisa 代表 Lisa del Giocondo，是佛羅倫斯（Florence）絲綢富商 Francesco del Giocondo 的妻子。
- 這意味著，Lisa 其實是一名貴族女性，擁有各式珠寶、項鍊和華麗的衣裳。
- 《蒙娜麗莎》肖像中，沒有閃閃發光的襯托，因為最重要的，只有她的面容！
:::

#

:::{.en}
<div class="imagecap" style="width:36%">
<img src="../images/Mona-Lisa-background.jpg"
     alt="Note the perspective of the painting, and backgrounds on either side." onclick="showPopup(this)" /></br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:36%">
<img src="../images/Mona-Lisa-background.jpg"
     alt="注意畫作的透視構圖，及人物兩側的背景。" onclick="showPopup(this)" /></br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- Since painting is not photography, the subject had to keep a strict posture for long hours.
- To keep Lisa relaxing and smiling, Leonardo da Vinci hired jesters and musicians to perform.
- The landscape behind Lisa were imagined by the painter, with progressive blur to create depth.
- The aerial view has on the left a river to a lake to distant mountains, and on the right a bridge.
:::

:::{.ch}
- 拍攝一下子完成，但繪畫與攝影不同：對象人物必須長時間保持姿勢，全神貫注。
- 為了讓 Lisa 放鬆，保持微笑，Leonardo da Vinci 聘請小丑和音樂家，作現場表演。
- Lisa 身後背景，其實是畫師想像出來。透過漸進模糊形式，表達深度，遠山近水。
- 背景是鳥瞰圖，左側有彎彎河流、湖泊和遙遠山脈，右側有一座橋樑，仔細看看。
:::

<!--

LINES OF SIGHT
A NEW WAY OF LOOKING AT THE MONA LISA IN THE 21ST CENTURY
by Kelvin Busher, 15 June 2021.
https://kelvinbusher.medium.com/lines-of-sight-10993c61b0a9
All the ideas and theories presented in this article are original to the author and have not, to the best of the author’s knowledge, appeared anywhere in any published form before the release date of this article.
The main aim of this article is to show in a fresh, new way, why the Mona Lisa is a unique, authentic masterpiece.
https://miro.medium.com/v2/resize:fit:1400/format:webp/1*1SJ2ru3t0fJe8Hnf0Ipx5w.png
The Mona Lisa was never meant to be viewed from a central position but rather from left of centre, in order to have the viewer look directly at Lisa and connect with her. The bases are composed in such a way as to facilitate this happening.
(Vertical viewing position) as ../images/Mona-Lisa-viewport.jpg
https://miro.medium.com/v2/resize:fit:1400/format:webp/1*UC3lA_-Q8jusq3KBAYHcKQ.jpeg 

THE BACKGROUND

The most interesting thing about the background of the Mona Lisa in terms of composition is how it is staggered. It seems to meander, twisting and turning in all directions. There are no parts of the landscape that run horizontally across the painting. It has its own foreground, middle ground and background.

A common question often asked is “why the landscape to the right side of Lisa’s head is at a different level than that to her left?” The simple answer is that the mountains to Lisa’s right slope up towards the horizon from middle ground to background. This creates an interesting dynamic throughout the painting.

(background composition) ../images/Mona-Lisa-background.jpg
https://miro.medium.com/v2/resize:fit:1400/format:webp/1*cCzdF9PHABAdZI0114_EnQ.jpeg

MONA LISA
https://www.discoveringdavinci.com/mona-lisa
Is the most famous work of art in existence. Her face is also one of the most recognizable - and is potentially worth a billion dollars. It's both a mystery and a controversy over who she really is and how she was painted.

-->

#

:::{.en}
<div class="imagecap" style="width:80%">
<img src="../images/Mona-Lisa-head-mono.jpg" style="width:49%"
     alt="Mona Lisa black and white image with size reduced insert." onclick="showPopup(this)" />
<img src="../images/Mona-Lisa-head-blur.jpg" style="width:46.8%"
     alt="Mona Lisa image blurred to reveal the elusive smile." onclick="showPopup(this)" />
</br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:80%">
<img src="../images/Mona-Lisa-head-mono.jpg" style="width:49%"
     alt="蒙娜麗莎黑白圖像，帶有縮小尺寸的插圖。" onclick="showPopup(this)" />
<img src="../images/Mona-Lisa-head-blur.jpg" style="width:46.8%"
     alt="蒙娜麗莎模糊形像，露出難以捉摸的微笑。" onclick="showPopup(this)" />
</br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- The painter applied shades for Lisa's mouth skilfully, showing different shapes from different angles.
- When you look at the lips directly, Lisa is not quite smiling, as the mouth ends are not prominent.
- When you look away, say turning to her eyes (or hands or background), you see the lips from an angle.
- From an angle, your eyes will perceive mouth ends curl up ever so slightly (by shade), so Lisa smiles.
- This is because, from an angle, your peripherial vision is blurred, similar to the right or the small insert.
- Such an elusive smile can only be created by a genius, a master of art, science, optics and illusion!
:::

:::{.ch}
- 畫師為 Lisa 的嘴巴塗陰，十分之巧妙。從不同的角度觀察，會呈現不同的形狀。
- 當你直視她嘴唇時，Lisa 並沒有向你微笑，因為兩邊嘴角微合，並不見彎向上。
- 當你把目光移開時，例如轉向她的眼睛（或雙手或背景），你會從斜角看嘴唇。
- 從傾斜角度望過去，你會感覺到嘴角稍稍上翹（由於陰影），所以 Lisa 微笑了。
- 這是因為，從斜角看，周邊視力變得模糊不清，類似於右圖或中間的小小插圖。
- 如此若隱若現的微笑，不愧是天才大師的手筆，結合藝術、科學、光學和幻覺！
:::

<!--

Vision and Art (by Margaret Livingstone)
https://livingstone.hms.harvard.edu/vision-and-art
The elusive quality of the Mona Lisa's smile can be explained by the fact that her smile is almost entirely in low spatial frequencies, and so is seen best by your peripheral vision (Science, 290, 1299). These three images show her face filtered to show selectively lowest (left) low (middle) and high (right) spatial frequencies.

So when you look at her eyes or the background, you see a smile like the one on the left, or in the middle, and you think she is smiling. But when you look directly at her mouth, it looks more like the panel on the right, and her smile seems to vanish. The fact that the degree of her smile varies so much with gaze angle makes her expression dynamic, and the fact that her smile vanishes when you look directly at it, makes it seem elusive.  


The Secret of Leonardo da Vinci’s Interactive Masterpiece
Recreating the subtle illusion behind an elusive smile
by Tangibit Studios, 10 January 2022.
https://medium.com/tangibit-studios/the-secret-of-leonardo-da-vincis-interactive-masterpiece-cadfa66141a4
TL;DR This article uses image processing to examine the Mona Lisa Effect- an expression that seems to change depending on where you are looking. After verifying the perceptual mechanism leading to the elusive smile, the theory is tested by trying to create a modified Mona Lisa with a lessened effect. The same mechanism is then used to recreate the effect in a hybrid image of a modern subject. A link to the Jupyter notebook with the Python code used for the analysis is given at the end of the article.
Mona Lisa elusive smile (source: Wikipedia/J. van Saders) as ../images/Mona-Lisa-head-color.jpg
https://miro.medium.com/v2/resize:fit:1400/format:webp/1*nv_OLiXl6eViQB13E4ocvA.png
>>>
“As you look at the Mona Lisa, your eye moves around the painting. When you look away from her mouth, it enters your peripheral vision and Mona Lisa appears to smile.” — Decoding da Vinci PBS Nova

“She seems like she’s alive because she looks different depending on where you are looking” — Margaret Livingstone

The Mona Lisa is an interactive work of art, providing a deeper experience than just looking at a painting.
>>>
The illusive smile doesn’t depend on color (source: J. van Saders) ../images/Mona-Lisa-head-mono.jpg
https://miro.medium.com/v2/resize:fit:4800/format:webp/1*bAg9-pIqhXAhdLb4rmF7JA.png (Mona Lisa in b/w)
Low frequencies contain an ear to ear grin (source: J. van Saders) ../images/Mona-Lisa-head-blur.jpg
https://miro.medium.com/v2/resize:fit:4800/format:webp/1*h_TH44VVo3V8rjkdGo5l1A.png
>>>
Maximal precision he holds occurs with the beam that travels along the optical axis or “central line”. The more a beam of light diverges from this the less clear the image. “The eye”, he writes,” has only one central line, and all the things that come to the eye along this line are well seen. Around this line are infinite other lines which are so much less efficient as they are a greater distance from the central line.” (Q.V.12v.)

In these words he accounts for the differences between central and peripheral vision. — K. D. Keele
>>>
da Vinci eye line of sight diagram (mirrored from notebook original) (source: J. van Saders)
https://miro.medium.com/v2/resize:fit:4800/format:webp/1*_-HAxldFerfpdleXIJ-94g.png
>>>
Livingstone noted that the low spatial frequencies (that convey shadings) suggest “she’s grinning from ear to ear”. The higher spatial frequencies in the painting portray the details creating a subtly smirking mouth.

The image below has the high frequencies filtered out, similar to our perception by peripheral vision. It shows how da Vinci’s use of sfumato (softening the blend between colors) suggests a smile through subtle shading.
>>>

-->

#

:::{.en}
<div class="imagecap" style="width:68%;">
<img src="../images/Mona-Lisa-in-Louvre.jpg"
     alt="Mona Lisa is the treasure of the Louvre Museum." onclick="showPopup(this)" /></br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:68%;">
<img src="../images/Mona-Lisa-in-Louvre.jpg"
     alt="《蒙娜麗莎》是羅浮宮博物館鎮館之寶。" onclick="showPopup(this)" /></br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- Leonardo da Vinci started painiting Mona Lisa in 1503, but kept working on it over 16 years.
- In 1516, he accepted an invitation from King Francis I of France to settle in one of his castles.
- He left most paintings in Italy, but brought Mona Lisa to France as he regarded this as unfinished.
- Two years later he died, and the painting was passed on to his assistant as part of inheritance.
- King Francis I bought Mona Lisa from the assistant for 4,000 gold coins and kept in his palace.
- The painting was put in Louvre Museum since 1797, was stolen in 1911 and absent for two years.
- The thief was caught when he wanted to sell the painting to an Italian art dealer in Florence.
- The painting, only 77 cm by 53 cm, is now on display inside a bulletproof armoured glass box.
:::

:::{.ch}
- Leonardo da Vinci 於1503年開始繪畫《蒙娜麗莎》，歴時16多年，持續創作。
- 1516年，他接受法國國王 Francis I 的邀請，遠離家鄉，定居在其中一座城堡。
- 他把大部分畫作，留在義大利，但帶《蒙娜麗莎》到法國，因作品尚未完成。
- 兩年後，他與世長辭。這幅畫作傳給他的繪畫助手，作為繼承遺產的一部份。
- 國王 Francis I 花了 4000枚金幣，從助手買下《蒙娜麗莎》，並保存在宮殿。
- 這幅畫自 1797年起，一直存放在羅浮宮博物館，1911年曾被盜，失蹤兩年。
- 小偷終被擒獲，皆因他想出售畫作，賣給佛羅倫斯一位意大利藝術品經銷商。
- 這幅名畫，大小只有 77cm x 53cm。目前放在防彈裝甲玻璃箱內，獨立展出。
:::

<!--
Mona Lisa size = 77cm x 53cm = 30 inches x 21 inches
A4 size = 21cm x 29.7cm = 8.3 inches x 11.7 inches

     21 inches
+-----------------+
|                 |
|                 |
|                 |
|                 | 30 inches
|  8.3            |
+--------+        |
|        |        |
|        |        |
|        |11.7    |
|        |        |
|        |        |
+--------+--------+

8.3  inches x 2 = 16.6 inches < 21 inches
11.7 inches x 2 = 23.4 inches < 30 inches

Not much larger than 4 A4 pages in 2x2 array.


A complete list of all the times the Mona Lisa has been vandalised, from 1956 to 2024
By Dani Maher
https://harpersbazaar.com.au/mona-lisa-vandalism-incidents/
The famed painting, also known as Lia Gioconda, was most recently attacked in January 2024.
(Mona Lisa on display)
https://harpersbazaar.com.au/wp-content/uploads/2022/05/Mona-Lisa-vandalism-attempts.png


Why Is the World So Captivated by the Mona Lisa?
by Michelle Konstantinovsky, 31 May 2012.
https://entertainment.howstuffworks.com/arts/artwork/mona-lisa.htm
(The Mona Lisa resides in the Louvre Museum in Paris, France.)
https://media.hswstatic.com/eyJidWNrZXQiOiJjb250ZW50Lmhzd3N0YXRpYy5jb20iLCJrZXkiOiJnaWZcL21vbmEtbGlzYTIuanBnIiwiZWRpdHMiOnsicmVzaXplIjp7IndpZHRoIjo4Mjh9LCJ0b0Zvcm1hdCI6ImF2aWYifX0=
format avif as ../images/Mona-Lisa-in-Louvre.jpg

Mona Lisa to be moved as part of major Louvre overhaul
by Hugh Schofield, 28 January 2025.
https://www.bbc.com/news/articles/cde9r0xgk67o
The Mona Lisa will be moved to a new exhibition space at the Louvre in Paris as part of a plan to renovate the world's most frequented museum.

Mona Lisa to be moved after reports of dire conditions at the Louvre spur overhaul
by By Astha Rajvanshi and Gabrielle Nolin, 29 January 2025.
https://www.nbcnews.com/news/world/macron-visits-louvre-reports-disrepair-spur-concern-mona-lisa-rcna189553
President Emmanuel Macron attempts some damage control over the dire state of the world’s most-visited museum.
Macron announced that the “Mona Lisa” will “be installed in a special space, accessible independently of the rest of the museum,” which he said will be renovated and expanded in a major overhaul.

The ‘Mona Lisa’ Is Moving to a Room of Her Own at the Louvre
by Sonja Anderson, 29 January 2025.
https://www.smithsonianmag.com/smart-news/the-mona-lisa-is-moving-to-a-room-of-her-own-at-the-louvre-180985930/
As part of a massive renovation, the Leonardo da Vinci portrait will get its own gallery space accessible from a separate entrance

為改善「最令人失望」的觀展體驗，羅浮宮計畫重新遷移〈蒙娜麗莎〉展出位置！
https://artemperor.tw/focus/5983
巴黎羅浮宮（Musée du Louvre）最著名的展品之一為達文西的〈蒙娜麗莎〉（The Mona Lisa），被譽為是義大利文藝復興時期的傑作，每天迎來數名觀眾朝聖，為一睹此巨作，然而近期卻在網路上多則負面評價中提到，它是名畫裡「最令人失望」的觀展體驗。而羅浮宮為改善此現象，計畫將〈蒙娜麗莎〉遷移到地下展廳，為其打造一個獨立空間。

蒙娜麗莎將換地方展 馬克宏宣布羅浮宮逾242億整擴建計畫
〔記者凌美雪／台北報導〕2025/01/29 16:54
https://art.ltn.com.tw/article/breakingnews/4937151
法國總統馬克宏（Emmanuel Macron）於當地時間28日親自宣布，將針對羅浮宮進行一項「新文藝復興」（New Renaissance）計畫，除將把達文西名畫〈蒙娜麗莎〉移至新的空間展出，也將啟動一項新的「建築藍圖國際競賽」，以分散玻璃金字塔做為單一入口的人潮壓力，初估該計畫將耗資7至8億歐元（約242億以上），盼最遲於2031年完成。

-->

#

:::{.en}
<div class="imagecap" style="width:82%">
<img src="../images/Mona-Lisa-beneath-and-now.jpg"
     alt="Mona Lisa restored from behind layers of paint (left), compare to what we now see (right)." onclick="showPopup(this)" /></br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:82%">
<img src="../images/Mona-Lisa-beneath-and-now.jpg"
     alt="從油漆底層修復得出的《蒙娜麗莎》（左），相比我們現在看到的人像（右）。" onclick="showPopup(this)" /></br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- Leonardo da Vinci painted Mona Lisa on a piece of wood, then apply layers of paint to have the effect.
- In 2004, Pascal Cotte was granted access by the Louvre to find out what was underneath the layers.
- He took photos using many different wavelengths of light, a technique called multispectral imaging.
- These many exposures produce slightly different images, as each wavelength reach a distinct layer.
- Studying these images can reveal hidden details, even the original Mona Lisa (restored on the left).
:::

:::{.ch}
- Leonardo da Vinci 在木板上，開始畫《蒙娜麗莎》，然後塗上一層層油彩，以達到如此效果。
- 2004年，Pascal Cotte 獲得羅浮宮批准，進行研究，分析在這些油彩層下，畫師作畫的情況。
- 他使用各種波長的光，為畫作拍照，這種技術稱為「多光譜成像」（multispectral imaging）。
- 由於每個波長的光線，到達不同的層面，多次曝光後產生的影像，顯示每層人像，略有不同。
- 研究這些圖像，可以揭示蘊藏細節，甚至從最底層，還原《蒙娜麗莎》（左圖是原型修復）。
:::

<!--
![](){ width=48% }

Beneath the Mona Lisa Lies a Second Portrait, Scientist Claims
By Devin Coldewey, 9 December 2015.
https://www.nbcnews.com/tech/innovation/beneath-mona-lisa-lies-second-portrait-scientist-claims-n476461
Pascal Cotte claims that underneath the Mona Lisa's enigmatic smile lies a second, different portrait, perhaps even of a different person.
>>>
Perhaps the most famous painting in history, Leonardo Da Vinci's "Mona Lisa" is also among the most closely scrutinized — and one scientist has capped a decade of study with the claim (PDF) that underneath that enigmatic smile lies a second, different portrait, perhaps even of a different person.
>>>
http://www.lumiere-technology.com/PC_LML_ENG.pdf
https://media1.s-nbcnews.com/i/MSNBC/Components/Video/__NEW/2015-12-09T23-58-40-566Z--1280x720.jpg

REVEALED: The OTHER woman found hidden UNDER the iconic Mona Lisa painting
by Zoie O'Brien, 9 December 2015.
https://www.express.co.uk/news/weird/625115/Mona-lisa-Art-DaVinci-Paris-France
THIS digital recreation of an image found UNDER Leonardo da Vinci's masterpiece has thrown new doubts over the real identity of Mona Lisa.
Mona Lisa (right) and the original found underneath (left)
https://cdn.images.express.co.uk/img/dynamic/80/590x/monanew-625115.jpg as ../images/Mona-Lisa-compare-original.jpg
?r=1686998680160 as 

-->

#

:::{.small}
Decoding da Vinci | Full Documentary | NOVA | PBS【53:36】cc<br>
<a href="https://youtu.be/NGsUFvwgvCo">
<img src="http://img.youtube.com/vi/NGsUFvwgvCo/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- NOVA is an American TV series similar to BBC Horizon, winning many production awards.
- This documentary was based on a book ["Leonard da Vinci"](../books/Walter.Isaacson.Leonardo.da.Vinci.pdf), by Walter Isaacson in 2017.
- The author gave a [short interview](https://youtu.be/lHGaT3NpQAw) introducing his book, and why he decided to write it:
-
| _"I have always interested in people who could connect art to science, that's what Leonardo was."_
- In this video, the last part displays how to peel off layers of Mona Lisa to reveal the original.
:::

:::{.ch}
- NOVA 是美國電視節目系列，類似予 BBC 的《Horizon》，製作認真，榮獲多項獎項。
- 這部長篇紀錄片，內容根據 Walter Isaacson 在 2017年出版的書[《Leonard da Vinci》](../books/Walter.Isaacson.Leonardo.da.Vinci.pdf)。
- 作者在[簡短訪問](https://youtu.be/lHGaT3NpQAw)中，介紹他這本著作。並談及他為什麼會下定決心，撰寫這本傳記：
-
| _「探討 Leonardo，他是能夠結合藝術與科學的人物。我對這類人物，一直感興趣。」_
- 視頻中最後部分，展示如何抽絲剝繭，除去《蒙娜麗莎》的層層油彩，以揭示其原貌。
:::

#

:::{.small}
Mona Lisa (Full Length): Great Art Explained【32:54】cc<br>
<a href="https://youtu.be/ElWG0_kjy_Y">
<img src="http://img.youtube.com/vi/ElWG0_kjy_Y/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- This video has detailed information about the famous painting, as well as the famous painter.
- Explains why this unique masterpiece is a testament of Leonardo's pure passion and genius.
-
| _"Scientific analysis shows that Leonardo used up to 30 different layers of painted glaze on Mona Lisa,_
| _applied so thinly that it only totals 40 micrometers of paint! That's half the width of a human hair."_
- If you don't have half an hour to spend, there is a [short version](https://youtu.be/T9JvUDrrXmY), with less information of course.
:::

:::{.ch}
- 視頻內容資訊詳細，包括名畫本身以及畫家本人。全方位探討，五花八門。
- 解釋為何這幅傑作，獨一無二。表明 Leonardo 的求知熱誠，並且天才橫溢。
-
| _「科學分析表明，Leonardo 在《蒙娜麗莎》上使用了多達30層不同的油彩，_
| _塗得薄薄，總共油彩厚度只有40微米（micrometer）！ 即頭髮寬度的一半。」_
- 若沒有空閒花上半小時，視頻有[簡短版本](https://youtu.be/T9JvUDrrXmY)。資訊當然較少，但仍林林總總。
:::

#

:::{.small}
Da Vinci Tricked Everyone With A Secret Illusion【13:23】cc<br>
<a href="https://youtu.be/NctcpUvRcqM">
<img src="http://img.youtube.com/vi/NctcpUvRcqM/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- This is a good video explaining the tricks hidden in the famous painting Mona Lisa.
- Leonard da Vinci was a talented painter, an illustrator, a scientist and an inventor.
- He kept his many detailed observations, investigations and ideas in his notebooks.
-
| _"Leonard strategically painted shadows around Mona Lisa's mouth,_
| _so that when we look into her eyes she's grinning from ear to ear,_
| _but when we look at her mouth she's at best smirking, amazing!"_
:::

:::{.ch}
- 這個視頻很好，解釋這幅名畫《蒙娜麗莎》中，隱藏的技巧。
- Leonard da Vinci 是一位天才畫家、插畫家、科學家和發明家。
- 他的細心觀察、深入調查和奇妙想法，通通記錄在筆記本上。
- 
| _「Leonard 在《蒙娜麗莎》的嘴唇周圍塗上陰影，十分巧妙。_
| _因此，當我們望著她的眼睛時，她微笑似合不攏嘴，_
| _但是，當我們看到她的嘴巴時，她充其量只是扮笑，真棒！」_
:::

#

:::{.small}
How Leonardo da Vinci Changed the World【53:45】cc<br>
<a href="https://youtu.be/pN2pPhpAxXk">
<img src="http://img.youtube.com/vi/pN2pPhpAxXk/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- An excellent video biography of Leonardo da Vinci, following chronological order.
- Leonardo da Vinci was an artist, a scientist, even an inventor, indeed a rare genius.
- His inventions, although not realized at his time, are tested now to be workable.
- 
| _"His huge collection of notes has helped to form a great deal_
| _of what we now know and understand about Leonardo da Vinci._
| _And many of these collections of notes have become_
| _just as valuable and famous as his paintings."_
:::

:::{.ch}
- 精彩視像傳記，講述 Leonardo da Vinci 的一生。按年份排序，分析歴史情節。
- Leonardo da Vinci（達文西）是藝術家、科學家、發明家，簡直是稀世天才。
- 他的發明，雖然在他那個時代未能實現，但現在經過測試，證實的確是可行。
-
| _「現在，   我們對 Leonardo da Vinci 的認知，_
| _歸功於大量收集到留下的筆記，題材廣泛。_
| _其中許多筆記頁集，繪圖精緻，才華橫溢，_
| _就像他的畫作一樣，舉世聞名兼價值連城。」_
:::


#

:::{.small}
The Secret of Mona Lisa | Perspective【52:17】cc<br>
<a href="https://youtu.be/13TyiCiOBlI">
<img src="http://img.youtube.com/vi/13TyiCiOBlI/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- There are many different stories behind this world-famous work by Leonardo da Vinci.
- Historians form different opinions, selecting evidence to support their various theories.
- One team embraces the official story, that Lisa was the charming wife of a silk merchant.
- Another team offers a different story, that Leonardo just wanted to paint a perfect 'Lisa'.
- This documentary suggests that perhaps Mona Lisa is the imaginary picture of a mother.
:::

:::{.ch}
- 這一幅 Leonardo da Vinci 的作品，舉世知名。背後故事很多，各式各樣。
- 各派歷史學家，堅持不同的觀點，搜羅證據，支持他們各自相信的理論。
- 其中一個團隊，認同官方的說法，即 Lisa 確是一位迷人的絲綢商人妻子。
- 而另一個團隊，提供不同的觀點，Leonardo 只是想畫出完美的「Lisa」。
- 這部紀錄片提出，也許《蒙娜麗莎》，只不過是想像中母親的理想形象。
:::

<!--
29/01/24
========
Google: Science behind Mona Lisa

Mona Lisa - Ledonardo's masterful technique - PBS
https://www.pbs.org/treasuresoftheworld/mona_lisa/mlevel_1/m3technique.html
very little info.

The Art and Science of Mona Lisa’s Smile
by Dr. Haddad, 12 October 2017
https://www.rochesteradvanceddentistry.com/blog/the-art-and-science-of-mona-lisas-smile/
Leonardo da Vinci created the world’s most famous smile when he painted the Mona Lisa. It took him 16 years to complete the work. It is a painting of such singular genius that many of the techniques behind it have been lost to time, and are only now being rediscovered using a complex array of scientific techniques and historical investigations.
>>>
And his mastery of optics let him create a special trick when it comes to the smile. When you look directly at the smile, the contours and the shading create an effect which might not be a smile, exactly. But when your eyes move away from the smile, the unfocusing of your eye causes the smile to look more pronounced. In technical terms, this is because  “A clear smile is much more apparent in the low spatial frequency [blurrier] images than in the high spatial frequency image.” The practical effect is that Mona Lisa seems always to be smirking at you behind your back, but always has an innocent expression when you look at her directly, a delightfully mischievous effect.
>>>
Vision and Art
https://livingstone.hms.harvard.edu/vision-and-art
(show three images of different frequencies)

NOVA: Decoding da Vinci
Season 46 Episode 21 | 53m 31s  |
Video has closed captioning.  |
https://www.pbs.org/video/decoding-da-vinci-93ssvo/
(video not available - but see below)

NOVA: How did Leonardo da Vinci Paint the "Mona Lisa"?
Clip: Season 46 Episode 21 | 1m 45s  |
https://www.pbs.org/video/how-did-leonardo-da-vinci-paint-mona-lisa-w4ra4r/
(less than 2 minutes!)
An introduction by Walter Isaacson.

The Science Behind Mona Lisa’s Smile
How Leonardo da Vinci engineered the world’s most famous painting
By Walter Isaacson, November 2017 Issue of "The Atlantic"
https://www.theatlantic.com/magazine/archive/2017/11/leonardo-da-vinci-mona-lisa-smile/540636/
This article has been adapted from Walter Isaacson’s new book, Leonardo da Vinci.
>>>
By standing astride the intersection of the arts and the sciences, he became history’s most creative genius.
His greatest triumph of combining art, science, optics, and illusion was the smile of the Mona Lisa, which he started working on in 1503 and continued laboring over nearly until his death 16 years later. He dissected human faces, delineating the muscles that move the lips, and combined that knowledge with the science of how the retina processes perceptions. The result was a masterpiece that invites and responds to human interactions, making Leonardo a pioneer of virtual reality.

The magic of the Mona Lisa’s smile is that it seems to react to our gaze. What is she thinking? She smiles back mysteriously. Look again. Her smile seems to flicker. We glance away, and the enigmatic smile lingers in our minds, as it does in the collective mind of humanity. In no other painting are motion and emotion, the paired touchstones of Leonardo’s art, so intertwined.
>>>

How Leonardo Da Vinci 'Augmented Reality' — 500 Years Ago【3:01】cc
https://www.youtube.com/watch?v=GyT1wPQSER8
We may think of Leonardo Da Vinci as an artist, but he was also a scientist. By incorporating anatomy, chemistry, and optics into his artistic process, Da Vinci created an augmented reality experience centuries before the concept even existed. This video details how Da Vinci made the Mona Lisa interactive using innovative painting techniques and the physiology of the human eye.

YouTube "Mona Lisa PBS" or "Art and Science of Mona Lisa":

Mona Lisa (short version)i: Great Art Explained【13:51】cc
https://www.youtube.com/watch?v=T9JvUDrrXmY
There is a longer (30 minute) version of this video on my site which is better quality and deals with questions like "Why doesn't she have eyebrows?" and "Is she only famous because she was stolen" - this shorter version though, gives you a good overview.
(very good)

Mona Lisa (Full Length): Great Art Explained【32:54】cc
https://www.youtube.com/watch?v=ElWG0_kjy_Y
James Payne discusses the Mona Lisa in a special extended film. If you enjoyed the 15-minute film, then don't miss this one. Lots of new information, presented with just the facts. No conspiracy theories or nonsense in this film, just straight forward information.
(very good)

Mona Lisa - Secret Behind Mona Lisa - History Channel Documentary【50:12】cc
https://www.youtube.com/watch?v=r3Byzx0HoRk
(a documentary, investigate historical documents and forensic examination)
(another best, showing different versions of the painting, Isleworth Mona Lisa, even the original underneath)
(however, the documentary is incomplete -- cut at 50:12!)

Documentary Films 2017
https://www.youtube.com/channel/UCno-wMPE0L8QpZoXeWFVc6g/

Secret of Mona Lisa - History Channel Documentary
The Strange Attraction Of The Mona Lisa | The Secret of Mona Lisa | Perspective【52:17】cc
https://www.youtube.com/watch?v=13TyiCiOBlI
It's one of the most popular portraits in the history of art. Yet the subject of that portrait remains elusive. The Secret of Mona Lisa explores the trail of evidence that has gathered since the da Vinci masterwork was first unveiled around 1503.
(a film documentary) different from the one above.
A film by Klaus T. Steindl, concludes Mona Lisa is just an imaginary image of a mother.

The Real Story Behind The Da Vinci Code | The Da Vinci Code Decoded (2006) | Full Film【1:36:08】cc
https://www.youtube.com/watch?v=xQ56Xgz6iS0
(1 month ago!)
Dan Brown's 'The Da Vinci Code' was a literary phenomenon that enraged religious groups and infuriated critics. 'Da Vinci Code Decoded' is the definitive documentary on the theories behind the novel, from the real role of Mary Magdalene to the coded messages in Leonardo Da Vinci's paintings and the secrets of the Gnostic gospels.
(not Mona Lisa, but the bloodline of Jesus Christ, about the claims in the movie "The Da Vinci Code". Some parts just repeating, a duplicate section.)

Decoding da Vinci | Full Documentary | NOVA | PBS【53:36】cc
https://www.youtube.com/watch?v=NGsUFvwgvCo
PBS (10 months ago)
Leonardo da Vinci was a Renaissance genius. Not only did he paint masterpieces of art, but he was an obsessive scientist and inventor, dreaming up complex machines centuries ahead of his time, including parachutes, armored tanks, hang gliders, and robots. On the 500th anniversary of Leonardo’s death, with the help of biographer Walter Isaacson, NOVA investigates the secrets of Leonardo’s success. How did his scientific curiosity, from dissections of cadavers to studies of optics, shape his genius and help him create perhaps the most famous painting of all time, the "Mona Lisa"? 
(almost the best)

Decoding da Vinci FULL SPECIAL | NOVA | PBS America【53:04】cc
https://www.youtube.com/watch?v=DXGTH_Lqook
PBS (9 months ago) (same as above, just 3 minutes short)
Journey to Florence to discover how Leonardo da Vinci used science, from human dissections to innovative painting techniques, to create his legendary artwork. Learn why Mona Lisa's smile is so captivating—and what it took to create it.

What the Mona Lisa Originally Looked Like【3:12】cc
https://www.youtube.com/watch?v=FL1iGnKQ5Rs
PBS (2 years ago)
An engineer used powerful lights, cameras, and computer software to digitally remove the varnish from the "Mona Lisa," allowing people today to see the painting as Leonardo da Vinci created it.
(extract from the last part of the full PBS documentary)

Da Vinci Tricked Everyone With A Secret Illusion【13:23】cc
https://www.youtube.com/watch?v=NctcpUvRcqM
>>>
In 1503, Leonardo da Vinci was approached by a wealthy silk merchant named Francesco del Giocondo to paint a portrait of his 24-year old wife, Lisa. What started as a run-of-the-mill portrait turned into Leonardo da Vinci’s magnum opus. A masterpiece the artist would continue working on until his death. A painting he would never part with. 

The story of how the Mona Lisa became the most famous painting in the world is a messy one. In 1911, she was stolen from the Louvre by a man named Vincenzo Peruggia who falsely believed France possessed the painting because Napoleon Bonaparte stole it from Italy. The world was fascinated by the theft and it became a national scandal seemingly overnight. This event undoubtedly accelerated the Mona Lisa’s rise to fame. 

That being said, if there were an alternate reality where the Mona Lisa was never stolen, I believe she would still be famous. The Mona Lisa had many admirers before the heist and she was created by one of the greatest geniuses who ever lived. In addition. Leonardo da Vinci used many innovative techniques to create the Mona Lisa. I think there’s a good chance Leonardo knew this was his greatest masterpiece and that’s why he never let her go. Thanks for watching!
>>>
(good story)

How Leonardo da Vinci Changed the World【53:45】cc
https://www.youtube.com/watch?v=pN2pPhpAxXk
Leonardo da Vinci, the famous Italian artist and polymath who lived during the high renaissance changed the world in a huge way. He is best known for his painting the Mona Lisa, which is the most famous piece of art in the world. Many believe Leonardo da Vinci - Polymath to be one of the greatest artists and painters ever to live, and this is probably true. But Leonardo da Vinci was so much more than just one of the greatest artists. He was an architect, an astronomer, a botanist, a cartographer, an engineer, a geologist, a hydrodynamicist, a mathematician, a musician, a theatre producer, a scientist, an inventor and much, much more. Leonardo was a pioneer in so many different areas and he truly was a genius.   
Many people ask Did Leonardo da Vinci do this or Was Leonardo da Vinci that… well in this video I do my best to answer many of the most popular questions about Leonardo, whilst exploring his life and asking the question How Did Leonardo da Vinci Change the World?  
(very good biography of Leonardo da Vinci, coming from Walter Isaacson's book)
>>>
Leonardo da Vinci was left handed, and in those days the ink used was very easily smudged.
So to avoid constantly smearing his work, Leonardo devised a way of writing in mirror script.

His huge collection of notes has helped to form a great deal of what we now know and understand about Leonardo da Vinci.
And many of these collections of notes have become just as valuable and famous as his paintings.
>>>

History Documentary BBC ❖ Leonardo DaVinci, behind a Genius【1:28:26】cc
https://www.youtube.com/watch?v=5yT4IkPtTRo
BBC (2017) not HD, like fuzzy VHS

Raiders of the Lost Art | Episode 4 | Leonardo & the Mona Lisa | Free Documentary History【44:14】cc
https://www.youtube.com/watch?v=lwTrlw3kwl0
The story of the theft of the Mona Lisa in 1911 from the Louvre and how Pablo Picasso was accused of the crime - plus the incredible tales of how many other of da Vinci's works were lost to history and had to be reclaimed.
(very good)

Better Know the Mona Lisa | The Art Assignment | PBS Digital Studios【5:47】cc
https://www.youtube.com/watch?v=Ooz3u9y7P7I
A short story of the painting.

Why is the Mona Lisa so famous? - Noah Charney【5:37】cc
https://www.youtube.com/watch?v=yRK_uCMwZPY
(2 years)
Discover what factors helped Leonardo da Vinci’s “Mona Lisa” become the world’s most famous painting.
A cartoon story of the painting, passing through various people.

The true colours of the Mona Lisa【3:04】no cc
https://www.youtube.com/watch?v=rDcCeeSe5EY
In this video I will restore the most important work of art in the world. La Gioconda or Mona lisa of the Louvre museum.
This piece is not possible to restore due to the conditions it is in. Leonardo Da Vinci painted this painting with innovative paintings for his time, but unfortunately these have caused The Mona Lisa to be full of totally irreparable cracks.
To imagine how a real restoration of this painting would be, we will resort to the most advanced digital techniques. In this way we will eliminate the yellowish varnish and correct all the fissures, giving back the color that it should have when it was painted by Leonardo.
1. de-varnishing  2. chromatic reintegration  3. color restoration

The uncommon mastermind behind Mona Lisa’s smile【8:17】cc
https://www.youtube.com/watch?v=lHGaT3NpQAw
PBS NewsHour
Leonardo da Vinci’s inquisitive spirit defined the quintessential “renaissance man.” The master artist, scientist and inventor is now the subject of a new biography by Walter Isaacson: "Leonardo da Vinci".
WALTER ISAACSON: I have always been interested in people who could connect art to science. It's imaginative to people who count.
And that is what Leonardo was. He was somebody who loved both art and science.
And by standing at that intersection, I said, oh, this is how imagination works.
I was totally blown away by certain things of Leonardo, first of all, the role of theatrics and pageant.
Secondly, the depth of his curiosity about science.
It's that notion that we can embrace the beauty of every pattern in nature. That's what we need to relearn today.

-->

<!--
pandoc -t revealjs -s --mathjax monalisa.md -o monalisa.html
-->
