---
title: New Year 2024
author: <span class="en">and the number 2025</span><span class="ch">和數字 2025</span>
date: <span class="en">January 2024</span><span class="ch">2024年1月</span>
theme: solarized
header-includes: |
    <base target="_blank"/>
    <link rel="stylesheet" type="text/css" href="../css/slides.css"></link>
    <script type="text/javascript" src="../scripts/site.js"></script>
include-before: |
    <!-- Any raw HTML code right after body tag, before anything else. -->
    <div id="modal" class="modal">
      <img id="modal-content" />
      <div id="modal-caption"></div>
    </div>
    <div align="right" class="banner">
    <input type="radio" name="lang" onclick="set_lang('en')" id="english" value="English" checked /> English |
    <input type="radio" name="lang" onclick="set_lang('ch')" id="chinese" value="Chinese" /> 中文
    </div>
include-after: |
    <!-- Any raw HTML code right before body end tag, after anything else. -->
    <script>window.onload = check</script>
    <!-- file:///Users/josephchan/jc/q/site/slides/newyear2024.html -->
    <!-- https://jhlchan.bitbucket.io/slides/newyear2024.html -->
...


#

:::{.en}
<div class="imagecap" style="width:40%">
<img src="../images/new_year_2024.jpg"
     alt="New Year 2024, anticipating 2025." onclick="showPopup(this)" /></br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:40%">
<img src="../images/new_year_2024.jpg"
     alt="新年2024年，展望2025年。" onclick="showPopup(this)" /></br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- A student sends me this picture to celebrate the New Year.
- The calculations are correct (can verify directly in Google).
- It seems to be a numerical coincidence, but it is actually due to $45\times{45} = 2025$.
- But surrounding the two equations is a regular heptadagon ($17$-gon) which is more interesting.
- If you want to know the math behind, keep going!
:::

:::{.ch}
- 學生傳來的圖片，祝賀新年。
- 算式正確（可直接𨫡入 Google 驗証）。
- 看似是數字巧合，其實皆因 $45\times{45} = 2025$。
- 但可知道，在兩個算式外圍的正十七邊形（$17$-gon）更有意思。
- 如想了解其中的數學，請繼續！
:::

---

:::{.en}
<div class="imagecap" style="width:98%">
<img src="../images/compute_2024.jpg" style="width:80%"
     alt="Verify the first equation in Google." onclick="showPopup(this)" />
<img src="../images/compute_2025.jpg" style="width:80%"
     alt="Verify the second equation in Google." onclick="showPopup(this)" />
</br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:98%">
<img src="../images/compute_2024.jpg" style="width:80%"
     alt="Google 驗證第一個算式。" onclick="showPopup(this)" />
<img src="../images/compute_2025.jpg" style="width:80%"
     alt="Google 驗證第二個算式。" onclick="showPopup(this)" />
</br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

<!--

Google:
https://www.google.com/
(20 + 24) + (20 + 24)(20 + 24) + (20 + 24) = 2024
1^3 + 2^3 + 3^3 + 4^3 + 5^3 + 6^3 + 7^3 + 8^3 + 9^3 = 2025

WolframAlpha:
https://www.wolframalpha.com/
sum n^3, n=1 to 9    gives: sum_(n=1)^9 n^3 = 2025
sum n^3, n=1 to k    gives: sum_(n=1)^k n^3 = 1/4 k^2 (k + 1)^2

-->


#

:::{}
$15\times{15} =\ ?$</br>
$25\times{25} =\ ?$</br>
$35\times{35} =\ ?$</br>
$45\times{45} =\ ?$</br>
$\dots\dots$</br>
$95\times{95} =\ ?$</br>
:::
</br>

:::{.en}
- To compute these squares $15\times{15}, 25\times{25}, 35\times{35}, \dots$ there is a trick.
- You can do this trick in your head: multiply left by one plus left, and multiply right by right.
- For example, $75\times{75} = 7\times{8} \text{ with } 5\times{5} = 5625$, that’s it.
- Later when you encounter the square of $(?{5})$, you can just tell the result like a whiz-kid 😲
:::

:::{.ch}
- 計算這些平方 $15\times{15}、25\times{25}、35\times{35}、\dots$ 有速算法。
- 簡單容易，速算法是：左乘左加一，右乘右。
- 例如，$75\times{75} = 7\times{8} \text{ 補上 } 5\times{5} = 5625$，即管試吓。
- 以後遇上「乜五」自乘，你便隨口嗡出，表演「神算子」😲
:::


<!--

# $45^{2} = 2025$

+--------------:+:--------------+:------------------:+
| Right         | Left          | Centered           |
+---------------+---------------+--------------------+
| Good          | Better        | Best               |
+---------------+---------------+--------------------+
| Bad           | Worse         | Worst              |
+---------------+---------------+--------------------+

This simple table has horizontal lines, not suitable.

-->

#

:::{.en}
<div class="imagecap" style="width:36%;">
<img src="../images/binomial-square.jpg"
     alt="Formula for (a + b)(a + b)." onclick="showPopup(this)" /></br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:36%;">
<img src="../images/binomial-square.jpg"
     alt="(a + b)(a + b) 的公式。" onclick="showPopup(this)" /></br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- Refer to the picture above to get: $(a + b)^{2} = a^{2} + 2ab + b^{2}$.
- Putting $a = n, b = 1$, we have: $(n + 1)^{2} = n^{2} + 2n + 1$.
- Therefore, compute: $2024 = 2025 - 1 = 45^{2} - 1 = (44 + 1)^{2} - 1 = 44^{2} + 2\times{44} = 44 + 44\times{44} + 44$.
- With $44 = 20 + 24$, this gives the first formula: $2024 = (20 + 24) + (20 + 24)(20 + 24) + (20 + 24)$.
:::

:::{.ch}
- 參考上圖，就知道：$(a + b)^{2} = a^{2} + 2ab + b^{2}$。
- 設 $a = n, b = 1$，就有：$(n + 1)^{2} = n^{2} + 2n + 1$。
- 因此，計算：$2024 = 2025 - 1 = 45^{2} - 1 = (44 + 1)^{2} - 1 = 44^{2} + 2\times{44} = 44 + 44\times{44} + 44$。
- 配合 $44 = 20 + 24$，便是第一式：$2024 = (20 + 24) + (20 + 24)(20 + 24) + (20 + 24)$。
:::

#

:::{.en}
<div class="imagecap" style="width:32%;">
<img src="../images/sum-of-cubes.jpg"
     alt="Formula for adding consecutive cubes." onclick="showPopup(this)" /></br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:32%;">
<img src="../images/sum-of-cubes.jpg"
     alt="連續立方數相加的公式。" onclick="showPopup(this)" /></br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- The picture above shows this math formula: sum of cubes from $1$ to $n$ = square of (sum of $1$ to $n$).
- That is, $1^{3} + 2^{3} + \dots + n^{3} = (1 + 2 + \dots + n)^{2}$.
- Put $n = 9$, note that $1 + 2 + \dots + 9$ = (first + last) x number of terms ÷ 2 = $10\times{9}/2 = 45$.
- Remember the trick for $45^{2}$? This gives the second formula: $2025 = 1^{3} + 2^{3} + 3^{3} + 4^{3} + 5^{3} + 6^{3} + 7^{3} + 8^{3} + 9^{3}$.
:::

:::{.ch}
- 上圖表明此數學公式：$1$ 至 $n$ 的立方總和 =（$1$ 相加至 $n$）的平方。
- 即是： $1^{3} + 2^{3} + \dots + n^{3} = (1 + 2 + \dots + n)^{2}$。
- 代入 $n = 9$，注意： $1 + 2 + \dots + 9$ = (頭+尾)x項數÷2 = $10\times{9}/2 = 45$。
- 還記得 $45^{2}$ 的速算法嗎？因此有第二式：$2025 = 1^{3} + 2^{3} + 3^{3} + 4^{3} + 5^{3} + 6^{3} + 7^{3} + 8^{3} + 9^{3}$。
:::

<!--

Proof with words: Sum of Cubes
https://maa.org/sites/default/files/Alan_L49975._Fry.pdf

Proof without words: Sum of squares
https://maa.org/sites/default/files/Siu15722.pdf

-->

#

:::{.en}
<div class="imagecap" style="width:98%">
<img src="../images/Gauss_1840_by_Jensen.jpg" style="width:25%"
     alt="Carl Friedrich Gauss is known as the prince of mathematics." onclick="showPopup(this)" />
<img src="../images/Construct_regular_17_gon.gif" style="width:32%"
     alt="An animation showing how to construct the regular 17-gon." onclick="showPopup(this)" />
</br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:98%">
<img src="../images/Gauss_1840_by_Jensen.jpg" style="width:25%"
     alt="Carl Friedrich Gauss 被譽為「數學王子」。" onclick="showPopup(this)" />
<img src="../images/Construct_regular_17_gon.gif" style="width:32%"
     alt="動畫顯示如何繪畫正規17邊形。" onclick="showPopup(this)" />
</br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- In mathematics, the regular $17$-sided polygon is a wonderful curiosity.
- This is because it can be drawn with compass and ruler (very tedious and complicated, be patient).
- This result, unknown since ancient Greek geometry for 2000 years, was obtained by Gauss, the "Prince of Mathematics".
- A regular polygon with $n$ sides is an $n$-gon, $n > 2$. What $n$-gon can be drawn with compass and ruler?
- When he was 18 years old, he proved that if $n$ is a prime number ($3, 5, 7, 11, 13, 17, \dots$), then $n$ can only be $1$ more than a repeated square of $2$ ($2, 4 = 2^{2}, 16 = 4^{2}, 256 = 16^{2}, \dots$).
- So the regular prime $n$ polygons drawn by geometric methods are:</br> $2+1=3$-gon (triangle), $4+1=5$-gon (pentagon), $16+1=17$-gon, and others.
:::

:::{.ch}
- 在數學中，正 $17$ 邊形是美妙的奇葩。
- 因為它可用圓規和直尺繪製（既複雜又繁瑣，要有耐心）。
- 這個結果，從2000年前古希臘幾何以來，冇人知道，由「數學王子」Gauss（高斯）得出。
- 正多邊形 $n$-gon 有 $n$ 條邊，$n > 2$。甚麽 $n$-gon 能以圓規直尺繪出？
- 他18歲時證明：若 $n$ 是素數（$3、5、7、11、13、17、\dots$），則 $n$ 與 $2$ 的疊方數（$2、4 = 2^{2}、16 = 4^{2}、256 = 16^{2}、\dots$）只能多出 $1$。
- 於是，由幾何方法繪畫的素數 $n$ 正多邊形，有：</br> $2+1=3$-gon (三角形），$4+1=5$-gon (五邊形)、$16+1=17$-gon，還有其他。
:::

<!--

Carl Friedrich Gauss
https://en.wikipedia.org/wiki/Carl_Friedrich_Gauss
https://upload.wikimedia.org/wikipedia/commons/thumb/e/ec/Carl_Friedrich_Gauss_1840_by_Jensen.jpg/440px-Carl_Friedrich_Gauss_1840_by_Jensen.jpg
~/images/Carl_Friedrich_Gauss_1840_by_Jensen.jpg

卡爾·腓特烈·高斯
https://zh.wikipedia.org/zh-hk/卡爾·弗里德里希·高斯

regular 17-gon
十七邊形
https://zh.wikipedia.org/zh-hk/十七边形
1796年高斯（時年18歲）證明了可以用尺規作圖作出正十七邊形，同時發現了可作圖多邊形的條件。正十七邊形其中一個作圖方法如下：
https://upload.wikimedia.org/wikipedia/commons/d/d1/Regular_Heptadecagon_Inscribed_in_a_Circle.gif
~/images/Construct_regular_17_gon.gif

Heptadecagon
https://en.wikipedia.org/wiki/Heptadecagon
more animations, with quite detailed but complex explanations. No explanation for the one above.

gif frame extractorworld's simplest gif tool
https://onlinegiftools.com/extract-gif-frames
https://upload.wikimedia.org/wikipedia/commons/d/d1/Regular_Heptadecagon_Inscribed_in_a_Circle.gif
This one has 462 frames, take several minutes to extract.
at 117 to 119.
To see all frames, use:
https://ezgif.com/split/
這一步圓規跨度是隨意。斜線與垂直線角度是 α，往後的步驟只是把角度二分再二分，得出 α/4（有紅α/4、綠α顯示）。
跟著畫呀畫，得出45°（紅色），已抹去所有弧線。再努力畫，得出3x360°/17（紅色），又抹去其他線條。但已擁有最重要的角，最後完成。

https://ezgif.com/split/
如果想睇清楚，把動畫網址 (https://upload.wikimedia.org/wikipedia/commons/d/d1/Regular_Heptadecagon_Inscribed_in_a_Circle.gif) copy/paste 到以上網頁，便逐格顯示。共462格，有排睇！


JOHANN CARL FRIEDRICH GAUSS CHANGED HISTORY WITH HIS 17-SIDED SHAPE
by Yasmin Tayag, 1 May 2018.
https://www.inverse.com/article/44309-johann-carl-friedrich-gauss-math-statistics-accomplishments
He famously called math "the queen of the sciences."
(still too hard)
The Amazing Heptadecagon (17-gon) - Numberphile)【13:40】cc
https://www.youtube.com/watch?v=87uo2TPrsl8
(a bit long)

Heptadecagon and Fermat Primes (the math bit) - Numberphile【12:37】cc
https://www.youtube.com/watch?v=oYlB5lUGlbw

The 17-gon【1:32】no cc
https://www.youtube.com/watch?v=Ev9V_K6y4Gc
Just say so, then contruct, no narrative, background music.

The problem Euclid couldn't solve【16:53】cc?
https://www.youtube.com/watch?v=BaegtyXSxwo
With geometric explanation, a bit hard for general viewers.


Fermat number
https://en.wikipedia.org/wiki/Fermat_number
{\displaystyle F_{n}=2^{2^{n}}+1,} where n is a non-negative integer.
The first few Fermat numbers are:
3, 5, 17, 257, 65537, 4294967297, 18446744073709551617, ... (sequence A000215 in the OEIS).

-->

#

:::{.land}
&#10022;<span class="en">Mathland</span><span class="ch">數學樂園</span>&#10022;
:::

:::{.en}
- Math teachers should understand the following points.
- $(a + b)^{2} = a^{2} + 2ab + b^{2}$ is a special case of the general binomial formula $(a + b)^{n}$ for $n = 2$.
- The reason for the special trick of $(?5)\times{(?5)}$ is this:</br>
$(10a + 5)(10a + 5) = 100a^{2} + 2\times{10a}\times{5} + 5^{2} = 100a^{2} + 100a + 25 = 100a(a + 1) + 25$.
- How do you explain this formula to students?</br>
$1 + 2 + \dots + n$  = (first + last) x number of terms ÷ 2 = $n(n + 1)/2$.
- I use this method:
:::

:::{.ch}
- 數學老師聰明，應該明白下列要點。
- $(a + b)^{2} = a^{2} + 2ab + b^{2}$ 是一般二項式公式 $(a + b)^{n}$ 的特例，其中 $n = 2$ 。
- $(?5)\times{(?5)}$ 有速算法，原因是：</br>
$(10a + 5)(10a + 5) = 100a^{2} + 2\times{10a}\times{5} + 5^{2} = 100a^{2} + 100a + 25 = 100a(a + 1) + 25$。
- 你如何向學生解釋這個公式？</br>
$1 + 2 + \dots + n$ = (頭+尾) x 項數 ÷ 2 = $n(n + 1)/2$。
- 我用此方法：
:::

:::{.en}
<div class="imagecap" style="width:48%;">
<img src="../images/sum_formula.jpg"
     alt="Adding consecutive numbers." onclick="showPopup(this)" /></br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:48%;">
<img src="../images/sum_formula.jpg"
     alt="連續數相加。" onclick="showPopup(this)" /></br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

<!--
pandoc -t revealjs -s --mathjax newyear2024.md -o newyear2024.html
-->
