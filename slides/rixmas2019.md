---
title: Christmas Lectures 2019
author: <span class="en">The Royal Institution, London</span><span class="ch">倫敦皇家研究所</span>
date: <span class="en">December 2024</span><span class="ch">2024年12月</span>
theme: solarized
header-includes: |
    <base target="_blank"/>
    <link rel="stylesheet" type="text/css" href="../css/slides.css"></link>
    <script type="text/javascript" src="../scripts/site.js"></script>
include-before: |
    <!-- Any raw HTML code right after body tag, before anything else. -->
    <div id="modal" class="modal">
      <img id="modal-content" />
      <div id="modal-caption"></div>
    </div>
    <div align="right" class="banner">
    <input type="radio" name="lang" onclick="set_lang('en')" id="english" value="English" checked /> English |
    <input type="radio" name="lang" onclick="set_lang('ch')" id="chinese" value="Chinese" /> 中文
    </div>
include-after: |
    <!-- Any raw HTML code right before body end tag, after anything else. -->
    <script>window.onload = check</script>
    <!-- file:///Users/josephchan/jc/q/site/slides/rixmas2019.html -->
    <!-- https://jhlchan.bitbucket.io/slides/rixmas2019.html -->
...

#

:::{.en}
<div class="imagecap" style="width:100%;">
<img src="../images/ri-xmas-2019.jpg"
     alt="A poster of 2019 Christmas Lectures." onclick="showPopup(this)" /></br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:100%;">
<img src="../images/ri-xmas-2019.jpg"
     alt="2019年聖誕講座海報。" onclick="showPopup(this)" /></br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- xx The Royal Institution (RI) in London delivers Christmas lectures every year.
- These public science lectures are very popular, especially for school kids.
- For the 2023 Christmas lectures, the theme is "The Truth about AI".
- But first, let me tell you a bit more about these famous lectures ...
:::

:::{.ch}
- xx 倫敦皇家學院 (RI = Royal Institution) 每年都會舉辦「聖誕講座」。
- 這些公開科學講座，非常受歡迎，尤其吸引年輕學生。
- 2023年聖誕節講座，主題是「人工智能的真相」。
- 首先，讓我告訴你更多關於這些著名講座的資訊 ...
:::

<!--
Royal Institution Christmas Lectures
https://en.wikipedia.org/wiki/Royal_Institution_Christmas_Lectures
The Royal Institution Christmas Lectures are a series of lectures on a single topic each, which have been held at the Royal Institution in London each year since 1825. Michael Faraday initiated the Christmas Lecture series in 1825, at a time when organised education for young people was scarce. Faraday presented nineteen series of lectures in all.

1989    Charles Taylor  Exploring Music (5 lectures on science of music)
1997    Ian Stewart The Magical Maze (5 lectures on mathematics)
2006    Marcus du Sautoy    The Num8er My5teries (5 lectures on numbers)
2019    Hannah Fry  Secrets and Lies: The Hidden Power of Maths (3 lectures on probability)
2023    Michael Wooldridge  The Truth about AI (3 lectures about AI)

Royal Institution Christmas Lectures
The Royal Institution (19 videos)
https://www.youtube.com/playlist?list=PLbnrZHfNEDZyQJZLPMjwEoOLdkFBLU2m1
Watch full Christmas lectures from the Royal Institution, from 2018 onwards.
For older lectures you can see  the full archive here on our website:
https://www.rigb.org/christmas-lectures/watch-royal-institution-christmas-lectures-archive

Christmas Lectures 2019: How to Get Lucky - Hannah Fry【59:03】cc
The Royal Institution
https://www.youtube.com/watch?v=_q4DrUHKC0Q
223K views 3 years ago

At the limits of astrophysics – with Katy Clough【55:55】cc
The Royal Institution
https://www.youtube.com/watch?v=n4RbkTCp16k
169K views 8 months ago

How did Michael Faraday invent? – with David Ricketts【56:33】cc
The Royal Institution
330K views 1 year ago
https://www.youtube.com/watch?v=z1uOsg2-LTA

Prelude To Power: 1931 Michael Faraday Celebration【33:26】cc
Ri Archives
73K views 7 years ago
https://www.youtube.com/watch?v=mxwVIOHEG4I
-->

#

:::{.en}
<div class="imagecap" style="width:100%;">
<img src="../images/faraday_at_xmas_1827.jpg"
     alt="Faraday delivered the Christmas Lectures in 1827." onclick="showPopup(this)" /></br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:100%;">
<img src="../images/faraday_at_xmas_1827.jpg"
     alt="1827年，Faraday 發表聖誕節演講。" onclick="showPopup(this)" /></br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- The Royal Institution of London was founded in 1799 "for diffusing the Knowledge".
- In 1825, Michael Faraday became Director of the Laboratory of the Royal Institution.
- He organised public lectures at Christmas to engage and educate young people about science.
- Various scientists delivered the lectures, but those by Michael Faraday were most entertaining.
- Except during WWII, they were held annually ever since, known as "The Christmas Lectures".
:::

:::{.ch}
- 倫敦皇家學會於1799年正式成立，「旨在傳播知識」。
- 1825年，Michael Faraday（法拉第）成為皇家研究所實驗室主任。
- 他開始在聖誕節期間，安排公開講座，吸引年輕人，以教育科學知識。
- 演講由多位科學家輪流主持，但 Michael Faraday 的演講，最為有趣。
- 此後除二次世界大戰時期外，年年舉行，被稱為「聖誕講座」 (The Christmas Lectures) 。
:::

<!--
History of the CHRISTMAS LECTURES
https://www.rigb.org/christmas-lectures/history-christmas-lectures
Michael Faraday started the CHRISTMAS LECTURES at the Ri in 1825, to engage and educate young people about science.
-->

#

:::{.en}
<div class="imagecap" style="width:98%">
<img src="../images/Michael-Faraday.jpg" style="width:32%"
     alt="Michael Faraday is known as the father of electricity." onclick="showPopup(this)" />
<img src="../images/faraday-motor-1821.gif" style="width:51%"
     alt="Demonstration of electric motor by Faraday." onclick="showPopup(this)" />
</br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:98%">
<img src="../images/Michael-Faraday.jpg" style="width:32%"
     alt="Michael Faraday 被稱為「電力之父」。" onclick="showPopup(this)" />
<img src="../images/faraday-motor-1821.gif" style="width:51%"
     alt="Faraday 的電動摩打示範。" onclick="showPopup(this)" />
</br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- Michael Faraday had no formal education. At 14 he was an apprentice to a bookbinder.
- When people sent in science books and encyclopedia for rebinding, he read the pages.
- Once a wealthy customer gave him a ticket to attend a lecture at the Royal Institution.
- He applied to be a lab assistant by presenting to the director the lecture notes taken.
- Through his own repeated experiments, he worked out the principles of electromagnetism.
- He invented the electric motor, the electric generator, and established laws for electrolysis.
:::

:::{.ch}
- Michael Faraday 冇機會受正規教育。14歲時，他跟隨一名訂裝工人做學徒。
- 當人們送來科學書籍和百科全書重新訂裝時，他會逐頁翻開，細心閱讀。
- 有一次，一位貴客知道他的興趣，送他一張門票，前往皇家學院聽講座。
- 後來，他向學院主任展示自己抄下的演講筆記，成功申請成為實驗室助理。
- 通過獨自反覆實驗，他弄清楚電磁學（electromagnetism）的基本原理。
- 他的發明，包括電動機、發電機，並制定了電解定律。
:::

#

:::{.small}
<span class="en">Story of Michael Faraday | The Father of Electricity</span><span class="ch">Michael Faraday 的故事 | 電力之父</span>【5:31】<br>
<a href="https://youtu.be/Wyh7E_FzxgY">
<img src="http://img.youtube.com/vi/Wyh7E_FzxgY/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- Michael Faraday put an effort to popularize science with his public lectures.
- His presentation was well prepared, with live demonstrations that amazed the Victorian audience.
- The audience included Queen Victoria’s husband, Prince Albert, and their children.
- His lifelong friend was Charles Babbage, a polymath in science, and they shared the same birth year.
- Ada Lovelace, working with Babbage on computing machines, also attended Faraday's lectures.
- The story of Victorian computing machines is told in my "gift" of [Xmas 2021](../2021/xmas.html).
:::

:::{.ch}
- Michael Faraday 致力於科學普及化，他利用公開演講，實現此目標。
- 他準備充足，演習講詞，加上現場示範，令維多利亞時代觀眾驚嘆不已。
- 觀眾包括名人：維多利亞女王的丈夫 Albert 親王，及一眾王子、公主。
- 他生平摯友是 Charles Babbage，一位多才博學者，兩人同齡。
- Ada Lovelace 與 Babbage 一起研究計算機器，她也曾出席 Faraday 的講座。
- 維多利亞時代計算機器的故事，我的「禮物」[2021年聖誕節](../2021/xmas.html) 有講述。
:::


<!--
Michael Faraday
https://www.faraday.cam.ac.uk/about/michael-faraday/
>>>
Early career

Michael Faraday (1791-1867) discovered many of the fundamental laws of physics and chemistry, despite the fact that he had virtually no formal education. The son of an English blacksmith, he was apprenticed at the age of 14 to a bookseller and bookbinder. He read every book on science in the bookshop and attended lectures given at the Royal Institution by various natural philosophers, including Sir Humphry Davy, the discoverer of several chemical elements. In 1812, he applied to Davy for a job, citing his interest in science and showing Davy the extensive lecture notes he had taken. Davy hired Faraday to assist with his research and lecture demonstrations.

Faraday was a brilliant iconoclast. Einstein remarked of Faraday that he, of all people, ‘had made the greatest change in our conception of reality’. Yet despite his achievements, Faraday remained a modest and humble person. He declined to be knighted or to receive honorary degrees and only reluctantly accepted a small pension on his retirement in 1858.

“Indeed, all I can say to you at the end of these lectures (for we must come to an end at one time or other) is to express a wish that you may, in your generation, be fit to compare to a candle; that, in all your actions, you may justify the beauty of the taper by making your deeds honourable and effectual in the discharge of your duty to your fellow-men”.

[Michael Faraday, closing his most famous Christmas series, ‘The Chemical History of a Candle’]

In 1833 he was appointed professor at the Royal Institution. After 1855 he retired to a house provided by Queen Victoria, but he declined a knighthood.
>>>

Faraday, the Apprentice Who Popularized Electricity
 22 September 2016
https://www.bbvaopenmind.com/en/science/leading-figures/faraday-the-apprentice-who-popularized-electricity/
>>>
Born into a very poor family, he had to start working early in life. He began as an apprentice bookbinder at a workshop in London and there, among the volumes of the Encyclopaedia Britannica of the early nineteenth century, Faraday discovered science. He spent his free time reading chemistry texts, taking notes, and going to conferences. Impressed by a series of talks given by Humphrey Davy, the young Faraday showed the Cornish chemist his well-bound book of notes from the talks, got himself hired, and once again became an apprentice, this time of chemistry.

Faraday, the Apprentice Who Popularized Electricity | Science pills【0:49】cc
https://www.youtube.com/watch?v=rQUwxjSaRc8
https://youtu.be/rQUwxjSaRc8
(too short, just text narrative with background music)
Though his discoveries helped to bring electricity into our homes, he continued to teach science by the light of a candle.
More about Faraday here: https://bbva.info/2NiS0Zj
>>>

The Right Chemistry: Michael Faraday generated buzz and popularized science
by Joe Schwarcz  •  29 December 2023.
https://montrealgazette.com/opinion/columnists/the-right-chemistry-michael-faraday-generated-buzz-and-popularized-science
Faraday had no formal education but learned from — and eventually surpassed — the leading scientists of the time.
>>>
I had long been captivated by the breadth of Faraday’s discoveries both in chemistry and physics, and particularly by his efforts to popularize science with his public lectures. He gave much thought to these presentations, took lessons in elocution and practiced his lectures in front of friends, who were invited to make suggestions. He learned to never repeat a phrase and worked hard to avoid any hemming and hawing.

The lectures were illustrated with live demonstrations that amazed Victorian audiences, including Queen Victoria’s husband, Prince Albert, and their children. They watched as an electric current separated water into oxygen and hydrogen, carbon dioxide was poured into a container on a balance to show that a gas has weight, and a U-shaped piece of iron turned into an electromagnet when a coil of wire wound around it was supplied with a current from a battery.
>>>

The life and times of Michael Faraday | The Right Chemistry【4:13】cc
https://www.youtube.com/watch?v=ohPmFCvQmJY
30 Dec 2023 (8 days ago! But just talking in front of the screen, and two little demo)

The Scientist Who Sucked at Math【11:38】cc
https://www.youtube.com/watch?v=xi1A1WTVKFQ
Despite his limitations, Michael Faraday started the electric revolution.

Story of Michael Faraday | The Father of Electricity【5:31】(embed cc)
https://www.youtube.com/watch?v=Wyh7E_FzxgY
Showing text with background music.

Michael Faraday: The Father of Electricity【6:19】cc
https://www.youtube.com/watch?v=Usj8DH84UV8
Michael Faraday is considered the father of electricity. He was a British physicist and chemist who is best known for his discoveries of electromagnetic induction and of the laws of electrolysis. His biggest breakthrough in electricity was his invention of the electric motor.
(video by IET = Institute of Engineering Technology)

PBS | Einstein's Big Idea | Michael Faraday - Part 1【9:06】cc
https://www.youtube.com/watch?v=TEVEBzNSwTU
PBS | Einstein's Big Idea | Michael Faraday - Part 2【5:12】cc
https://www.youtube.com/watch?v=yVDHKKTC4tA
-->

#

:::{.small}
Christmas Lectures 2019: How to Get Lucky - Hannah Fry【59:03】cc<br>
<a href="https://youtu.be/_q4DrUHKC0Q">
<img src="http://img.youtube.com/vi/_q4DrUHKC0Q/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- xx The 2023 Christmas Lectures have 3 parts, each about an hour, in a hall packed with young people.
- The first topic is "Intelligent Machines", the opening scene features the robot "AiDa" (for Ada).
- Unfortunately the video is broken from 21:40 to 22:20, again from 33:41 to 34:19.
- There is a live demonstration of the Turing Test, see my "gift" of [Lunar New Year 2022](../2022/lny.html).
- Explains how neuron firing works, and what is an artificial neural network.
- Demonstrates why large language models need a huge number of dimensions.
- Do you know that Google translate is not based on dictionaries?
:::

:::{.ch}
- xx 2023年聖誕講座分成 3 集，每集大約一小時，演講大廳裡坐滿年輕人。
- 第一個主題是「具智慧的機器」，開場場景是機器人「AiDa」（即 Ada）。
- 可惜的是，視頻從 21:40 到 22:20 中斷，又從 33:41 到 34:19 中斷。
- 現場演示「圖靈測試」（Turing Test），請參閱我的「禮物」[2022年農曆新春](../2022/lny.html)。
- 解釋神經元（neuron）放電如何運作，以及什麼是人工神經網路（artificial neural network）。
- 示範為什麼大型語言模型（large language models），需要多維度（many dimensions）。
- 你可知道， Google 翻譯不是基於字典嗎？
:::

#

:::{.small}
Christmas Lectures 2019: How to Bend the Rules - Hannah Fry【59:16】cc<br>
<a href="https://youtu.be/TtisQ9yZ2zo">
<img src="http://img.youtube.com/vi/TtisQ9yZ2zo/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- xx The second topic is "Everyday AI", with an opening video of daily life incorporating AI.
- Unfortunately the video is broken from 30:14 to 30:26.
- This talk features the role of games, like chess and go, in AI development.
- Live demo of reinforcement learning, how a machine can play prefect tic-tac-toe.
- Examples of application of AI in medical healthcare, molecular biophysics, etc.
- Live demonstration of generative AI in creativity, making a video from prompts.
:::

:::{.ch}
- xx 第二個主題為「生活上的人工智能」，開場播放一段融入 AI 的日常生活影片。
- 可惜的是，視頻從 30:14 到 30:26 中斷。
- 今次演講，重點介紹國際象棋和圍棋等遊戲，在人工智能開發中的作用。
- 強化學習（reinforcement learning）的現場演示，機器如何完美的玩井字遊戲。
- AI 在醫療保健、分子生物物理學等多方面的應用實例。
- 現場演示生成式人工智能（generative AI）的想像力，根據提示製作視頻。
:::

#

:::{.small}
Christmas Lectures 2019: How Can We All Win? - Hannah Fry【58:20】cc<br>
<a href="https://youtu.be/u5mNa6KE0lA">
<img src="http://img.youtube.com/vi/u5mNa6KE0lA/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- xx The third topic is "Future of AI", opening with travelling in a driverless car.
- Investigate the dreams and nightmares of AI, what directions should we take.
- Robots with autonomous actions, and issues about autonomous warfare.
- AI can amplify human bias, and affects voters via political advertising.
- Demonstration of deep fake, and the aims of AGI = artificial general intelligence.
- Design AI systems with objectives that allows for human control.
- The issues about conscious machines, and why human beings are unique.
:::

:::{.ch}
- xx 第三個主題是「人工智能的未來」，從無人駕駛汽車旅程開始。
- 探討 AI 的美夢和惡夢，我們應該走的方向，如何作出抉擇。
- 自主（autonomous）行動的機器人，以及有關自主戰爭的問題。
- 人工智能會放大人類偏見，並透過政治廣告，影響選票。
- 深度造假的展示，以及「通用人工智能」（AGI = artificial general intelligence）的目標。
- 要設計人工智能系統，具有容許人類控制的指標。
- 關於機器有或沒有意識的問題，以及為什麼人類是獨一無二的。
:::

<!--

The Royal Institution: CHRISTMAS LECTURES
https://www.rigb.org/christmas-lectures

2019 Hannah Fry: Secrets and Lies: The hidden power of maths
In a series of lectures packed with mind-boggling demos and live experiments, Hannah Fry shows us how to decode life's hidden numbers; to help us all make better choices, sort fact from fiction, and lead happier lives. But she also warns how our unwavering faith in figures can lead to disaster when we get the sums wrong. 

How to get lucky         (Hannah Fry explores luck and asks whether probability allow us to understand and predict complex systems.)
How to bend the rules    (Hannah Fry explores how have algorithms taken control of our lives and now guide almost everything that we do.)
How can we all win?      (Hannah Fry looks at why maths can fail and asks what the limits of maths are. Are there problems maths can’t or shouldn’t solve? And should we always trust in numbers?)

About the 2019 CHRISTMAS LECTURES

We think our lives unfold thanks to a mix of luck and our own personal choices. But that’s not quite true. An unseen layer of mathematics governs every aspect of our world.

Life’s most astonishing miracles can be understood with probability. Big data dictates many of the hot new fashions we follow. Even our choices on Netflix, or our choice of who we marry, is secretly influenced by computer algorithms.

In a series of Lectures packed with mind-boggling demos and live experiments, Hannah Fry shows us how to decode life’s hidden numbers; to help us all make better choices, sort fact from fiction, and lead happier lives. But she also warns how our unwavering faith in figures can lead to disaster when we get the sums wrong.

Unravelling suspicious statistics, engineering meltdowns and deadly data, Hannah asks big ethical questions about the trust we place in maths today. Are there any problems maths can’t or shouldn’t solve? Do computer algorithms have too much control over our lives and privacy? Could artificial intelligence decide if someone lives or dies?

Ultimately, by probing the limits of maths and its role in our modern world, Hannah ends up revealing and celebrating what makes our human minds so unique.
>>>

Secrets and Lies -- the Hidden Power of Math
Christmas Lectures 2019 by Hannah Fry
Lecture 1: How to Get Lucky         https://www.rigb.org/explore-science/explore/video/secrets-and-lies-how-get-lucky-2019
Lecture 2: How to Bend the Rules    https://www.rigb.org/explore-science/explore/video/secrets-and-lies-how-bend-rules-2019
Lecture 3: How Can We All Win?      https://www.rigb.org/explore-science/explore/video/secrets-and-lies-how-can-we-all-win-2019

Christmas Lectures 2019: How to Get Lucky - Hannah Fry【59:03】cc
https://www.youtube.com/watch?v=_q4DrUHKC0Q
The Royal Institution (This Christmas Lecture was originally broadcast on BBC4 on 26 December 2019.)
In the first lecture of the 2019 Christmas Lectures, Hannah Fry explores how mathematical thinking and probability can allow us to understand and predict complex systems - even helping us to make our own luck.
01:25 Chance of stopping a fall by counter weight.
05:10 Start the swinging 2D pendulum, with emerging pattern (attractor?).
05:29 Pattern of lined from crowd swapping.
08:02 How to win when pulling Christmas crackers.
(Crackers are traditionally pulled during Christmas dinner or at Christmas parties. One version of the cracker ritual holds that the person who ends up with the larger end of cracker earns the right to keep the contents of the cardboard tube.)
09:05 Normal (or Binomial?) distribution by flipping coins.

Christmas Lectures 2019: How to Bend the Rules - Hannah Fry【59:16】cc
https://www.youtube.com/watch?v=TtisQ9yZ2zo
The Royal Institution (This Christmas Lecture was originally broadcast on BBC4 on 27 December 2019. )
In the second lecture of the 2019 Christmas Lectures, Hannah Fry shows how data-gobbling algorithms have taken over our lives and now control almost everything we do without us even realising.

Christmas Lectures 2019: How Can We All Win? - Hannah Fry【58:20】cc
https://www.youtube.com/watch?v=u5mNa6KE0lA
The Royal Institution (This Christmas Lecture was originally broadcast on BBC4 on 28 December 2019. )
In the third and final lecture of the 2019 Christmas Lectures, Hannah Fry looks at why maths can fail and asks what the limits of maths are.

-->

#

:::{.en}
<div class="imagecap" style="width:48%;">
<img src="../ai/chatgpt-features.jpg"
     alt="ChatGPT frontpage in 2022." onclick="showPopup(this)" /></br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:48%;">
<img src="../ai/chatgpt-features.jpg"
     alt="2022 年 ChatGPT 頭版。" onclick="showPopup(this)" /></br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- ChatGPT developed by OpenAI and launched on 30 November 2022.
- T = Transformer, an advanced form of artificial neural network.
- A transformer tracking relationships in whole paragraphs, not word by word.
- P = Pre-trained, using large laugage model (LLM) with billions of parameters.
- The model takes data from the whole Web, with natural language processing.
- G = Generative, using deep learnig to produce natural language text.
- That means ChatGPT can deliver something new, not seen on the Web before.
- As explained in the Christmas lectures, ChatGPT is similar to auto-complete.
- Their difference is data scale. ChatGPT gives the most plausible response to a query.
:::

:::{.ch}
- ChatGPT 由 OpenAI 開發，於 2022年11月30日推出。
- T = Transformer（轉換器），是人工神經網路（artificial neural network）的進階形式。
- 轉換器（Transformer）追蹤整篇文字段落中的關係，而不是逐個字分析。
- P = Pre-trained（預訓練），使用大型語言模型 (LLM) 進行預訓練，具備參數以數十億計。
- 透過自然語言處理（natural language processing），此模型從整個 Web 獲取數據資料。
- G = Generative（生成式），使用深度學習（deep learnig）產生自然語言文本。
- 這意味著，ChatGPT 可以提供在 Web 上從未出現過的全新內容，改變潮流。
- 正如聖誕節講座中所解釋，ChatGPT 的功能，類似於手機填寫時的自動完成（auto-complete）。
- 兩者差別，在於數據規模大小。ChatGPT 對於查詢，給予最合情合理的回應。
:::

#

:::{.en}
<div class="imagecap" style="width:64%;">
<img src="../images/heads-understand-what.jpg"
     alt="Do you understand?" onclick="showPopup(this)" /></br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:64%;">
<img src="../images/heads-understand-what.jpg"
     alt="問你明吾明？" onclick="showPopup(this)" /></br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- I enjoy the 3 talks, because I want to know "the truth about AI".
- Moreover, I want to see how such an extended lecture can keep young kids excited.
- You see, for these lectures, a team designed the whole content throughout the year.
- Everything is well-planned to make sure that the lectures are entertaining at Christmas.
- The lectures are aimed at school students, so the presentations are niot pure talking.
- You'll appreciate what tricks and techniques they employ to grab people's attention.
- Even if you didn't get anything, just think about why these presentations are lively.
:::

:::{.ch}
- 我喜歡這 3 節演講，因為我想知道「人工智能的真相」。
- 另外，我想知道：冗長的演講，如何能讓孩子們興奮不已。
- 要明白，籌備這些講座，整個團隊花上全年設計全部內容。
- 一切都經過精心策劃，以確保聖誕節期間的講座充滿樂趣。
- 演講的對象是莘莘學子，因此流程不能只是滔滔不絕。
- 你會欣賞他們使用哪些技巧和妙法，吸引各人的注意。
- 即使你什麼也不入腦，想想為何這些演講，可以如此生動。
:::

<!--
pandoc -t revealjs -s --mathjax rixmas2019.md -o rixmas2019.html
-->
