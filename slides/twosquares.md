---
title: Windmills of the Minds
author: some thoughts on the algorithm
date: January 2024
theme: solarized
header-includes: |
    <style>
      .reveal {
      font-size: 16pt;
      line-height: 1.2em;
    }
     .reveal pre code {
      font-size: 14pt;
      line-height: 1.2em;
    }
    .reveal h1,
    .reveal h2,
    .reveal h3,
    .reveal h4,
    .reveal h5,
    .reveal h6 {
      text-transform: none;
    }
    </style>
    <base target="_blank"/>
include-after: |
    <!-- file:///Users/josephchan/jc/q/site/slides/twosquares.html -->
    <!-- https://jhlchan.bitbucket.io/slides/twosquares.html -->
...


# Windmills of the Minds

- A _tik_ prime is a prime $p$ such that $p\bmod{4}\equiv{1}$, _i.e._, $p = 4k + 1$ for some $k$.
- **Fermat's Two-Squares theorem** states that, a tik prime can be expressed as a sum of an odd square and an even square, uniquely.
- For a beautiful windmills proof of this theorem, with an algorithm to find the two squares, see [arXiv](https://arxiv.org/abs/2112.02556).
- Essentially a geometric view of Zagier's one-sentence proof, there are two involutions, **flip** and **zagier**.
- By developing a theory for involution composition orbits, the algorithm can be shown to be correct.

#

- The algorithm is an iteration of the map $\text{zagier}\circ\text{flip}$ from a `zagier`-fix to, hopefully, a `flip`-fix.
- Any tik number $n = 4k + 1$ has a `zagier`-fix $(1,1,k)$, starting an orbit in the set of windmills.
- As long as $n$ is not a square, in which the set of windmills is finite, the orbit leads to _another_ fix.
- The _another_ fix (at half period) is a `flip`-fix if period is **odd**, but a a `zagier`-flip is period is **even**.
- The `flip`-fix will correspond to an expression of $n$ by two squares, one odd and one even.

<p>
| tik $n$ | orbit                               | sum of two squares |
|--------:|-------------------------------------|--------------------|
|      1  | _square_                            |                    |
|      5  | (1,1,1)                             | 5 = 1² + 2²        |
|      9  | _square_                            |                    |
|     13  | (1,1,3)(3,1,1)                      | 13 = 3² + 2²       |
|     17  | (1,1,4)(3,1,2)(1,2,2)               | 17 = 1² + 4²       |
|     21  | (1,1,5)(3,1,3)(3,3,1)!              | _period even_      |
|     25  | _square_                            |                    |
|     29  | (1,1,7)(3,1,5)(5,1,1)               | 29 = 5² + 2²       |
|     33  | (1,1,8)(3,1,6)(5,1,2)(1,4,2)(3,2,3)(3,3,2)!  | _period even_  |
|     37  | (1,1,9)(3,1,7)(5,1,3)(1,3,3)        | 37 = 1² + 6²       |
|     41  | (1,1,10)(3,1,8)(5,1,4)(3,4,2)(1,2,5)(5,2,2)   | 41 = 5² + 4²  |

#

- A `flip`-fix has the form $(x,z,z)$, while a `zagier`-fix has the form $(x,x,z)$.
- For a tik $n = 4k + 1$ which is not a square, there is a `zagier`-fix $(1,1,k)$.
- It can be shown that if orbit period is even, the other `zagier`-fix is not this one.
- But a tik prime $n$ has only one `zagier`-fix $(1,1,k)$, so the period must be odd.
- This establishes the existence of the two squares for a tik prime $n$.

<p>

- Can this establish the uniqueness of the two squares for a tik prime $n$?
- Apparently not, because the orbit of $n = 4k + 1$ from $(1,1,k)$ does not cover all the windmills.
- There may be other windmill orbits, starting from a `flip`-fix to another `flip`-fix with even period.
- For a nonprime tik $n$, e.g. $n = 45$, or $n = 65$, there are other `zagier`-fix with orbit to `flip`-fix.
- Of course, for a tik prime $n$, the other orbits have no `flip`-fixes, but seems hard to prove.

<p>
| tik $n$ | orbit                               | sum of two squares |
|--------:|-------------------------------------|--------------------|
|      45 | (1,1,11)(3,1,9)(5,1,5)(5,5,1)!      | _period even_ |
|         | (3,3,3)                             | 45 = 3² + 6²  |
|      49 | _square_                            |                    |
|      53 | (1,1,13)(3,1,11)(5,1,7)(7,1,1)      | 53 = 7² + 2²       |
|      57 | (1,1,14)(3,1,12) ... (3,3,4)!       | _period even_      |
|      61 | (1,1,15)(3,1,13)(5,1,9)(7,1,3)(1,5,3)(5,3,3)  | 61 = 5² + 6²  |
|      65 | (1,1,16)(3,1,14)(5,1,10)(7.1,4)(1,4,4)        | 65 = 1² + 8²  |
|         | (5,5,2)(1,8,2)(3,2,7)(7,2,2)                  | 65 = 7² + 4²  |

#

| tik $n$ | orbit                               | sum of two squares |
|--------:|-------------------------------------|--------------------|
|      69 | (1,1,17)(3,1,15) ... (3,3,5)!       | _period even_      |
|      73 | (1,1,18)(3,1,16) ... (3,4,4)        | 73 = 3² + 8²       |
|      77 | (1,1,19)(3,1,17)(5,1,13)(7,1,7)(7,7,1)!       | _period even_ |
|      81 | _square_                            |                    |
|      85 | (1,1,21)(3,1,19)(5,1,15)(7,1,9)(9,1,1)        | 85 = 9² + 2²  |
|         | (5,5,3)(1,3,7)(7,3,3)               | 85 = 7² + 6²       |
|      89 | (1,1,22)(3,1,20) ... (5,4,4)        | 89 = 5² + 8²       |
|      93 | (1,1,23)(3,1,21) ... (3,3,7)!       | _period even_      |
|      97 | (1,1,24)(3,1,22) ... (9,2,2)        | 97 = 9² + 4²       |
|     101 | (1,1,25)(3,1,23) ... (1,5,5)        | 101 = 1² + 10²     |

<p>

- Which is the first tik prime $n$ with an orbit having no `flip`-fix?
- The first example is the tick prime $229$:
    + The number of windmills is $29$.
    + The orbit from $(1,1,57)$ leads to $(15,1,1)$, over a period $15$.
    + The orbit from $(1,3,19)$ has no fixes, period $7$.
    + The orbit from $(1,19,3)$ has no fixes, period $7$.
- Such orbits are _nontransitive_. Is there a criterion for _transitive_ orbits?
- In group theory, the action of group $G$ on set $A$ is transitive if there is only one orbit.

<!--

Transitive Group Actions
Keith Conrad
https://kconrad.math.uconn.edu/blurbs/grouptheory/transitive.pdf

-->

#

About the Uniquness of Two Squares
----------------------------------

- Only 5 = 1² + 2², 13 = 2² + 3² implies 65 = 5x13 = 7² + 4² = 1² + 8².
- To deduce: 7² + 4² , 1² + 8² come from (1² + 2²)(2² + 3²) is very hard!
- example of composite with unique sum of squares: 25 = 5x5 = 5² = 3² + 4².
- There is the Diophantus identity (or Brahmagupta–Fibonacci identity): 
 ${\displaystyle {\begin{aligned}\left(a^{2}+b^{2}\right)\left(c^{2}+d^{2}\right)&{}=\left(ac-bd\right)^{2}+\left(ad+bc\right)^{2}&&(1)\\&{}=\left(ac+bd\right)^{2}+\left(ad-bc\right)^{2}.&&(2)\end{aligned}}}$
- 65 = 5x13 = (1² + 2²)(3² + 2²), so a = 1, b = 2, c = 3, d = 2.
- by (1), 65 = (1x3 - 2x2)² + (1x2 + 2x3)² = (-1)² + 8² = 1² + 8² = x² + y²
- by (2), 65 = (1x3 + 2x2)² + (1x2 - 2x3)² = 7² + (-4)² = 7² + 4² = u² + v²
- so x = ac - bd, y = ad + bc, u = ac + bd, v = ad - bc.
- in matrix form,
-
~~~
 [x  y] = [ a  -b ] [c   d]
 [u  v] = [ a   b ] [d  -c]
~~~

- so given (a,b,c,d), we can compute (x,y,u,v). But given (x,y,u,v), how to compute (a,b,c,d)?

#

- given (a,b,c,d), x = ac - bd, y = ad + bc, u = ac + bd, v = ad - bc.
- This is composed by 4 cross terms, (a + b)(c + d) = ac + ad + bc + bd.
- given (x,y,u,v), x + u = 2ac, y + v = 2ad, u - x = 2bd, y - v = 2bc
- (x + u)/(y - v) = a/b = (y + v)/(u - x), this gives a, b in a ratio.
- (x + u)/(y + v) = c/d = (y - v)/(u - x), this gives c, d in a ratio.
- For 65 = 1² + 8² = 7² + 4², x = 1, y = 8, u = 7, v = 4.
- Thus a/b = (1 + 7)/(8 - 4) = 8/4 = 2/1, simplest is a = 2, b = 1.
- Thus c/d = (1 + 7)/(8 + 4) = 8/12 = 2/3, simplest is c = 2, d = 3.
- Indeed 65 = (2² + 1²)(2² + 3²) = 5x13!
- Given (x,y,u,v), ensure y is largest, v is smallest (all unequal) by swap (x,y), swap (u,v), and swap pairs.
- Compute e = x + u, f = y - v, g = y + v, all positive, let h = gcd(e,f), k = gcd(e,g).
- Take a = e/h, b = f/h, c = e/k, d = g/k.
- Example: 50 = 1² + 7² = 5² + 5², x = 1, y = 7, u = 5, v = 5.
- e = 1 + 5 = 6, f = 7 - 5 = 2, g = 7 + 5 = 12, h = gcd(6,2) = 2, k = gcd(6,12) = 6
- Take a = 6/2 = 3, b = 2/2 = 1, c = 6/6 = 1, d = 12/6 = 2.
- 50 = (3² + 1²)(1² + 2²) = 10x5!
- compare  ac = (e/h)(e/k)  and  bd = (f/h)(g/k), i.e. e² = (x + u)² vs fg = (y - v)(y + v) = y² - v².
- if no way to compare, treat two cases.

#

Forward Testing
---------------

<label for="abcd">Input:</label>
<input type="text" name="abcd" onkeydown="perfrom(this)" />
Output:<br>
<div id="result" style="width:500px;height:220px;margin:auto;text-align:left;zborder:1px solid black;"></div>


<script type="text/javascript">
// norm of x and y
function norm(x, y) {
    return x * x + y * y
}
function $(id) {
    return document.getElementById(id)
}
function log(message) {
    $('result').innerHTML += message + '<br>'
}
function perfrom(element) {
    if (event.key !== 'Enter') return
    $('result').innerHTML = ""
    var abcd = element.value.split(',').map(s => parseInt(s)), xyuv = forward(abcd)
    log('input  a,b,c,d: ' + abcd.join(' ,'))
    log('output x,y,u,v: ' + xyuv.join(' ,'))
    log('checking ...')
    check(log, abcd, xyuv)
}
// forward of a tuple t[0] to t[3]
function forward(t) {
    var a = t[0], b = t[1], c = t[2], d = t[3]
    var ac = a * c, bd = b * d, ad = a * d, bc = b * c
    return [bd < ac ? ac - bd : bd - ac,
            ad + bc,
            ac + bd,
            bc < ad ? ad - bc : bc - ad]
}
// checking with output function
function check(out, t, s) {
    var a = t[0], b = t[1], c = t[2], d = t[3]
    var x = s[0], y = s[1], u = s[2], v = s[3]
    var p = norm(a,b), q = norm(c,d), pq = p * q
    out('p = ' + a + '² + ' + b + '² = ' + p)
    out('q = ' + c + '² + ' + d + '² = ' + q + ', pq = ' + pq)
    out('x² + y² = ' + x + '² + ' + y + '² = ' + norm(x,y) + (pq == norm(x,y) ? ' ok' : ' !!'))
    out('u² + v² = ' + u + '² + ' + v + '² = ' + norm(u,v) + (pq == norm(u,v) ? ' ok' : ' !!'))
}
</script>

- Input: a, b, c, d.
- Compute: $x = ac-bd, y = ad+bc$
- $\left(a^{2}+b^{2}\right)\left(c^{2}+d^{2}\right)=\left(ac-bd\right)^{2}+\left(ad+bc\right)^{2}$
- Compute: $u = ac+bd, v = ad-bc$
- $\left(a^{2}+b^{2}\right)\left(c^{2}+d^{2}\right)=\left(ac+bd\right)^{2}+\left(ad-bc\right)^{2}$

#

Backward Testing
----------------

<label for="xyuv">Input:</label>
<input type="text" name="xyuv" onkeydown="action(this)" />
Output:<br>
<div id="display" style="width:800px;height:220px;margin:auto;text-align:left;zborder:1px solid black;"></div>


<script type="text/javascript">
// compute greatest common divisor
function gcd(x, y) {
    var t // Euclidean algorithm
    while (y) {
        t = y
        y = x % y // integer remainder
        x = t
    }
    return x
}
function out(message) {
    $('display').innerHTML += message + '<br>'
}
function action(element) {
    if (event.key !== 'Enter') return
    $('display').innerHTML = ""
    var xyuv = element.value.split(',').map(s => parseInt(s))
    out('input   x,y,u,v: ' + xyuv.join(' ,'))
    var x = xyuv[0], y = xyuv[1], u = xyuv[2], v = xyuv[3]
    if (norm(x,y) !== norm(u,v)) {
        out('Bad input:')
        out('x² + y² = ' + x + '² + ' + y + '² = ' + norm(x,y))
        out('u² + v² = ' + u + '² + ' + v + '² = ' + norm(u,v) + ' different!')
        return
    }
    if (x == u) {
        out('Bad input: (x,y) and (u,v) should be differnt.')
        return
    }
    if (y < v) { // need v <= y, note y < v implies u < x 
        y = xyuv[0], x = xyuv[1], v = xyuv[2], u = xyuv[3]
        xyuv = [x,y,u,v]
        out('adjust  x,y,u,v: ' + xyuv.join(' ,'))           
    }
    var abcd = backward(xyuv)
    out('output  a,b,c,d: ' + abcd.join(' ,'))
    out('checking ...')
    probe(out, xyuv, abcd)
}
// backward of a tuple t[0] to t[3]
function backward(t) {
    var x = t[0], y = t[1], u = t[2], v = t[3] // with v < y
    var e = x + u, f = y - v, g = y + v, h = gcd(e,f), k = gcd(e,g)
    return [e/h, f/h, e/k, g/k]
}
// checking with output function
function probe(log, t, s) {
    var x = t[0], y = t[1], u = t[2], v = t[3]
    var a = s[0], b = s[1], c = s[2], d = s[3]
    var p = norm(a,b), q = norm(c,d), pq = p * q
    log('p = ' + a + '² + ' + b + '² = ' + p)
    log('q = ' + c + '² + ' + d + '² = ' + q + ', pq = ' + pq)
    log('x² + y² = ' + x + '² + ' + y + '² = ' + norm(x,y) + (pq == norm(x,y) ? ' ok' : ' !!'))
    // log('u² + v² = ' + u + '² + ' + v + '² = ' + norm(u,v) + (pq == norm(u,v) ? ' ok' : ' !!'))
    var h = gcd(x+u,y-v), k = gcd(x+u,y+v), m = 2*(x+u)
    log('h = gcd (x+u,y-v) = ' + h + ', k = gcd (x+u,y+v) = ' + k + ', hk = ' + h * k + ', 2(x+u) = ' + m + (h * k == m ? ' ok' : ' ??'))
}

// a testing function
function test(x,y,u,v) {
    var h = gcd(x+u,y-v), k = gcd(x+u,y+v), m = 2*(x+u)
    return h * k == m
    // seems to be true when norm(x,y) = norm(u,v), otherwise false
}
</script>

- Input: x, y, u, v (with x² + y² = u² + v²)
- Output: a, b, c, d (need v < y, swap x,y and swap u,v if not)
- Compute: $e = x + u, f = y - v, g = y + v, h = gcd(e,f), k = gcd(e,g)$
- Then take: $a = e/h, b = f/h, c = e/k, d = g/k$
- So far, proof is not successful! Update: now done!
- 5x5x13 = 325 = 1² + 18² = 6² + 17² = 10² + 15²
- 6,17,10,15 ok, 1,18,10,15 not ok, 1,18,6,17 not ok.

#

More Examples
-------------

- 5x13x13 = 845 = 2² + 29² = 13² + 26² = 19² + 22²
- 2,29,13,26 not ok, 2,29,19,22 not ok, 13,26,19,22 ok.
- 5x13x17 = 1105 = 4² + 33² = 9² + 32² = 12² + 31² = 23² + 24²
- 4,33,9,32 not ok, 4,33,12,31 ok, 4,33,23,24 not ok,
- 9,32,12,31 not ok, 9,32,23,24 ok, 12,31,23,24 not ok.

A Puzzle
--------

- 325 = 5x5x13, and 5 = 1² + 2², and 5² = 3² + 4², 13 = 3² + 2²,
- Also 65 = 5x13 = 1² + 8² = 7² + 4². 
- Now (5²)(13) should give x,y,u,v. Is this one ok?
- Now 5(65) should give x,y,u,v. Is this one ok?
- That's all the x,y,u.v I can get, where is the third x,y,u,v?

The Answer
----------

- Using _forward_, 1,2,1,2 gives 3,4,5,0, so 25 = 3² + 4².
- Also _forward_, 1,2,3,2 gives 1,8,7,4, so 65 = 1² + 8² = 7² + 4².
- For (3² + 4²)(3² + 2²), 3,4,3,2 gives 1,18,17,6, ok (but not 1,18,6,17)
- For (1² + 2²)(1² + 8²), 1,2,1,8 gives 15,10,17,6, ok (also ok 6,17,10,15)
- For (1² + 2²)(7² + 4²), 1,2,7,4 gives 1,18,15,10, ok (but not 1,18,10,15)
- 5x5x13 = 325 = 1² + 18² = 17² + 6² = 15² + 10². So x,u (or y,v) _same_ parity!

#

Try to Prove
------------

- Given $norm(x,y) = norm(u,v)$, odd $x$, even $y$, odd $u$, even $v$, $v < y$.
- Compute: $e = x + u, f = y - v, g = y + v, h = gcd(e,f), k = gcd(e,g)$
- Let $a = e/h, b = f/h, c = e/k, d = g/k$, then $norm(a,b) norm(c,d) = norm(x,y)$.
- Known: $norm(a,b) norm(c,d) = norm(a * d + b * c, a * c - b * d)$.
- ad + bc = (e/h)(g/k) + (f/h)(e/k) = e (g + f)/(hk) = (x + u)(2y)/(hk) = y(2(x+u)/hk)
- ac - bd = (e/h)(e/k) - (f/h)(g/k) = (e² - f * g)/(hk) = ((x + u)² - (y² - v²))/hk
- ac - bd = ((x + u)² - (u² - x²))/hk, by x² + y² = u² + v².
- ac - bd = (u + x)((u + x) - (u - x))/hk = (x + u)(2x)/(hk) = x(2(x+u)/hk)
- Therefore, all good if $2(x+u) = hk = gcd(x+u,y-v) gcd(x+u,y+v)$. Test ok!

How about ...
-------------

- Known: $coprime(a,b) \implies gcd(a,c) gcd(b,c) = gcd(a * b, c)$, or GCD is multiplicative.
- Also: $gcd(m * a, m * b) = m * gcd(a,b)$, or GCD is scalar.
- gcd(x+u,y-v) gcd(x+u,y+v) = gcd((x+u)(y+v),(y-v)(y+v))/(y+v) gcd((x+u)(y-v),(y+v)(y-v))/(y-v)
- (not coprime!) = gcd ((x+u)(y+v)(x+u)(y-v), (y+v)(y-v)) /(y+v)(y-v) = gcd((x+u)²,1) = (x + u)²
- Now *forward* and *backward* have theorems, and this idea indeed works!
- TODO: (1) Gauss' formula for two squares, (2) Is there a group action in the orbits of $mill(n)$?
- Also (3) Understand the chessboard proof of Fermat's Two Squares Theorem.

<!--
pandoc -t revealjs -s --mathjax twosquares.md -o twosquares.html
-->