---
title: Windmills of the Minds
author: <span class="en">Pictorial Algorithms for</span><span class="ch">圖示演算法</span>
date: <span class="en">Fermat's Two Squares Theorem</span><span class="ch">費馬（Fermat）二平方定理</span>
theme: solarized
header-includes: |
    <base target="_blank"/>
    <link rel="stylesheet" type="text/css" href="../css/slides.css"></link>
    <script type="text/javascript" src="../scripts/site.js"></script>
    <script type="text/javascript" src="../scripts/svg.js"></script>
    <script type="text/javascript" src="../scripts/windmill.js"></script>
include-before: |
    <!-- Any raw HTML code right after body tag, before anything else. -->
    <div id="modal" class="modal">
      <img id="modal-content" />
      <div id="modal-caption"></div>
    </div>
    <div align="right" class="banner">
    <input type="radio" name="lang" onclick="set_lang('en')" id="english" value="English" checked /> English |
    <input type="radio" name="lang" onclick="set_lang('ch')" id="chinese" value="Chinese" /> 中文
    </div>
include-after: |
    <!-- Any raw HTML code right before body end tag, after anything else. -->
    <script>window.onload = check</script>
    <!-- file:///Users/josephchan/jc/q/site/slides/windmill.html -->
    <!-- https://jhlchan.bitbucket.io/slides/windmill.html -->
...

#

:::{.en}
<div class="imagecap" style="width:90%">
<img src="../images/JAR-volume-68-paper.jpg" style="width:100%"
     alt="My journal paper is published in December 2024." onclick="showPopup(this)" />
</br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:90%">
<img src="../images/JAR-volume-68-paper.jpg" style="width:100%"
     alt="我的期刊文章，發表於2024年12月。" onclick="showPopup(this)" />
</br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- Recently, I have a math paper published in the Journal of Automated Reasoning (JAR).
- *'Reasoning'* is to think by logic, which is how mathematics works, and mathematicians do.
- *'Automated Reasoning'* means using computers to do math logic, step by step automatically.
- Math proofs are rigid logical deductions, and computers can help to check all proof steps.
- Simply put, this is an application of AI in mathematics, especially for proving math theorems.
:::

:::{.ch}
- 最近，「自動推理」期刊雜誌（Journal of Automated Reasoning = JAR），發表一篇我的數理文章。
- *「推理」*（Reasoning）是指透過邏輯，進行思考和推敲。是數學的本質，也是數學家表達的方式。
- *「自動推理」*（Automated Reasoning）意指使用計算機，自動地一步又一步，進行數學邏輯推算。
- 數學證明，其實只是邏輯推理，十分機械化。利用電腦，幫助檢查證明中所有步驟，既快捷又妥當。
- 簡而言之，「自動推理」的研究領域，是把人工智能（AI），應用在數學中，尤其是證明數學定理。
:::

<!-- javascript setup for slides -->
<script type="text/javascript">
// locate DOM element
function $(id) {
    return document.getElementById(id)
}
// setup SVG for url(#grid)
var svg = defineGrid(scale)
// wrap inside a zero-dimension div and insert to DOM
var div = document.createElement("div")
div.style.width = '0px'
div.style.height = '0px'
div.appendChild(svg)
div.appendChild(defineArrowHead())
document.body.appendChild(div) // this avoid the problem of only 2 slides can use url(#grid)
// make a blank canvas with background only, customised for slides
function makeCanvasForSlides() {
    var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg")
    svg.setAttribute('width', '1000px')
    svg.setAttribute('height', '500px')
    // svg.setAttribute('viewBox', '0 0 600 300') // change ratio = 500/300 = 5/3
    // 600 400 gives max 60x40, 700 400 gives max 70x40, 800 400 gives max 80x40
    svg.setAttribute('viewBox', '0 0 700 400') // change ratio = 500/400 = 5/4, better
    svg.appendChild(makeBackground())
    return svg
}
// override for slides
makeCanvas = makeCanvasForSlides
var debug = false // true when slides keep refreshing
</script>

#

:::{#numbers}
:::

:::{.en}
- In <span class="land">Mathland</span>, the simplest are numbers. Let us take a look at whole numbers: 1, 2, 3 ...
- As we walk along the number line, we can imagine that each number has a shape.
- Numbers have many shapes: some are squares, like 25 = 5², some are cubes, like 27 = 3³.
- Most numbers are rectangles, like 26 = 2×13, or cuboids, like 28 = 2×2×7, etc.
- But some special numbers are just a single column like a skyscraper, e.g. 29 and 31.
- Such skyscraper numbers are called **primes**, starting with 2, 3, 5, 7, 11, 13, etc. forever.
:::

:::{.ch}
- 在 <span class="land">數學樂園</span>中，數字最淺白簡單。我們不妨齊齊過來，看看整數：1、2、3 ...
- 漫步數字「星光大道」（number line），可以想像每個數字，都有不同形狀。
- 數字形狀，林林總總。有些是平方，例如 25 = 5²，有些是立方，例如 27 = 3³。
- 許多數字，都呈長方形，例如 26 = 2×13，或像長方體，例如 28 = 2×2×7 等。
- 但有些特殊數字，像摩天大廈一樣，別具一格，獨樹一幟，例如 29 和 31。
- 這樣的摩天數字，稱為**素數**，以 2、3、5、7、11、13 ... 開始，源源不絕。
:::

<script type="text/javascript">
svg = makeCanvas()
// number line
svg.appendChild(makeArrow(0,36, 68,36))
svg.appendChild(makePoint(5,36))
svg.appendChild(makeText(4,38, '25'))
svg.appendChild(makePoint(13,36))
svg.appendChild(makeText(12,38, '26'))
svg.appendChild(makePoint(21,36))
svg.appendChild(makeText(20,38, '27'))
svg.appendChild(makePoint(29,36))
svg.appendChild(makeText(28,38, '28'))
svg.appendChild(makePoint(37,36))
svg.appendChild(makeText(36,38, '29'))
svg.appendChild(makePoint(45,36))
svg.appendChild(makeText(44,38, '30'))
svg.appendChild(makePoint(53,36))
svg.appendChild(makeText(52,38, '31'))
svg.appendChild(makePoint(61,36))
svg.appendChild(makeText(60,38, '32'))
// 25 = 5 x 5 as a square
svg.appendChild(makeBox(5,5, 3,34))
// 26 = 2 x 13
svg.appendChild(makeBox(2,13, 12,34))
// 27 = 3 x 3 x 3 as a cube
svg.appendChild(makeBox(3,3, 21,32))
svg.appendChild(makeBox(3,3, 20,33))
svg.appendChild(makeBox(3,3, 19,34))
// 28 = 2 x 2 x 7
svg.appendChild(makeBox(2,7, 29,33))
svg.appendChild(makeBox(2,7, 28,34))
// 29 is prime
svg.appendChild(makeBox(1,29, 36,34))
// 30 = 2 x 3 x 5
svg.appendChild(makeBox(3,5, 44,33))
svg.appendChild(makeBox(3,5, 43,34))
// 31 is prime
svg.appendChild(makeBox(1,31, 52,34))
// 32 = 2 x 4 x 4
svg.appendChild(makeBox(2,4, 63,31))
svg.appendChild(makeBox(2,4, 62,32))
svg.appendChild(makeBox(2,4, 61,33))
svg.appendChild(makeBox(2,4, 60,34))
// done
$('numbers').appendChild(svg)
</script>

#

:::{#forms}
:::

:::{.en}
- The number 29 is an odd prime, so its shape is a skyscraper. Can it form into some nice shapes?
- Being odd, 29 = 1 + even number, which happens to be a multiple of 4, so 29 = 4k + 1 for some k.
- Indeed, 29 = 4×7 + 1, giving a 4-column shape with one extra bit, a little 1×1 square. Ok.
- Moreover, 29 = 25 + 4 = 5² + 2², a sum of two squares, rather surprising. Is this accidental?
- Fermat's **Two Squares Theorem** answers this question:
-
| _Any prime of the form 4k + 1 is a sum of two squares: one odd, one even, and the two squares are unique._
:::

:::{.ch}
- 數字 29 是單數，也是素數，因此形如單棟大廈，毫無美感。它可具有其他形狀，帶有趣味性？
- 既然是單數，29 = 1 + 雙數，該雙數恰巧是 4 的倍數。4 的倍數，表示為 4k，即 29 = 4k + 1。
- 事實上，29 = 4×7 + 1，形如 4 段加 1。多出來的一點點，是個 1×1 的小方塊。嗯，有點意思。
- 更有趣：29 = 25 + 4 = 5² + 2²，是兩個平方之和。這個形狀，出乎意料，是否有咁啱、得咁蹺？
- Fermat（費馬）的 **二平方定理**，正正回答此問題：
-
| _任何形如 4k + 1 的素數，都是兩個平方之和：一單一雙，而且兩個平方是唯一的。_

:::

<script type="text/javascript">
function slideFermat(en) {
if ($('forms').firstChild) $('forms').firstChild.remove()
svg = makeCanvas()
// 29 is prime
svg.appendChild(makeBox(1,29, 5,34))
// as 4-column and a bit
svg.appendChild(makeBox(1,7, 15,34))
svg.appendChild(makeBox(1,7, 16,34))
svg.appendChild(makeBox(1,7, 17,34))
svg.appendChild(makeBox(1,7, 18,34))
svg.appendChild(makeBox(1,1, 19,34))
// as sum of two squares
svg.appendChild(makeBox(5,5, 32,34))
svg.appendChild(makeBox(2,2, 37,34))
// text
svg.appendChild(makeText(2,38, en ? '29 prime' : '素數 29'))
svg.appendChild(makeText(15,38, '29 = 4×7 + 1'))
svg.appendChild(makeText(32,38, '29 = 5² + 2²'))
// arrow
svg.appendChild(makeArrow(0,0, 0,0, null, 3, 'M150,250 S180,100 320,270'))
// bubbles
svg.appendChild(makeBubble(10,5, 25,15)) // arrow bubble
svg.appendChild(makeImage('../images/Pierre-de-Fermat.jpg',  40,2))
svg.appendChild(makeBubble(20,8, 36,20,true)) // Fermat's bubble
if (en) {
    svg.appendChild(makeText(27,17, 'Always', 16, 'orange'))
    svg.appendChild(makeText(27,19, 'possible?', 16, 'orange'))
    svg.appendChild(makeText(38,24, 'Yes, for any prime', 18, 'purple'))
    svg.appendChild(makeText(38,26, 'of the form 4k + 1!', 18, 'purple'))
}
else {
    svg.appendChild(makeText(26,18, '總有可能 ?', 16, 'orange'))
    svg.appendChild(makeText(37,24, '係呀，凡是形如 4k + 1', 18, 'purple'))
    svg.appendChild(makeText(37,26, '的素數，都必定可以！', 18, 'purple'))
}
// done
$('forms').appendChild(svg)
} // end slideFermat
debug && slideFermat(isEnglish()) // no need, but good for slide refresh
langCallbacks.push(slideFermat) // will run this when onload()
</script>

#

:::{#windmills}
:::

<style type="text/css">
tick {  background-color:azure; color:green; font-weight:bold; }
x {  background-color: #ffe033; /* 60% lighter gold */ }
y {  background-color: aqua; }
z { background-color: lawngreen; }
</style>

:::{.en}
- Rather than proving the theorem (by math), let's consider finding the two squares (by pictures).
- The prime 29 has the form 4k + 1, put 4 columns around the extra 1×1 square gives a windmill. 
- Also the prime 29 = 5² + 2², the odd square is 25 = 5², the even square is 4 = 2² = (2×1)² = 4×1².
- Halving each side of the even square gives 4 squares, with odd square makes another windmill.
- In general, a windmill has 4 rotating arms, clockwise <tick>⟳</tick> or anticlockwise <tick>↺</tick>, around a central square.
- How to start from 1-square windmill but end up with the final square-arms windmill? Use minds!
:::

:::{.ch}
- 證明定理，涉及不少數學，相信大家都不耐煩。倒不如轉戰：畫吓圖畫，學吓如何找到兩個平方！
- 己知，素數 29 = 4k + 1，形如 4 段加1。其中 1=1×1 是平方，把 4 段各圍繞平方，便形成四葉風車。
- 還有，素數 29 = 5² + 2²，單平方是 25 = 5²，雙平方是 4 = 2² = (2×1)² = 4×1²，又係 4 的倍數 🖐️。
- 雙平方容許分月餅一樣：先直刀平分，再橫刀平分，便得4個方餅仔，圍繞單平方，又成四葉風車。
- 一般而言，風車（windmill）由四葉（手臂）組成，順時針 <tick>⟳</tick> 或逆時針 <tick>↺</tick>，圍繞方軸，優美對稱。
- 不期然會問：從最基本的方軸1風車開始，達致最終的方葉風車，能否做到？原來要使用「心法」！


:::

<script type="text/javascript">
function slideWindmill(en) {
if ($('windmills').firstChild) $('windmills').firstChild.remove()
svg = makeCanvas()
// 29 is prime
svg.appendChild(makeBox(1,29, 5,34))
svg.appendChild(makeLine(5,27, 6,27))
svg.appendChild(makeLine(5,20, 6,20))
svg.appendChild(makeLine(5,13, 6,13))
svg.appendChild(makeLine(5,6, 6,6))
// as 4-column and a bit
svg.appendChild(makeBox(1,7, 13,34))
svg.appendChild(makeBox(1,7, 14,34))
svg.appendChild(makeBox(1,7, 15,34))
svg.appendChild(makeBox(1,7, 16,34))
svg.appendChild(makeBox(1,1, 17,34))
// as sum of two squares, split even square
svg.appendChild(makeBox(5,5, 38,34))
svg.appendChild(makeBox(2,2, 43,34))
svg.appendChild(makeLine(43,33, 45,33))
svg.appendChild(makeLine(44,32, 44,34))
// windmills
resetColors()
windmill(svg, 1,1,7, 17,18)
windmill(svg, 5,1,1, 38,21)
// text
svg.appendChild(makeText(2,38, en ? '29 prime' : '素數 29', 18))
svg.appendChild(makeText(13,38, '29 = 4×7 + 1' + (en ? ' as windmil' : ' 形成風車'), 18))
svg.appendChild(makeText(38,38, '29 = 5² + 2²' + (en ? ' as windmil' : ' 形成風車'), 18))
// indicator
svg.appendChild(makeArrow(0,0, 0,0, null, 3, 'M180,100 C220,50 280,80 380,190'))
svg.appendChild(makeBubble(18,5, 28,5)) // arrow bubble
svg.appendChild(makeImage('../images/my-logo.jpg',  50,3, '20%'))
svg.appendChild(makeBubble(20,7, 42,12, true)) // answer bubble
if (en) {
    svg.appendChild(makeText(29,8, 'Can this become that?', 16, 'orange'))
    svg.appendChild(makeText(43,15, 'Yes, the windmills are', 18, 'purple'))
    svg.appendChild(makeText(43,17, 'related through minds!', 18, 'purple'))
}
else {
    svg.appendChild(makeText(29,8, '呢個變嗰個，有辦法？', 16, 'orange'))
    svg.appendChild(makeText(44,15, '對呀，因為兩個', 18, 'purple'))
    svg.appendChild(makeText(44,17, '風車心靈相通！', 18, 'purple'))
}
// done
$('windmills').appendChild(svg)
} // end slideWindmill
debug && slideWindmill(isEnglish()) // no need, but good for slide refresh
langCallbacks.push(slideWindmill) // will run this when onload()
</script>

#

:::{#example1}
:::

:::{.en}
- The prime 29 = 4×7 + 1 = <x>1</x>² + 4(<y>1</y>)(<z>7</z>), a windmill with <x>1</x>-square and 4 arms, each of <y>1</y>-by-<z>7</z>.
- To play with this windmill, we can flip each rectangle arm to 7-by-1, around the central 1-square.
- This windmill by **flip** still has center 1-square (yellow), but it looks like this center can grow.
- The largest (red) square frame of a windmill is its **mind**: when center grows, the arms shrink.
- Keeping overall shape, center grows to mind, forms a windmill with 3-square and 4 arms of 1-by-5.
- Rightmost windmill reads <x>3</x>² + 4(<y>1</y>)(<z>5</z>) = 29, it is denoted by (<x>3</x>,<y>1</y>,<z>5</z>); leftmost windmill is (1,1,7). 
:::

:::{.ch}
- 素數 29 = 4×7 + 1 = <x>1</x>² + 4(<y>1</y>)(<z>7</z>)，代表風車形狀：方軸每邊為<x>1</x>，加四片車葉，每片車葉長濶 <y>1</y>×<z>7</z>。
- 對稱風車，有什麼好玩？因為車葉（手臂）是長方形，長與濶可以對調，車葉每片改成長濶 7×1。
- 這個稱為 **「反手」**（flip）風車，方軸邊長仍然是 1（黃色），但似乎四邊都有空間，讓方軸放大。
- 風車能夠容納的最大方框（紅色），稱為 **「中心」**（mind）。注意：若方軸增大，四片車葉縮短。
- 如圖所示，方軸增大至中心，外形冇變，但內裡變成另一風車，方軸邊為 3，車葉每片長濶 1×5。
- 最右的風車，讀出方軸加四片葉，表示 <x>3</x>² + 4(<y>1</y>)(<z>5</z>) = 29，記為 (<x>3</x>,<y>1</y>,<z>5</z>)。最左的風車，記為 (1,1,7)。
:::

<script type="text/javascript">
function slideExample1(en) {
if ($('example1').firstChild) $('example1').firstChild.remove()
svg = makeCanvas()
// windmills
resetColors()
windmill(svg, 1,1,7, 8,15)
windmill(svg, 1,7,1, 23,15)
windmill(svg, 1,7,1, 45,15, true)
rotateColors(-1)
windmill(svg, 3,1,5, 58,14, true)
// arrows
svg.appendChild(makeArrow(10,28, 17,28, null, 3))
svg.appendChild(makeText(34,16, '='))
svg.appendChild(makeArrow(45,28, 55,28, null, 3))
// text
if (en) {
    svg.appendChild(makeText(10,31, 'flip arms', 18))
    svg.appendChild(makeText(45,31, 'same minds', 18))
}
else {
    svg.appendChild(makeText(10,31, '反手', 18))
    svg.appendChild(makeText(45,31, '同心', 18))
}
// labels
svg.appendChild(makeText(6,36, '(1,1,7)', 18))
svg.appendChild(makeText(21,36, '(1,7,1)', 18))
svg.appendChild(makeText(43,36, '(1,7,1)', 18))
svg.appendChild(makeText(57,36, '(3,1,5)', 18))
// done
$('example1').appendChild(svg)
} // end slideExample1
debug && slideExample1(isEnglish()) // no need, but good for slide refresh
langCallbacks.push(slideExample1) // will run this when onload()
</script>

#

:::{#example2}
:::

:::{.en}
- This is fun! For the prime 29, now windmill (3,1,5) can flip again to windmill (3,5,1).
- By sharing the same mind (in red), windmill (3,5,1) changes to windmill (5,1,1). 
- This windmill with 4 square arms cannot flip, and 4 arms combine to form an even square.
- Windmill (5,1,1) reads as 29 = 5² + 4(1)(1) = 5² + (2×1)² = 5² + 2², a sum of two squares!
- Therefore, repeating **flip** and sharing **mind** will bring (1,1,(29 - 1)/4) = (1,1,7) to (5,1,1).
- This method works for a prime of the form 4k + 1, as in Fermat's Two Squares theorem.
:::

:::{.ch}
- 好玩嗎？繼續啦！素數 29 的風車 (3,1,5)，又來一次「反手」，變身成為風車 (3,5,1)。
- 心心相印：利用「同心」（紅色）互相連通，風車 (3,5,1) 於是化身，成為風車 (5,1,1)。
- 這個風車，四片葉皆呈方形，「反手」無效，成終極風車。四片方葉可合拼，成為雙平方。
- 終極風車 (5,1,1)，數數格仔，代表 29 = 5² + 4(1)(1) = 5² + (2×1)² = 5² + 2²，即兩平方之和！
- 由此可見，重覆「反手」及永結「同心」，便能把 (1,1,(29 - 1)/4) = (1,1,7) 轉化成 (5,1,1)。
- 這一套風車「變心大法」，適用於任何形如 4k + 1 的素數，正如 Fermat 二平方定理所指。
:::

<script type="text/javascript">
function slideExample2(en) {
if ($('example2').firstChild) $('example2').firstChild.remove()
svg = makeCanvas()
// windmills
resetColors()
rotateColors(-1) // color as last slide
windmill(svg, 3,1,5, 8,15)
windmill(svg, 3,5,1, 20,15)
windmill(svg, 3,5,1, 34,15, true)
rotateColors(-1)
windmill(svg, 5,1,1, 44,14, true)
// sum of two squares
sumOfSquares(svg, 5,1, 54,14)
// arrows
svg.appendChild(makeArrow(10,28, 19,28, null, 3))
svg.appendChild(makeText(28,17, '='))
svg.appendChild(makeArrow(34,28, 46,28, null, 3))
// text
if (en) {
    svg.appendChild(makeText(10,31, 'flip arms', 18))
    svg.appendChild(makeText(34,31, 'same minds', 18))
    svg.appendChild(makeText(54,31, 'sum of 2 squares', 18))
}
else {
    svg.appendChild(makeText(10,31, '反手', 18))
    svg.appendChild(makeText(34,31, '同心', 18))
    svg.appendChild(makeText(54,31, '兩平方之和', 18))
}
// labels
svg.appendChild(makeText(7,36, '(3,1,5)', 18))
svg.appendChild(makeText(19,36, '(3,5,1)', 18))
svg.appendChild(makeText(33,36, '(3,5,1)', 18))
svg.appendChild(makeText(44,36, '(5,1,1)', 18))
svg.appendChild(makeText(54,36, '5² + 2² = 29', 18))
// done
$('example2').appendChild(svg)
} // end slideExample2
debug && slideExample2(isEnglish()) // no need, but good for slide refresh
langCallbacks.push(slideExample2) // will run this when onload()
</script>

#

:::{#example3}
:::

:::{.en}
- Above is shown the method applying to the prime 41 = 4×10 + 1, so we start with windmill (1,1,10).
- Then *flip* gives (1,10,1), sharing *mind* to be (3,1,8); *flip* to (3,8,1), with *mind* to (5,1,4); centers grow.
- For the next row, *flip* to (5,4,1) with *mind* to (3,4,2); *flip* to (3,2,4) with *mind* to (1,2,5); centers shrink.
- Since the arms are not squares, *flip* to (1,5,2) with *mind* to (5,2,2), and now the 4 arms are squares.
- Combining the 4 square arms to form a square, then 41 = 5² + (2×2)² = 5² + 4², a sum of two squares.
- With a large graph paper and efforts to draw windmills, you'll get the two square for any 4k + 1 prime.
:::

:::{.ch}
- 上圖顯示，「變心大法」應用於素數 41 = 4×10 + 1，因此起始風車為 (1,1,10)。
- 反手成 (1,10,1)，同心變 (3,1,8)。又反成 (3,8,1)，再心化 (5,1,4)。留意方軸增大。
- 轉下一行，反至 (5,4,1)，心至(3,4,2)。反至 (3,2,4)，心至 (1,2,5)。留意方軸縮小。
- 車葉仍是長濶參差，再反成 (1,5,2) ，又心成 (5,2,2)。終於雲開見月：四葉齊方！
- 四方葉合成正方形，配上風車方軸，讀出 41 = 5² + (2×2)² = 5² + 4²，二平方之和。
- 只要格仔紙夠大張，給予堅毅無比的耐性，你一定找到：4k + 1 素數的兩個平方！
:::

<script type="text/javascript">
function slideExample3(en) {
if ($('example3').firstChild) $('example3').firstChild.remove()
svg = makeCanvas()
svg.setAttribute('viewBox', '0 0 800 400') // for max 80x40
// windmills
resetColors()
rotateColors(-1) // color as last slide
// first row
var h1 = 10
windmill(svg, 1,1,10, 10,h1)
windmill(svg, 1,10,1, 31,h1, true) // flip
rotateColors(-1)
windmill(svg, 3,1,8, 46,h1 - 1, true)
windmill(svg, 3,8,1, 60,h1 - 1, true) // flip
rotateColors(-1)
windmill(svg, 5,1,4, 71,h1 - 2, true)
// second row
var h2 = 27
windmill(svg, 5,4,1, 71,h2, true) // flip
windmillShift(svg, 3,4,2, 63,h2 + 1, true)
// windmill(svg, 3,4,2, 56,h2 + 1, true)  // confusing
windmill(svg, 3,2,4, 52,h2 + 1, true) // flip
windmillShift(svg, 1,2,5, 40,h2 + 2, true)
// windmill(svg, 1,2,5, 23,h2 + 2, true)  // confusing
windmill(svg, 1,5,2, 28,h2 + 2, true) // flip
rotateColors(-1)
windmill(svg, 5,2,2, 15,h2, true)
// sum of squares
sumOfSquares(svg, 5,2, 2,h2)
// arrows
svg.appendChild(makeArrow(18,h1 - 3, 22,h1 - 3, null, 3))
svg.appendChild(makeText(18,h1 - 4, en ? 'flip' : '反', 12))
svg.appendChild(makeArrow(38,h1 - 3, 42,h1 - 3, null, 3))
svg.appendChild(makeText(38,h1 - 4, en ? 'mind' : '心', 12))
svg.appendChild(makeArrow(53,h1 - 3, 57,h1 - 3, null, 3))
svg.appendChild(makeText(53,h1 - 4, en ? 'flip' : '反', 12))
svg.appendChild(makeArrow(65,h1 - 3, 69,h1 - 3, null, 3))
svg.appendChild(makeText(65,h1 - 4, en ? 'mind' : '心', 12))
svg.appendChild(makeArrow(74,h1 + 7, 74,h2 - 4, null, 3))
svg.appendChild(makeText(75,h2 - 6, en ? 'flip' : '反', 12))
svg.appendChild(makeArrow(71,h2 - 2, 67,h2 - 2, null, 3))
svg.appendChild(makeText(67,h2 - 3, en ? 'mind' : '心', 12))
// svg.appendChild(makeArrow(63,h2 - 2, 59,h2 - 2, null, 3))
// svg.appendChild(makeText(59,h2 - 3, en ? 'shift' : '移', 12))
svg.appendChild(makeArrow(62,h2 - 2, 58,h2 - 2, null, 3))
svg.appendChild(makeText(58,h2 - 3, en ? 'flip' : '反', 12))
svg.appendChild(makeArrow(48,h2 - 2, 44,h2 - 2, null, 3))
svg.appendChild(makeText(44,h2 - 3, en ? 'mind' : '心', 12))
// svg.appendChild(makeArrow(32,h2 - 2, 28,h2 - 2, null, 3))
// svg.appendChild(makeText(28,h2 - 3, en ? 'shift' : '移', 12))
svg.appendChild(makeArrow(36,h2 - 2, 32,h2 - 2, null, 3))
svg.appendChild(makeText(32,h2 - 3, en ? 'flip' : '反', 12))
svg.appendChild(makeArrow(24,h2 - 2, 20,h2 - 2, null, 3))
svg.appendChild(makeText(20,h2 - 3, en ? 'mind' : '心', 12))
svg.appendChild(makeArrow(13,h2 - 2, 9,h2 - 2, null, 3))
svg.appendChild(makeText(en ? 5 : 8,h2 - 3, en ? 'sum of squares' : '二平方和', 12))
// done
$('example3').appendChild(svg)
} // end slideExample3
debug && slideExample3(isEnglish()) // no need, but good for slide refresh
langCallbacks.push(slideExample3) // will run this when onload()
</script>

#

:::{.en}
<div class="imagecap" style="width:90%">
<img src="../images/CPP2022-paper.jpg" style="width:100%"
     alt="My first paper is presented to a conference, media is the talk video, file is the talk slides." onclick="showPopup(this)" />
</br>
<span>Click to enlarge (click again to close)</span>
</div>
:::
:::{.ch}
<div class="imagecap" style="width:90%">
<img src="../images/CPP2022-paper.jpg" style="width:100%"
     alt="我第一篇文章提交上學術研討會，媒體是視像講話，文件是講話簡報片。" onclick="showPopup(this)" />
</br>
<span>點擊放大(再擊關閉)</span>
</div>
:::

:::{.en}
- Of course, drawing all windmills is impractical. Better to use math to compute the windmills instead.
- The windmill computations use only + and −, no ×, no ÷. There is ×2 (doubling), which is adding twice. 
- The two squares will be found because the algorithm is proven correct <tick>✔</tick>. Please see my [2022 article](https://dl.acm.org/doi/pdf/10.1145/3497775.3503673).
- Academic papers rarely mix text with pictures, so just take a peek and appreciate my style is enough. 
- About Fermat’s two-square theorem, I did not invent the windmill proof, nor the windmill calculations.
- I am the first to apply automated reasoning to show that the algorithm is correct, like icing on a cake.
:::

:::{.ch}
- 話雖如此，利用圖畫進行變心大法，始終不切實際。明白其中的數學，以計算代替畫圖，方便得多。
- 風車計算，實際上只有 + 和 −，毋須 × 和 ÷。其中有計算 ×2：即雙倍（double），等於數字加兩次。
- 我拍心口保證，你一定找得到，因為有數學證明：算法正確無誤 <tick>✔</tick>。請參閱 2022年我的[首篇文章](https://dl.acm.org/doi/pdf/10.1145/3497775.3503673)。
- 唔明唔緊要，最緊要欣賞。學術性文章，鮮有圖文並茂。有人肯瞧一瞧我的作品，已十分心滿意足。
- 順帶一提，關於 Fermat 二平方定理，以風車作證明，不是我想出來。以風車作運算，也非我原創。
- 文章能夠刊登，只因我是首次借助電腦，以「自動推理」證明風車演算方法是正確，有點錦上添花。
:::

#

:::{#hopping}
:::

:::{.en}
- My second paper in JAR is about a **Hopping** method to find Fermat's two squares for any 4k + 1 prime.
- While the original method starts from windmill (1,1,k), this improvement starts from (1,k,1) instead.
- It can jump over several steps of *flip* and *mind* in one hop, then keeps hopping to the final windmill.
- The hops are computed by a formula, derived from the math behind *flip* and *mind*, see my [JAR paper](https://rdcu.be/dXE1Y).
- With prime 41, it starts with (1,10,1), the *flip* of (1,1,10), and hops directly to (5,1,4), end of first row.
- Then it hops to (3,4,2), and finally hops to (5,2,2) with 4 square arms, thus 41 is a sum of two squares.
:::

:::{.ch}
- 在 JAR 刊登的，是我第二篇文章。談及改良版 **「跳躍大法」**，能快速找到 4k + 1 素數的兩個平方。
- 原本的「變心大法」，從風車 (1,1,k) 開始。新的「跳躍大法」，卻從風車 (1,k,1) 開始，稍稍不同。
- 新方法一「跳」就跨越多次「反手」和「同心」，降落遠程風車。繼而不斷「跳」，直奔終極風車。
- 跳躍步驟長短，由公式計算，建基於「反手」和「同心」背後的數學，請參閱我的 [JAR 期刊文章](https://rdcu.be/dXE1Y)。
- 例如素數 41，開始為風車 (1,10,1)，即 (1,1,10) 的反手風車。第一跳直接到達 (5,1,4)，在第一行末。
- 跟著一跳只到 (3,4,2)，繼而再跳往 (5,2,2)。結果風車四葉均是正方形，成功表示 41 是二平方之和。
:::



<script type="text/javascript">
function slideHopping(en) {
if ($('hopping').firstChild) $('hopping').firstChild.remove()
svg = makeCanvas()
svg.setAttribute('viewBox', '0 0 800 400') // for max 80x40
// windmills
resetColors()
rotateColors(-1) // color as last slide
// first row
var h1 = 10
windmill(svg, 1,1,10, 10,h1)
windmill(svg, 1,10,1, 31,h1) // flip
rotateColors(-1)
windmill(svg, 3,1,8, 46,h1 - 1)
windmill(svg, 3,8,1, 60,h1 - 1) // flip
rotateColors(-1)
windmill(svg, 5,1,4, 71,h1 - 2)
// second row
var h2 = 27
windmill(svg, 5,4,1, 71,h2) // flip
windmillShift(svg, 3,4,2, 63,h2 + 1)
// windmill(svg, 3,4,2, 56,h2 + 1)  // confusing
windmill(svg, 3,2,4, 52,h2 + 1) // flip
windmillShift(svg, 1,2,5, 40,h2 + 2)
// windmill(svg, 1,2,5, 23,h2 + 2)  // confusing
windmill(svg, 1,5,2, 28,h2 + 2) // flip
rotateColors(-1)
windmill(svg, 5,2,2, 15,h2)
// sum of squares
sumOfSquares(svg, 5,2, 2,h2)
// hops
svg.appendChild(makeArrow(0,0, 0,0, null, 3, 'M280,80 C-200,-20 680,10 750,50'))
svg.appendChild(makeText(39,3, en ? 'hop' : '跳', 14))
svg.appendChild(makeArrow(0,0, 0,0, null, 3, 'M770,180 C770,200 700,240 660,240'))
svg.appendChild(makeText(69,22, en ? 'hop' : '跳', 14))
svg.appendChild(makeArrow(0,0, 0,0, null, 3, 'M630,250 C600,200 400,180 180,240'))
svg.appendChild(makeText(40,22, en ? 'hop' : '跳', 14))
// done
$('hopping').appendChild(svg)
} // end slideHopping
debug && slideHopping(isEnglish()) // no need, but good for slide refresh
langCallbacks.push(slideHopping) // will run this when onload()
</script>

#

:::{.small}
Windmills of the Minds: An Algorithm for Fermat's Two Squares Theorem【15:14】cc<br>
<a href="https://youtu.be/ETL3bE3kDfA">
<img src="http://img.youtube.com/vi/ETL3bE3kDfA/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- When my first [conference paper](https://popl22.sigplan.org/home/CPP-2022#program) was published in 2022, my presentation was a talk.
- Each speaker was allocated 15 minutes, so I only had time to cover the [main ideas](../books/windmills_slides.pdf).
- The JAR editor-in-chief was an organizer of the conference, and he was impressed.
- He invited me to write an extended version of my interesting paper for his journal.
- An extended version has to include at least 1/3 of new material compared to original.
- That's how my improved method appeared in [JAR](https://link.springer.com/journal/10817/volumes-and-issues/68-4), a well-known journal in AI for math.
:::

:::{.ch}
- 我的首篇文章，被[學術研討會](https://popl22.sigplan.org/home/CPP-2022#program)採納。2022年初仍有旅遊限制，大會要求作者上載視頻，簡介內容。
- 每位講者只安排15分鐘，長話短說，頗具挑戰性。我唯有重點介紹，[集中講解](../books/windmills_slides.pdf)風車「變心大法」。
- JAR 期刊的總編輯，原來有份參與籌備研討會。可能我的圖畫解說，備受好評，他留下深刻印象。
- 學術研討會結束後，他主動與我聯絡，誠意邀請我為他的期刊，寫一篇「數學上的風車」 加長版。
- 所謂「加長版」，是指內容對比原版，起碼有1/3是全新。於是著手研究，如何改良「變心大法」。
- 孜孜不倦，終於完成「跳躍大法」，結果在[今期 JAR](https://link.springer.com/journal/10817/volumes-and-issues/68-4) 發表。期刊在數學 AI 應用範疇，甚具權威性。
:::

#

:::{.small}
Dusty Springfield - The Windmills of Your Mind (1969)【4:11】<br>
<a href="https://youtu.be/qJAVP7jP0bI">
<img src="http://img.youtube.com/vi/qJAVP7jP0bI/0.jpg" width="48%"/></a></br>
<span class="en">Click to play</span><span class="ch">點擊播放</span>
:::

:::{.en}
- You may know that the title "Windmills of the Minds" is taken from a popular movie song.
- The song has nothing to do with math, but the [lyrics](../songs/WindmillsOfYourMind.html) does mention circles, a hint of math.
- The movie "The Thomas Crown Affairs" tells the mind game between a man and a woman.
- I picked this song in my [2022 Lunar New Year](../2022/lny.html) gift, with a theme about AI and intelligence.
- The movie was so remarkable that a 1999 remake had a similar story, using the same song.
:::

:::{.ch}
- 精明的讀者，或心水清的朋友，會知道文章標題《Windmills of the Minds》，取自一首流行曲。
- 當然啦，歌曲與數學，風馬牛不相及。但[歌詞](../songs/WindmillsOfYourMind.html)放眼是圓形、旋轉，隱隱與數學扯上一丁點關係。
- 歌曲是電影《龍鳳鬥智》主題曲，故事講述男女主角的內心交戰，可說是人腦智能的最高境界。
- 我在[2022年農曆新春](../2022/lny.html)「禮物」選上此曲，該集主題是人工智能 AI，比較人腦與電腦，相得益彰。
- 電影情節吸引，當年口碑載道。電影於1999年翻拍，採用相似故事，選相同主題曲，大獲好評。
:::

<!--

#

- 當然啦，從數學角度看👀，例子只能作說明，不能充當證明。若想明白箇中道理，請參閱我的發表文章。
- 又當然啦，學術性文章，行外人一般看不懂。但你只要翻頁揭下揭下，便意識到風格不同。
- 內容以圖為主，在學術性文章少見鮮有聽聞。熟悉電腦程式編寫，還可以找到編寫演算方法。
- 順帶一提，以風車作證明，我不是首個。提出以風車作運算，我也不是最先。已經有人提出。首次 首先
- 我只是第一位以電腦證明演算方法是正確✅，有點錦上添花而已。
- 講真，JAR 期刊在行內頗有名氣，通過評審員一關，已經不錯，甚感欣慰。


Windmills of the Minds: An Algorithm for Fermat's Two Squares Theorem【15:14】cc
https://www.youtube.com/watch?v=ETL3bE3kDfA
Talk at CPP2022, based on these slides: books/windmills_slides.pdf
-->

<!--

#

:::{#pic02}
:::

- Tik prime 17: [1,1,4] ping [3,1,2] pong [1,2,2]
- start with (1,1,4), flip to (1,4,1).
- share the same mind, (1,4,1) becomes (3,1,2)
- now with (3,1,2), flip to (3,2,1)
- share the same mind, (3,2,1) becomes (1,2,2), square arms!

<script type="text/javascript">
resetColors() // initialise colors
svg = makeCanvas()
windmill(svg, 1,1,4, 6, 10, true)
windmill(svg, 1,4,1, 17, 10, true)
rotateColors(-1) // ping
windmill(svg, 3,1,2, 26, 10 - (3-1)/2, true)
windmill(svg, 3,2,1, 35, 10 - (3-1)/2, true)
// pong, keep colors
windmillShift(svg, 1,2,2, 44, 10 - (1-1)/2, true)
windmill(svg, 1,2,2, 54, 10 - (1-1)/2, true)
// (1,1,4) → (1,4,1)
svg.appendChild(makeText(10,18, '→', 48))
svg.appendChild(makeText(10,19, 'flip（反）', 12))
// (1,4,1) → (3,1,2)
svg.appendChild(makeText(20,18, '→', 48))
svg.appendChild(makeText(20,19, 'mind（心）', 12))
// (3,1,2) → (3,2,1)
svg.appendChild(makeText(30,18, '→', 48))
svg.appendChild(makeText(30,19, 'flip（反）', 12))
// (3,2,1) → (1,2,2)*
svg.appendChild(makeText(38,18, '→', 48))
svg.appendChild(makeText(38,19, 'mind（心）', 12))
// (1,2,2)* → (1,2,2)
svg.appendChild(makeText(47,18, '→', 48))
svg.appendChild(makeText(47,19, 'shift（移）', 12))
svg.appendChild(makeText(5,25, 'Windmills for 17, start with (1,1,4), end with (1,2,2)'))
$('pic02').appendChild(svg)
</script>

#

:::{#pic03}
:::

- Checking windmill (1,1,4) can still be drawn, with canvas background.
- Everything seems ok!
- Very satisfying.

<script type="text/javascript">
resetColors() // initialise colors
svg = makeCanvas()
windmill(svg, 1,1,4, 10, 10)
$('pic03').appendChild(svg)
</script>


-->

<!--

Windmills and Fermat's Two Squares Theorem

Symbols: ▲ ping, ⬥ pong, ▼ pung, ❚ hop
Analyis for next 20 tik primes from: 5

5: [1,1,1], hop: , total steps: 0
13: [1,3,1] ⬥ [1,1,3] ▲ [3,1,1] ❚ , hop: 2, total steps: 2
17: [1,4,1] ⬥ [1,1,4] ▲ [3,1,2] ❚ ⬥ [1,2,2] ❚ , hop: 2,1, total steps: 3
29: [1,7,1] ⬥ [1,1,7] ▲ [3,1,5] ▲ [5,1,1] ❚ , hop: 3, total steps: 3
37: [1,9,1] ⬥ [1,1,9] ▲ [3,1,7] ▲ [5,1,3] ❚ ⬥ [1,3,3] ❚ , hop: 3,1, total steps: 4
41: [1,10,1] ⬥ [1,1,10] ▲ [3,1,8] ▲ [5,1,4] ❚ ⬥ [3,4,2] ❚ ⬥ [1,2,5] ▲ [5,2,2] ❚ , hop: 3,1,2, total steps: 6
53: [1,13,1] ⬥ [1,1,13] ▲ [3,1,11] ▲ [5,1,7] ▲ [7,1,1] ❚ , hop: 4, total steps: 4
61: [1,15,1] ⬥ [1,1,15] ▲ [3,1,13] ▲ [5,1,9] ▲ [7,1,3] ❚ ▼ [1,5,3] ⬥ [5,3,3] ❚ , hop: 4,2, total steps: 6
73: [1,18,1] ⬥ [1,1,18] ▲ [3,1,16] ▲ [5,1,12] ▲ [7,1,6] ❚ ⬥ [5,6,2] ❚ ▼ [1,9,2] ⬥ [3,2,8] ▲ [7,2,3] ❚ ▼ [1,6,3] ⬥ [5,3,4] ❚ ⬥ [3,4,4] ❚ , hop: 4,1,3,2,1, total steps: 11
89: [1,22,1] ⬥ [1,1,22] ▲ [3,1,20] ▲ [5,1,16] ▲ [7,1,10] ▲ [9,1,2] ❚ ▼ [5,8,2] ▼ [1,11,2] ⬥ [3,2,10] ▲ [7,2,5] ❚ ⬥ [3,5,4] ❚ ⬥ [5,4,4] ❚ , hop: 5,4,1,1, total steps: 11
97: [1,24,1] ⬥ [1,1,24] ▲ [3,1,22] ▲ [5,1,18] ▲ [7,1,12] ▲ [9,1,4] ❚ ▼ [1,6,4] ⬥ [7,4,3] ❚ ▼ [1,8,3] ⬥ [5,3,6] ❚ ⬥ [7,6,2] ❚ ▼ [3,11,2] ⬥ [1,2,12] ▲ [5,2,9] ▲ [9,2,2] ❚ , hop: 5,2,2,1,4, total steps: 14
101: [1,25,1] ⬥ [1,1,25] ▲ [3,1,23] ▲ [5,1,19] ▲ [7,1,13] ▲ [9,1,5] ❚ ⬥ [1,5,5] ❚ , hop: 5,1, total steps: 6
109: [1,27,1] ⬥ [1,1,27] ▲ [3,1,25] ▲ [5,1,21] ▲ [7,1,15] ▲ [9,1,7] ❚ ⬥ [5,7,3] ❚ ⬥ [1,3,9] ▲ [7,3,5] ❚ ⬥ [3,5,5] ❚ , hop: 5,1,2,1, total steps: 9
113: [1,28,1] ⬥ [1,1,28] ▲ [3,1,26] ▲ [5,1,22] ▲ [7,1,16] ▲ [9,1,8] ❚ ⬥ [7,8,2] ❚ ▼ [3,13,2] ⬥ [1,2,14] ▲ [5,2,11] ▲ [9,2,4] ❚ ▼ [1,7,4] ⬥ [7,4,4] ❚ , hop: 5,1,4,2, total steps: 12
137: [1,34,1] ⬥ [1,1,34] ▲ [3,1,32] ▲ [5,1,28] ▲ [7,1,22] ▲ [9,1,14] ▲ [11,1,4] ❚ ▼ [3,8,4] ⬥ [5,4,7] ❚ ⬥ [9,7,2] ❚ ▼ [5,14,2] ▼ [1,17,2] ⬥ [3,2,16] ▲ [7,2,11] ▲ [11,2,2] ❚ , hop: 6,2,1,5, total steps: 14
149: [1,37,1] ⬥ [1,1,37] ▲ [3,1,35] ▲ [5,1,31] ▲ [7,1,25] ▲ [9,1,17] ▲ [11,1,7] ❚ ⬥ [3,7,5] ❚ ⬥ [7,5,5] ❚ , hop: 6,1,1, total steps: 8
157: [1,39,1] ⬥ [1,1,39] ▲ [3,1,37] ▲ [5,1,33] ▲ [7,1,27] ▲ [9,1,19] ▲ [11,1,9] ❚ ⬥ [7,9,3] ❚ ▼ [1,13,3] ⬥ [5,3,11] ▲ [11,3,3] ❚ , hop: 6,1,3, total steps: 10
173: [1,43,1] ⬥ [1,1,43] ▲ [3,1,41] ▲ [5,1,37] ▲ [7,1,31] ▲ [9,1,23] ▲ [11,1,13] ▲ [13,1,1] ❚ , hop: 7, total steps: 7
181: [1,45,1] ⬥ [1,1,45] ▲ [3,1,43] ▲ [5,1,39] ▲ [7,1,33] ▲ [9,1,25] ▲ [11,1,15] ▲ [13,1,3] ❚ ▼ [7,11,3] ▼ [1,15,3] ⬥ [5,3,13] ▲ [11,3,5] ❚ ▼ [1,9,5] ⬥ [9,5,5] ❚ , hop: 7,4,2, total steps: 13
193: [1,48,1] ⬥ [1,1,48] ▲ [3,1,46] ▲ [5,1,42] ▲ [7,1,36] ▲ [9,1,28] ▲ [11,1,18] ▲ [13,1,6] ❚ ▼ [1,8,6] ⬥ [11,6,3] ❚ ▼ [5,14,3] ⬥ [1,3,16] ▲ [7,3,12] ▲ [13,3,2] ❚ ▼ [9,14,2] ▼ [5,21,2] ▼ [1,24,2] ⬥ [3,2,23] ▲ [7,2,18] ▲ [11,2,9] ❚ ⬥ [7,9,4] ❚ ⬥ [1,4,12] ▲ [9,4,7] ❚ ⬥ [5,7,6] ❚ ⬥ [7,6,6] ❚ , hop: 7,2,4,6,1,2,1,1, total steps: 24

First tik prime involving pung ▼ : 61,  6 steps, hop: 4,2.
61: [1,15,1] ⬥ [1,1,15] ▲ [3,1,13] ▲ [5,1,9] ▲ [7,1,3] ❚ ▼ [1,5,3] ⬥ [5,3,3] ❚ , hop: 4,2, total steps: 6
iterate: [1,1,15] ▲ [3,1,13] ▲ [5,1,9] ▲ [7,1,3] ❚ ▼ [1,5,3] ⬥ [5,3,3] ❚ 
hopping: [1,15,1] ... [7,1,3] ❚ ... [5,3,3] ❚ 

-->

<!--
pandoc -t revealjs -s --mathjax windmill.md -o windmill.html
-->
